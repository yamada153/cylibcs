﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// DllImportに必要
using System.Runtime.InteropServices;

class CoordSysConv
{
    [DllImport("GdalWrapperDLL.dll", CallingConvention = CallingConvention.StdCall)]
    static extern int proj_init();
    [DllImport("GdalWrapperDLL.dll", CallingConvention = CallingConvention.StdCall)]
    static extern void proj_free();
    [DllImport("GdalWrapperDLL.dll", CallingConvention = CallingConvention.StdCall)]
    static extern int proj_setconv(int src, int dst);
    [DllImport("GdalWrapperDLL.dll", CallingConvention = CallingConvention.StdCall)]
    static extern void proj_conv(ref double x, ref double y);

    bool flginstance = false;

    public ShapeList Conv(ShapeList sl)
    {
        ShapeList result = new ShapeList(sl.Count());

        for(int i = 0; i < sl.Count(); i++)
        {
            switch (sl[i].Type)
            {
                case ShapeStruct.ObjType.Null:
                    result.AddNull();
                    break;
                case ShapeStruct.ObjType.Point:
                    result.AddPoint(ConvXY(sl[i].Points[0]));
                    break;
                case ShapeStruct.ObjType.MultiPoint:
                    result.AddMultiPoint(ConvXY(sl[i].Points));
                    break;
                case ShapeStruct.ObjType.PolyLine:
                    result.AddPolyLine(ConvXY(sl[i].PolyLines()));
                    break;
                case ShapeStruct.ObjType.Polygon:
                    result.AddPolygon(ConvXY(sl[i].PolyLines()));
                    break;
                default:
                    result.AddNull();
                    break;
            }
        }

        return result;
    }

    public CoordSysConv(int srcEpsg, int dstEpsg)
    {
        int d = proj_init();
        if (d != 0)
            throw new Exception("CoordSysConvの初期化に失敗しました。「" + d.ToString() + "」");

        flginstance = true;
        int result = proj_setconv(srcEpsg, dstEpsg);
    }

    public void SetConv(int srcEpsg, int dstEpsg)
    {
        proj_setconv(srcEpsg, dstEpsg);
    }

    /// <summary>
    /// ディストラクタがいつ呼ばれるかわからないため
    /// </summary>
    public void Free()
    {
        if (flginstance)
        {
            proj_free();
            flginstance = false;
        }
    }

    ~CoordSysConv()
    {
    }

    public GeoPoint[][] ConvXY(GeoPoint[][] pntss)
    {
        GeoPoint[][] result = new GeoPoint[pntss.Length][];

        for (int i = 0; i < result.Length; i++)
            result[i] = ConvXY(pntss[i]);
        return result;
    }

    public GeoPoint[] ConvXY(GeoPoint[] pnts)
    {
        GeoPoint[] result = new GeoPoint[pnts.Length];

        for (int i = 0; i < result.Length; i++)
            result[i] = ConvXY(pnts[i]);
        return result;
    }

    public GeoPoint ConvXY(GeoPoint pnt)
    {
        double x = pnt.X;
        double y = pnt.Y;
        proj_conv(ref x, ref y);

        GeoPoint p = new GeoPoint(x, y);
        return p;
    }
}
