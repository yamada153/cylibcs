﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;

class WKTFunc
{
    public enum ObjType { None, Point, MPoint, Line, MLine, Polygon, MPolygon, Collection, Other };

    static public Tuple<ShapeList, StringTable> FromStringTable(StringTable st)
    {
        StringTable from = new StringTable(st);

        ShapeList sl = new ShapeList(from.Count());
        if (from.FieldExist("wkt"))
        {
            for (int i = 0; i < from.Count(); i++)
            {
                switch (GetType(from[i, "wkt"]))
                {
                    case ObjType.None:
                        sl.AddNull();
                        break;
                    case ObjType.Point:
                        sl.AddPoint(ToPoint(from[i, "wkt"]));
                        break;
                    case ObjType.MPoint:
                        sl.AddMultiPoint(ToMultiPoint(from[i, "wkt"]));
                        break;
                    case ObjType.Line:
                        sl.AddPolyLine(new GeoPoint[][] { ToLineString(from[i, "wkt"]) });
                        break;
                    case ObjType.MLine:
                        sl.AddPolyLine(ToMultiLineString(from[i, "wkt"]));
                        break;
                    case ObjType.Polygon:
                        sl.AddPolygon(ToPolygon(from[i, "wkt"]));
                        break;
                    case ObjType.MPolygon:
                        sl.AddPolygon(ToMultiPolygon(from[i, "wkt"], false)[0]);
                        break;
                    default:
                        throw new Exception("WKTFunc.FromStringTableの" + (i + 1).ToString() + "行目「" + from[i, "wkt"] + "」に未対応です。");
                }
            }
            from.DeleteField("wkt");
        }
        else
        {
            for (int i = 0; i < from.Count(); i++)
                sl.AddNull();
        }
        return new Tuple<ShapeList, StringTable>(sl, from);
    }

    static public StringTable ToStringTable(ShapeList sl, StringTable st)
    {
        if (sl.Count() != st.Count())
            throw new ArgumentException("WKTFunc.ToStringTableの引数slとstの行数が合いません。sl = " + sl.Count().ToString() + ": st = " + st.Count().ToString());

        string[] fieldnames = ArrayFunc.Concat(new string[] { "WKT" }, st.GetFieldNames());
        StringTable result = new StringTable(fieldnames, sl.Count());
        for(int i = 0;i < sl.Count(); i++)
        {
            switch (sl[i].Type)
            {
                case ShapeStruct.ObjType.Null:
                    result.Add(ArrayFunc.Concat(new string[] { "POINT EMPTY" }, st.GetStringsFromRowNumber(i)));
                    break;
                case ShapeStruct.ObjType.Point:
                    string wktstr = ToString(sl[i].Points[0]);
                    result.Add(ArrayFunc.Concat(new string[] { wktstr }, st.GetStringsFromRowNumber(i)));
                    break;
                case ShapeStruct.ObjType.PolyLine:
                    if(sl[i].NumParts == 1)
                    {
                        wktstr = ToString(sl[i].Points, ObjType.Line);
                        result.Add(ArrayFunc.Concat(new string[] { wktstr }, st.GetStringsFromRowNumber(i)));
                    }
                    else
                    {
                        wktstr = ToString(sl.PolyLines(i), ObjType.MLine);
                        result.Add(ArrayFunc.Concat(new string[] { wktstr }, st.GetStringsFromRowNumber(i)));
                    }
                    break;
                case ShapeStruct.ObjType.Polygon:
                    GeoPoint[][][] tmp = Geo2DExtra.PolygonToMultiPolygon(sl.PolyLines(i));
                    if(tmp.Length == 1)
                    {
                        wktstr = ToString(tmp[0], ObjType.Polygon);
                        result.Add(ArrayFunc.Concat(new string[] { wktstr }, st.GetStringsFromRowNumber(i)));
                    }
                    else
                    {
                        wktstr = ToString(tmp);
                        result.Add(ArrayFunc.Concat(new string[] { wktstr }, st.GetStringsFromRowNumber(i)));
                    }
                    break;
                case ShapeStruct.ObjType.MultiPoint:
                    wktstr = ToString(sl[i].Points, ObjType.MPoint);
                    result.Add(ArrayFunc.Concat(new string[] { wktstr }, st.GetStringsFromRowNumber(i)));
                    break;
                default:
                    throw new Exception("WKTFunc.ToStringTableの引数slの" + i.ToString() + "行目のオブジェクトをwktに変換できませんでした。\n" + sl[i].ToString());
            }
        }
        return result;
    }

    /// <summary>
    /// wkt形式の文字列から、オブジェクトタイプを取得する。
    /// </summary>
    public static ObjType GetType(string wkt_format_string)
    {
        string wktstr = wkt_format_string.Trim().ToUpper();
        try
        {
            if (wktstr == "")
                return ObjType.None;
            else if (wktstr == "POINT EMPTY")
                return ObjType.None;
            else if (wktstr.Substring(0, 5) == "POINT")
                return ObjType.Point;
            else if (wktstr.Substring(0, 10) == "LINESTRING")
                return ObjType.Line;
            else if (wktstr.Substring(0, 7) == "POLYGON")
                return ObjType.Polygon;
            else if (wktstr.Substring(0, 10) == "MULTIPOINT")
                return ObjType.MPoint;
            else if (wktstr.Substring(0, 15) == "MULTILINESTRING")
                return ObjType.MLine;
            else if (wktstr.Substring(0, 12) == "MULTIPOLYGON")
                return ObjType.MPolygon;
            else if (wktstr.Substring(0, 18) == "GEOMETRYCOLLECTION")
                return ObjType.Collection;
            else
                return ObjType.Other;
        }
        catch
        {
            return ObjType.Other;
        }
    }

    /// <summary>
    /// WKT形式の文字列に変換する。
    /// </summary>
    public static string ToString(ShapeStruct s)
    {
        switch (s.Type)
        {
            case ShapeStruct.ObjType.Point:
                return ToString(s.Points[0]);
            case ShapeStruct.ObjType.MultiPoint:
                return ToString(s.Points, ObjType.MPoint);
            case ShapeStruct.ObjType.PolyLine:
                if (s.NumParts == 1)
                    return ToString(s.Points, ObjType.Line);
                else
                    return ToString(s.PolyLines(), ObjType.MLine);
            case ShapeStruct.ObjType.Polygon:
                GeoPoint[][][] mpol = Geo2DExtra.PolygonToMultiPolygon(s.PolyLines());
                if (mpol.Length == 1)
                    return ToString(mpol[0], ObjType.Polygon);
                else
                    return ToString(mpol);
            case ShapeStruct.ObjType.Null:
                return "";
            default:
                throw new Exception(s.Type.ToString() + "には対応していません");
        }
    }

    /// <summary>
    /// WKT形式の文字列に変換する。
    /// </summary>
    public static string ToString(GeoPoint pnt)
    {
        return "POINT (" + pnt.X.ToString() + " " + pnt.Y.ToString() + ")";
    }

    /// <summary>
    /// WKT形式の文字列に変換する。
    /// </summary>
    public static string ToString(GeoPoint[] pnts, ObjType typ)
    {
        string obj = "";
        if (typ == ObjType.MPoint)
            obj = "MULTIPOINT ";
        else if (typ == ObjType.Line)
            obj = "LINESTRING ";
        else
            return "";

        StringBuilder sb = new StringBuilder(1000); //適当
        sb.Append(obj);
        sb.Append("(");
        for (int i = 0; i < pnts.Length; i++)
        {
            sb.Append(pnts[i].X.ToString());
            sb.Append(" ");
            sb.Append(pnts[i].Y.ToString());
            if(i != (pnts.Length - 1))
                sb.Append(",");
        }
        sb.Append(")");

        return sb.ToString();
    }

    /// <summary>
    /// WKT形式の文字列に変換する。
    /// </summary>
    public static string ToString(GeoPoint[][] lines, ObjType typ)
    {
        string obj = "";
        if (typ == ObjType.MLine)
            obj = "MULTILINESTRING";
        else if (typ == ObjType.Polygon)
            obj = "POLYGON";
        else
            return "";

        string geostr = "(";
        foreach (GeoPoint[] pnts in lines)
        {
            geostr += "(";

            string[] tmp = new string[pnts.Length];
            for (int i = 0; i < pnts.Length; i++)
                tmp[i] = pnts[i].X.ToString() + " " + pnts[i].Y.ToString();
            geostr += string.Join(",", tmp);

            geostr = geostr.TrimEnd(',') + "),";
        }

        return obj + geostr.TrimEnd(',') + ")";
    }

    /// <summary>
    /// WKT形式の文字列に変換する。
    /// </summary>
    public static string ToString(GeoPoint[][][] pols)
    {
        string geostr = "(";
        foreach (GeoPoint[][] pol in pols)
        {
            geostr += "(";
            foreach (GeoPoint[] pnts in pol)
            {
                geostr += "(";

                string[] tmp = new string[pnts.Length];
                for (int i = 0; i < pnts.Length; i++)
                    tmp[i] = pnts[i].X.ToString() + " " + pnts[i].Y.ToString();
                geostr += string.Join(",", tmp);

                geostr = geostr.TrimEnd(',') + "),";
            }
            geostr = geostr.TrimEnd(',') + "),";
        }

        return "MULTIPOLYGON " + geostr.TrimEnd(',') + ")";
    }

    /// <summary>
    /// wkt形式の文字列のGeometryCollectionを各オブジェクトに分ける
    /// </summary>
    public static string[] ExploadGeometryCollection(string wkt_format_string)
    {
        List<string> result = new List<string>(100);
        foreach (string s in GetPointObjectFromGeometryCollection(wkt_format_string)) result.Add(s);
        foreach (string s in GetMultiPointObjectFromGeometryCollection(wkt_format_string)) result.Add(s);
        foreach (string s in GetLineStringObjectFromGeometryCollection(wkt_format_string)) result.Add(s);
        foreach (string s in GetMultiLineStringObjectFromGeometryCollection(wkt_format_string)) result.Add(s);
        foreach (string s in GetPolygonObjectFromGeometryCollection(wkt_format_string)) result.Add(s);
        foreach (string s in GetMultiPolygonObjectFromGeometryCollection(wkt_format_string)) result.Add(s);

        return result.ToArray();
    }

    /// <summary>
    /// wkt形式の文字列からGeoPoint型に変換する。
    /// </summary>
    public static GeoPoint ToPoint(string wkt_format_string)
    {
        GeoPoint[] pnts = GetPointsFromText(wkt_format_string);
        if ((pnts == null) || (pnts.Length != 1))
            throw new ArgumentException("不正な引数「" + wkt_format_string + "」");

        return pnts[0];
    }

    /// <summary>
    /// wkt形式の文字列からGeoPoint[]型に変換する。
    /// </summary>
    public static GeoPoint[] ToLineString(string wkt_format_string)
    {
        GeoPoint[] result;
        result = GetPointsFromText(wkt_format_string);
        if (result == null)
            throw new ArgumentException("不正な引数「" + wkt_format_string + "」");

        return result;
    }

    /// <summary>
    /// wkt形式の文字列からGeoPoint[]型に変換する。
    /// </summary>
    public static GeoPoint[] ToMultiPoint(string wkt_format_string)
    {
        try
        {
            return ToLineString(wkt_format_string);
        }
        catch (ArgumentException e)
        {
            throw new ArgumentException("不正な引数「" + wkt_format_string + "」");
        }
    }

    /// <summary>
    /// wkt形式の文字列からGeoPoint[][]型に変換する。
    /// </summary>
    public static GeoPoint[][] ToPolygon(string wkt_format_string)
    {
        List<GeoPoint[]> pnts = new List<GeoPoint[]>(100);
        foreach (string seg in GetSegmentFromText(wkt_format_string))
        {
            GeoPoint[] tmp = GetPointsFromText(seg);
            if (tmp == null)
                throw new ArgumentException("不正な引数「" + wkt_format_string + "」");
            pnts.Add(tmp);
        }

        if (GetType(wkt_format_string) == ObjType.MLine)
            return pnts.ToArray();

        GeoPoint[][][] pols = Geo2DExtra.PolygonToMultiPolygon(pnts.ToArray());
        if (pols.Length != 1)
            throw new ArgumentException("「" + wkt_format_string + "」はポリゴンではありません");
        return  pols[0];
    }

    /// <summary>
    /// wkt形式の文字列からGeoPoint[][]型に変換する。
    /// </summary>
    public static GeoPoint[][] ToMultiLineString(string wkt_format_string)
    {
        try
        {
            return ToPolygon(wkt_format_string);
        }
        catch (ArgumentException e)
        {
            throw new ArgumentException("不正な引数「" + wkt_format_string + "」");
        }
    }

    /// <summary>
    /// wkt形式の文字列からGeoPoint[][][]型に変換する。multi = falseなら内外で分けない(実質GeoPoint[][]を返す)
    /// </summary>
    public static GeoPoint[][][] ToMultiPolygon(string wkt_format_string, bool multi)
    {
        List<GeoPoint[]> pnts = new List<GeoPoint[]>(100);

        foreach (string mseg in GetMultiSegmentFromText(wkt_format_string))
            foreach (string seg in GetSegmentFromText(mseg))
            {
                GeoPoint[] tmp = GetPointsFromText(seg);
                if (tmp == null)
                    throw new ArgumentException("不正な引数「" + wkt_format_string + "」");
                pnts.Add(tmp);
            }

        if(multi)
            return Geo2DExtra.PolygonToMultiPolygon(pnts.ToArray());
        else
            return new GeoPoint[][][] { pnts.ToArray()};
    }

    /// <summary>
    /// ()で囲まれた文字列からPoint型を取り出す。失敗したらnullを返す。
    /// </summary>
    private static GeoPoint[] GetPointsFromText(string str)
    {
        string[] geos = Regex.Matches(StrUtils.DuplicateToOne(str, " "), "\\(.*?\\)")[0].Value.Replace("(", "").Replace(")", "").Split(',');
        if ((geos == null) || (geos.Length == 0)) return null;

        List<GeoPoint> pnts = new List<GeoPoint>(100);
        foreach(string s in geos)
        {
            string[] tmp = s.Trim().Split(' ');
            double x, y;
            if (tmp.Length != 2) return null;
            if (double.TryParse(tmp[0], out x) == false) return null;
            if (double.TryParse(tmp[1], out y) == false) return null;

            pnts.Add(new GeoPoint(x, y));
        }

        return pnts.ToArray();
    }

    /// <summary>
    /// ((),(),...())形式の文字列から(),(),...,()を取り出す。
    /// </summary>
    private static IEnumerable<string> GetSegmentFromText(string str)
    {
        return new Regex("\\(.*?\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("((", "("));
    }

    /// <summary>
    /// ((()),(()),...(()))形式の文字列から(()),(()),...,(())を取り出す。
    /// </summary>
    private static IEnumerable<string> GetMultiSegmentFromText(string str)
    {
        return new Regex("\\(\\(.*?\\)\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(((", "(("));
    }

    /// <summary>
    /// GeometryCollectionからPointを取り出す。
    /// </summary>
    private static IEnumerable<string> GetPointObjectFromGeometryCollection(string str)
    {
        return new Regex("[^MULTI]POINT\\s*\\(.*?\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(P", "P").Replace(",P", "P").Trim());
    }

    /// <summary>
    /// GeometryCollectionからMultiPointを取り出す。
    /// </summary>
    private static IEnumerable<string> GetMultiPointObjectFromGeometryCollection(string str)
    {
        return new Regex("MULTIPOINT\\s*\\(.*?\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(M", "M").Replace(",M", "M").Trim());
    }

    /// <summary>
    /// GeometryCollectionからLineStringを取り出す。
    /// </summary>
    private static IEnumerable<string> GetLineStringObjectFromGeometryCollection(string str)
    {
        return new Regex("[^MULTI]LINESTRING\\s*\\(.*?\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(L", "L").Replace(",L", "L").Trim());
    }

    /// <summary>
    /// GeometryCollectionからMultiLineStringを取り出す。
    /// </summary>
    private static IEnumerable<string> GetMultiLineStringObjectFromGeometryCollection(string str)
    {
        return new Regex("MULTILINESTRING\\s*\\(\\(.*?\\)\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(M", "M").Replace(",M", "M").Trim());
    }

    /// <summary>
    /// GeometryCollectionからPolygonを取り出す。
    /// </summary>
    private static IEnumerable<string> GetPolygonObjectFromGeometryCollection(string str)
    {
        return new Regex("[^MULTI]POLYGON\\s*\\(\\(.*?\\)\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(P", "P").Replace(",P", "P").Trim());
    }

    /// <summary>
    /// GeometryCollectionからMultiPolygonを取り出す。
    /// </summary>
    private static IEnumerable<string> GetMultiPolygonObjectFromGeometryCollection(string str)
    {
        return new Regex("MULTIPOLYGON\\s*\\(\\(\\(.*?\\)\\)\\)").Matches(str).Cast<Match>().Select(x => x.Value.Replace("(M", "M").Replace(",M", "M").Trim());
    }
}
