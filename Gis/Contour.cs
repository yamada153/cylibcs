﻿/******************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 *****************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

class Contour
{
    GeoPoint[][] _contours;

    /// <summary>
    /// コンターをGisファイルから取得する。コンターはラインでなければならない
    /// </summary>
    public Contour(string filename, string elv_field)
    {
        Tuple<ShapeList, StringTable> contourfile = GisUtils.LoadFromFile(filename);

        if (!contourfile.Item2.FieldExist(elv_field))
            throw new Exception("コンターファイル「" + filename + "」の読み込みエラー:標高フィールド「" + elv_field + "」が見つかりませんでした。");

        List<GeoPoint[]> tmp = new List<GeoPoint[]>(contourfile.Item1.Count());
        for(int i = 0; i < contourfile.Item1.Count(); i++)
        {
            if (contourfile.Item1[i].Type != ShapeStruct.ObjType.PolyLine) continue;

            double hyoko = double.Parse(contourfile.Item2[i, elv_field]);
            GeoPoint[][] polylines = contourfile.Item1[i].PolyLines();
            for(int j = 0; j < polylines.Length; j++)
            {
                GeoPoint[] pnts = (GeoPoint[])polylines[j].Clone();
                for(int k = 0; k < polylines[j].Length; k++)
                    pnts[k].Z = hyoko;
                tmp.Add(pnts);
            }
        }
        _contours = tmp.ToArray();
    }

    /// <summary>
    /// 指定した点の標高値を取得する
    /// </summary>
    public double GetElv(GeoPoint pnt, double limit_meter)
    {
        StringTable dst = new StringTable(new string[] { "WKT", "等高線", "標高", "距離" });
        /////pntとの距離が最短になる等高線上の点を取得する
        for (int i = 0; i < _contours.Length; i++)
        {
            GeoPoint[] pnts = _contours[i];
            GeoPoint p = Geo2DBase.ClosestPoint(pnt, pnts);
            double len = Geo2DBase.Distance(p, pnt);
            if (len > limit_meter) continue;
            dst.Add(new string[] { WKTFunc.ToString(p), WKTFunc.ToString(_contours[i], WKTFunc.ObjType.Line), _contours[i][0].Z.ToString(), len.ToString() });
        }
        /////最短距離1位の隣の標高の等高線を取得する
        dst.NumSort("距離");
        dst.Uniq("標高");
        double mid = double.Parse(dst[0, "標高"]);
        double dhigh = double.MaxValue;
        double dlow = double.MinValue;
        int limit = 3;
        limit = Math.Min(limit, dst.Count());
        for (int i = 1; i < limit; i++)
        {
            double dtmp = double.Parse(dst[i, "標高"]);
            if (mid > dtmp)
                dlow = Math.Max(dlow, dtmp);
            else if (mid <= dtmp)
                dhigh = Math.Min(dhigh, dtmp);
        }
        string dhighstr = dhigh.ToString();
        string dlowstr = dlow.ToString();
        int[] ihigh = dst.Search("標高", dhighstr);
        int[] ilow = dst.Search("標高", dlowstr);
        /////pntの隣の等高線を取得する/////
        List<int> nearcontourindex = new List<int>();
        GeoPoint[] contourline = WKTFunc.ToLineString(dst[0, "等高線"]);
        if ((ihigh != null) && (ihigh.Length != 0))
        {
            GeoPoint[] s = new GeoPoint[] { pnt, WKTFunc.ToPoint(dst[ihigh[0], "WKT"]) }; //pntと等高線の最短距離点を結ぶ線
            GeoPoint[] p = Geo2DIntersect.Intersection(s, contourline);
            if (double.IsNaN(p[0].X))
                nearcontourindex.Add(ihigh[0]);
        }
        if ((ilow != null) && (ilow.Length != 0))
        {
            GeoPoint[] s = new GeoPoint[] { pnt, WKTFunc.ToPoint(dst[ilow[0], "WKT"]) }; //pntと等高線の最短距離点を結ぶ線
            GeoPoint[] p = Geo2DIntersect.Intersection(s, contourline);
            if (double.IsNaN(p[0].X))
                nearcontourindex.Add(ilow[0]);
        }
        if (nearcontourindex.Count == 0)
            return mid;
        /////補間処理/////
        double midpntlen = double.Parse(dst[0, "距離"]);
        double otherpntlen = double.Parse(dst[nearcontourindex[0], "距離"]);
        double midhigh = double.Parse(dst[0, "標高"]);
        double otherhigh = double.Parse(dst[nearcontourindex[0], "標高"]);

        double t = otherhigh - midhigh;
        double k = otherpntlen + midpntlen;
        return (t / k) * midpntlen + midhigh;
    }

    /// <summary>
    /// 指定したポリゴン内のコンターを取得する
    /// </summary>
/*
    public Contour(Point[][] pol, string wktfilename, string elvfieldname, char delimiter)
    {
        bool dummy = false;
        WKTFile contour = new WKTFile(wktfilename, delimiter);
        _line = new List<Point[]>(contour.Count());
        _elv = new List<double>(contour.Count());

        for (int i = 0; i < contour.Count(); i++)
        {
            double elv;
            if (!double.TryParse(contour.GetValue(i, elvfieldname), out elv))
                continue;

            Point[][] mline = null;
            WKTFunc.ObjType objtype = WKTFunc.GetType(contour.ObjStr(i));
            switch (objtype)
            {
                case WKTFunc.ObjType.Line:
                    mline = new Point[][] { WKTFunc.ToLineString(contour.ObjStr(i)) };
                    break;
                case WKTFunc.ObjType.MLine:
                    mline = WKTFunc.ToMultiLineString(contour.ObjStr(i));
                    break;
                default:
                    continue;
            }

            for (int j = 0; j < mline.Length; j++)
            {
                Point[][] clipped = GeometryFunc.Clip(mline[j], pol);
                if (clipped.Length == 0) continue;
                if (clipped.Length == 1)
                {
                    _line.Add(clipped[0]);
                    _elv.Add(elv);
                }
                else
                {
                    for (int k = 0; k < clipped.Length; k++)
                    {
                        _line.Add(clipped[k]);
                        _elv.Add(elv);
                    }
                }
            }
        }
    }
*/


    /// <summary>
    /// コンターとlineとの交点を返す。elvsには標高が返る
    /// </summary>
/*
    public GeoPoint[] GetKoten(Point[] line, out double[] elvs)
    {
                List<double> tmpelv = new List<double>(100); //適当
                List<Point> tmpres = new List<Point>(100);

                for (int i = 0; i < _line.Count; i++)
                {
                    Point[] p = GeometryFunc.Intersection(_line[i], line);
                    if (double.IsNaN(p[0].X)) continue;

                    for(int j = 0; j < p.Length; j++)
                    {
                        tmpres.Add(p[j]);
                        tmpelv.Add(_elv[i]);
                    }
                }

                elvs = tmpelv.ToArray();
                return tmpres.ToArray();
        
    }

    /// <summary>
    /// 横断線を作成する
    /// </summary>
    public Point[][] GetOdansen(Point[] line, out double[][] elvs)
    {
        Point[][] odansen = GeometryFunc.MakeOdansen(line, 10, 50);
        Point[][] resultodansen = new Point[odansen.Length][];
        double[][] resultelv = new double[odansen.Length][];

        for (int j = 0; j < odansen.Length; j++)
        {
            List<Point> odantmpgeo = new List<Point>(100);
            List<double> odantmpelv = new List<double>(100);
            /////横断線の始点と終点の標高は補間で取得する
            odantmpgeo.Add(odansen[j][0]);
            odantmpelv.Add(GetElv(odansen[j][0], 50));
            odantmpgeo.Add(odansen[j][odansen[j].Length - 1]);
            odantmpelv.Add(GetElv(odansen[j][odansen[j].Length - 1], 50));
            /////横断線とコンターの交点を取得する
            double[] tmpelvs;
            Point[] koten = GetKoten(odansen[j], out tmpelvs);
            for (int k = 0; k < koten.Length; k++)
            {
                odantmpgeo.Add(koten[k]);
                odantmpelv.Add(tmpelvs[k]);
            }
            /////横断線の道順に交点を並び替える
            int[] indexs = GeometryFunc.DirectionSortIndex(odantmpgeo.ToArray(), odansen[j]);
            Point[] pnttmps = new Point[indexs.Length];
            double[] dtmps = new double[indexs.Length];
            for(int k = 0; k < indexs.Length; k++)
            {
                pnttmps[k] = odantmpgeo[indexs[k]];
                dtmps[k] = odantmpelv[indexs[k]];
            }

            resultodansen[j] = pnttmps;
            resultelv[j] = dtmps;
        }

        elvs = resultelv;
        return resultodansen;
    }

    /// <summary>
    /// 横断線の中心から両端にかけて、低い標高の位置を取得する。尾根は超えない
    /// lowcenterにtrueが返された場合、中心が低いということ
    /// </summary>
    public Point GetLowPositionFromCenter(Point[] odansen, double[] elvs, bool isleft, out bool lowcenter, out double minelv)
    {
        Point center = GeometryFunc.BoundCenter(odansen);
        Point pdeg;
        if (isleft)
            pdeg = odansen[0];
        else
            pdeg = odansen[odansen.Length - 1];

        /////低い位置を探す/////
        int[] indexs = GeometryFunc.DirectionSortIndex(odansen, new Point[] { center, pdeg });
        minelv = elvs[indexs[0]];
        lowcenter = false;
        Point pstart = odansen[indexs[0]];
        Point pend = new Point(double.NaN, double.NaN); //低い標高が続いた場合、中心を取りたい
        for (int k = 1; k < indexs.Length; k++)
        {
            //次の標高は高い
            if (elvs[indexs[k - 1]] < elvs[indexs[k]])
                break;
            //次の標高は低い
            else
            {
                //低い標高が続いていた
                if (elvs[indexs[k]] == minelv)
                    pend = odansen[indexs[k]];
                //さらに低い標高が見つかった
                else
                {
                    lowcenter = true;
                    pstart = odansen[indexs[k]];
                    pend = new Point(double.NaN, double.NaN);
                    minelv = elvs[indexs[k]];
                }
            }
        }

        if (double.IsNaN(pend.X))
            return new Point(pstart.X, pstart.Y);
        else
            return GeometryFunc.BoundCenter(new Point[] { pstart, pend });
    }
    */
}
