﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

static class XYZTileFunc
{
    /// <summary>
    /// XYZTileファイルのフルパスからzoom、x、yの値を取得する
    ///</summary>
    static public bool GetZoomAndXAndY(string filename, out int zoom, out int y, out int x, char sep = '\\')
    {
        zoom = -9999;
        x = -9999;
        y = -8888;

        string[] splited = Path.ChangeExtension(filename, "").Replace(".", "").Split(sep);
        if (!int.TryParse(splited[splited.Length - 3], out zoom)) return false;
        if (!int.TryParse(splited[splited.Length - 2], out x)) return false;
        if (!int.TryParse(splited[splited.Length - 1], out y)) return false;

        return true;
    }

    /// <summary>
    /// タイルのメッシュポリゴンを返す
    /// </summary>
    static public GeoPoint[][] Polygon(int zoom, int x, int y)
    {
        GeoPoint[][] pol = new GeoPoint[1][];
        pol[0] = new GeoPoint[5];
        pol[0][0] = SW(zoom, x, y);
        pol[0][1] = NW(zoom, x, y);
        pol[0][2] = NE(zoom, x, y);
        pol[0][3] = SE(zoom, x, y);
        pol[0][4] = SW(zoom, x, y);

        return pol;
    }

    /// <summary>
    /// タイルのメッシュポリゴンを返す。失敗したらnullを返す
    /// </summary>
    static public GeoPoint[][] Polygon(string path, char sep = '\\')
    {
        int zoom = 0;
        int x = 0;
        int y = 0;
        if (!GetZoomAndXAndY(path, out zoom, out y, out x, sep))
            return null;

        return Polygon(zoom, x, y);
    }

    /// <summary>
    /// XYZTileのタイル(256×256)をMapInfoで読み込むためのTABファイルを作成する。通常swxとnex、swyとneyは同じ数値を入れていい。
    /// filenameにはラスタファイルの名前を入れる。保存するTABファイル名ではない。
    /// </summary>
    static public void WriteTabFileFromRaster(string filename, int zoom, int swx, int swy, int nex, int ney, int pngwidth = 256, int pngheight = 256)
    {
        if (File.Exists(filename) == false)
            throw new FileNotFoundException(filename + "が存在しません。");

        string tabfile = Path.ChangeExtension(filename, ".tab");
        using (StreamWriter tab = new StreamWriter(tabfile, false, Encoding.GetEncoding("shift_jis")))
        {
            GeoPoint sw = XYZTileFunc.SW(zoom, swx, swy);
            GeoPoint ne = XYZTileFunc.NE(zoom, nex, ney);

            double[] box = new double[4];
            box[0] = ne.X;
            box[1] = ne.Y;
            box[2] = sw.X;
            box[3] = sw.Y;

            tab.WriteLine("!table");
            tab.WriteLine("!version 300");
            tab.WriteLine("!charset WindowsLatin1");

            tab.WriteLine("\nDefinition Table");
            tab.WriteLine(string.Format("\tFile \"{0}\"", Path.GetFileName(filename)));
            tab.WriteLine(string.Format("\tType \"RASTER\""));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 1\",", box[2], box[1], 1, 1));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 2\",", box[2], box[3], 1, pngheight));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 3\",", box[0], box[3], pngwidth, pngheight));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 4\"", box[0], box[1], pngwidth, 1));
            tab.WriteLine(string.Format("\t{0}", CoordSysString.MifCoordStringsJGD[0]));

            tab.WriteLine("\tUnits \"degree\"");
        }
    }

    /// <summary>
    /// ラスターをQGIS等で読み込むためのBPWファイルを作成する。nwは左上のピクセルの中心の座標を指定する
    /// filenameにはラスタファイルの名前を入れる。保存するBPWファイル名ではない。
    /// </summary>
    static public void WriteWorldFileFromRaster(string filename, int zoom, int swx, int swy, int nex, int ney)
    {
        //画像サイズ
        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(filename);
        double bmpwidth = bmp.Width;
        double bmpheight = bmp.Height;
        //グリッド幅
        GeoPoint sw = SW(zoom, swx, swy);
        GeoPoint ne = NE(zoom, nex, ney);

        CoordSysConv csc = new CoordSysConv(4612, 3857);
        try
        {
            sw = csc.ConvXY(sw);
            ne = csc.ConvXY(ne);
        }
        finally
        {
            csc.Free();
        }

        double unit_x = (ne.X - sw.X) / bmpwidth;
        double unit_y = (sw.Y - ne.Y) / bmpheight;

        string ext = Path.GetExtension(filename).ToLower();
        string wfile = "";
        switch(ext)
        {
            case ".jpg": wfile = Path.ChangeExtension(filename, ".jgw"); break;
            case ".png": wfile = Path.ChangeExtension(filename, ".pgw"); break;
            case ".bmp": wfile = Path.ChangeExtension(filename, ".bpw"); break;
            case ".tif": wfile = Path.ChangeExtension(filename, ".tfw"); break;
            default: new Exception("拡張子「" + ext + "」には未対応です。"); break;
        }

        using (StreamWriter w = new StreamWriter(wfile, false, Encoding.GetEncoding("shift_jis")))
        {
            w.WriteLine(unit_x.ToString());
            w.WriteLine("0.0");
            w.WriteLine("0.0");
            w.WriteLine(unit_y.ToString());
            w.WriteLine((sw.X + unit_x / 2).ToString("F6"));
            w.WriteLine((ne.Y + unit_y / 2).ToString("F6"));
        }
    }

    /// <summary>
    /// 指定したXYZTileのメッシュの北西の緯度経度を取得する
    /// </summary>
    public static GeoPoint NW(int zoom, int x, int y)
    {
        double lat = 0, lon = 0;
        XYZTileFunc.ToLat(zoom, y, ref lat);
        XYZTileFunc.ToLon(zoom, x, ref lon);
        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 指定したXYZTileのメッシュの南西の緯度経度を取得する
    /// </summary>
    public static GeoPoint SW(int zoom, int x, int y)
    {
        double lat = 0, lon = 0;
        XYZTileFunc.ToLat(zoom, y + 1, ref lat);
        XYZTileFunc.ToLon(zoom, x, ref lon);
        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 指定したXYZTileのメッシュの北東の緯度経度を取得する
    /// </summary>
    public static GeoPoint NE(int zoom, int x, int y)
    {
        double lat = 0, lon = 0;
        XYZTileFunc.ToLat(zoom, y, ref lat);
        XYZTileFunc.ToLon(zoom, x + 1, ref lon);
        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 指定したXYZTileのメッシュの南東の緯度経度を取得する
    /// </summary>
    public static GeoPoint SE(int zoom, int x, int y)
    {
        double lat = 0, lon = 0;
        XYZTileFunc.ToLat(zoom, y + 1, ref lat);
        XYZTileFunc.ToLon(zoom, x + 1, ref lon);
        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 指定したXYZTileのメッシュの北西の緯度を取得する
    /// </summary>
    public static void ToLat(int zoom, int y, ref double lat)
    {
        double n = Math.Pow(2, zoom);
        double latrad = Math.Atan(Math.Sinh(Math.PI * (1 - 2 * y / n)));
        lat = latrad * 180.0 / Math.PI;
    }

    /// <summary>
    /// 指定したXYZTileのメッシュの北西の経度を取得する
    /// </summary>
    public static void ToLon(int zoom, int x, ref double lon)
    {
        double n = Math.Pow(2, zoom);
        lon = x / n * 360.0 - 180.0;
    }

    /// <summary>
    /// 指定した緯度のタイル番号xを取得する
    /// </summary>
    public static void ToX(double lon, double zoom, ref int x)
    {
        x = (int)((lon / 180 + 1) * Math.Pow(2, zoom) / 2);
    }

    /// <summary>
    /// 指定した緯度のタイル番号xとタイル内座標px_xを取得する
    /// </summary>
    public static void ToX(double lon, double zoom, ref int x, ref int px_x)
    {
        x = (int)((lon / 180 + 1) * Math.Pow(2, zoom) / 2);
        px_x = (int)(x % Math.Pow(2, zoom));
    }

    /// <summary>
    /// 指定した経度のタイル番号yを取得する
    /// </summary>
    public static void ToY(double lat, double zoom, ref int y)
    {
        y = (int)(((-Math.Log(Math.Tan((45 + lat / 2) * Math.PI / 180)) + Math.PI) * Math.Pow(2, zoom) / (2 * Math.PI)));
    }

    /// <summary>
    /// 指定した経度のタイル番号yとタイル内座標px_yを取得する
    /// </summary>
    public static void ToY(double lat, double zoom, ref int y, ref int px_y)
    {
        y = (int)(((-Math.Log(Math.Tan((45 + lat / 2) * Math.PI / 180)) + Math.PI) * Math.Pow(2, zoom) / (2 * Math.PI)));
        px_y = (int)(y % Math.Pow(2, zoom));
    }

    /// <summary>
    /// 指定したコードから、自身も含め周囲の9メッシュコードを取得する。コードが無ければ空文字を返す。
    /// <para>配列0番目...左</para>
    /// <para>配列1番目...左上</para>
    /// <para>配列2番目...上</para>
    /// <para>配列3番目...右上</para>
    /// <para>配列4番目...右</para>
    /// <para>配列5番目...右下</para>
    /// <para>配列6番目...下</para>
    /// <para>配列7番目...左下</para>
    /// <para>配列8番目...中心</para>
    /// </summary>   
    public static string[] Neighborhood(string path, char sep = '\\')
    {
        int zoom = 0;
        int x = 0;
        int y = 0;
        if (!GetZoomAndXAndY(path, out zoom, out y, out x, sep))
            return null;

        string[] ret = new string[9];
        string zoom_str = zoom.ToString();
        ret[0] = zoom_str + sep +  (x - 1).ToString() + sep + y.ToString();
        ret[1] = zoom_str + sep + (x - 1).ToString() + sep + (y - 1).ToString();
        ret[2] = zoom_str + sep + x.ToString() + sep + (y - 1).ToString();
        ret[3] = zoom_str + sep + (x + 1).ToString() + sep + (y - 1).ToString();
        ret[4] = zoom_str + sep + (x + 1).ToString() + sep + y.ToString();
        ret[5] = zoom_str + sep + (x + 1).ToString() + sep + (y + 1).ToString();
        ret[6] = zoom_str + sep + x.ToString() + sep + (y + 1).ToString();
        ret[7] = zoom_str + sep + (x - 1).ToString() + sep + (y + 1).ToString();
        ret[8] = zoom_str + sep + x.ToString() + sep + y.ToString();
        return ret;
    }
}