﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

static class CoordSysString
{
    /// <summary>
    /// ヘッダーに与える座標系の文字列
    /// </summary>
    static public string[] MifCoordStringsJGD =
    {
        "CoordSys Earth Projection 1, 152",
        "CoordSys Earth Projection 8, 152, \"m\", 129.5, 33, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 131, 33, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 132.166666, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 133.5, 33, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 134.333333, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 136, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 137.166666, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 138.5, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 139.833333, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 140.833333, 40, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 140.25, 44, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 142.25, 44, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 144.25, 44, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 142, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 127.5, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 124, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 131, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 136, 20, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 152, \"m\", 154, 26, 0.9999, 0, 0",
    };

    /// <summary>
    /// ヘッダーに与える座標系の文字列
    /// </summary>
    static public string[] MifCoordStringsTokyo =
    {
        "CoordSys Earth Projection 1, 97",
        "CoordSys Earth Projection 8, 97, \"m\", 129.5, 33, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 131, 33, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 132.166666, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 133.5, 33, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 134.333333, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 136, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 137.166666, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 138.5, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 139.833333, 36, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 140.833333, 40, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 140.25, 44, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 142.25, 44, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 144.25, 44, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 142, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 127.5, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 124, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 131, 26, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 136, 20, 0.9999, 0, 0",
        "CoordSys Earth Projection 8, 97, \"m\", 154, 26, 0.9999, 0, 0"
    };

    /// <summary>
    /// ヘッダーに与える座標系の文字列
    /// </summary>
    static public string[] ShpCoordStringsJGD =
    {
        "GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]]",
        "PROJCS[\"JGD2000_Japan_Zone_1\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",129.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",33.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_2\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",131.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",33.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_3\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",132.1666666666667],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_4\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",133.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",33.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_5\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",134.3333333333333],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_6\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",136.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_7\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",137.1666666666667],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_8\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",138.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_9\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",139.8333333333333],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_10\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",140.8333333333333],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",40.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_11\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",140.25],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",44.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_12\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",142.25],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",44.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_13\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",144.25],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",44.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_14\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",142.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_15\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",127.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_16\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",124.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_17\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",131.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_18\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",136.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",20.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"JGD2000_Japan_Zone_19\",GEOGCS[\"GCS_JGD_2000\",DATUM[\"D_JGD_2000\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",154.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]"
    };

    /// <summary>
    /// ヘッダーに与える座標系の文字列
    /// </summary>
    static public string[] ShpCoordStringsTokyo =
    {
        "GEOGCS[\"Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]]",
        "PROJCS[\"Japan_Zone_1\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",129.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",33.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_2\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",131.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",33.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_3\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",132.1666666666667],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_4\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",133.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",33.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_5\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",134.3333333333333],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_6\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",136.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_7\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",137.1666666666667],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_8\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",138.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_9\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",139.8333333333333],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",36.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_10\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",140.8333333333333],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",40.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_11\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",140.25],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",44.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_12\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",142.25],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",44.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_13\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",144.25],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",44.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_14\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",142.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_15\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",127.5],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_16\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",124.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_17\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",131.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_18\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",136.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",20.0],UNIT[\"Meter\",1.0]]",
        "PROJCS[\"Japan_Zone_19\",GEOGCS[\"GCS_Tokyo\",DATUM[\"D_Tokyo\",SPHEROID[\"Bessel_1841\",6377397.155,299.1528128]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",154.0],PARAMETER[\"Scale_Factor\",0.9999],PARAMETER[\"Latitude_Of_Origin\",26.0],UNIT[\"Meter\",1.0]]"
    };

    static public string EpsgToMifCoordsysString(int epsg)
    {
        switch(epsg)
        {
            case 4301: return MifCoordStringsTokyo[0];
            case 30161: return MifCoordStringsTokyo[1];
            case 30162: return MifCoordStringsTokyo[2];
            case 30163: return MifCoordStringsTokyo[3];
            case 30164: return MifCoordStringsTokyo[4];
            case 30165: return MifCoordStringsTokyo[5];
            case 30166: return MifCoordStringsTokyo[6];
            case 30167: return MifCoordStringsTokyo[7];
            case 30168: return MifCoordStringsTokyo[8];
            case 30169: return MifCoordStringsTokyo[9];
            case 30170: return MifCoordStringsTokyo[10];
            case 30171: return MifCoordStringsTokyo[11];
            case 30172: return MifCoordStringsTokyo[12];
            case 30173: return MifCoordStringsTokyo[13];
            case 30174: return MifCoordStringsTokyo[14];
            case 30175: return MifCoordStringsTokyo[15];
            case 30176: return MifCoordStringsTokyo[16];
            case 30177: return MifCoordStringsTokyo[17];
            case 30178: return MifCoordStringsTokyo[18];
            case 30179: return MifCoordStringsTokyo[19];
            case 4326: return MifCoordStringsJGD[0];
            case 2443: return MifCoordStringsJGD[1];
            case 2444: return MifCoordStringsJGD[2];
            case 2445: return MifCoordStringsJGD[3];
            case 2446: return MifCoordStringsJGD[4];
            case 2447: return MifCoordStringsJGD[5];
            case 2448: return MifCoordStringsJGD[6];
            case 2449: return MifCoordStringsJGD[7];
            case 2450: return MifCoordStringsJGD[8];
            case 2451: return MifCoordStringsJGD[9];
            case 2452: return MifCoordStringsJGD[10];
            case 2453: return MifCoordStringsJGD[11];
            case 2454: return MifCoordStringsJGD[12];
            case 2455: return MifCoordStringsJGD[13];
            case 2456: return MifCoordStringsJGD[14];
            case 2457: return MifCoordStringsJGD[15];
            case 2458: return MifCoordStringsJGD[16];
            case 2459: return MifCoordStringsJGD[17];
            case 2460: return MifCoordStringsJGD[18];
            case 2461: return MifCoordStringsJGD[19];
            default: return null;
        }
    }

    static public string EpsgToShpCoordsysString(int epsg)
    {
        switch (epsg)
        {
            case 4301: return ShpCoordStringsTokyo[0];
            case 30161: return ShpCoordStringsTokyo[1];
            case 30162: return ShpCoordStringsTokyo[2];
            case 30163: return ShpCoordStringsTokyo[3];
            case 30164: return ShpCoordStringsTokyo[4];
            case 30165: return ShpCoordStringsTokyo[5];
            case 30166: return ShpCoordStringsTokyo[6];
            case 30167: return ShpCoordStringsTokyo[7];
            case 30168: return ShpCoordStringsTokyo[8];
            case 30169: return ShpCoordStringsTokyo[9];
            case 30170: return ShpCoordStringsTokyo[10];
            case 30171: return ShpCoordStringsTokyo[11];
            case 30172: return ShpCoordStringsTokyo[12];
            case 30173: return ShpCoordStringsTokyo[13];
            case 30174: return ShpCoordStringsTokyo[14];
            case 30175: return ShpCoordStringsTokyo[15];
            case 30176: return ShpCoordStringsTokyo[16];
            case 30177: return ShpCoordStringsTokyo[17];
            case 30178: return ShpCoordStringsTokyo[18];
            case 30179: return ShpCoordStringsTokyo[19];
            case 4326: return ShpCoordStringsJGD[0];
            case 2443: return ShpCoordStringsJGD[1];
            case 2444: return ShpCoordStringsJGD[2];
            case 2445: return ShpCoordStringsJGD[3];
            case 2446: return ShpCoordStringsJGD[4];
            case 2447: return ShpCoordStringsJGD[5];
            case 2448: return ShpCoordStringsJGD[6];
            case 2449: return ShpCoordStringsJGD[7];
            case 2450: return ShpCoordStringsJGD[8];
            case 2451: return ShpCoordStringsJGD[9];
            case 2452: return ShpCoordStringsJGD[10];
            case 2453: return ShpCoordStringsJGD[11];
            case 2454: return ShpCoordStringsJGD[12];
            case 2455: return ShpCoordStringsJGD[13];
            case 2456: return ShpCoordStringsJGD[14];
            case 2457: return ShpCoordStringsJGD[15];
            case 2458: return ShpCoordStringsJGD[16];
            case 2459: return ShpCoordStringsJGD[17];
            case 2460: return ShpCoordStringsJGD[18];
            case 2461: return ShpCoordStringsJGD[19];
            default: return null;
        }
    }

    static public int ZahyokeiToEPSG(int zahyokei)
    {
        if (zahyokei == 0)
            return 4326;
        else
            return 2442 + zahyokei;
    }

    static public int EpsgToKei(int epsg)
    {
        switch (epsg)
        {
            //新座標
            case 4326: return 0;
            case 2443: return 1;
            case 2444: return 2;
            case 2445: return 3;
            case 2446: return 4;
            case 2447: return 5;
            case 2448: return 6;
            case 2449: return 7;
            case 2450: return 8;
            case 2451: return 9;
            case 2452: return 10;
            case 2453: return 11;
            case 2454: return 12;
            case 2455: return 13;
            case 2456: return 14;
            case 2457: return 15;
            case 2458: return 16;
            case 2459: return 17;
            case 2460: return 18;
            case 2461: return 19;
            //旧座標
            case 4301: return 20;
            case 30161: return 21;
            case 30162: return 22;
            case 30163: return 23;
            case 30164: return 24;
            case 30165: return 25;
            case 30166: return 26;
            case 30167: return 27;
            case 30168: return 28;
            case 30169: return 29;
            case 30170: return 30;
            case 30171: return 31;
            case 30172: return 32;
            case 30173: return 33;
            case 30174: return 34;
            case 30175: return 35;
            case 30176: return 36;
            case 30177: return 37;
            case 30178: return 38;
            case 30179: return 39;

            default: throw new Exception("EPSG「" + epsg + "」には対応していません");
        }
    }

}