/******************************************************************************************
 *  Copyright (c) 2018 Chiaki Yamada
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;

class Asc
{
    private double _ncols = double.NaN;
    private double _nrows = double.NaN;
    private double _xllcenter = double.NaN;
    private double _yllcenter = double.NaN;
    private double _xllcorner = double.NaN;
    private double _yllcorner = double.NaN;
    private double _cellsize = double.NaN;
    private double _nodata = double.NaN;
    public Grid Grid { get; }

    public Asc(string asciigridfilename, bool folder = false)
    {
        //ファイル単体で呼び出す場合
        if (folder == false)
        {
            string[] contents = FileFunc.LoadFromFile(asciigridfilename);

            for (int i = 0; i < contents.Length; i++)
            {
                string tmp = contents[i].ToUpper();
                if (tmp.Contains("NCOLS"))
                    if (!double.TryParse(tmp.Replace("NCOLS", "").Trim(), out _ncols)) continue;
                if (tmp.Contains("NROWS"))
                    if (!double.TryParse(tmp.Replace("NROWS", "").Trim(), out _nrows)) continue;
                if (tmp.Contains("XLLCENTER"))
                    if (!double.TryParse(tmp.Replace("XLLCENTER", "").Trim(), out _xllcenter)) continue;
                if (tmp.Contains("YLLCENTER"))
                    if (!double.TryParse(tmp.Replace("YLLCENTER", "").Trim(), out _yllcenter)) continue;
                if (tmp.Contains("XLLCORNER"))
                    if (!double.TryParse(tmp.Replace("XLLCORNER", "").Trim(), out _xllcorner)) continue;
                if (tmp.Contains("YLLCORNER"))
                    if (!double.TryParse(tmp.Replace("YLLCORNER", "").Trim(), out _yllcorner)) continue;
                if (tmp.Contains("CELLSIZE"))
                    if (!double.TryParse(tmp.Replace("CELLSIZE", "").Trim(), out _cellsize)) continue;
                if (tmp.Contains("NODATA_VALUE"))
                    if (!double.TryParse(tmp.Replace("NODATA_VALUE", "").Trim(), out _nodata)) continue;
            }
            if (double.IsNaN(_ncols)) throw new Exception("Asc@コンストラクタで例外が発生しました。:NCOLの値を取得できませんでした");
            if (double.IsNaN(_nrows)) throw new Exception("Asc@コンストラクタで例外が発生しました。:NROWSの値を取得できませんでした");
            if (double.IsNaN(_xllcenter) && double.IsNaN(_xllcorner))
                throw new Exception("Asc@コンストラクタで例外が発生しました。:XLLCENTERかXLLCORNERの値を取得できませんでした");
            if (double.IsNaN(_yllcenter) && double.IsNaN(_yllcorner))
                throw new Exception("Asc@コンストラクタで例外が発生しました。:YLLCENTERかYLLCORNERの値を取得できませんでした");
            if (double.IsNaN(_cellsize)) throw new Exception("Asc@コンストラクタで例外が発生しました。:CELLSIZEの値を取得できませんでした");
            if (double.IsNaN(_nodata)) throw new Exception("Asc@コンストラクタで例外が発生しました。:NODATA_VALUEの値を取得できませんでした");

            double[,] data = Array2D.Calloc<double>((int)_ncols, (int)_nrows, _nodata);
            int iRow = 0;
            for (int i = 6; i < contents.Length; i++)
            {
                string[] splited = contents[i].Trim().Split(' ');
                double tmp = 0;
                for (int j = 0; j < _ncols; j++)
                {
                    if (!double.TryParse(splited[j], out tmp))
                        throw new Exception("Asc@コンストラクタで例外が発生しました。:" + (i + 1).ToString() + "行目" + (j + 1).ToString() + "列目で変換エラーが発生しました");
                    data[j, iRow] = tmp;
                }
                iRow++;
            }
            Array2D.MirrorTopUnder(ref data, _nodata);
            double MinX, MinY;
            if (!double.IsNaN(_xllcenter))
                MinX = _xllcenter;
            else
            {
                MinX = _xllcorner + _cellsize / 2;
                _xllcenter = MinX;
            }
            if (!double.IsNaN(_yllcenter))
                MinY = _yllcenter;
            else
            {
                MinY = _yllcorner + _cellsize / 2;
                _yllcenter = MinY;
            }
            Grid = new Grid(data, MinX, MinY, _cellsize, _nodata);
        }
        //フォルダでまとめて呼び出す場合
        else
        {
            string[] grdfiles = FileFunc.FileList(asciigridfilename, "*.grd", false);

            string[] codes = FileFunc.GetFileNamesWithoutExtension(grdfiles);
            if (codes.Length == 0)
                new Exception("指定したフォルダから、grdファイルを取得することができませんでした。");

            MapFrame mf = null;
            if ((mf = MapFrameGetter.GetMapFlameClass(codes[0])) == null)
                new Exception("指定したフォルダ内のgrdファイルから、図郭のコードを取得することができませんでした。");

            Asc tmp = new Asc(grdfiles[0]); //後で使う

            //統合先を作成する
            GeoPoint[] pnts = new GeoPoint[codes.Length];
            for (int i = 0; i < codes.Length; i++)
                pnts[i] = mf.Center(codes[i]);
            double[] bound = Geo2DBase.Bound(pnts);

            int nX = (int)(Math.Abs(bound[0] - bound[2]) / mf.UnitX) + 1;
            int nY = (int)(Math.Abs(bound[1] - bound[3]) / mf.UnitY) + 1;
            int w = Array2D.Width(tmp.Grid.Data);
            int h = Array2D.Height(tmp.Grid.Data);
            double[,] merge = Array2D.Calloc<double>(nX * w, nY * h, tmp.Grid.NoData);

            //統合先にgrdを貼り付ける
            for (int iX = 0; iX < nX; iX++)
            {
                for (int iY = 0; iY < nY; iY++)
                {
                    string code = mf.MeshCode(bound[2] + mf.UnitX * iX, bound[3] + mf.UnitY * iY);
                    string grdfile = asciigridfilename + "\\" + code + ".grd";
                    if (!File.Exists(grdfile)) continue;

                    Asc grd = new Asc(grdfile);
                    Array2D.Append(ref merge, grd.Grid.Data, w * iX, h * iY, -9999);
                    grd = null;
                }
            }

            GeoPoint sw = mf.SouthWest(mf.MeshCode(bound[2], bound[3]));

            _ncols = Array2D.Width(merge);
            _nrows = Array2D.Height(merge);
            _xllcenter = sw.X + tmp.Grid.Span / 2;
            _yllcenter = sw.Y + tmp.Grid.Span / 2;
            _cellsize = tmp.Grid.Span;
            _nodata = tmp._nodata;

            Grid = new Grid(merge, _xllcenter, _yllcenter, _cellsize, _nodata);
        }
    }

    public Asc(double[,] data, string code, double nodata = -9999)
    {
        MapFrame mf = null;
        if ((mf = MapFrameGetter.GetMapFlameClass(code)) == null)
            new Exception("指定したコード「" + code + "」から、図郭のコードを取得することができませんでした。");

        _ncols = Array2D.Width(data);
        _nrows = Array2D.Height(data);
        GeoPoint sw = mf.SouthWest(code);
        _xllcenter = sw.X;
        _yllcenter = sw.Y;
        double tmpa = mf.UnitX / _ncols;
        double tmpb = mf.UnitY / _nrows;
        _cellsize = (tmpa + tmpb) / 2;
        _nodata = nodata;
        Grid = new Grid(data, _xllcenter, _yllcenter, _cellsize, _nodata);
    }

    /// <summary>
    /// アスキーグリッド形式で保存する
    /// </summary>
    public void SaveToFile(string filename)
    {
        using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            double[,] data = Grid.Data;
            int width = Array2D.Width(data);
            int height = Array2D.Height(data);
            //ヘッダー
            sw.WriteLine("NCOLS " + width.ToString());
            sw.WriteLine("NROWS " + height.ToString());
            sw.WriteLine("XLLCENTER " + _xllcenter);
            sw.WriteLine("YLLCENTER " + _yllcenter);
            sw.WriteLine("CELLSIZE " + _cellsize.ToString("F15"));
            sw.WriteLine("NODATA_VALUE " + _nodata.ToString());
            //データ
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                    sw.Write(data[j, height - i - 1].ToString() + " ");
                sw.Write("\n");
            }
        }
    }
}