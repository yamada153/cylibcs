/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Linq;
using System.IO;

static class DXFR12File
{
    /// <summary>
    /// DXF Ver.R12を出力する。
    /// </summary>
    static public void SaveToDXFR12(string filename, ShapeList sl, StringTable st)
    {
        //レイヤ名フィールドが無ければ作成する。レイヤ名はデフォルトの「0」を入れておく
        if (!st.FieldExist("レイヤ名"))
        {
            st.InsertField(st.FieldCount(), "レイヤ名");
            for (int i = 0; i < st.Count(); i++)
                st[i, "レイヤ名"] = "0";
        }

        using (StreamWriter dxf = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            WriteDxfHeaderSection(dxf, sl);
            WriteDxfTableSection(dxf, st);
            WriteDxfEntitiesSection(dxf, sl, st);
        }
    }

    static private void WriteDxfHeaderSection(StreamWriter dxf, ShapeList sl)
    {
        /////全オブジェクトから、座標値の最大、最小を取得する/////
        double maxx = double.NegativeInfinity;
        double maxy = double.NegativeInfinity;
        double minx = double.PositiveInfinity;
        double miny = double.PositiveInfinity;
        double[] tmp = sl.GetBox();
        maxx = Math.Max(maxx, tmp[0]);
        maxy = Math.Max(maxy, tmp[1]);
        minx = Math.Min(minx, tmp[2]);
        miny = Math.Min(miny, tmp[3]);
        /////書き込み/////
        dxf.WriteLine("  0\nSECTION\n  2\nHEADER\n  9\n$ACADVER\n  1\nAC1009\n  9\n$DWGCODEPAGE\n  3\ndos932");  //変更無いだろうと思う部分
        dxf.WriteLine("  9\n$INSBASE\n 10\n0.0\n 20\n0.0\n 30\n0.0");  //挿入時の基点
        dxf.WriteLine("  9\n$EXTMIN\n 10\n" + minx.ToString() + "\n 20\n" + miny.ToString() + "\n 30\n0.0"); //図形の最小の座標値
        dxf.WriteLine("  9\n$EXTMAX\n 10\n" + maxx.ToString() + "\n 20\n" + maxy.ToString() + "\n 30\n0.0"); //図形の最大の座標値
        dxf.WriteLine("  9\n$OSMODE\n 70\n   511"); //オブジェクトスナップモード
        dxf.WriteLine("  9\n$TEXTSIZE\n 40\n2.5");  //デフォルトテキスト高さ
        dxf.WriteLine("  9\n$TRACEWID\n 40\n1.0");  //デフォルトの太線幅
        dxf.WriteLine("  9\n$TEXTSTYLE\n  7\nSTANDARD");  //カレントテキストスタイル名
        dxf.WriteLine("  9\n$CLAYER\n  8\n0");  //カレントのレイヤ名
        dxf.WriteLine("  9\n$CELTYPE\n  6\nBYLAYER");  //Entity線種名 "BYLAYER"
        dxf.WriteLine("  9\n$CECOLOR\n 62\n   256");  //Entity色番号 256=BYLAYER
        dxf.WriteLine("  9\n$DIMSCALE\n 40\n1.0");  //全ての寸法のスケールファクタ
        dxf.WriteLine("  0\nENDSEC");
    }

    static private void WriteDxfTableSection(StreamWriter dxf, StringTable st)
    {
        /////レイヤー名を取得する/////        
        string[] layernames = st.GetStringsFromColumnName("レイヤ名").Distinct().ToArray();
        if (!layernames.Contains("0"))
            layernames = new string[] { "0" }.Concat(layernames).ToArray();
        /////書き込み/////
        dxf.WriteLine("  0\nSECTION\n  2\nTABLES");
        //線種//
        dxf.WriteLine("  0\nTABLE\n  2\nLTYPE\n 70\n     1");
        dxf.WriteLine("  0\nLTYPE\n  2\nCONTINUOUS\n 70\n     0\n  3\nSolid line\n 72\n    65\n 73\n     0\n 40\n0.0");
        dxf.WriteLine("  0\nENDTAB");
        //レイヤ//
        dxf.WriteLine("  0\nTABLE\n  2\nLAYER\n 70\n     " + layernames.Length.ToString());
        for (int i = 0; i < layernames.Length; i++)
            dxf.WriteLine("  0\nLAYER\n  2\n" + layernames[i] + "\n 70\n     0\n 62\n     7\n  6\nCONTINUOUS");
        dxf.WriteLine("  0\nENDTAB");
        //字体//
        dxf.WriteLine("  0\nTABLE\n  2\nSTYLE\n 70\n     1");
        dxf.WriteLine("  0\nSTYLE\n  2\nSTANDARD\n 70\n     0\n 40\n0.0\n 41\n1.0\n 50\n0.0\n 71\n     0\n 42\n2.5\n  3\nMS Gothic\n  4\n");
        dxf.WriteLine("  0\nENDTAB");
        dxf.WriteLine("  0\nENDSEC");
    }

    static private void WriteDxfEntitiesSection(StreamWriter dxf, ShapeList sl, StringTable st)
    {
        int cnt = 100; //スタートこれで良い?

        dxf.WriteLine("  0\nSECTION\n  2\nENTITIES");
        for (int i = 0; i < sl.Count(); i++)
        {
            switch (sl[i].Type)
            {
                case ShapeStruct.ObjType.Point:
                case ShapeStruct.ObjType.MultiPoint:
                    WriteDxfEntitiesPoint2D(dxf, i, sl, st, ref cnt);
                    break;
                case ShapeStruct.ObjType.PolyLine:
                    WriteDxfEntitiesPolyLine2D(dxf, i, sl, st, ref cnt);
                    break;
                case ShapeStruct.ObjType.Polygon:
                    WriteDxfEntitiesPolygon2D(dxf, i, sl, st, ref cnt);
                    break;
                default:
                    WriteDxfEntitiesText2D(dxf, i, st, ref cnt);
                    break;
            }
        }
        dxf.WriteLine("  0\nENDSEC\n  0\nEOF");
    }

    static private void WriteDxfEntitiesPoint2D(StreamWriter dxf, int index, ShapeList sl, StringTable st, ref int cnt)
    {
        WriteDxfPoint(dxf, sl[index].Points, st[index, "レイヤ名"], ref cnt);
    }

    static private void WriteDxfEntitiesPolyLine2D(StreamWriter dxf, int index, ShapeList sl, StringTable st, ref int cnt)
    {
        GeoPoint[][] mpnts = sl[index].PolyLines();

        for (int i = 0; i < mpnts.Length; i++)
            WriteDxfPolyLine(dxf, mpnts[i], st[index, "レイヤ名"], false, ref cnt);
    }

    static private void WriteDxfEntitiesPolygon2D(StreamWriter dxf, int index, ShapeList sl, StringTable st, ref int cnt)
    {
        GeoPoint[][][] mpol =  Geo2DExtra.PolygonToMultiPolygon(sl[index].PolyLines());

        for (int i = 0; i < mpol.Length; i++)
            for (int j = 0; j < mpol[i].Length; j++)
                WriteDxfPolyLine(dxf, mpol[i][j], st[index, "レイヤ名"], true, ref cnt);
    }

    static private void WriteDxfPoint(StreamWriter dxf, GeoPoint[] pnts, string layer, ref int cnt)
    {
        for (int j = 0; j < pnts.Length; j++)
        {
            dxf.WriteLine("  0\nPOINT");
            cnt++;
            string x = pnts[j].X.ToString();
            string y = pnts[j].Y.ToString();
            dxf.WriteLine("  5\n" + cnt.ToString("X") + "\n  8\n" + layer + "\n 10\n" + x + "\n 20\n" + y + "\n 30\n0.0");
            cnt++;
            dxf.WriteLine("  0\nSEQEND\n  5\n" + cnt.ToString("X") + "\n  8\n" + layer);
        }
    }

    static private void WriteDxfPolyLine(StreamWriter dxf, GeoPoint[] pnts, string layer, bool close, ref int cnt)
    {
        string flag = "0";
        if (close) flag = "1";

        dxf.WriteLine("  0\nPOLYLINE");
        dxf.WriteLine("  5\n" + cnt.ToString("X") + "\n  8\n" + layer + "\n 66\n     1\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n     " + flag);
        cnt++;
        for (int j = 0; j < pnts.Length; j++)
        {
            string x = pnts[j].X.ToString();
            string y = pnts[j].Y.ToString();
            dxf.WriteLine("  0\nVERTEX\n  5\n" + cnt.ToString("X") + "\n  8\n" + layer + "\n 10\n" + x + "\n 20\n" + y + "\n 30\n0.0");
            cnt++;
        }
        dxf.WriteLine("  0\nSEQEND\n  5\n" + cnt.ToString("X") + "\n  8\n" + layer);
    }

    static private void WriteDxfEntitiesText2D(StreamWriter dxf, int index, StringTable st, ref int cnt)
    {
        if (!st.FieldExist("位置x")) return;
        if (!st.FieldExist("位置y")) return;
        if (!st.FieldExist("高さ")) return;
        string x = st[index, "位置x"];
        string y = st[index, "位置x"];
        string height = st[index, "高さ"];
        double tmp;
        if (!double.TryParse(x, out tmp)) return;
        if (!double.TryParse(y, out tmp)) return;
        if (!double.TryParse(height, out tmp)) return;

        dxf.WriteLine("  0\nTEXT\n  5\n" + cnt.ToString("X") + "\n  8\n" + st[index, "レイヤ名"] + "\n  6\nCONTINUOUS\n 10\n" + x + "\n 20\n" + y + "\n 30\n0.0\n 40\n" + height + "\n  1\n" + st[index, "WKT"]);
        cnt++;
    }
}