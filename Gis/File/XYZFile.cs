﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

class XYZFile
{
    List<GeoPoint> _xyz = null;
    uint _colxindex = 0;
    uint _colyindex = 0;
    uint _colzindex = 0;
    double _nodata = -9999;
    bool _except_flg = true;
    char _delimiter;
    double _zchosei;

    public XYZFile(char delimiter, uint colX, uint colY, uint colZ, double zchosei, double nodata, bool except, int capacity = 100000)
    {
        _colxindex = colX - 1;
        _colyindex = colY - 1;
        _colzindex = colZ - 1;
        _delimiter = delimiter;
        _nodata = nodata;
        _except_flg = except;
        _zchosei = zchosei;
        _xyz = new List<GeoPoint>(capacity);
    }

    public XYZFile(string filename, char delimiter, uint colX, uint colY, uint colZ, double zchosei, double nodata, bool except, int capacity = 100000)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("XYZの初期化に失敗しました。「" + filename + "」が見つかりませんでした");

        _colxindex = colX - 1;
        _colyindex = colY - 1;
        _colzindex = colZ - 1;
        _delimiter = delimiter;
        _nodata = nodata;
        _except_flg = except;
        _zchosei = zchosei;
        _xyz = new List<GeoPoint>(capacity);

        Append(filename, _except_flg);
    }

    /// <summary>
    /// XYZのXYを構造体のリストに、属性をStringTableに読み込む
    /// </summary>
    public Tuple<ShapeList, StringTable> ToShapeStructs()
    {
        ShapeList sl = new ShapeList(_xyz.Count);
        StringTable st = new StringTable(new string[] { "標高" }, _xyz.Count);

        for (int i = 0; i < _xyz.Count; i++)
        {
            sl.AddPoint(new GeoPoint(_xyz[i].X, _xyz[i].Y));
            st.Add(new string[] { _xyz[i].Z.ToString() });
        }

        return new Tuple<ShapeList, StringTable>(sl, st);
    }

    public void Append(string filename, bool z_flg_except = true)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("XYZ.Appendの引数filename「" + filename + "」が見つかりませんでした");

        string msg = "XYZ.Appendでエラーが発生しました。";
        using (StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("shift_jis")))
        {
            double x, y, z;
            int rowcount = 0;
            while (!sr.EndOfStream)
            {
                rowcount++;

                string stmp = sr.ReadLine();
                string[] splited = stmp.Split(_delimiter);

                if (!double.TryParse(splited[_colxindex], out x))
                    throw new FormatException(msg + "「" + filename + "」の" + rowcount.ToString() + "行目でX座標の取得ができませんでした");
                if (!double.TryParse(splited[_colyindex], out y))
                    throw new FormatException(msg + "「" + filename + "」の" + rowcount.ToString() + "行目でY座標の取得ができませんでした");
                if (!double.TryParse(splited[_colzindex], out z))
                {
                    if (z_flg_except)
                        throw new FormatException(msg + "「" + filename + "」の" + rowcount.ToString() + "行目で標高値Zの取得ができませんでした");
                    else
                        z = _nodata;
                }
                else
                {
                    if (_zchosei != 1)
                        z *= _zchosei;
                }
                _xyz.Add(new GeoPoint(x, y, z));
            }
        }
    }

    public Grid ToGrid(double span)
    {
        double[] bnd = Geo2DBase.Bound(_xyz.ToArray());
        double MaxX = bnd[0];
        double MaxY = bnd[1];
        double MinX = bnd[2];
        double MinY = bnd[3];

        return ToGrid(span, MaxX, MaxY, MinX, MinY);
    }

    public Grid ToGrid(double span, double MaxX, double MaxY, double MinX, double MinY, bool except = true)
    {
        int iMaxX = (int)((MaxX - MinX) / span);
        int iMaxY = (int)((MaxY - MinY) / span);
        Real2DEx r2d = new Real2DEx(iMaxX, iMaxY, -9999);

        for (int i = 0; i < _xyz.Count; i++)
        {
            try
            {
                GeoPoint idx = GisUtils.ToIndex(_xyz[i].X, _xyz[i].Y, MinX, MinY, span);
                r2d[Math.Min((int)idx.X, iMaxX), Math.Min((int)idx.Y, iMaxY)] = _xyz[i].Z;
            }
            catch (Exception ex)
            {
                if (!except)
                    continue;
                throw new Exception(ex.Message);
            }
        }

        return new Grid(r2d, MinX, MinY, span);
    }
}
