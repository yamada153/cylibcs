﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

//プロジェクト->参照の追加->COM->microsoft activex data object6.1 library

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

class ShapeFile
{
    /// <summary>
    /// shpファイルのオブジェクトを構造体のリストに、属性をStringTableに読み込む
    /// </summary>
    static public Tuple<ShapeList, StringTable> LoadFromShp(string filename)
    {
        ShapeList sl = ShpToStruct(filename);

        string dbffilename = Path.ChangeExtension(filename, ".dbf");
        StringTable st = DbfToStringTable(dbffilename);

        return new Tuple<ShapeList, StringTable>(sl, st);
    }

    /// <summary>
    /// ShapeListのデータをshp, shxに、StringTableのデータをdbfファイルに書き出す
    /// </summary>
    static public void SaveToShp(string filename, ShapeList sl, StringTable st, int epsg)
    {
        SaveToShp(sl, filename);
        SaveToDbfFile(st, Path.ChangeExtension(filename, ".dbf"));
        SaveToPrj(Path.ChangeExtension(filename, ".prj"), epsg);
    }

    /// <summary>
    /// ShapeListのデータをshp, shxに、StringTableのデータをdbfファイルに書き出す
    /// </summary>
    static public void SaveToShp(string filename, ShapeList sl, StringTable st)
    {
        SaveToShp(sl, filename);
        SaveToDbfFile(st, Path.ChangeExtension(filename, ".dbf"));
    }

    /// <summary>
    /// ShapeStructの内容をShpに書き出す
    /// </summary>
    static private void SaveToShp(ShapeList shplist, string filename)
    {
        int[] sizelist = null;

        /////.shpに書き込む/////
        using (FileStream fs = new FileStream(filename, FileMode.Create))
        {
            BinaryWriter bw = new BinaryWriter(fs);
            WriteShpHeader(shplist, bw);
            sizelist = WriteShpRecords(shplist, bw);
            int size = sizelist.Sum() + 100 + 8 * sizelist.Length;
            fs.Seek(24, SeekOrigin.Begin);
            bw.Write(ByteUtils.ReverseByte((int)(size / 2)));
        }
        /////.shxに書き込む/////
        byte[] b = new byte[100];
        using (FileStream fs = new FileStream(filename, FileMode.Open))
            fs.Read(b, 0, b.Length);
        string shxfile = Path.ChangeExtension(filename, ".shx");
        using (FileStream fs = new FileStream(shxfile, FileMode.Create))
        {
            fs.Write(b, 0, b.Length);
            BinaryWriter bw = new BinaryWriter(fs);
            WriteShxRecord(bw, sizelist);
            int size = 100 + 8 * sizelist.Length;
            fs.Seek(24, SeekOrigin.Begin);
            bw.Write(ByteUtils.ReverseByte((int)(size / 2)));
        }
    }

    /// <summary>
    /// StringTableの内容をDBFに書き出す
    /// </summary>
    static private void SaveToDbfFile(StringTable st, string filename)
    {
        int fieldcount = st.FieldCount();
        List<int> lenlist = new List<int>(fieldcount);
        for (int i = 0; i < fieldcount; i++)
        {
            string[] strs = st.GetStringsFromColumnNumber(i);
            int maxlen = 0;
            for (int j = 0; j < strs.Length; j++)
            {
                int tmplen = Encoding.GetEncoding("shift_jis").GetBytes(strs[j]).Length;
                maxlen = Math.Max(tmplen, maxlen);
            }
            if (maxlen > 254) maxlen = 254; //https://pro.arcgis.com/ja/pro-app/tool-reference/appendices/geoprocessing-considerations-for-shapefile-output.htm dbf文字数制限
            lenlist.Add((byte)maxlen);
        }

        //小数部の長さ
        int digit = 0;
        for (int i = 0; i < fieldcount; i++)
        {
            string[] values = st.GetStringsFromColumnNumber(i);
            string coltype = StrUtils.TypeFromString(values);
            if (coltype == "FLOAT")
            {
                for (int j = 0; j < values.Length; j++)
                {
                    if (!values[j].Contains(".")) continue;
                    string[] tmp = values[j].Split('.');
                    digit = Math.Max(digit, tmp[1].Length);
                }
            }
        }

        using (FileStream fs = new FileStream(filename, FileMode.Create))
        {
            BinaryWriter bw = new BinaryWriter(fs);

            bw.Write((byte)3); //version
            bw.Write((byte)(DateTime.Now.Year % 100));
            bw.Write((byte)(DateTime.Now.Month % 100));
            bw.Write((byte)(DateTime.Now.Day % 100));
            bw.Write(st.Count()); //recordcount
            bw.Write((Int16)(st.FieldCount() * 32 + 32 + 1)); //headerlen
            bw.Write((Int16)(lenlist.Sum() + 1)); //recordlen
            bw.Write(new byte[2]);
            bw.Write((byte)0); //transaction
            bw.Write((byte)0); //encode
            bw.Write(new byte[12]);
            bw.Write((byte)0);
            bw.Write((byte)0); //lang driver id
            bw.Write(new byte[2]);

            /////フィールド設定/////
            string[] fields = st.GetFieldNames();
            for (int i = 0; i < fieldcount; i++)
            {
                int namelen = 10;
                byte[] strarray = Encoding.GetEncoding("shift_jis").GetBytes(fields[i]);
                namelen = Math.Min(namelen, strarray.Length);

                byte[] fieldname = new byte[11];
                for (int j = 0; j < namelen; j++)
                    fieldname[j] = (byte)strarray[j];
                bw.Write(fieldname); //フィールド名

                //フィールド型
                string coltype = StrUtils.TypeFromString(st.GetStringsFromColumnNumber(i));
                if (coltype == "INTEGER")
                    bw.Write((byte)'N');
                else if (coltype == "FLOAT")
                    bw.Write((byte)'F');
                else
                    bw.Write((byte)'C');

                bw.Write(new byte[4]);
                bw.Write((byte)lenlist[i]); //フィールド長                
                bw.Write((byte)digit); //小数部の長さ
                bw.Write(new byte[2]);
                bw.Write((byte)0); //作業領域ID
                bw.Write(new byte[10]);
                bw.Write((byte)0); //MDX
            }
            bw.Write((byte)0x0D);
            /////本文/////
            for (int i = 0; i < st.Count(); i++)
            {
                bw.Write((byte)0x20);
                for (int j = 0; j < fieldcount; j++)
                {
                    byte[] strarray = Encoding.GetEncoding("shift_jis").GetBytes(st[i, fields[j]]);
                    byte[] value = Enumerable.Repeat<byte>(0x20, lenlist[j]).ToArray();
                    string tmp = st[i, fields[j]];
                    for (int k = 0; k < strarray.Length; k++)
                    {
                        if (k >= lenlist[j]) break;
                        value[k] = (byte)strarray[k];
                    }
                    bw.Write(value);
                }
            }
            bw.Write((byte)0x1A);
        }
    }

    /// <summary>
    /// DBFファイルをStringTableに格納する
    /// </summary>
    static private StringTable DbfToStringTable(string filename)
    {
        using (FileStream fs = new FileStream(filename, FileMode.Open))
        {
            BinaryReader br = new BinaryReader(fs);
            ////////DBFのヘッダ部分を読み込む/////////
            byte version = br.ReadByte();
            byte yy = br.ReadByte();
            byte mm = br.ReadByte();
            byte dd = br.ReadByte();
            int recordcount = br.ReadInt32();
            int headerlen = br.ReadInt16();
            int recordlen = br.ReadInt16();
            br.ReadBytes(2);
            byte transaction = br.ReadByte();
            byte encode = br.ReadByte();
            br.ReadBytes(12);
            byte mdx = br.ReadByte();
            byte langdriverid = br.ReadByte();
            br.ReadBytes(2);
            /////属性名読み込み/////
            List<string> fieldslist = new List<string>();
            List<byte> typelist = new List<byte>();
            List<int> lenlist = new List<int>();
            try
            {
                for (int i = 0; i < 256; i++)
                {
                    byte[] fieldname = br.ReadBytes(11);
                    byte type = br.ReadByte();
                    br.ReadBytes(4);
                    byte len = br.ReadByte();
                    byte decimalrenamed = br.ReadByte();
                    br.ReadBytes(2);
                    byte workid = br.ReadByte();
                    br.ReadBytes(10);
                    byte mdx_field = br.ReadByte();

                    fieldslist.Add(DeleteCode(Encoding.GetEncoding(932).GetString(fieldname)));
                    typelist.Add(type);
                    lenlist.Add(len);

                    if (br.ReadByte() == 0x0D)
                        break;
                    else
                        fs.Seek(-1, SeekOrigin.Current);
                }
            }
            catch
            {
                throw new Exception("「" + filename + "」のフォーマットが正しくない可能性があります。フィールドの終わりを示す符号が見つかりませんでした");
            }
            /////値取得/////
            StringTable st = new StringTable(fieldslist.ToArray());
            for (int i = 0; i < recordcount; i++)
            {
                string[] tmprecord = new string[fieldslist.Count];
                byte delflg = br.ReadByte();
                for (int j = 0; j < fieldslist.Count; j++)
                {
                    string tmpstr = DeleteCode(Encoding.GetEncoding(932).GetString(br.ReadBytes(lenlist[j]))).Trim();

                    switch (typelist[j])
                    {
                        case 70: //"F" float
                            double dtmp;
                            if (double.TryParse(tmpstr, out dtmp))
                                tmpstr = dtmp.ToString();
                            break;
                        case 78: //"N" int
                            int itmp;
                            if (int.TryParse(tmpstr, out itmp))
                                tmpstr = itmp.ToString();
                            break;
                        default: //"C"他 char
                            break;
                    }
                    tmprecord[j] = tmpstr;
                }
                if (delflg == 0x2a) continue;
                st.Add(tmprecord);
            }
            return st;
        }
    }

    /// <summary>
    /// Shpファイルのオブジェクト情報をShapeStructに格納する
    /// </summary>
    static private ShapeList ShpToStruct(string filename)
    {
        List<int> offset = new List<int>(100); //適当
        List<int> length = new List<int>(100); //適当
        ShapeList shplist = new ShapeList();
        /////Shxファイル読み込み/////
        string shxfilename = Path.ChangeExtension(filename, ".shx");
        byte[] tmp = new byte[4];
        using (FileStream fs = new FileStream(shxfilename, FileMode.Open))
        {
            //ファイルヘッダー部分を読み飛ばすため
            fs.Seek(100, SeekOrigin.Begin);

            while (true)
            {
                if (fs.Read(tmp, 0, 4) == 0) break;
                offset.Add(BitConverter.ToInt32(tmp.Reverse().ToArray(), 0) * 2); //ワード単位のため*2している
                if (fs.Read(tmp, 0, 4) == 0) break;
                length.Add(BitConverter.ToInt32(tmp.Reverse().ToArray(), 0) * 2);
            }
        }
        /////Shp読み込み/////
        using (FileStream fs = new FileStream(filename, FileMode.Open))
        {
            if (offset.Count == 0) return null;
            BinaryReader br = new BinaryReader(fs);
            int count = 0;
            foreach (int index in offset)
            {
                fs.Seek(index, SeekOrigin.Begin);
                int recordnum = ByteUtils.ReverseByte(br.ReadInt32());
                int contentslen = ByteUtils.ReverseByte(br.ReadInt32());
                //オブジェクトタイプを読んで、それぞれの処理に分岐する
                int object_type = br.ReadInt32();
                switch (object_type)
                {
                    case 0: //Null Shape
                        shplist.AddNull();
                        break;
                    case 1: //Point
                        shplist.AddPoint(ShpPointToPoint(br));
                        break;
                    case 3: //PolyLine
                        shplist.AddPolyLine(ShpPolyLineToPoints(br));
                        break;
                    case 5: //Polygon
                        shplist.AddPolygon(ShpPolyLineToPoints(br));
                        break;
                    case 8: //MultiPoint 未テスト
                        shplist.AddMultiPoint(ShpMultiPointToPoints(br));
                        break;
                    default: throw new ApplicationException("未対応のオブジェクト「" + object_type.ToString() + "」が渡されました");
                }
                count++;
            }
        }
        return shplist;
    }

    static private GeoPoint ShpPointToPoint(BinaryReader br)
    {
        GeoPoint pnt = new GeoPoint();
        pnt.X = MathEx.RoundOff(br.ReadDouble(), 10);
        pnt.Y = MathEx.RoundOff(br.ReadDouble(), 10);

        return pnt;
    }

    static private GeoPoint[] ShpMultiPointToPoints(BinaryReader br)
    {
        //バウンドボックス部分は除く
        for (int i = 0; i < 4; i++)
            MathEx.RoundOff(br.ReadDouble(), 10);

        int NumPoints = br.ReadInt32();
        GeoPoint[] pnts = new GeoPoint[NumPoints];
        for (int i = 0; i < NumPoints; i++)
        {
            pnts[i].X = MathEx.RoundOff(br.ReadDouble(), 10);
            pnts[i].Y = MathEx.RoundOff(br.ReadDouble(), 10);
        }
        return pnts;
    }

    static private GeoPoint[][] ShpPolyLineToPoints(BinaryReader br)
    {
        //バウンドボックス部分は除く
        for (int i = 0; i < 4; i++)
            MathEx.RoundOff(br.ReadDouble(), 10);

        int NumParts = br.ReadInt32();
        int NumPoints = br.ReadInt32();
        int[] Parts = new int[NumParts + 1];
        for (int i = 0; i < NumParts; i++)
            Parts[i] = br.ReadInt32();
        Parts[NumParts] = NumPoints;

        GeoPoint[][] result = new GeoPoint[NumParts][];
        for (int i = 0; i < NumParts; i++)
        {
            int index_len = Parts[i + 1] - Parts[i];
            result[i] = new GeoPoint[index_len];
            for (int j = 0; j < index_len; j++)
            {
                result[i][j].X = MathEx.RoundOff(br.ReadDouble(), 10);
                result[i][j].Y = MathEx.RoundOff(br.ReadDouble(), 10);
            }
        }
        return result;
    }

    static private void WriteShpHeader(ShapeList shplist, BinaryWriter bw)
    {
        bw.Write(ByteUtils.ReverseByte(9994));
        for (int i = 0; i < 5; i++)
            bw.Write(0);
        bw.Write(0); //ファイル長は後で入れる
        bw.Write(1000);
        ShapeStruct.ObjType ot = shplist[0].Type;
        switch (ot)
        {
            case ShapeStruct.ObjType.Null:
                bw.Write(0);
                break;
            case ShapeStruct.ObjType.Point:
                bw.Write(1);
                break;
            case ShapeStruct.ObjType.MultiPoint:
                bw.Write(8);
                break;
            case ShapeStruct.ObjType.PolyLine:
                bw.Write(3);
                break;
            case ShapeStruct.ObjType.Polygon:
                bw.Write(5);
                break;
            default: throw new NotSupportedException("ShapeFile.WriteShpHeaderで問題が発生しました。");
        }
        WriteBound(bw, shplist.GetBox());
        bw.Write(0.0);
        bw.Write(0.0);
        bw.Write(0.0);
        bw.Write(0.0);
    }

    static private int[] WriteShpRecords(ShapeList shplist, BinaryWriter bw)
    {
        GeoPoint pnt;
        GeoPoint[] pnts;
        GeoPoint[][] pol;
        List<int> sizelist = new List<int>(100); //適当

        for (int i = 0; i < shplist.Count(); i++)
        {
            int size = 0;
            bw.Write(ByteUtils.ReverseByte(i + 1)); //レコード番号
            ShapeStruct.ObjType ot = shplist[i].Type;
            switch (ot)
            {
                case ShapeStruct.ObjType.Null:
                    WriteEx(bw, 2); //コンテンツ長
                    //レコードコンテンツ
                    size += WriteEx(bw, 0);
                    break;
                case ShapeStruct.ObjType.Point:
                    pnt = shplist[i].Points[0];
                    bw.Write(ByteUtils.ReverseByte((int)((4 + 16) / 2))); //コンテンツ長
                    //レコードコンテンツ
                    size += WriteEx(bw, 1);
                    size += WriteEx(bw, pnt.X);
                    size += WriteEx(bw, pnt.Y);
                    break;
                case ShapeStruct.ObjType.MultiPoint:
                    pnts = shplist[i].Points;
                    bw.Write(ByteUtils.ReverseByte((int)((40 + 16 * shplist[i].NumPoints) / 2))); //コンテンツ長
                    //レコードコンテンツ
                    size += WriteEx(bw, 8);
                    size += WriteBound(bw, shplist[i].Box);
                    size += WriteEx(bw, shplist[i].NumPoints);
                    size += WritePoints(bw, pnts);
                    break;
                case ShapeStruct.ObjType.PolyLine:
                case ShapeStruct.ObjType.Polygon:
                    pol = shplist.PolyLines(i);
                    bw.Write(ByteUtils.ReverseByte((int)((48 + 16 * shplist[i].NumPoints) / 2))); //コンテンツ長
                    //レコードコンテンツ
                    size += WriteEx(bw, (int)shplist[i].Type);
                    size += WriteBound(bw, shplist[i].Box);
                    size += WriteEx(bw, shplist[i].NumParts);
                    size += WriteEx(bw, shplist[i].Points.Length);
                    size += WriteEx(bw, 0);
                    int tmp = 0;
                    for (int j = 0; j < pol.Length - 1; j++)  //最後の値は書き込まないから-1している
                    {
                        tmp += pol[j].Length;
                        size += WriteEx(bw, tmp);
                    }
                    size += WritePoints(bw, shplist[i].Points);
                    break;
                default: throw new NotSupportedException("「" + shplist[i].Type.ToString() + "」はサポートされないオブジェクトです");
            }
            sizelist.Add(size);
        }
        return sizelist.ToArray();
    }

    static private int WriteBound(BinaryWriter bw, double[] bnd)
    {
        int tmp = 0;
        tmp += WriteEx(bw, bnd[2]);
        tmp += WriteEx(bw, bnd[3]);
        tmp += WriteEx(bw, bnd[0]);
        tmp += WriteEx(bw, bnd[1]);
        return tmp;
    }

    static private int WritePoints(BinaryWriter bw, GeoPoint[] pnts)
    {
        int tmp = 0;
        for (int i = 0; i < pnts.Length; i++)
        {
            tmp += WriteEx(bw, pnts[i].X);
            tmp += WriteEx(bw, pnts[i].Y);
        }
        return tmp;
    }

    static private int WriteEx(BinaryWriter bw, int i)
    {
        bw.Write(i);
        return 4;
    }

    static private int WriteEx(BinaryWriter bw, double i)
    {
        bw.Write(i);
        return 8;
    }

    static private void WriteShxRecord(BinaryWriter bw, int[] sizelist)
    {
        if (sizelist.Length <= 0) return;
        int[] offsetlist = new int[sizelist.Length];

        offsetlist[0] = 100;
        for (int i = 1; i < sizelist.Length; i++)
            offsetlist[i] = sizelist[i - 1] + offsetlist[i - 1] + 8;//「8」はレコード番号とコンテンツ長の分

        for (int i = 0; i < sizelist.Length; i++)
        {
            bw.Write(ByteUtils.ReverseByte((int)(offsetlist[i] / 2)));
            bw.Write(ByteUtils.ReverseByte((int)(sizelist[i] / 2)));
        }
    }

    /// <summary>
    /// 文字列中の制御コードを削除する
    /// </summary>
    public static string DeleteCode(string str)
    {
        string tmp = str;
        for (int i = 1; i < 9; i++)
            tmp = tmp.Replace((char)i, '\0');
        for (int i = 11; i < 13; i++)
            tmp = tmp.Replace((char)i, '\0');
        for (int i = 14; i < 32; i++)
            tmp = tmp.Replace((char)i, '\0');
        tmp = tmp.Replace((char)127, '\0');

        return tmp.Replace("\0", "");
    }

    /// <summary>
    /// Projectionファイルを保存する
    /// </summary>
    static private void SaveToPrj(string filename, int epsg)
    {
        string coordstr = CoordSysString.EpsgToShpCoordsysString(epsg);
        FileFunc.SaveToFile(new string[] { coordstr }, filename);
    }
}
