﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;

class Lem
{
    private string _sokuryonen = "";
    private string _shuseinen = "";
    private string _tensu_tozai = "";
    private string _tensu_nanboku = "";
    private string _span_tozai = "";
    private string _span_nanboku = "";
    private string _left_under_ido = "";
    private string _left_under_keido = "";
    private string _right_under_ido = "";
    private string _right_under_keido = "";
    private string _left_top_ido = "";
    private string _left_top_keido = "";
    private string _right_top_ido = "";
    private string _right_top_keido = "";
    private string _zumei = "";
    private string _recordsu = "";
    private string _zahyobango = "";
    private string _zukaku_leftunder_x = "";
    private string _zukaku_leftunder_y = "";
    private string _zukaku_righttop_x = "";
    private string _zukaku_righttop_y = "";
    private string _comment = "";
    private int _xnum;
    private int _ynum;
    public int Coord { get; }
    public MapFrame MapFrame { get; }
    public Grid Grid { get; }
    public string FileName { get; }

    /// <summary>
    /// 拡張子.lemと.csvを読み込む
    /// </summary> 長い...
    public Lem(string lemfilename, string csvfilename)
    {
        //////////////////CSVファイル読み込み///////////////////////////
        string[] csvcontents = FileFunc.LoadFromFile(csvfilename);
        bool sokuryonen = false;
        bool shuseinen = false;
        bool tensu_tozai = false;
        bool tensu_nanboku = false;
        bool span_tozai = false;
        bool span_nanboku = false;
        bool left_under_ido = false;
        bool left_under_keido = false;
        bool right_under_ido = false;
        bool right_under_keido = false;
        bool left_top_ido = false;
        bool left_top_keido = false;
        bool right_top_ido = false;
        bool right_top_keido = false;
        bool zumei = false;
        bool recordsu = false;
        bool zahyobango = false;
        bool zukaku_leftunder_x = false;
        bool zukaku_leftunder_y = false;
        bool zukaku_righttop_x = false;
        bool zukaku_righttop_y = false;
        bool comment = false;
        for (int i = 0; i < csvcontents.Length; i++)
        {
            if (csvcontents[i].Contains("測量年"))
            {
                _sokuryonen = csvcontents[i].Split(',')[1];
                sokuryonen = true;
            }
            if (csvcontents[i].Contains("修正年"))
            {
                _shuseinen = csvcontents[i].Split(',')[1];
                shuseinen = true;
            }
            if (csvcontents[i].Contains("東西方向の点数"))
            {
                string[] splited = csvcontents[i].Split(',');
                if ((splited.Length >= 2) && (int.TryParse(splited[1], out _xnum)))
                {
                    _tensu_tozai = csvcontents[i].Split(',')[1];
                    tensu_tozai = true;
                }
            }
            if (csvcontents[i].Contains("南北方向の点数"))
            {
                string[] splited = csvcontents[i].Split(',');
                if ((splited.Length >= 2) && (int.TryParse(splited[1], out _ynum)))
                {
                    _tensu_nanboku = csvcontents[i].Split(',')[1];
                    tensu_nanboku = true;
                }
            }
            if (csvcontents[i].Contains("東西方向のデータ間隔"))
            {
                _span_tozai = csvcontents[i].Split(',')[1];
                span_tozai = true;
            }
            if (csvcontents[i].Contains("南北方向のデータ間隔"))
            {
                _span_nanboku = csvcontents[i].Split(',')[1];
                span_nanboku = true;
            }
            if (csvcontents[i].Contains("区画左下の緯度"))
            {
                _left_under_ido = csvcontents[i].Split(',')[1];
                left_under_ido = true;
            }
            if (csvcontents[i].Contains("区画左下の経度"))
            {
                _left_under_keido = csvcontents[i].Split(',')[1];
                left_under_keido = true;
            }
            if (csvcontents[i].Contains("区画右下の緯度"))
            {
                _right_under_ido = csvcontents[i].Split(',')[1];
                right_under_ido = true;
            }
            if (csvcontents[i].Contains("区画右下の経度"))
            {
                _right_under_keido = csvcontents[i].Split(',')[1];
                right_under_keido = true;
            }
            if (csvcontents[i].Contains("区画右上の緯度"))
            {
                _left_top_ido = csvcontents[i].Split(',')[1];
                left_top_ido = true;
            }
            if (csvcontents[i].Contains("区画右上の経度"))
            {
                _left_top_keido = csvcontents[i].Split(',')[1];
                left_top_keido = true;
            }
            if (csvcontents[i].Contains("区画左上の緯度"))
            {
                _right_under_ido = csvcontents[i].Split(',')[1];
                right_top_ido = true;
            }
            if (csvcontents[i].Contains("区画左上の経度"))
            {
                _right_top_keido = csvcontents[i].Split(',')[1];
                right_top_keido = true;
            }
            if (csvcontents[i].Contains("図名"))
            {
                string[] splited = csvcontents[i].Split(',');
                if (splited.Length >= 2)
                {
                    if (splited[1].Length == 4)
                    {
                        this.MapFrame = new BaseLV50000();
                    }
                    else if (splited[1].Length == 6)
                    {
                        this.MapFrame = new BaseLV5000();
                    }
                    else if (splited[1].Length == 7)
                    {
                        this.MapFrame = new BaseLV2500();
                    }
                    _zumei = splited[1];
                    zumei = true;
                }
            }
            if (csvcontents[i].Contains("記録レコード数"))
            {
                _recordsu = csvcontents[i].Split(',')[1];
                recordsu = true;
            }
            if (csvcontents[i].Contains("平面直角座標系番号"))
            {
                string[] splited = csvcontents[i].Split(',');
                int itmp;
                if ((splited.Length >= 2) && (int.TryParse(splited[1], out itmp)))
                {
                    Coord = itmp;
                    zahyobango = true;
                    _zahyobango = csvcontents[i].Split(',')[1];
                }
            }
            if (csvcontents[i].Contains("区画左下X座標"))
            {
                _zukaku_leftunder_x = csvcontents[i].Split(',')[1];
                zukaku_leftunder_x = true;
            }
            if (csvcontents[i].Contains("区画左下Y座標"))
            {
                _zukaku_leftunder_y = csvcontents[i].Split(',')[1];
                zukaku_leftunder_y = true;
            }
            if (csvcontents[i].Contains("区画右上X座標"))
            {
                _zukaku_righttop_x = csvcontents[i].Split(',')[1];
                zukaku_righttop_x = true;
            }
            if (csvcontents[i].Contains("区画右上Y座標"))
            {
                _zukaku_righttop_y = csvcontents[i].Split(',')[1];
                zukaku_righttop_y = true;
            }
            if (csvcontents[i].Contains("コメント"))
            {
                _comment = csvcontents[i].Split(',')[1];
                comment = true;
            }
        }
        if (!sokuryonen) { throw new Exception("Csvファイル「" + csvfilename + "」に測量年がありません。"); }
        if (!shuseinen) { throw new Exception("Csvファイル「" + csvfilename + "」に修正年がありません。"); }
        if (!tensu_tozai) { throw new Exception("Csvファイル「" + csvfilename + "」に東西方向の点数がありません。"); }
        if (!tensu_nanboku) { throw new Exception("Csvファイル「" + csvfilename + "」に南北方向の点数がありません。"); }
        if (!span_tozai) { throw new Exception("Csvファイル「" + csvfilename + "」に東西方向のデータ間隔がありません。"); }
        if (!span_nanboku) { throw new Exception("Csvファイル「" + csvfilename + "」に南北方向のデータ間隔がありません。"); }
        if (!left_under_ido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画左下の緯度がありません。"); }
        if (!left_under_keido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画左下の経度がありません。"); }
        if (!right_under_ido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画右下の緯度がありません。"); }
        if (!right_under_keido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画右下の経度がありません。"); }
        if (!left_top_ido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画右上の緯度がありません。"); }
        if (!left_top_keido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画右上の経度がありません。"); }
        if (!right_top_ido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画左上の緯度がありません。"); }
        if (!right_top_keido) { throw new Exception("Csvファイル「" + csvfilename + "」に区画左上の経度がありません。"); }
        if (!zumei) { throw new Exception("Csvファイル「" + csvfilename + "」に図名がありません。"); }
        if (!recordsu) { throw new Exception("Csvファイル「" + csvfilename + "」に記録レコード数がありません。"); }
        if (!zahyobango) { throw new Exception("Csvファイル「" + csvfilename + "」に平面直角座標系番号がありません。"); }
        if (!zukaku_leftunder_x) { throw new Exception("Csvファイル「" + csvfilename + "」に区画左下X座標がありません。"); }
        if (!zukaku_leftunder_y) { throw new Exception("Csvファイル「" + csvfilename + "」に区画左下Y座標がありません。"); }
        if (!zukaku_righttop_x) { throw new Exception("Csvファイル「" + csvfilename + "」に区画右上X座標がありません。"); }
        if (!zukaku_righttop_y) { throw new Exception("Csvファイル「" + csvfilename + "」に区画右上Y座標がありません。"); }
        if (!comment) { throw new Exception("Csvファイル「" + csvfilename + "」にコメントがありません。"); }
        csvcontents = null;
        //////////////////LEMファイル読み込み///////////////////////////
        string[] lemcontents = FileFunc.LoadFromFile(lemfilename);
        if (_ynum != lemcontents.Length)
            throw new Exception("Csvファイル「" + csvfilename + "」の南北方向の点数とLemファイル「" + lemfilename + "」のレコード数が一致しません。");
        Real2DEx r2d = new Real2DEx(_xnum, _ynum, -9999);

        for (int i = 0; i < lemcontents.Length; i++)
        {
            string[] tmps = StrUtils.Split(lemcontents[i], 5);
            if (tmps.Length != (_xnum + 2)) // +2 は行番号の分（最初の10文字）
                throw new Exception("Csvファイル「" + csvfilename + "」の東西方向の点数とLemファイル「" + lemfilename + "」のレコード数が一致しません。");
            for (int j = 2; j < tmps.Length; j++)
            {
                double tmp = double.Parse(tmps[j]);
                if (tmp == -9999) continue;
                if (tmp == -8888) continue;
                if (tmp == -1111) continue;  
                r2d[j - 2, i] = tmp;
            }
        }

        r2d.MirrorTopUnder();
        GeoPoint sw = this.MapFrame.SouthWest(_zumei);
        double gridspan = this.MapFrame.UnitY / _xnum;
        this.Grid = new Grid(r2d, sw.Y + gridspan / 2, sw.X + gridspan / 2, gridspan);
        this.FileName = lemfilename;
    }

    /// <summary>
    /// Lem形式で保存する
    /// </summary>
    public void SaveToFile(string filename)
    {
        string csvfile = Path.ChangeExtension(filename, ".csv");
        string lemfile = filename;

        //csvファイルに書き込み
        using (StreamWriter sw = new StreamWriter(csvfile, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            sw.WriteLine("測量年," + _sokuryonen);
            sw.WriteLine("修正年," + _shuseinen);
            sw.WriteLine("東西方向の点数," + _tensu_tozai);
            sw.WriteLine("南北方向の点数," + _tensu_nanboku);
            sw.WriteLine("東西方向のデータ間隔," + _span_tozai);
            sw.WriteLine("南北方向のデータ間隔," + _span_nanboku);
            sw.WriteLine("区画左下の緯度," + _left_under_ido);
            sw.WriteLine("区画左下の経度," + _left_under_keido);
            sw.WriteLine("区画右下の緯度," + _right_under_ido);
            sw.WriteLine("区画右下の経度," + _right_under_keido);
            sw.WriteLine("区画右上の緯度," + _left_top_ido);
            sw.WriteLine("区画右上の経度," + _left_top_keido);
            sw.WriteLine("区画左上の緯度," + _right_top_ido);
            sw.WriteLine("区画左上の経度," + _right_top_keido);
            sw.WriteLine("図名," + _zumei);
            sw.WriteLine("記録レコード数," + _recordsu);
            sw.WriteLine("平面直角座標系番号," + _zahyobango);
            sw.WriteLine("区画左下X座標," + _zukaku_leftunder_x);
            sw.WriteLine("区画左下Y座標," + _zukaku_leftunder_y);
            sw.WriteLine("区画右上X座標," + _zukaku_righttop_x);
            sw.WriteLine("区画右上Y座標," + _zukaku_righttop_y);
            sw.WriteLine("コメント," + _comment);

            int recordnum = 0;
            if (int.TryParse(_recordsu, out recordnum))
            {
                for (int i = 0; i < recordnum; i++)
                {
                    sw.WriteLine("レコード" + (i + 1).ToString() + "のフラグ,1");
                }
            }
        }
        //lemファイルに書き込み
        using (StreamWriter sw = new StreamWriter(lemfile, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            Real2DEx data = Grid.GetData();
            for (int i = 0; i < data.Height; i++)
            {
                sw.Write((i + 1).ToString().PadLeft(10, ' '));
                for (int j = 0; j < data.Width; j++)
                    sw.Write(data[j, data.Height - i - 1].ToString().PadLeft(5, ' '));
                sw.Write("\n");
            }
        }
    }

    /// <summary>
    /// 自身も含め周囲の9図郭のファイル名を取得する。
    /// <para>配列0番目...左</para>
    /// <para>配列1番目...左上</para>
    /// <para>配列2番目...上</para>
    /// <para>配列3番目...右上</para>
    /// <para>配列4番目...右</para>
    /// <para>配列5番目...右下</para>
    /// <para>配列6番目...下</para>
    /// <para>配列7番目...左下</para>
    /// <para>配列8番目...中心</para>
    /// </summary>    
    public string[] Neighborhood()
    {
        string[] splited = Path.GetFileName(FileName).Split('_');

        string[] neighborhood_codes = MapFrame.Neighborhood(splited[0]);
        for (int i = 0; i < neighborhood_codes.Length; i++)
            neighborhood_codes[i] += "_" + splited[1];

        return neighborhood_codes;
    }
}
