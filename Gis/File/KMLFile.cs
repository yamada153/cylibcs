/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Linq;
using System.IO;

static class KMLFile
{
    /// <summary>
    /// KMLを出力する。
    /// </summary>
    static public void SaveToKML(string filename, ShapeList sl, StringTable st)
    {
        string name = Path.GetFileNameWithoutExtension(filename);
        string[] fields = st.GetFieldNames();

        using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            sw.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?><kml><Document id=\"root_doc\">");
            sw.Write("<Folder>");
            sw.Write("<name>" + name + "</name>");
            for (int i = 0; i < st.Count(); i++)
            {
                //色設定
                string color = "ba00ffff";
                if (st.FieldExist("色"))
                    color = st[i, "色"];

                sw.Write("<Placemark>");
                /////スタイル/////
                sw.Write("<Style>");
                switch (sl[i].Type)
                {
                    case ShapeStruct.ObjType.Point:
                    case ShapeStruct.ObjType.MultiPoint:
                        sw.Write("<IconStyle>");
                        sw.Write("<color>" + color + "</color>");
                        sw.Write("<scale>1.1</scale>");
                        sw.Write("<Icon><href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href></Icon>");
                        sw.Write("<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>");
                        sw.Write("</IconStyle>");
                        break;
                    case ShapeStruct.ObjType.PolyLine:
                        sw.Write("<LineStyle><color>" + color + "</color></LineStyle>");
                        sw.Write("<PolyStyle><fill>0</fill><color>" + color + "</color></PolyStyle>");
                        break;
                    case ShapeStruct.ObjType.Polygon:
                        sw.Write("<LineStyle><color>" + color + "</color></LineStyle>");
                        sw.Write("<PolyStyle><fill>1</fill><color>" + color + "</color></PolyStyle>");
                        break;
                    default: break;
                }
                sw.Write("</Style>\n");
                //////属性/////
                sw.Write("<ExtendedData><SchemaData schemaUrl=\"#" + name + "\">");
                for (int j = 0; j < fields.Count(); j++)
                {
                    string tmp = st[i, fields[j]];
                    tmp = tmp.Replace("<", "&lt;").Replace(">", "&gt;");
                    sw.WriteLine("<SimpleData name=\"" + fields[j] + "\"> " + tmp + "</SimpleData>");
                }
                sw.Write("</SchemaData></ExtendedData>");
                //////オブジェクト/////
                switch (sl[i].Type)
                {
                    case ShapeStruct.ObjType.Point:
                        PointToKML(sw, i, sl);
                        break;
                    case ShapeStruct.ObjType.MultiPoint:
                        MultiPointToKML(sw, i, sl);
                        break;
                    case ShapeStruct.ObjType.PolyLine:
                        PolyLineToKML(sw, i, sl);
                        break;
                    case ShapeStruct.ObjType.Polygon:
                        PolygonToKML(sw, i, sl);
                        break;
                    default: break;
                }
                sw.Write("</Placemark>");
            }
            sw.Write("</Folder></Document></kml>");
        }
    }

    static private string GetGeoString(GeoPoint[] pnts)
    {
        string tmp = "";
        for (int i = 0; i < pnts.Length; i++)
            tmp += pnts[i].X.ToString() + "," + pnts[i].Y.ToString() + " ";
        return StrUtils.TrimEnd(tmp, ' ');
    }

    static private void PointToKML(StreamWriter sw, int index, ShapeList sl)
    {
        sw.Write("<Point><coordinates>" + GetGeoString(sl[index].Points) + "</coordinates></Point>");
    }

    static private void MultiPointToKML(StreamWriter sw, int index, ShapeList sl)
    {
        sw.Write("<MultiGeometry>");
        for (int i = 0; i < sl[index].Points.Length; i++)
            sw.Write("<Point><coordinates>" + GetGeoString(new GeoPoint[] { sl[index].Points[i]}) + "</coordinates></Point>");
        sw.Write("</MultiGeometry>");
    }

    static private void PolyLineToKML(StreamWriter sw, int index, ShapeList sl)
    {
        GeoPoint[][] pntss = sl[index].PolyLines();

        if (sl[index].NumParts != 1) sw.Write("<MultiGeometry>");
        for (int i = 0; i < pntss.Length; i++)
            sw.Write("<LineString><coordinates>" + GetGeoString(pntss[i]) + "</coordinates></LineString>");
        if (sl[index].NumParts != 1) sw.Write("</MultiGeometry>");
    }

    static private void PolygonToKML(StreamWriter sw, int index, ShapeList sl)
    {
        GeoPoint[][][] mpol = Geo2DExtra.PolygonToMultiPolygon(sl[index].PolyLines());

        if (mpol.Length != 1) sw.Write("<MultiGeometry>");
        for(int i = 0; i < mpol.Length; i++)
        {
            sw.Write("<Polygon><altitudeMode>clampToGround</altitudeMode>");
            for (int j = 0; j < mpol[i].Length; j++)
                if (j == 0)
                    sw.Write("<outerBoundaryIs><LinearRing><coordinates>" + GetGeoString(mpol[i][j]) + "</coordinates></LinearRing></outerBoundaryIs>");
                else
                    sw.Write("<innerBoundaryIs><LinearRing><coordinates>" + GetGeoString(mpol[i][j]) + "</coordinates></LinearRing></innerBoundaryIs>");
            sw.WriteLine("</Polygon>");
        }
        if (mpol.Length != 1) sw.Write("</MultiGeometry>");
    }
}