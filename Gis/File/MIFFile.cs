﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

static class MIFFile
{
    static private bool IsMatch(string str, string matchstr, int num)
    {
        if (str.Length < num) return false;

        for(int i = 0; i < num; i++)
            if (str[i] != matchstr[i])
                return false;
        return true;
    }

    /// <summary>
    /// MifファイルをWKTに変換する。
    /// flgがtrueならば、mifのオブジェクト数とmidの列数が異なる場合に例外を発生させる
    /// </summary>
    static public Tuple<ShapeList, StringTable> LoadFromMif(string filename, bool flg = true)
    {
        ShapeList objlist = new ShapeList();
        string[] fields = null;

        /////MIF読み込み/////
        using (StreamReader sr = new StreamReader(filename, System.Text.Encoding.GetEncoding("shift_jis")))
            while (!sr.EndOfStream)
            {
                string line = StrUtils.DuplicateToOne(sr.ReadLine().Trim().ToUpper(), " ");

                if (IsMatch(line, "COLUMNS", 7))
                    fields = GetField(sr, int.Parse(line.Split(' ')[1]));
                else if (IsMatch(line, "POINT", 5))
                    objlist.AddPoint(MifPointToPoint(line));
                else if (IsMatch(line, "MULTIPOINT", 10))
                    objlist.AddMultiPoint(MifPlineToArray(sr, int.Parse(line.Split(' ')[1])));
                else if (IsMatch(line, "PLINE MULTIPLE", 14))
                    objlist.AddPolyLine(MifPlineMultipleToArray(sr, int.Parse(line.Split(' ')[2])));
                else if (IsMatch(line, "PLINE", 5))
                    objlist.AddPolyLine(new GeoPoint[][] { MifPlineToArray(sr, int.Parse(line.Split(' ')[1])) });
                else if (IsMatch(line, "LINE", 4))
                    objlist.AddPolyLine(new GeoPoint[][] { MifLineToArray(line) });
                else if (IsMatch(line, "REGION", 6))
                    objlist.AddPolygon(MifRegionToArray(sr, int.Parse(line.Split(' ')[1])));
                else if (IsMatch(line, "NONE", 4))
                    objlist.AddNull();
            }
        /////MID/////
        string midfilename = Path.ChangeExtension(filename, ".mid");
        StringTable st = SeparateText.LoadFromFile(midfilename, ',', fields);

        return new Tuple<ShapeList, StringTable>(objlist, st);
    }

    /// <SUMMARY>
    /// MIF形式で出力する。対応しているオブジェクトは
    /// <PARA>ポイント</PARA>
    /// <PARA>マルチポイント</PARA>
    /// <PARA>ライン</PARA>
    /// <PARA>マルチライン</PARA>
    /// <PARA>ポリゴン</PARA>
    /// <PARA>マルチポリゴン</PARA>
    /// </SUMMARY>
    static public void SaveToMIF(string filename, ShapeList sl, StringTable st, int epsg)
    {
        string miffile = filename;
        using (StreamWriter mif = new StreamWriter(miffile, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            foreach (string s in MakeMifHeader(st, epsg)) mif.WriteLine(s);

            mif.WriteLine("Data");

            for (int i = 0; i < sl.Count(); i++)
            {
                string[] data_strings;
                try { data_strings = MakeMifData(sl[i]); }
                catch (Exception ex)
                {
                    throw new FormatException("MifFile.SaveToMIF" + ex.Message + "\n" + (i + 1).ToString() + "行目の変換中にエラーが発生しました。");
                }

                foreach (string s in data_strings) mif.WriteLine(s);
            }
        }

        WriteMID(st, Path.ChangeExtension(miffile, ".mid"));
    }

    /// <summary>
    /// MIFのポイントをPoint型に変換して返す
    /// </summary>
    static private GeoPoint MifPointToPoint(string str)
    {
        string[] splited = str.Split(' ');
        return new GeoPoint(double.Parse(splited[1]), double.Parse(splited[2]));
    }

    /// <summary>
    /// MIFのラインを配列に変換して返す
    /// </summary>
    static private GeoPoint[] MifLineToArray(string str)
    {
        string[] splited = str.Split(' ');
        GeoPoint[] pnts = new GeoPoint[2];
        pnts[0] = new GeoPoint(double.Parse(splited[1]), double.Parse(splited[2]));
        pnts[1] = new GeoPoint(double.Parse(splited[3]), double.Parse(splited[4]));
        return pnts;
    }

    /// <summary>
    /// Mifのポリラインをポイントの配列に変換する。
    /// </summary>
    static private GeoPoint[] MifPlineToArray(StreamReader sr, int num)
    {
        GeoPoint[] pnts = new GeoPoint[num];

        int count = 0;
        while (!sr.EndOfStream)
        {
            string[] geostr = sr.ReadLine().Trim().Split(' ');
            if (geostr.Length != 2) continue;

            pnts[count++] = new GeoPoint(double.Parse(geostr[0]), double.Parse(geostr[1]));
            if (count == num) break;
        }
        return pnts;
    }

    static private string MifPlineToWKT(StreamReader sr, int num)
    {
        string[] result = new string[num+1];

        int count = 0;
        while (!sr.EndOfStream)
        {
            string[] geostr = sr.ReadLine().Trim().Split(' ');
            if (geostr.Length != 2) continue;

            result[count++] = geostr[0] + ' ' + geostr[1];
            if (count == num) break;
        }
        return "LINESTRING (" + string.Join(",", result).TrimEnd(',') + ")";
    }


    /// <summary>
    /// Mifのポリラインマルチをポイントの配列に変換する。
    /// </summary>
    static private GeoPoint[][] MifPlineMultipleToArray(StreamReader sr, int num)
    {
        GeoPoint[][] pntss = new GeoPoint[num][];
        int node_num = -1; //座標値を受け取れる状態でなければ-1

        int count = 0;
        int node_count = 0;
        while (!sr.EndOfStream)
        {
            string[] geostr = sr.ReadLine().Trim().Split(' ');
            /////ノード数/////
            if (geostr.Length == 1)
            {
                node_num = int.Parse(geostr[0]);
                pntss[count] = new GeoPoint[node_num];
            }
            /////X,Y座標値/////
            else if ((geostr.Length == 2) && (node_num != -1))
            {
                pntss[count][node_count++] = new GeoPoint(double.Parse(geostr[0]), double.Parse(geostr[1]));
                if (node_count == node_num)
                {
                    count++;
                    node_num = -1;
                    node_count = 0;
                }
                if (count == num) break;
            }
        }
        return pntss;
    }

    /// <summary>
    /// Mifのポリゴンをポイントの配列に変換する。
    /// </summary>
    static private GeoPoint[][] MifRegionToArray(StreamReader sr, int num)
    {
        return MifPlineMultipleToArray(sr, num);
    }

    /// <summary>
    /// 読み込んだMIFから、属性情報を取得する。
    /// </summary>
    static private string[] GetField(StreamReader sr, int num)
    {
        string[] fieldnames = new string[num];

        int count = 0;
        while (!sr.EndOfStream)
        {
            string fieldname = sr.ReadLine().Trim().Split(' ')[0];
            if (fieldname == "") continue;

            fieldnames[count++] = fieldname;
            if (count == num) break;
        }
        return fieldnames;
    }

    /// <summary>
    /// mid形式で出力する。
    /// </summary>
    static private void WriteMID(StringTable st, string filename)
    {
        string[] fields = st.GetFieldNames();

        using (StreamWriter mid = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            for (int i = 0; i < st.Count(); i++)
            {
                string line = "";
                foreach (string str in fields)
                    line += st[i, str] + ',';
                mid.WriteLine(StrUtils.TrimEnd(line, ','));
            }
        }
    }

    /// <summary>
    /// mifのヘッダー部分を作成する
    /// </summary>
    static private string[] MakeMifHeader(StringTable st, int epsg)
    {
        string[] result = new string[] { };
        string[] fieldnames = st.GetFieldNames();

        List<string> fields = new List<string>(100);
        foreach (string f in fieldnames)
            fields.Add("  " + f + " " + StrUtils.TypeFromString(st.GetStringsFromColumnName(f)));

        result = result.Concat(Header(epsg)).ToArray();
        result = result.Concat(new string[] { "Columns " + st.FieldCount().ToString() }).ToArray();
        result = result.Concat(fields.ToArray()).ToArray();
        return result;
    }

    /// <summary>
    /// mifのData部分を作成する。
    /// </summary>
    static private string[] MakeMifData(ShapeStruct ss)
    {
        GeoPoint pnt;
        GeoPoint[] pnts;
        GeoPoint[][] pol;
        GeoPoint[][][] mpol;

        switch (ss.Type)
        {
            case ShapeStruct.ObjType.Null:
                return new string[] { "NONE" };
            case ShapeStruct.ObjType.Point:
                pnt = ss.Points[0];
                return new string[] { "POINT " + pnt.X.ToString() + " " + pnt.Y.ToString() };
            case ShapeStruct.ObjType.MultiPoint:
                pnts = ss.Points;
                return LineStringToMIFString(pnts, "MULTIPOINT");
            case ShapeStruct.ObjType.PolyLine:
                pol = ss.PolyLines();
                if(pol.Length == 1)
                    return LineStringToMIFString(ss.Points, "PLINE");
                else
                    return MultiLineStringToMIFString(pol, "PLINE MULTIPLE ");
            case ShapeStruct.ObjType.Polygon:
                mpol = Geo2DExtra.PolygonToMultiPolygon(ss.PolyLines());
                if (mpol.Length == 1)
                    return PolygonToMIFString(mpol[0], "REGION");
                else
                    return MultiPolygonToMIFString(mpol, "REGION");
            default: throw new NotSupportedException("MifFile.MakeMifData「" + ss.Type.ToString() + "」はサポートされないオブジェクトです");
        }
    }

    /// <summary>
    /// マルチポイントをMIF形式に変換する。
    /// </summary>
    static private string[] MultiPointToMIFString(GeoPoint[] pnts, string objname)
    {
        return LineStringToMIFString(pnts, objname);
    }

    /// <summary>
    /// ラインをMIF形式に変換する。
    /// </summary>
    static private string[] LineStringToMIFString(GeoPoint[] line, string objname)
    {
        string[] result = new string[line.Length + 1];

        result[0] = objname + " " + line.Length.ToString();
        for (int i = 0; i < line.Length; i++)
            result[i + 1] = line[i].X.ToString() + " " + line[i].Y.ToString();
        return result;
    }

    /// <summary>
    /// マルチラインをMIF形式に変換する。
    /// </summary>
    static private string[] MultiLineStringToMIFString(GeoPoint[][] lines, string objname)
    {
        GeoPoint[][][] mpol = new GeoPoint[1][][];
        mpol[0] = lines;

        return MultiPolygonToMIFString(mpol, "PLINE MULTIPLE");
    }

    /// <summary>
    /// ポリゴンをMIF形式に変換する。
    /// </summary>
    static private string[] PolygonToMIFString(GeoPoint[][] pol, string objname)
    {
        GeoPoint[][][] mpol = new GeoPoint[1][][];
        mpol[0] = pol;

        return MultiPolygonToMIFString(mpol, "REGION");
    }

    /// <summary>
    /// マルチポリゴンをMIF形式に変換する。
    /// </summary>
    static private string[] MultiPolygonToMIFString(GeoPoint[][][] mpol, string objname)
    {
        int polnum = mpol.Select(d => d.Length).Sum();
        int geonum = 0;
        foreach (GeoPoint[][] pol in mpol) geonum += pol.Select(d => d.Length).Sum();

        string[] result = new string[polnum + geonum + 1];

        int cnt = 0;
        result[cnt++] = objname + " " + polnum.ToString();
        foreach (GeoPoint[][] pol in mpol)
            foreach (GeoPoint[] pnts in pol)
            {
                result[cnt++] = "\t" + pnts.Length.ToString();
                foreach (GeoPoint pnt in pnts)
                    result[cnt++] = pnt.X.ToString() + " " + pnt.Y.ToString();
            }
        return result;
    }

    /// <summary>
    /// ヘッダーの文字列を取得する。
    /// </summary>
    static private string[] Header(int epsg)
    {
        return new string[] { "Version 300", "Charset \"Neutral\"", "Delimiter \",\"", CoordSysString.EpsgToMifCoordsysString(epsg) };
    }
}