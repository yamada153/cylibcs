/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

struct ShapeStruct
{
    public enum ObjType { Null=0, Point=1, MultiPoint=8, PolyLine=3, Polygon=5 };
    //アクセスレベルどうしよう...
    public ObjType Type;
    public double[] Box; //Xmax,Ymax,Xmin,Ymin
    public int NumParts;
    public int NumPoints;
    public int[] Parts;
    public GeoPoint[] Points;

    //コンストラクタ
    public ShapeStruct(ObjType type, int numparts, int numpoints, int[] parts, GeoPoint[] pnts) : this()
    {
        Type = type;

        if (Type == ObjType.Null)
        {
            Box = new double[] { double.NegativeInfinity, double.NegativeInfinity, double.PositiveInfinity, double.PositiveInfinity};
            parts = null;
            Points = null;
        }
        else
        {
            Box = Bound(pnts);
            Parts = (int[])parts.Clone();
            Points = (GeoPoint[])pnts.Clone();
        }
        NumParts = numparts;
        NumPoints = numpoints;
    }

    /// <summary>
    /// 一次元配列のポイントを二次元配列にして返す
    /// </summary>
    public GeoPoint[][] PolyLines()
    {
        GeoPoint[][] result = new GeoPoint[NumParts][];

        for (int i = 0, j = 0; i < NumParts; i++, j += 2)
            result[i] = Points.Skip(Parts[j]).Take(Parts[j + 1] - Parts[j] + 1).ToArray();
        return result;
    }

    public override string ToString()
    {
        string result = "";
        result += "Type=" + ((int)Type).ToString() + ":";
        result += "Box=" + Box[0].ToString() + "," + Box[1].ToString() + "," + Box[2].ToString() + "," + Box[3].ToString() + ":";
        result += "NumParts=" + NumParts.ToString() + ":";
        result += "NumPoints=" + NumPoints.ToString() + ":";
        string stmp = "";
        for (int i = 0; i < Points.Length; i++)
            stmp += Points[i].ToString() + ",";
        stmp = stmp.Substring(0, stmp.Length - 1);

        return result + stmp;
    }

    /// <summary>
    /// 図形の最小矩形範囲を返す。
    /// 戻り値の配列のインデックス0 = 最大X、1 = 最大Y、2 = 最小X、3 = 最小Y
    /// </summary>
    private double[] Bound(GeoPoint[] pnts)
    {
        double[] result = new double[4];
        result[0] = double.NegativeInfinity;
        result[1] = double.NegativeInfinity;
        result[2] = double.PositiveInfinity;
        result[3] = double.PositiveInfinity;

        for (int i = 0; i < pnts.Length; i++)
        {
            result[0] = Math.Max(result[0], pnts[i].X);
            result[1] = Math.Max(result[1], pnts[i].Y);
            result[2] = Math.Min(result[2], pnts[i].X);
            result[3] = Math.Min(result[3], pnts[i].Y);
        }

        return result;
    }
}

class ShapeList
{
    List<ShapeStruct> _data;
    double[] _box; //Xmax,Ymax,Xmin,Ymin

    public ShapeStruct this[int index]
    {
        get
        {
            return _data[index];
        }
        set
        {
            _data[index] = value;
        }
    }

    public ShapeList(int capacity = 100)
    {
        _data = new List<ShapeStruct>(capacity);
        _box = new double[] { double.NegativeInfinity, double.NegativeInfinity, double.PositiveInfinity, double.PositiveInfinity};
    }

    public int Count()
    {
        return _data.Count;
    }

    public double[] GetBox()
    {
        return _box;
    }

    /// <summary>
    /// 一次元配列のポイントを２次元配列にして返す
    /// </summary>
    public GeoPoint[][] PolyLines(int index)
    {
        GeoPoint[][] result = new GeoPoint[_data[index].NumParts][];

        for (int i = 0, j = 0; i < _data[index].NumParts; i++, j += 2)
            result[i] = _data[index].Points.Skip(_data[index].Parts[j]).Take(_data[index].Parts[j + 1] - _data[index].Parts[j] + 1).ToArray();
        return result;
    }

    /// <summary>
    /// オブジェクトをリストに追加する度に_boxの値を更新する
    /// </summary>
    private void SetBoxValue()
    {
        _box[0] = Math.Max(_data[_data.Count - 1].Box[0], _box[0]);
        _box[1] = Math.Max(_data[_data.Count - 1].Box[1], _box[1]);
        _box[2] = Math.Min(_data[_data.Count - 1].Box[2], _box[2]);
        _box[3] = Math.Min(_data[_data.Count - 1].Box[3], _box[3]);
    }

    /// <summary>
    /// オブジェクトを更新する度に_boxの値を更新する
    /// </summary>
    private void UpdataBoxValue(int index)
    {
        _box[0] = Math.Max(_data[index].Box[0], _box[0]);
        _box[1] = Math.Max(_data[index].Box[1], _box[1]);
        _box[2] = Math.Min(_data[index].Box[2], _box[2]);
        _box[3] = Math.Min(_data[index].Box[3], _box[3]);
    }

    /// <summary>
    /// GeoPointの2次元配列を1次元配列に並べ直す
    /// </summary>
    private GeoPoint[] CreateOneArray(GeoPoint[][] pnts)
    {
        List<GeoPoint> result = new List<GeoPoint>(1000); //適当
        for (int i = 0; i < pnts.Length; i++)
            for (int j = 0; j < pnts[i].Length; j++)
                result.Add(pnts[i][j]);

        return result.ToArray();
    }

    /// <summary>
    /// GeoPointの2次元配列を1次元配列に変換したときの、各配列の先頭、末尾のインデックスを返す
    /// </summary>
    private int[] CreateIndexs(GeoPoint[][] pnts)
    {
        int[] indexs = new int[pnts.Length * 2];

        for (int i = 0; i < pnts.Length; i++)
        {
            int idx = i * 2;

            if (idx == 0)
                indexs[idx] = 0;
            else
                indexs[idx] = indexs[idx - 1] + 1;

            indexs[idx + 1] = indexs[idx] + pnts[i].Length - 1;
        }

        return indexs;
    }

    /****************************************************************************************************
     *                                          追加
     ****************************************************************************************************/
    /// <summary>
    /// Nullを追加する
    /// </summary>
    public void AddNull()
    {
        _data.Add(new ShapeStruct(ShapeStruct.ObjType.Null, 0, 0, null, null));
    }

    /// <summary>
    /// GeoPointを追加する
    /// </summary>
    public void AddPoint(GeoPoint pnt)
    {
        _data.Add(new ShapeStruct(ShapeStruct.ObjType.Point, 1, 1, new int[] { 0 }, new GeoPoint[] { pnt }));
        SetBoxValue();
    }

    /// <summary>
    /// MultiPointを追加する
    /// </summary>
    public void AddMultiPoint(GeoPoint[] pnts)
    {
        int[] indexs = new int[pnts.Length];
        for (int i = 0; i < pnts.Length; i++)
            indexs[i] = i;

        _data.Add(new ShapeStruct(ShapeStruct.ObjType.MultiPoint, pnts.Length, pnts.Length, indexs, pnts));
        SetBoxValue();
    }

    /// <summary>
    /// PolyLineを追加する
    /// </summary>
    public void AddPolyLine(GeoPoint[][] pnts)
    {
        int[] indexs = CreateIndexs(pnts);
        GeoPoint[] ar = CreateOneArray(pnts);
        _data.Add(new ShapeStruct(ShapeStruct.ObjType.PolyLine, indexs.Length / 2, ar.Length, indexs, ar));
        SetBoxValue();
    }

    /// <summary>
    /// Polygonを追加する
    /// </summary>
/*
Polygonは1個または複数のリングで構成されます。
ここでリングとは、4個以上のPointを順につないだもので、自分自身に交差しない閉曲線のことを指します。
Polygonではいくつかのリングが入れ子になっていることがあります。
リングを構成する頂点の順番、すなわちリングの向きによって、リングのどちら側をPolygonの内部とみなすかが決まります。
頂点の順にリングを辿るとき、右側にある方をPolygonの内部と定義します。
Polygon内部の穴をあらわす場合、リングの向きは反時計回りになります。
1本のリングで構成されるPolygonの場合には、リングの向きは常に時計回りになります。
このリングのことを、Polygonのパートと呼びます。
仕様では、連続する2個のPointが全く同じ座標値を持つ場合を禁じていないので、
シェープファイルを利用する場合はこのことを想定しておく必要があります。
しかし、長さゼロ・面積ゼロの、縮退したパートは許されないことに注意してください。
*/
    public void AddPolygon(GeoPoint[][] pnts)
    {
        int[] indexs = CreateIndexs(pnts);
        GeoPoint[] ar = CreateOneArray(pnts);

        _data.Add(new ShapeStruct(ShapeStruct.ObjType.Polygon, indexs.Length / 2, ar.Length, indexs, ar));
        SetBoxValue();
    }
    /****************************************************************************************************
     *                                          更新
     ****************************************************************************************************/
    /// <summary>
    /// Nullで更新する
    /// </summary>
    public void ChangeNull(int index)
    {
        _data[index] = new ShapeStruct(ShapeStruct.ObjType.Null, 0, 0, null, null);
        UpdataBoxValue(index);
    }

    /// <summary>
    /// Pointで更新する
    /// </summary>
    public void ChangePoint(GeoPoint pnt, int index)
    {
        _data[index] = new ShapeStruct(ShapeStruct.ObjType.Point, 1, 1, new int[] { 0 }, new GeoPoint[] { pnt });
        UpdataBoxValue(index);
    }

    /// <summary>
    /// MultiPointで更新する
    /// </summary>
    public void ChangeMultiPoint(GeoPoint[] pnts, int index)
    {
        int[] indexs = new int[pnts.Length];
        for (int i = 0; i < pnts.Length; i++)
            indexs[i] = i;

        _data[index] = new ShapeStruct(ShapeStruct.ObjType.MultiPoint, pnts.Length, pnts.Length, indexs, pnts);
        UpdataBoxValue(index);
    }

    /// <summary>
    /// PolyLineで更新する
    /// </summary>
    public void ChangePolyLine(GeoPoint[][] pnts, int index)
    {
        int[] indexs = CreateIndexs(pnts);
        GeoPoint[] ar = CreateOneArray(pnts);
        _data[index] = new ShapeStruct(ShapeStruct.ObjType.PolyLine, indexs.Length / 2, ar.Length, indexs, ar);
        UpdataBoxValue(index);
    }

    /// <summary>
    /// Polygonで更新する
    /// </summary>
    public void ChangePolygon(GeoPoint[][] pnts, int index)
    {
        int[] indexs = CreateIndexs(pnts);
        GeoPoint[] ar = CreateOneArray(pnts);

        _data[index] = new ShapeStruct(ShapeStruct.ObjType.Polygon, indexs.Length / 2, ar.Length, indexs, ar);
        UpdataBoxValue(index);
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestShapeStruct
{
    public static void AddPolyLine(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[][] pnts = new GeoPoint[3][];
        pnts[0] = new GeoPoint[] { new GeoPoint(30, 20), new GeoPoint(10, 40), new GeoPoint(10, 70) };
        pnts[1] = new GeoPoint[] { new GeoPoint(30, 40), new GeoPoint(40, 60), new GeoPoint(60, 60), new GeoPoint(70, 45), new GeoPoint(80, 50) };
        pnts[2] = new GeoPoint[] { new GeoPoint(50, 20), new GeoPoint(70, 20) };

        ShapeList sl = new ShapeList();
        sl.AddPolyLine(pnts);
    }
}