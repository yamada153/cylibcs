/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

static class GisUtils
{
    /// <summary>
    /// GIS関係のファイルを、拡張子を気にせず読み込む。shp、mif、mgr、wktに対応している。
    /// </summary>
    public static Tuple<ShapeList, StringTable> LoadFromFile(string filename)
    {
        string ext = Path.GetExtension(filename).ToLower();
        switch (ext)
        {
            case ".shp":
                return ShapeFile.LoadFromShp(filename);
            case ".mif":
                return MIFFile.LoadFromMif(filename);
            case ".wkt":
                return WKTFunc.FromStringTable(SeparateText.LoadFromFile(filename, '\t'));
            case ".csv":
                StringTable st = SeparateText.LoadFromFile(filename, ',');
                ShapeList sl = new ShapeList(st.Count());
                for (int i = 0; i < st.Count(); i++)
                    sl.AddNull();
                return new Tuple<ShapeList, StringTable>(sl, st);
            default: break;
        }

        return null;
    }

    /// <summary>
    /// ポリゴン内に点群を生成する
    /// </summary>
    static public GeoPoint[] GetPointsInPolygon(GeoPoint[][] pol, double unit_x, double unit_y)
    {
        double[] bnd = Geo2DBase.Bound(pol);

        GeoPoint org = new GeoPoint(bnd[2] + unit_x / 2, bnd[3] + unit_y / 2);
        int nX = (int)((bnd[0] - bnd[2]) / unit_x);
        int nY = (int)((bnd[1] - bnd[3]) / unit_y);

        List<GeoPoint> tmp = new List<GeoPoint>(nX * nY);
        for (int iX = 0; iX <= nX; iX++)
            for (int iY = 0; iY <= nY; iY++)
            {
                GeoPoint pnt = new GeoPoint(org.X + iX * unit_x, org.Y + iY * unit_y);
                if(Geo2DIntersect.WithIn(pnt, pol))
                    tmp.Add(pnt);
            }

        return tmp.ToArray();
    }

    static public void WriteTabFileFromRaster(string meshcode, MapFrame mf, int coord, string filename)
    {
        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(filename);
        int bmpwidth = bmp.Width;
        int bmpheight = bmp.Height;

        string tabfile = Path.ChangeExtension(filename, ".tab");
        using (StreamWriter tab = new StreamWriter(tabfile, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            GeoPoint sw = mf.SouthWest(meshcode);
            double MaxX = sw.X + mf.UnitX;
            double MaxY = sw.Y + mf.UnitY;
            double MinX = sw.X;
            double MinY = sw.Y;

            tab.WriteLine("!table");
            tab.WriteLine("!version 300");
            tab.WriteLine("!charset WindowsLatin1");

            tab.WriteLine("\nDefinition Table");
            tab.WriteLine(string.Format("\tFile \"{0}\"", Path.GetFileName(filename)));
            tab.WriteLine(string.Format("\tType \"RASTER\""));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 1\",", MinX, MaxY, 0, 0));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 2\",", MinX, MinY, 0, bmpheight));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 3\",", MaxX, MinY, bmpwidth, bmpheight));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 4\"", MaxX, MaxY, bmpwidth, 0));
            tab.WriteLine(string.Format("\t{0}", CoordSysString.MifCoordStringsJGD[coord]));

            if (coord == 0)
                tab.WriteLine("\tUnits \"degree\"");
            else
                tab.WriteLine("\tUnits \"m\"");
        }
    }

    /// <summary>
    /// ラスターをMapInfoで読み込むためのTABファイルを作成する。
    /// filenameにはラスタファイルの名前を入れる。保存するTABファイル名ではない。
    /// </summary>
    static public void WriteTabFileFromRaster(Grid grd, GeoPoint sw, string filename, int epsg)
    {
        //画像サイズ
        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(filename);
        double bmpwidth = bmp.Width;
        double bmpheight = bmp.Height;

        string tabfile = Path.ChangeExtension(filename, ".tab");
        using (StreamWriter tab = new StreamWriter(tabfile, false, Encoding.GetEncoding("shift_jis")))
        {
            tab.WriteLine("!table");
            tab.WriteLine("!version 300");
            tab.WriteLine("!charset WindowsLatin1");
            tab.WriteLine("\nDefinition Table");
            tab.WriteLine(string.Format("\tFile \"{0}\"", Path.GetFileName(filename)));
            tab.WriteLine(string.Format("\tType \"RASTER\""));

            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 1\",", sw.X, sw.Y + grd.Span * bmpheight, 0, 0));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 2\",", sw.X, sw.Y, 0, bmpheight));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 3\",", sw.X + grd.Span * bmpwidth, sw.Y, bmpwidth, bmpheight));
            tab.WriteLine(string.Format("\t({0},{1}) ({2},{3}) Label \"Pt 4\"", sw.X + grd.Span * bmpwidth, sw.Y + grd.Span * bmpheight, bmpwidth, 0));
            tab.WriteLine(string.Format("\t{0}", CoordSysString.EpsgToMifCoordsysString(epsg)));

            if ((epsg == 4301) || (epsg == 4326))
                tab.WriteLine("\tUnits \"degree\"");
            else
                tab.WriteLine("\tUnits \"m\"");
        }
    }

    /// <summary>
    /// ラスターをQGIS等で読み込むためのBPWファイルを作成する。nwは左上のピクセルの中心の座標を指定する
    /// filenameにはラスタファイルの名前を入れる。保存するBPWファイル名ではない。
    /// </summary>
    static public void WriteBpwFileFromRaster(Grid grd, GeoPoint nw, string filename)
    {
        //画像サイズ
        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(filename);
        double bmpwidth = bmp.Width;
        double bmpheight = bmp.Height;
        //グリッド幅
        double unit_x = grd.Span;
        double unit_y = grd.Span;

        string wfile = Path.ChangeExtension(filename, ".bpw");
        using (StreamWriter w = new StreamWriter(wfile, false, Encoding.GetEncoding("shift_jis")))
        {
            w.WriteLine(unit_x.ToString("F12"));
            w.WriteLine("0.0");
            w.WriteLine("0.0");
            w.WriteLine("-" + unit_y.ToString("F12"));
            w.WriteLine(nw.X.ToString());
            w.WriteLine(nw.Y.ToString());
        }
    }

    /// <summary>
    /// Projectionファイルを保存する
    /// </summary>
    static public void SaveToPrj(string filename, int epsg)
    {
        string coordstr = CoordSysString.EpsgToShpCoordsysString(epsg);
        FileFunc.SaveToFile(new string[] { coordstr }, filename);
    }

    /// <summary>
    /// valueで指定された10進形式の度を度分秒形式に変換する。
    /// </summary>
    public static double DmsToDegree(int degree, int minutes, double second)
    {
        return degree + minutes / Math.Pow(60, 1) + second / Math.Pow(60, 2);
    }

    /// <summary>
    /// strで指定された10進形式の度を度分秒形式に変換する。
    /// str = 34°42′24.6″みたいな形式
    /// </summary>
    public static double DmsToDegree(string str, char do_str, char fun_str, char byo_str)
    {
        string[] sstmp = null;

        sstmp = str.Split(do_str);
        string sdo = sstmp[0];

        sstmp = str.Replace(sdo + do_str, "").Split(fun_str);
        string sfun = sstmp[0];

        string sbyo = str.Split(fun_str)[1].Replace("．", ".");

        return int.Parse(sdo) + int.Parse(sfun) / Math.Pow(60, 1) + double.Parse(sbyo.Replace(byo_str.ToString(), "").Replace(byo_str.ToString(), "")) / Math.Pow(60, 2);
    }

    /// <summary>
    /// png画像を100枚つなげた画像のwmtsを作成する関数
    /// </summary>
    /*
    static public void MakeNewWMTS10X10(string LoadFolderPath, string SaveFolderPath)
    {
        string[] filenames = FileFunc.FileList(LoadFolderPath, "*.png");
        int zoom = 0, x, y;

        StringTable st = new StringTable(new string[] { "ファイル名", "zoom", "x", "y" });
        for (int i = 0; i < filenames.Length; i++)
        {
            WMTSFunc.GetZoomAndXAndY(filenames[i], out zoom, out y, out x);
            st.Add(new string[] { filenames[i], zoom.ToString(), x.ToString(), y.ToString() });
        }
        /////x、yの最大値、最小値を取得する
        double maxx = 0, minx = 0;
        st.NumMaxMin("x", out maxx, out minx);
        double maxy = 0, miny = 0;
        st.NumMaxMin("y", out maxy, out miny);
        /////ループ回数を決める
        int ix = (int)(Useful.Kiriage(maxx, 10) - Useful.Kirisute(minx, 10)) / 10;
        int iy = (int)(Useful.Kiriage(maxy, 10) - Useful.Kirisute(miny, 10)) / 10;
        /////
        for (int i = 0; i < ix; i++)
        {
            for (int j = 0; j < iy; j++)
            {
                /////新しい画像領域に、画像を張り付ける
                Real2DEx newimg = new Real2DEx(2560, 2560, 0x00ffffff);
                for (int m = 0; m < 10; m++)
                {
                    for (int n = 0; n < 10; n++)
                    {
                        x = (int)minx + i * 10 + m;
                        y = (int)miny + j * 10 + n;

                        string imgfile = LoadFolderPath + "\\" + x.ToString() + @"\" + y.ToString() + @".png";
                        if (!File.Exists(imgfile)) continue;
                        Real2DEx img = new Real2DEx(imgfile, 0x00ffffff);
                        newimg.Append(img, 256 * m, 256 * n);
                    }
                }
                /////新しくできた画像を保存する
                string savefile = SaveFolderPath + @"\" + i.ToString() + "_" + j.ToString() + ".png";
                newimg.SaveToBitmap(savefile);

                WriteTabFileFromRaster(savefile, zoom, (int)minx + i * 10, (int)miny + j * 10, (int)minx + i * 10 + 9, (int)miny + j * 10 + 9);
            }
        }
    }
    */
}