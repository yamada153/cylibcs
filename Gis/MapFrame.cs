﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

abstract class MapFrame
{
    abstract public double UnitX { get; } 
    abstract public double UnitY { get; } 
    abstract public MapFrame Parent { get; }
    abstract public string MeshCode(double x, double y, int coord = 0);
    abstract public GeoPoint SouthWest(string code);
    abstract public string Left(string code);
    abstract public string Top(string code);
    abstract public string Right(string code);
    abstract public string Under(string code);
    abstract public bool CheckFormat(string code, bool except = true);
    abstract public string[] ChildCodes(string code);

    /// <summary>
    /// 指定したメッシュコードから、そのメッシュの左上のメッシュコードを取得する。コードが無ければ空文字を返す。
    /// </summary>
    public string LeftTop(string code)
    {
        string tmp = Top(code);
        if (tmp == "") return "";
        return Left(tmp);
    }

    /// <summary>
    /// 指定したメッシュコードから、そのメッシュの右上のメッシュコードを取得する。コードが無ければ空文字を返す。
    /// </summary>
    public string RightTop(string code)
    {
        string tmp = Top(code);
        if (tmp == "") return "";
        return Right(tmp);
    }

    /// <summary>
    /// 指定したメッシュコードから、そのメッシュの右下のメッシュコードを取得する。コードが無ければ空文字を返す。
    ///</summary>
    public string RightUnder(string code)
    {
        string tmp = Under(code);
        if (tmp == "") return "";
        return Right(tmp);
    }

    /// <summary>
    /// 指定したメッシュコードから、そのメッシュの左下のメッシュコードを取得する。コードが無ければ空文字を返す。
    /// </summary>    
    public string LeftUnder(string code)
    {
        string tmp = Under(code);
        if (tmp == "") return "";
        return Left(tmp);
    }

    /// <summary>
    /// 指定したメッシュコードから、自身も含め周囲の9メッシュコードを取得する。コードが無ければ空文字を返す。
    /// <para>配列0番目...左</para>
    /// <para>配列1番目...左上</para>
    /// <para>配列2番目...上</para>
    /// <para>配列3番目...右上</para>
    /// <para>配列4番目...右</para>
    /// <para>配列5番目...右下</para>
    /// <para>配列6番目...下</para>
    /// <para>配列7番目...左下</para>
    /// <para>配列8番目...中心</para>
    /// </summary>    
    public string[] Neighborhood(string code)
    {
        string[] ret = new string[9];
        ret[0] = Left(code);
        ret[1] = LeftTop(code);
        ret[2] = Top(code);
        ret[3] = RightTop(code);
        ret[4] = Right(code);
        ret[5] = RightUnder(code);
        ret[6] = Under(code);
        ret[7] = LeftUnder(code);
        ret[8] = code;
        return ret;
    }

    /// <summary>
    /// コードからポリゴンを取得する。
    /// </summary>
    public GeoPoint[] Polygon(string code)
    {
        GeoPoint pnt = SouthWest(code);
        if (pnt.X.Equals(double.NaN)) return null;

        GeoPoint[] pnts = new GeoPoint[]{
            new GeoPoint(pnt.X, pnt.Y),
            new GeoPoint(pnt.X, pnt.Y + UnitY),
            new GeoPoint(pnt.X + UnitX, pnt.Y + UnitY),
            new GeoPoint(pnt.X + UnitX, pnt.Y),
            new GeoPoint(pnt.X, pnt.Y)
        };

        return pnts;
    }

    /// <summary>
    /// コードから中心点を取得する。
    /// </summary>
    public GeoPoint Center(string code)
    {
        GeoPoint pnt = SouthWest(code);
        if (pnt.X.Equals(double.NaN)) return new GeoPoint(double.NaN, double.NaN);

        return new GeoPoint(pnt.X + UnitX / 2, pnt.Y + UnitY / 2);
    }

    public bool CheckLatLon(double lon, double lat, bool except = true)
    {
        if (!((-180 <= lon) && (lon <= 180)))
        {
            if (except)
                throw new ArgumentException("CheckLatLonでエラー(" + lon.ToString() + ") -180～180の間で指定して下さい。");
            else
                return false;
        }
        if (!((-90 <= lat) && (lat <= 90)))
        {
            if (except)
                throw new ArgumentException("CheckLatLonでエラー(" + lat.ToString() + ") - 90～90の間で指定して下さい。");
            else
                return false;
        }
        return true;
    }

    public bool CheckBaseCode(double x, double y, int coord, bool except = true)
    {
        if (!((-300000 <= x) && (x <= 300000)))
        {
            if (except)
                throw new ArgumentException("CheckBaseCodeでエラー(" + x.ToString() + " ) -300000～300000の間で指定して下さい。");
            else
                return false;
        }
        if (!((-160000 <= y) && (y <= 160000)))
        {
            if (except)
                throw new ArgumentException("CheckBaseCodeでエラー(" + y.ToString() + " ) -160000～160000の間で指定して下さい。");
            else
                return false;
        }
        if (!(((1 <= coord) && (coord <= 39))))
        {
            if (except)
                throw new ArgumentException("CheckBaseCodeでエラー(" + coord.ToString() + " ) 1～39の間で指定して下さい。");
            else
                return false;
        }
        return true;
    }
}

/// <summary>
/// 第1次地域区画
/// </summary>
class PrimaryAreaMesh : MapFrame
{
    public override MapFrame Parent { get; }
    override public double UnitX { get; }  //Lon
    override public double UnitY { get; }  //Lat

    public PrimaryAreaMesh()
    {
        UnitX = 1;                    // 1°
        UnitY = 0.6666666666666667;   // (2/3)°
        Parent = null;
    }

    /// <summary>
    /// 指定された点を含む1次メッシュコードを取得する。
    /// </summary>    
    override public string MeshCode(double lon, double lat, int coord = 0)
    {
        CheckLatLon(lon, lat);
        return (Math.Floor(lat * 1.5)).ToString().PadLeft(2, '0') + ((int)lon - 100).ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// 1次メッシュコードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        double lat = xy.Y / 1.5;
        double lon = xy.X + 100;

        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 1次メッシュコードから、そのメッシュの左のメッシュコードを取得する。
    /// </summary>
    override public string Left(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);

        return xy.Y.ToString().PadLeft(2, '0') + (xy.X - 1).ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// 1次メッシュコードから、そのメッシュの上のメッシュコードを取得する。
    ///</summary>
    override public string Top(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);

        return (xy.Y + 1).ToString().PadLeft(2, '0') + xy.X.ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// 1次メッシュコードから、そのメッシュの右のメッシュコードを取得する。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);

        return xy.Y.ToString().PadLeft(2, '0') + (xy.X + 1).ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// 1次メッシュコードから、そのメッシュの下のメッシュコードを取得する。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        return (xy.Y - 1).ToString().PadLeft(2, '0') + xy.X.ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// 1次メッシュコードから、2次メッシュコードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        List<string> tmp = new List<string>(100); //適当
        for (int i = 0; i <= 7; i++)
            for (int j = 0; j <= 7; j++)
                tmp.Add(code + i.ToString() + j.ToString());
        return tmp.ToArray();
    }

    /// <summary>
    /// コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !Regex.IsMatch(code, @"\d\d\d\d"))
        {
            if (except)
                throw new ArgumentException("「" + code + "」は1次メッシュコードの書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 1次メッシュコードの文字列からX部分とY部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private GeoPoint GetLatLon(string code)
    {
        int lat = int.Parse(code.Substring(0, 2));
        int lon = int.Parse(code.Substring(2, 2));

        return new GeoPoint(lon, lat);
    }
}

/// <summary>
/// 第2次地域区画
/// </summary>
class SecondaryAreaMesh : MapFrame
{
    public override MapFrame Parent { get; }
    override public double UnitX { get; } 
    override public double UnitY { get; } 

    public SecondaryAreaMesh()
    {
        UnitX = 0.125;                // 1°     /8
        UnitY = 0.0833333333333333;   // (2/3)° /8
        Parent = new PrimaryAreaMesh();
    }

    /// <summary>
    /// 指定された点を含む2次メッシュコードを取得する。
    /// <para>例</para>
    /// <para>MeshCode(130.586, 34.044); // </para>
    /// </summary>    
    override public string MeshCode(double lon, double lat, int coord = 0)
    {
        CheckLatLon(lon, lat);

        string _parent__mesh = Parent.MeshCode(lon, lat);
        GeoPoint org = Parent.SouthWest(_parent__mesh);

        int mesh_y = (int)Math.Truncate(((lat - org.Y) / UnitY));
        int mesh_x = (int)Math.Truncate(((lon - org.X) / UnitX));

        return _parent__mesh + mesh_y.ToString() + mesh_x.ToString();
    }

    /// <summary>
    /// 2次メッシュコードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        GeoPoint org = Parent.SouthWest(code.Substring(0, 4));

        double lat = org.Y + xy.Y * UnitY;
        double lon = org.X + xy.X * UnitX;

        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 2次メッシュコードから、そのメッシュの左のメッシュコードを取得する。
    /// </summary>
    override public string Left(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.X == 0)
        {
            tmp_parent_ = Parent.Left(tmp_parent_);
            xy.X = 7;
        }
        else
            xy.X--;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 2次メッシュコードから、そのメッシュの上のメッシュコードを取得する。
    /// </summary>
    override public string Top(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.Y == 7)
        {
            tmp_parent_ = Parent.Top(tmp_parent_);
            xy.Y = 0;
        }
        else
            xy.Y++;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 2次メッシュコードから、そのメッシュの右のメッシュコードを取得する。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.X == 7)
        {
            tmp_parent_ = Parent.Right(tmp_parent_);
            xy.X = 0;
        }
        else
            xy.X++;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 2次メッシュコードから、そのメッシュの下のメッシュコードを取得する。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.Y == 0)
        {
            tmp_parent_ = Parent.Under(tmp_parent_);
            xy.Y = 7;
        }
        else
            xy.Y--;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 2次メッシュコードから、3次メッシュコードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        List<string> tmp = new List<string>(100); //適当
        for (int i = 0; i <= 9; i++)
            for (int j = 0; j <= 9; j++)
                tmp.Add(code + i.ToString() + j.ToString());
        return tmp.ToArray();
    }

    /// <summary>
    /// コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}"))
        {
            if (except)
                throw new ArgumentException("「" + code + "」は2次メッシュコードの書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 2次メッシュコードの文字列からX部分とY部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private GeoPoint GetLatLon(string code)
    {
        int lat = int.Parse(code.Substring(4, 1));
        int lon = int.Parse(code.Substring(5, 1));

        return new GeoPoint(lon, lat);
    }
}

/// <summary>
/// 第3次地域区画(基準地域メッシュ)
/// </summary>
class TertiaryArea : MapFrame
{
    public override MapFrame Parent { get; }
    override public double UnitX { get; } 
    override public double UnitY { get; } 

    public TertiaryArea()
    {
        UnitX = 0.0125;        // 1°    /8/10
        UnitY = 0.0083333333333333;   // (2/3)°/8/10
        Parent = new SecondaryAreaMesh();
    }

    /// <summary>
    /// 指定された点を含む3次メッシュコードを取得する。
    /// <para>例</para>
    /// <para>MeshCode(130.586, 34.044); // </para>
    /// </summary>    
    override public string MeshCode(double lon, double lat, int coord = 0)
    {
        CheckLatLon(lon, lat);

        string _parent__mesh = Parent.MeshCode(lon, lat);
        GeoPoint org = Parent.SouthWest(_parent__mesh);

        int mesh_y = (int)Math.Truncate(((lat - org.Y) / UnitY));
        int mesh_x = (int)Math.Truncate(((lon - org.X) / UnitX));

        return _parent__mesh + mesh_y.ToString() + mesh_x.ToString();
    }

    /// <summary>
    /// 3次メッシュコードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        GeoPoint org = Parent.SouthWest(code.Substring(0, 6));

        double lat = org.Y + xy.Y * UnitY;
        double lon = org.X + xy.X * UnitX;

        return new GeoPoint(lon, lat);
    }

    /// <summary>
    /// 3次メッシュコードから、そのメッシュの左のメッシュコードを取得する。
    /// </summary>
    override public string Left(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 6);
        if (xy.X == 0)
        {
            tmp_parent_ = Parent.Left(tmp_parent_);
            xy.X = 9;
        }
        else
            xy.X--;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 3次メッシュコードから、そのメッシュの上のメッシュコードを取得する。
    /// </summary>
    override public string Top(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 6);
        if (xy.Y == 9)
        {
            tmp_parent_ = Parent.Top(tmp_parent_);
            xy.Y = 0;
        }
        else
            xy.Y++;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 3次メッシュコードから、そのメッシュの右のメッシュコードを取得する。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 6);
        if (xy.X == 9)
        {
            tmp_parent_ = Parent.Right(tmp_parent_);
            xy.X = 0;
        }
        else
            xy.X++;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 3次メッシュコードから、そのメッシュの下のメッシュコードを取得する。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 6);
        if (xy.Y == 0)
        {
            tmp_parent_ = Parent.Under(tmp_parent_);
            xy.Y = 9;
        }
        else
            xy.Y--;

        return tmp_parent_ + xy.Y.ToString() + xy.X.ToString();
    }

    /// <summary>
    /// 3次メッシュコードから、2分の1地域区画コードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        string[] result = new string[4];
        for (int i = 1; i <= 4; i++)
            result[i - 1] = code + i.ToString();
        return result;
    }

    /// <summary>
    /// コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d"))
        {
            if (except)
                throw new ArgumentException("「" + code + "」は3次メッシュコードの書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 3次メッシュコードの文字列からX部分とY部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private GeoPoint GetLatLon(string code)
    {
        int lat = int.Parse(code.Substring(6, 1));
        int lon = int.Parse(code.Substring(7, 1));

        return new GeoPoint(lon, lat);
    }
}

/// <summary>
/// 2分の1地域区画
/// </summary>
class OneHalf : MapFrame
{
    public override MapFrame Parent { get; }
    override public double UnitX { get; } 
    override public double UnitY { get; } 

    public OneHalf()
    {
        UnitX = 0.00625;            // 1°    /8/10/2
        UnitY = 0.0041666666666667; // (2/3)°/8/10/2
        Parent = new TertiaryArea();
    }

    /// <summary>
    /// 指定された点を含む2分の1地域区画コードを取得する。
    /// <para>例</para>
    /// <para>MeshCode(130.586, 34.044); // </para>
    /// </summary>    
    override public string MeshCode(double lon, double lat, int coord = 0)
    {
        CheckLatLon(lon, lat);

        string _parent__mesh = Parent.MeshCode(lon, lat);
        GeoPoint org = Parent.SouthWest(_parent__mesh);

        int mesh_y = (int)Math.Truncate(((lat - org.Y) / UnitY)) + 1;
        int mesh_x = (int)Math.Truncate(((lon - org.X) / UnitX)) + 1;

        return _parent__mesh + mesh_y.ToString() + mesh_x.ToString();
    }

    /// <summary>
    /// 2分の1地域区画コードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint result = new GeoPoint();
        GeoPoint org = Parent.SouthWest(code.Substring(0, 8));
        switch (GetLatLon(code))
        {
            case 1: result = new GeoPoint(org.X, org.Y); break;
            case 2: result = new GeoPoint(org.X + UnitX, org.Y); break;
            case 3: result = new GeoPoint(org.X, org.Y + UnitY); break;
            case 4: result = new GeoPoint(org.X + UnitX, org.Y + UnitY); break;
        }

        return result;
    }

    /// <summary>
    /// 2分の1地域区画コードから、そのメッシュの左のコードを取得する。
    /// </summary>
    override public string Left(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 8);
        string result = "";
        switch (lastcode)
        {
            case 1: result = Parent.Left(code) + "2"; break;
            case 2: result = code.Substring(0, 8) + "1"; break;
            case 3: result = Parent.Left(code) + "4"; break;
            case 4: result = code.Substring(0, 8) + "3"; break;
        }
        return result;
    }

    /// <summary>
    /// 2分の1地域区画コードから、そのメッシュの上のコードを取得する。
    /// </summary>
    override public string Top(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 8);
        string result = "";
        switch (lastcode)
        {
            case 1: result = code.Substring(0, 8) + "3"; break;
            case 2: result = code.Substring(0, 8) + "4"; break;
            case 3: result = Parent.Top(code) + "1"; break;
            case 4: result = Parent.Top(code) + "2"; break;
        }
        return result;
    }

    /// <summary>
    /// 2分の1地域区画コードから、そのメッシュの右のコードを取得する。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 8);
        string result = "";
        switch (lastcode)
        {
            case 1: result = code.Substring(0, 8) + "2"; break;
            case 2: result = Parent.Right(code) + "1"; break;
            case 3: result = code.Substring(0, 8) + "4"; break;
            case 4: result = Parent.Right(code) + "3"; break;
        }
        return result;
    }

    /// <summary>
    /// 2分の1地域区画コードから、そのメッシュの下のコードを取得する。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 8);
        string result = "";
        switch (lastcode)
        {
            case 1: result = Parent.Under(code) + "3"; break;
            case 2: result = Parent.Under(code) + "4"; break;
            case 3: result = code.Substring(0, 8) + "1"; break;
            case 4: result = code.Substring(0, 8) + "2"; break;
        }
        return result;
    }

    /// <summary>
    /// 2分の1地域区画コードから、4分の1地域区画コードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        string[] result = new string[4];
        for (int i = 1; i <= 4; i++)
            result[i - 1] = code + i.ToString();
        return result;
    }

    /// <summary>
    /// コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d[1-4]\d"))
        {
            if (except)
                throw new ArgumentException("「" + code + "」は2分の1地域区画コードの書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 2分の1地域区画コードの文字列から末尾部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private int GetLatLon(string code)
    {
        int section = int.Parse(code.Substring(8, 1));
        return section;
    }
}

/// <summary>
/// 4分の1地域区画
/// </summary>
class OneQuarter : MapFrame
{
    public override MapFrame Parent { get; }
    override public double UnitX { get; } 
    override public double UnitY { get; } 

    public OneQuarter()
    {
        UnitX = 0.003125;            // 1°    /8/10/4
        UnitY = 0.0020833333333334; // (2/3)°/8/10/4
        Parent = new TertiaryArea();
    }

    /// <summary>
    /// 指定された点を含む4分の1地域区画コードを取得する。
    /// <para>例</para>
    /// <para>MeshCode(130.586, 34.044); // </para>
    /// </summary>    
    override public string MeshCode(double lon, double lat, int coord = 0)
    {
        CheckLatLon(lon, lat);

        string _parent__mesh = Parent.MeshCode(lon, lat);
        GeoPoint org = Parent.SouthWest(_parent__mesh);

        int mesh_y = (int)Math.Truncate(((lat - org.Y) / UnitY));
        int mesh_x = (int)Math.Truncate(((lon - org.X) / UnitX));

        return _parent__mesh + mesh_y.ToString() + mesh_x.ToString();
    }

    /// <summary>
    /// 4分の1地域区画コードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint result = new GeoPoint();
        GeoPoint org = Parent.SouthWest(code.Substring(0, 9));
        switch (GetLatLon(code))
        {
            case 1: result = new GeoPoint(org.X, org.Y); break;
            case 2: result = new GeoPoint(org.X + UnitX, org.Y); break;
            case 3: result = new GeoPoint(org.X, org.Y + UnitY); break;
            case 4: result = new GeoPoint(org.X + UnitX, org.Y + UnitY); break;
        }

        return result;
    }

    /// <summary>
    /// 4分の1地域区画コードから、そのメッシュの左のコードを取得する。
    /// </summary>
    override public string Left(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 9);
        string result = "";
        switch (lastcode)
        {
            case 1: result = Parent.Left(code) + "2"; break;
            case 2: result = code.Substring(0, 9) + "1"; break;
            case 3: result = Parent.Left(code) + "4"; break;
            case 4: result = code.Substring(0, 9) + "3"; break;
        }
        return result;
    }

    /// <summary>
    /// 4分の1地域区画コードから、そのメッシュの上のコードを取得する。
    /// </summary>
    override public string Top(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 9);
        string result = "";
        switch (lastcode)
        {
            case 1: result = code.Substring(0, 9) + "3"; break;
            case 2: result = code.Substring(0, 9) + "4"; break;
            case 3: result = Parent.Top(code) + "1"; break;
            case 4: result = Parent.Top(code) + "2"; break;
        }
        return result;
    }

    /// <summary>
    /// 4分の1地域区画コードから、そのメッシュの右のコードを取得する。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 9);
        string result = "";
        switch (lastcode)
        {
            case 1: result = code.Substring(0, 9) + "2"; break;
            case 2: result = Parent.Right(code) + "1"; break;
            case 3: result = code.Substring(0, 9) + "4"; break;
            case 4: result = Parent.Right(code) + "3"; break;
        }
        return result;
    }

    /// <summary>
    /// 4分の1地域区画コードから、そのメッシュの下のコードを取得する。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 9);
        string result = "";
        switch (lastcode)
        {
            case 1: result = Parent.Under(code) + "3"; break;
            case 2: result = Parent.Under(code) + "4"; break;
            case 3: result = code.Substring(0, 9) + "1"; break;
            case 4: result = code.Substring(0, 9) + "2"; break;
        }
        return result;
    }

    /// <summary>
    /// 4分の1地域区画コードから、8分の1地域区画コードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        string[] result = new string[4];
        for (int i = 1; i <= 4; i++)
            result[i-1] = code + i.ToString();
        return result;
    }

    /// <summary>
    /// コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d[1-4]\d[1-4]\d"))
        {
            if (except)
                throw new ArgumentException("「" + code + "」は4分の1地域区画コードの書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 4分の1地域区画コードの文字列から末尾部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private int GetLatLon(string code)
    {
        int section = int.Parse(code.Substring(9, 1));
        return section;
    }
}

/// <summary>
/// 8分の1地域区画
/// </summary>
class OneEighth : MapFrame
{
    public override MapFrame Parent { get; }
    override public double UnitX { get; } 
    override public double UnitY { get; } 

    public OneEighth()
    {
        UnitX = 0.0015625;            // 1°    /8/10/8
        UnitY = 0.0010416666666667; // (2/3)°/8/10/8
        Parent = new TertiaryArea();
    }

    /// <summary>
    /// 指定された点を含む8分の1地域区画コードを取得する。
    /// <para>例</para>
    /// <para>MeshCode(130.586, 34.044); // </para>
    /// </summary>    
    override public string MeshCode(double lon, double lat, int coord = 0)
    {
        CheckLatLon(lon, lat);

        string _parent__mesh = Parent.MeshCode(lon, lat);
        GeoPoint org = Parent.SouthWest(_parent__mesh);

        int mesh_y = (int)Math.Truncate(((lat - org.Y) / UnitY));
        int mesh_x = (int)Math.Truncate(((lon - org.X) / UnitX));

        return _parent__mesh + mesh_y.ToString() + mesh_x.ToString();
    }

    /// <summary>
    /// 8分の1地域区画コードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint result = new GeoPoint();
        GeoPoint org = Parent.SouthWest(code.Substring(0, 10));
        switch (GetLatLon(code))
        {
            case 1: result = new GeoPoint(org.X, org.Y); break;
            case 2: result = new GeoPoint(org.X + UnitX, org.Y); break;
            case 3: result = new GeoPoint(org.X, org.Y + UnitY); break;
            case 4: result = new GeoPoint(org.X + UnitX, org.Y + UnitY); break;
        }

        return result;
    }

    /// <summary>
    /// 8分の1地域区画コードから、そのメッシュの左のコードを取得する。
    /// </summary>
    override public string Left(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 10);
        string result = "";
        switch (lastcode)
        {
            case 1: result = Parent.Left(code) + "2"; break;
            case 2: result = code.Substring(0, 10) + "1"; break;
            case 3: result = Parent.Left(code) + "4"; break;
            case 4: result = code.Substring(0, 10) + "3"; break;
        }
        return result;
    }

    /// <summary>
    /// 8分の1地域区画コードから、そのメッシュの上のコードを取得する。
    /// </summary>
    override public string Top(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 10);
        string result = "";
        switch (lastcode)
        {
            case 1: result = code.Substring(0, 10) + "3"; break;
            case 2: result = code.Substring(0, 10) + "4"; break;
            case 3: result = Parent.Top(code) + "1"; break;
            case 4: result = Parent.Top(code) + "2"; break;
        }
        return result;
    }

    /// <summary>
    /// 8分の1地域区画コードから、そのメッシュの右のコードを取得する。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 10);
        string result = "";
        switch (lastcode)
        {
            case 1: result = code.Substring(0, 10) + "2"; break;
            case 2: result = Parent.Right(code) + "1"; break;
            case 3: result = code.Substring(0, 10) + "4"; break;
            case 4: result = Parent.Right(code) + "3"; break;
        }
        return result;
    }

    /// <summary>
    /// 8分の1地域区画コードから、そのメッシュの下のコードを取得する。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        int lastcode = GetLatLon(code);
        string tmp_parent_ = code.Substring(0, 10);
        string result = "";
        switch (lastcode)
        {
            case 1: result = Parent.Under(code) + "3"; break;
            case 2: result = Parent.Under(code) + "4"; break;
            case 3: result = code.Substring(0, 10) + "1"; break;
            case 4: result = code.Substring(0, 10) + "2"; break;
        }
        return result;
    }

    /// <summary>
    /// この地域メッシュの下は今のところ無い
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        return null;
    }

    /// <summary>
    /// コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d[1-4]\d[1-4]\d[1-4]\d"))
        {
            if (except)
                throw new ArgumentException("「" + code + "」は8分の1地域区画コードの書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 8分の1地域区画コードの文字列から末尾部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private int GetLatLon(string code)
    {
        int section = int.Parse(code.Substring(10, 1));
        return section;
    }
}

/// <summary>
/// 基本図図郭の地図情報レベル50000クラス
/// </summary>
class BaseLV50000 : MapFrame
{
    public override MapFrame Parent { get; }
    //注意! 数学的なXYと逆になる
    override public double UnitX { get; }
    override public double UnitY { get; }

    public BaseLV50000()
    {
        UnitX = 30000;
        UnitY = 40000;
        Parent = null;
    }

    /// <summary>
    /// 指定された点を含む地図情報レベル50000の図郭コードを取得する。
    /// </summary>
    override public string MeshCode(double x, double y, int coord)
    {
        CheckBaseCode(x, y, coord);

        char base_x = (char)('T' - (int)((x + 300000) / UnitX));
        char base_y = (char)('A' + (int)((y + 160000) / UnitY));

        return coord.ToString().PadLeft(2, '0') + base_x + base_y;
    }

    /// <summary>
    /// 地図情報レベル50000の図郭コードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        double x = 270000 - (xy.X - 'A') * UnitX;
        double y = (xy.Y - 'A') * UnitY - 160000;

        return new GeoPoint(x, y);
    }

    /// <summary>
    /// 地図情報レベル50000の図郭コードから、その左のコードを取得する。
    /// <para> codeより左に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Left(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        if ((char)xy.Y == 'A') return "";
        xy.Y--;

        return code.Substring(0, 2) + (char)(xy.X) + (char)(xy.Y);
    }

    /// <summary>
    /// 地図情報レベル50000の図郭コードから、その上のコードを取得する。
    /// <para> codeより上に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Top(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        if ((char)xy.X == 'A') return "";
        xy.X--;

        return code.Substring(0, 2) + (char)(xy.X) + (char)(xy.Y);
    }

    /// <summary>
    /// 地図情報レベル50000の図郭コードから、その右のコードを取得する。
    /// <para> codeより右に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        if ((char)xy.Y == 'H') return "";
        xy.Y++;

        return code.Substring(0, 2) + (char)(xy.X) + (char)(xy.Y);
    }

    /// <summary>
    /// 地図情報レベル50000の図郭コードから、その下のコードを取得する。
    /// <para> codeより下に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        if ((char)xy.X == 'T') return "";
        xy.X++;

        return code.Substring(0, 2) + (char)(xy.X) + (char)(xy.Y);
    }

    /// <summary>
    /// LV50000から、LV5000のコードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        List<string> tmp = new List<string>(100); //適当
        for (int i = 0; i <= 9; i++)
            for (int j = 0; j <= 9; j++)
                tmp.Add(code + i.ToString() + j.ToString());
        return tmp.ToArray();
    }

    /// <summary>
    /// 図郭コードのチェック
    /// </summary>
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !((code.Substring(0, 2) != "00") && (Regex.IsMatch(code, @"[0-1]\d[a-tA-T][a-hA-H]"))))
        {
            if (except)
                throw new ArgumentException("「" + code + "」はBaseLv50000の書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 地図情報レベル50000の図郭コード「code」からX部分とY部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// <para>「09LD」ならX部分は「L」、Y部分は「D」</para>
    /// </summary>
    private GeoPoint GetXY(string code)
    {
        string x = code.Substring(2, 1).ToUpper();
        string y = code.Substring(3, 1).ToUpper();

        return new GeoPoint(x.ToCharArray()[0], y.ToCharArray()[0]);
    }
}

/// <summary>
/// 基本図図郭の地図情報レベル5000クラス
/// </summary>
class BaseLV5000 : MapFrame
{
    public override MapFrame Parent { get; }
    //注意! 数学的なXYと逆になる
    override public double UnitX { get; } 
    override public double UnitY { get; }

    public BaseLV5000()
    {
        UnitX = 3000;
        UnitY = 4000;
        Parent = new BaseLV50000();
    }

    /// <summary>
    /// 指定された点を含む地図情報レベル5000の図郭コードを取得する。
    /// </summary>    
    override public string MeshCode(double x, double y, int coord)
    {
        if (coord >= 20) coord -= 20; //旧座標を与えられた場合

        CheckBaseCode(x, y, coord);

        GeoPoint pnt = Parent.SouthWest(Parent.MeshCode(x, y, coord));
        int a = 9 - (int)((x - pnt.X) / UnitX);
        int b = (int)((y - pnt.Y) / UnitY);

        return Parent.MeshCode(x, y, coord) + a + b;
    }

    /// <summary>
    /// 地図情報レベル5000の図郭コードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint sw = GetXY(code);
        GeoPoint org = Parent.SouthWest(code.Substring(0, 4));

        double x = org.X + (9 - sw.X) * UnitX;
        double y = org.Y + sw.Y * UnitY;

        return new GeoPoint(x, y);
    }

    /// <summary>
    /// 地図情報レベル5000の図郭コードから、その左のコードを取得する。
    /// <para> codeより左に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Left(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.Y == 0)
        {
            if ((tmp_parent_ = Parent.Left(tmp_parent_)) == "") return "";
            xy.Y = 9;
        }
        else
            xy.Y--;

        return tmp_parent_ + xy.X.ToString() + xy.Y.ToString();
    }

    /// <summary>
    /// 地図情報レベル5000の図郭コードから、その上のコードを取得する。
    /// <para> codeより上に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Top(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.X == 0)
        {
            if ((tmp_parent_ = Parent.Top(tmp_parent_)) == "") return "";
            xy.X = 9;
        }
        else
            xy.X--;

        return tmp_parent_ + xy.X.ToString() + xy.Y.ToString();
    }

    /// <summary>
    /// 地図情報レベル5000の図郭コードから、その右のコードを取得する。
    /// <para> codeより右に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.Y == 9)
        {
            if ((tmp_parent_ = Parent.Right(code)) == "") return "";
            xy.Y = 0;
        }
        else
            xy.Y++;

        return tmp_parent_ + xy.X.ToString() + xy.Y.ToString();
    }

    /// <summary>
    /// 地図情報レベル5000の図郭コードから、その下のコードを取得する。
    /// <para> codeより下に図郭コードが無ければ空の文字列を返す。</para>
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        GeoPoint xy = GetXY(code);
        string tmp_parent_ = code.Substring(0, 4);
        if (xy.X == 9)
        {
            if ((tmp_parent_ = Parent.Under(code)) == "") return "";
            xy.X = 0;
        }
        else
            xy.X++;

        return tmp_parent_ + xy.X.ToString() + xy.Y.ToString();
    }

    /// <summary>
    /// LV5000コードから、LV2500のコードを取得する。
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        List<string> tmp = new List<string>(4);
        for (int i = 0; i <= 4; i++)
            tmp.Add(code + i.ToString());
        return tmp.ToArray();
    }

    /// <summary>
    /// 図郭コードのチェック
    /// </summary>
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !((code.Substring(0, 2) != "00") && (Regex.IsMatch(code, @"[0-1]\d[a-tA-T][a-hA-H]\d\d"))))
        {
            if (except)
                throw new ArgumentException("「" + code + "」はBaseLv5000の書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 地図情報レベル5000の図郭コード「code」からX部分とY部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// <para>「09LD71」ならX部分は「7」、Y部分は「1」</para>
    /// </summary>
    private GeoPoint GetXY(string code)
    {
        int x = int.Parse(code.Substring(4, 1));
        int y = int.Parse(code.Substring(5, 1));

        return new GeoPoint(x, y);
    }
}

/// <summary>
/// 基本図図郭の地図情報レベル2500クラス
/// </summary>
class BaseLV2500 : MapFrame
{
    public override MapFrame Parent { get; }
    //注意! 数学的なXYと逆になる
    override public double UnitX { get; } 
    override public double UnitY { get; } 

    public BaseLV2500()
    {
        UnitX = 1500;
        UnitY = 2000;
        Parent = new BaseLV5000();
    }

    /// <summary>
    /// 指定された点を含む地図情報レベル2500の図郭コードを取得する。
    /// </summary>    
    override public string MeshCode(double x, double y, int coord)
    {
        CheckBaseCode(x, y, coord);

        GeoPoint pnt = Parent.SouthWest(Parent.MeshCode(x, y, coord));
        int a = (int)((x - pnt.X) / UnitX);
        int b = (int)((y - pnt.Y) / UnitY);

        string result = "";
        if ((a == 0) && (b == 0)) result = Parent.MeshCode(x, y, coord) + '3';
        else if ((a == 0) && (b == 1)) result = Parent.MeshCode(x, y, coord) + '4';
        else if ((a == 1) && (b == 0)) result = Parent.MeshCode(x, y, coord) + '1';
        else if ((a == 1) && (b == 1)) result = Parent.MeshCode(x, y, coord) + '2';

        return result;
    }

    /// <summary>
    /// 地図情報レベル2500の図郭コードの南西の座標を取得する。
    /// </summary>
    override public GeoPoint SouthWest(string code)
    {
        CheckFormat(code);

        GeoPoint result = new GeoPoint();
        GeoPoint org = Parent.SouthWest(code.Substring(0, 6));
        switch (GetSection(code))
        {
            case 1: result = new GeoPoint(org.X + UnitX, org.Y); break;
            case 2: result = new GeoPoint(org.X + UnitX, org.Y + UnitY); break;
            case 3: result = new GeoPoint(org.X, org.Y); break;
            case 4: result = new GeoPoint(org.X, org.Y + UnitY); break;
        }

        return result;
    }

    /// <summary>
    /// 地図情報レベル2500の図郭コードから、その左のコードを取得する。
    /// codeより左に図郭コードが無ければ空の文字列を返す。
    /// </summary>    
    override public string Left(string code)
    {
        CheckFormat(code);

        string result = "";
        string tmp_parent_ = code.Substring(0, 6);
        switch (GetSection(code))
        {
            case 1:
                if (Parent.Left(tmp_parent_) == "") return "";
                result = Parent.Left(code) + "2"; break;
            case 2: result = code.Substring(0, 6) + "1"; break;
            case 3:
                if (Parent.Left(tmp_parent_) == "") return "";
                result = Parent.Left(code) + "4"; break;
            case 4: result = code.Substring(0, 6) + "3"; break;
        }

        return result;
    }

    /// <summary>
    /// 地図情報レベル2500の図郭コードから、その上のコードを取得する。
    /// codeより上に図郭コードが無ければ空の文字列を返す。
    /// </summary>    
    override public string Top(string code)
    {
        CheckFormat(code);

        string result = "";
        string tmp_parent_ = code.Substring(0, 6);
        switch (GetSection(code))
        {
            case 1:
                if (Parent.Top(tmp_parent_) == "") return "";
                result = Parent.Top(code) + "3"; break;
            case 2:
                if (Parent.Top(tmp_parent_) == "") return "";
                result = Parent.Top(code) + "4"; break;
            case 3: result = code.Substring(0, 6) + "1"; break;
            case 4: result = code.Substring(0, 6) + "2"; break;
        }

        return result;
    }

    /// <summary>
    /// 地図情報レベル2500の図郭コードから、その右のコードを取得する。
    /// codeより右に図郭コードが無ければ空の文字列を返す。
    /// </summary>    
    override public string Right(string code)
    {
        CheckFormat(code);

        string result = "";
        string tmp_parent_ = code.Substring(0, 6);
        switch (GetSection(code))
        {
            case 1: result = code.Substring(0, 6) + "2"; break;
            case 2:
                if (Parent.Right(tmp_parent_) == "") return "";
                result = Parent.Right(code) + "1"; break;
            case 3: result = code.Substring(0, 6) + "4"; break;
            case 4:
                if (Parent.Right(tmp_parent_) == "") return "";
                result = Parent.Right(code) + "3"; break;
        }

        return result;
    }

    /// <summary>
    /// 地図情報レベル2500の図郭コードから、その下のコードを取得する。
    /// codeより下に図郭コードが無ければ空の文字列を返す。
    /// </summary>    
    override public string Under(string code)
    {
        CheckFormat(code);

        string result = "";
        string tmp_parent_ = code.Substring(0, 6);

        switch (GetSection(code))
        {
            case 1: result = tmp_parent_ + "3"; break;
            case 2: result = tmp_parent_ + "4"; break;
            case 3:
                if (Parent.Under(tmp_parent_) == "") return "";
                result = Parent.Under(code) + "1"; break;
            case 4:
                if (Parent.Under(tmp_parent_) == "") return "";
                result = Parent.Under(code) + "2"; break;
        }

        return result;
    }

    /// <summary>
    /// このレベルより下のレベルは未だ実装していない
    /// </summary>    
    override public string[] ChildCodes(string code)
    {
        return null;
    }

    /// <summary>
    /// 図郭コードの文字列チェック
    /// </summary>    
    override public bool CheckFormat(string code, bool except = true)
    {
        if ((code == "") || !((code.Substring(0, 2) != "00") && (Regex.IsMatch(code, @"[0-1]\d[a-tA-T][a-hA-H]\d\d[1-4]"))))
        {
            if (except)
                throw new ArgumentException("「" + code + "」はBaseLv2500の書式ではありません。");
            else
                return false;
        }
        return true;
    }

    /// <summary>
    /// 地図情報レベル2500の図郭コードの文字列から区画部分を取得する。この関数呼ぶ前に引数のチェックはしておいて!
    /// </summary>    
    private int GetSection(string code)
    {
        int section = int.Parse(code.Substring(6, 1));
        return section;
    }
}

/// <summary>
/// 指定したコードから、MapFrameの子クラスを取得するためのクラス
/// </summary>
static class MapFrameGetter
{
    static public MapFrame GetMapFlameClass(string code)
    {
        if (code == "") return null;
        else if (Regex.IsMatch(code, @"[0-1]\d[a-tA-T][a-hA-H]\d\d[1-4]")) return new BaseLV2500();
        else if (Regex.IsMatch(code, @"[0-1]\d[a-tA-T][a-hA-H]\d\d")) return new BaseLV5000();
        else if (Regex.IsMatch(code, @"[0-1]\d[a-tA-T][a-hA-H]")) return new BaseLV50000();
        else if (Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d[1-4]\d[1-4]\d[1-4]\d")) return new OneEighth();
        else if (Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d[1-4]\d[1-4]\d")) return new OneQuarter();
        else if (Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d[1-4]\d")) return new OneHalf();
        else if (Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}\d\d")) return new TertiaryArea();
        else if (Regex.IsMatch(code, @"\d\d\d\d[0-7]{2}")) return new SecondaryAreaMesh();
        else if (Regex.IsMatch(code, @"\d\d\d\d")) return new PrimaryAreaMesh();

        return null;
    }
}
