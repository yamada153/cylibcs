﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;

class Grid
{
    public double[,] Data { get; }
    public double MinX { get; }
    public double MinY { get; }
    public double Span { get; }
    public double NoData { get; }

    /// <summary>
    /// 指定したインデックスのポイントを取得する
    /// </summary>
    public GeoPoint Point(int iX, int iY)
    {
        return ToGeo(iX, iY, MinX, MinY, Span);
    }

    /// <summary>
    /// 最小矩形範囲を返す。戻り値の配列のインデックス0 = 最大X、1 = 最大Y、2 = 最小X、3 = 最小Y
    /// </summary>
    public double[] Bound()
    {
        double[] result = new double[4];
        result[2] = MinX - Span / 2;
        result[3] = MinY - Span / 2;
        result[0] = result[2] + Span * Array2D.Width(Data);
        result[1] = result[3] + Span * Array2D.Height(Data);

        return result;
    }

    /// <summary>
    /// 指定した座標の値を補間して取得する
    /// </summary>
    public double Interpolation(double x, double y)
    {
        //補間したい点の周囲4点が有効かな?
        GeoPoint index = ToIndex(x, y, MinX, MinY, Span);
        int iX = (int)index.X;
        int iY = (int)index.Y;
        if (!IndexCheck(iX, iY)) return NoData;
        if (!IndexCheck(iX, iY + 1)) return NoData;
        if (!IndexCheck(iX + 1, iY + 1)) return NoData;
        if (!IndexCheck(iX + 1, iY)) return NoData;
        GeoPoint p1 = ToGeo(iX, iY, MinX, MinY, Span);
        GeoPoint p2 = ToGeo(iX + 1, iY + 1, MinX, MinY, Span);
        double x1 = p1.X;
        double x2 = p2.X;
        double y1 = p1.Y;
        double y2 = p2.Y;
        //周囲4点の値が全て有効かな?
        double a = Data[iX, iY];
        double b = Data[iX, iY + 1];
        double c = Data[iX + 1, iY + 1];
        double d = Data[iX + 1, iY];
        if ((a == NoData) || (b == NoData) || (c == NoData) || (d == NoData)) return NoData;

        double z1 = ((x - x1) / (x2 - x1)) * (d - a) + a;
        double z2 = ((x - x1) / (x2 - x1)) * (c - b) + b;
        return ((y - y1) / (y2 - y1)) * (z2 - z1) + z1;
    }

    /// <summary>
    /// 指定した座標の値を補間して取得する
    /// </summary>
    public double Sampling(GeoPoint pnt)
    {
        return Interpolation(pnt.X, pnt.Y);
    }

    /// <summary>
    /// 指定した座標の値を補間して取得する。平均値を返す
    /// </summary>
    public double Sampling(GeoPoint[] pnts)
    {
        double sum = 0;
        for (int i = 0; i < pnts.Length; i++)
            sum += Interpolation(pnts[i].X, pnts[i].Y);
        return sum / pnts.Length;
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public Grid(double[,] data, double minx, double miny, double span, double nodata)
    {
        Data = data;
        MinX = minx;
        MinY = miny;
        Span = span;
        NoData = nodata;
    }

    /// <summary>
    /// 指定したインデックスが有効ならtrueを返す
    /// </summary>
    private bool IndexCheck(int x, int y)
    {
        if ((x < 0) || (y < 0) || (x >= Array2D.Width(Data)) || (y >= Array2D.Height(Data))) return false;
        return true;
    }

    private bool CheckNeighborhood(int i, int j)
    {
        if (Data[j - 1, i - 1] == NoData) return false;
        if (Data[j - 1, i] == NoData) return false;
        if (Data[j - 1, i + 1] == NoData) return false;
        if (Data[j, i - 1] == NoData) return false;
        if (Data[j, i] == NoData) return false;
        if (Data[j, i + 1] == NoData) return false;
        if (Data[j + 1, i - 1] == NoData) return false;
        if (Data[j + 1, i] == NoData) return false;
        if (Data[j + 1, i + 1] == NoData) return false;

        return true;
    }

    private GeoPoint ToIndex(double x, double y, double minx, double miny, double span)
    {
        return new GeoPoint(Math.Truncate((x - minx) / span), Math.Truncate((y - miny) / span));
    }

    private GeoPoint ToGeo(int iX, int iY, double minx, double miny, double span)
    {
        return new GeoPoint(span * iX + minx, span * iY + miny);
    }

    public GeoPoint IndexToGeometry(int iX, int iY)
    {
        return ToGeo(iX, iY, MinX, MinY, Span);
    }
}
