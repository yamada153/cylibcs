﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

partial class FileList : UserControl
{
    int[] _open_folder_columns = null;
    int[] _open_file_columns = null;
    string _prefix = "";

    public void ReSize(int width, int height)
    {
        this.Width = width;
        this.Height = height;

        dataGridView1.Width = this.Width - 1;
        dataGridView1.Height = this.Height - 1;
    }

    public string Prefix {
        set{ _prefix = value; }
        get {return _prefix == null ? "" : _prefix ; }
    }

    /// <summary>
    /// セルをダブルクリックしたときに、セルの値をエクスプローラーで開く列を指定する
    /// </summary>
    public void SetOpenFolderColumns(int[] cols)
    {
        _open_folder_columns = cols;
    }

    /// <summary>
    /// セルをダブルクリックしたときに、セルの値を関連付けられたアプリケーションで開く列を指定する
    /// </summary>
    public void SetOpenFileColumns(int[] cols)
    {
        _open_file_columns = cols;
    }

    /// <summary>
    /// StringTableの内容をDataGridViewに書き込む
    /// </summary>
    public void StringTableToDataGridView(StringTable st, int cell_width)
    {
        //フィールド設定
        string[] fields = st.GetFieldNames();
        foreach (string f in fields)
        {
            DataGridViewTextBoxColumn textColumn = new DataGridViewTextBoxColumn();
            textColumn.Name = f;
            textColumn.HeaderText = f;
            textColumn.ReadOnly = true;
            textColumn.Width = cell_width;
            dataGridView1.Columns.Add(textColumn);
        }
        //各行に書き込む
        for (int i = 0; i < st.Count(); i++)
        {
            dataGridView1.Rows.Add();
            for (int j = 0; j < fields.Count(); j++)
                dataGridView1[j, i].Value = st[i, fields[j]];
        }
    }

    public StringTable ToStringTable()
    {
        int colnum = dataGridView1.Columns.Count;
        int rownum = dataGridView1.Rows.Count;

        //フィールド取得
        string[] fields = new string[colnum];
        for (int i = 0; i < colnum; i++)
            fields[i] = dataGridView1.Columns[i].Name;
        StringTable result = new StringTable(fields);
        //本体
        for (int i = 0; i < rownum; i++)
        {
            result.Add(new string[] { });
            for (int j = 0; j < colnum; j++)
            {
                result[i, fields[j]] = (string)dataGridView1[j, i].Value;
                if (result[i, fields[j]] == null) result[i, fields[j]] = "";
            }
        }
        return result;
    }

    /// <summary>
    /// DataGridViewの行、列をヘッダーを含めて削除する。
    /// </summary>
    public void DataGridViewClear()
    {
        int c = dataGridView1.Columns.Count;
        for (int i = 0; i < c; i++)
            dataGridView1.Columns.RemoveAt(0);
    }

    private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
        int row = e.RowIndex;
        int col = e.ColumnIndex;

        if ((row < 0) || (col < 0))
            return;

        //フォルダを開く
        if((_open_folder_columns != null) && _open_folder_columns.Contains(col))
        {
            string dirname = _prefix + (string)dataGridView1[col, row].Value;
            if (!Directory.Exists(dirname))
                throw new Exception("「" + dirname  + "」はフォルダではありません");

            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
            psi.FileName = "explorer.exe";
            psi.Arguments = @"""" + dirname + @"""";
            System.Diagnostics.Process.Start(psi);
        }
        else if((_open_file_columns != null) && _open_file_columns.Contains(col))
        {
            string tmp = _prefix + (string)dataGridView1[col, row].Value;
            if (tmp == null) return;
            try
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                psi.FileName = "notepad.exe";
                psi.Arguments = @"""" + tmp + @"""";
                System.Diagnostics.Process.Start(psi);
            }
            catch (Win32Exception)
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                psi.FileName = "notepad.exe";
                psi.Arguments = @"""" + tmp + @"""";
                System.Diagnostics.Process.Start(psi);
            }
        }
    }

    public FileList()
    {
        InitializeComponent();
        dataGridView1.AutoGenerateColumns = false;
    }
}