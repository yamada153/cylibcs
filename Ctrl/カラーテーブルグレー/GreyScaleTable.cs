﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

public partial class GreyScaleTable: UserControl
{
    private object _old;

    /// <summary>
    /// 任意の値から、光度を取得する。
    /// </summary>
    public int GetKodo(double elev)
    {
        if (double.IsNaN(GetValueFromRow(0))) return -1;
        if (elev < GetValueFromRow(0)) return GetKodoFromRow(0);

        if (double.IsNaN(GetValueFromRow(dataGridView1.RowCount - 1))) return -1;
        if (elev >= GetValueFromRow(dataGridView1.RowCount - 1)) return GetKodoFromRow(dataGridView1.RowCount - 1);

        for(int i = 0; i < dataGridView1.RowCount - 1; i++)
        {
            double pre_elev = GetValueFromRow(i);
            double nxt_elev = GetValueFromRow(i+1);
            double pre_color = GetKodoFromRow(i);
            double nxt_color = GetKodoFromRow(i + 1);

            if (!((pre_elev <= elev) && (elev < nxt_elev))) continue;
            return (int)(((nxt_color - pre_color) / (nxt_elev - pre_elev)) * (elev - pre_elev) + pre_color);
        }
        return -1;
    }

    /// <summary>
    /// 指定した行の光度を取得する。失敗した場合は0を返す。(真っ黒)
    /// </summary>
    private int GetKodoFromRow(int row)
    {
        int result = -1;

        if (dataGridView1[1, row].Value == null) return 0;
        if (int.TryParse(dataGridView1[1, row].Value.ToString(), out result) == false) return 0;
        return result;
    }

    /// <summary>コンストラクタ
    /// <para></para>
    /// </summary>
    public GreyScaleTable()
    {
        InitializeComponent();
        dataGridView1.Rows.Add();
        dataGridView1.Rows.Add();

        dataGridView1[0, 0].Value = 0;
        dataGridView1[0, 1].Value = 10;
        dataGridView1[1, 0].Value = 0;
        dataGridView1[1, 1].Value = 255;
        RepaintCell();

        dataGridView1.AllowUserToAddRows = false;
    }

    /// <summary>
    /// 光度列の値を使って、色列を塗りつぶす。
    /// </summary>
    private void RepaintCell()
    {
        for (int i = 0; i < dataGridView1.Rows.Count; i++)
        {
            int c = GetKodoFromRow(i);
            dataGridView1[2, i].Style.BackColor = System.Drawing.Color.FromArgb(c, c, c);
        }
    }

    /// <summary>
    /// 値列を使って、数値で並び替え
    /// <para>例外</para>
    /// <para>InvalidOperationException</para>
    /// </summary>
    public void Sort()
    {
        dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);
    }

    /// <summary>
   ///  数値で並び替えるための判定関数 
   /// </summary>
    /// <see cref="https://social.msdn.microsoft.com/forums/ja-JP/167f2ac9-d51c-4b62-8523-64692800de96/datagridview"/>
    private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
    {
        double a, b;
        if (double.TryParse(e.CellValue1.ToString(), out a) == false) return;
        if (double.TryParse(e.CellValue2.ToString(), out b) == false) return;
        double c = a - b;
        if (c < 0)
            e.SortResult = -1;
        else
            e.SortResult = 1;
        e.Handled = true;
    }

    /// <summary>
    /// 編集モードが終わった場所が値列なら並び替え、光度列なら色列の塗りつぶしが実行される。
    /// </summary>
    private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
    {
        if (e.ColumnIndex == 0)
        {
            try { Sort(); }
            catch(Exception ex)
            {
                dataGridView1[0, e.RowIndex].Value = _old;
            }
        }
        else if (e.ColumnIndex == 1)
        {
            RepaintCell();
        }
    }

    /// <summary>
    /// 指定した行の値を取得する。失敗した場合はNaNを返す。
    /// </summary>
    private double GetValueFromRow(int row)
    {
        double result = double.NaN;

        if (dataGridView1[0, row].Value == null) return double.NaN;
        if (double.TryParse(dataGridView1[0, row].Value.ToString(), out result) == false) return double.NaN;
        return result;
    }

    /// <summary>
    /// 最初と最後の行は削除させない。
    /// </summary>
    private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
    {
        if ((e.Row.Index == 0) || (e.Row.Index == dataGridView1.RowCount)) e.Cancel = true;
    }

    /// <summary>
    /// 最後の行以外で行ヘッダをダブルクリックした時、下の行に、値入りで1行挿入される。
    /// </summary>
    private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
        if (e.ColumnIndex != -1) return;
        if (e.RowIndex >= dataGridView1.RowCount - 1) return;
        if (0 > e.RowIndex) return;

        dataGridView1.Rows.Insert(e.RowIndex+1);

        double elev = (GetValueFromRow(e.RowIndex) + GetValueFromRow(e.RowIndex + 2)) / 2;
        double color = (GetKodoFromRow(e.RowIndex) + GetKodoFromRow(e.RowIndex + 2)) / 2;
        dataGridView1[0, e.RowIndex + 1].Value = elev;
        dataGridView1[1, e.RowIndex + 1].Value = color;

        RepaintCell();
    }

    /// <summary>
    /// </summary>
    private void Load_Click(object sender, EventArgs e)
    {
        if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
        
        StringTable st = SeparateText.LoadFromFile(openFileDialog1.FileName, ',');

        int cnt = dataGridView1.RowCount;
        for (int i = 0; i < cnt; i++)
            dataGridView1.Rows.RemoveAt(0);

        for (int i = 0; i < st.Count(); i++)
        {
            dataGridView1.Rows.Add();

            dataGridView1[0, i].Value = st[i, "値"];
            dataGridView1[1, i].Value = st[i, "光度"];
        }

        Sort();
        RepaintCell();
    }

    /// <summary>
    /// </summary>
    private void Save_Click(object sender, EventArgs e)
    {
        if (saveFileDialog1.ShowDialog() != DialogResult.OK) return;

        StringTable st = new StringTable(new string[] { "値", "光度" });

        for (int i = 0; i < dataGridView1.Rows.Count; i++)
            st.Add(new string[] { dataGridView1[0, i].Value.ToString(), dataGridView1[1, i].Value.ToString() });

        SeparateText.SaveToFile(st, saveFileDialog1.FileName, ',');
    }

    /// <summary>
    /// </summary>
    private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
    {
        _old = dataGridView1[0, e.RowIndex].Value;
    }
}

partial class Real2DEx
{
    /// <summary>
    /// 2次元配列の内容をBMPに書き出す。
    /// </summary>
    public void SaveToBitmap(string filename, GreyScaleTable ct, bool nodataprint = false)
    {
        Bitmap bmp = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);

        for (int i = 0; i < Height; i++)
            for (int j = 0; j < Width; j++)
                if ((nodataprint == false) && (_data[j, i] == _nodata))
                    bmp.SetPixel(j, i, Color.FromArgb(-9999));
                else
                {
                    int color = ct.GetKodo(_data[j, i]);
                    bmp.SetPixel(j, i, Color.FromArgb(color, color, color));
                }
        bmp.Save(filename);
    }
}