﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

class StringTable
{
    protected List<string> _fields;
    protected Dictionary<string, int> _fielddic;
    protected List<List<string>> _value;

    public string ToString(char delimiter = ',')
    {
        string result = string.Join(delimiter.ToString(), GetFieldNames()) + "\n";

        for (int i = 0; i < Count(); i++)
           result += string.Join(delimiter.ToString(), GetStringsFromRowNumber(i)) + "\n";

        return result.Substring(0, result.Length - 1);
    }

    public string this[int row, string fieldname]
    {
        get
        {
            int col = CheckFieldName("Getインデクサ", fieldname);
            return _value[col][row];
        }
        set
        {
            _value[CheckFieldName("Setインデクサ", fieldname)][row] = value;
        }
    }

    public string this[int row, int col]
    {
        get
        {
            return _value[col][row];
        }
        set
        {
            _value[col][row] = value;
        }
    }

    /// <summary>
    /// 行数を返す。
    /// </summary>
    public int Count()
    {
        return _value[0].Count;
    }

    /// <summary>
    /// フィールド数を返す。
    /// </summary>
    public int FieldCount()
    {
        return _fields.Count;
    }

    /// <summary>
    /// テーブルの末尾にitemsを追加する。引数がnullまたは要素内にnullがあれば、空文字に置き換えられる。
    /// </summary>
    public void Add(string[] items)
    {
        Array.Resize(ref items, FieldCount());
        for (int i = 0; i < FieldCount(); i++)
        {
            if (items[i] == null)
                _value[i].Add("");
            else
                _value[i].Add(items[i]);
        }
    }

    /// <summary>
    /// 行を削除する。
    /// </summary>
    public void DeleteRow(int row)
    {
        CheckRow("DeleteRow", row);

        for (int i = 0; i < _fields.Count; i++)
            _value[i].RemoveAt(row);
    }

    /// <summary>
    /// 行を挿入する。指定した行番号が0以下なら0に、最大行番号以上なら最大行番号になる。
    /// </summary>
    public void InsertRow(int row, string[] items)
    {
        if (row < 0) row = 0;

        Array.Resize(ref items, _fields.Count);

        if (row >= Count())
        {
            Add(items);
            return;
        }
        else
        {
            for (int i = 0; i < _fields.Count; i++)
                if (items[i] == null)
                    _value[i].Insert(row, "");
                else
                    _value[i].Insert(row, items[i]);
        }
    }

    /// <summary>
    /// 指定した列を削除する。
    /// </summary>
    public void DeleteField(string fieldname)
    {
        int index = CheckFieldName("DeleteField", fieldname);

        _fields.RemoveAt(index);
        SetDic();

        _value.RemoveAt(index);
    }

    /// <summary>
    /// フィールド名を変更する。
    /// </summary>
    public void ChangeField(string fieldname, string newfieldname)
    {
        if (string.IsNullOrEmpty(newfieldname))
            throw new ArgumentException("StringTable.ChangeFieldの引数「newfieldname」が空文字列かnullを受け取りました。");

        int index = CheckFieldName("ChangeField", fieldname);

        _fields[index] = newfieldname;
        SetDic();
    }

    /// <summary>
    /// indexの次の列に新しい列を挿入する。挿入された列は""で埋められる。
    /// </summary>
    public void InsertField(int idx, string fieldname)
    {
        if (string.IsNullOrEmpty(fieldname) || string.IsNullOrEmpty(fieldname.Trim()))
            throw new ArgumentException("StringTable.InsertFieldの引数「fieldname」が空文字列かnullを受け取りました。");

        if (idx < 0) idx = 0;
        if (idx >= _fields.Count) idx = _fields.Count;

        _fields.Insert(idx, fieldname);
        try
        {
            SetDic();
        }
        catch (ArgumentException e)
        {
            throw new ArgumentException("StringTable.InsertFieldの引数「fieldname(" + fieldname + ")」が既存のフィールドの" + e.Message + "番目と同じです。");
        }

        List<string> l = new List<string>();
        for (int i = 0; i < Count(); i++)
            l.Add("");
        _value.Insert(idx, l);
    }

    /// <summary>
    /// フィールド名を返す。
    /// </summary>
    public string[] GetFieldNames()
    {
        return _fields.ToArray();
    }

    /// <summary>
    /// 指定した列の値を返す。
    /// </summary>
    public string[] GetStringsFromColumnName(string fieldname)
    {
        int index = CheckFieldName("GetStringsFromColumnName", fieldname);

        return _value[index].ToArray();
    }

    /// <summary>
    /// 指定した列の値を返す。
    /// </summary>
    public string[] GetStringsFromColumnNumber(int i)
    {
        return _value[i].ToArray();
    }

    /// <summary>
    /// 指定した行の値を返す。
    /// </summary>
    public string[] GetStringsFromRowNumber(int row)
    {
        string[] strs = new string[FieldCount()];
        for (int i = 0; i < FieldCount(); i++)
            strs[i] = _value[i][row];
        return strs;
    }

    /// <summary>
    /// 指定したフィールド名が存在すればtrueを返す
    /// </summary>
    public bool FieldExist(string fieldname)
    {
        if (string.IsNullOrEmpty(fieldname)) return false;

        return IndexOf(fieldname) == -1 ? false : true;
    }

    /// <summary>
    /// 指定したフィールドのインデックス番号を返す。フィールドが存在しなければ-1を返す。
    /// </summary>
    private int IndexOf(string fieldname)
    {
        int result = 0;

        if (_fielddic.TryGetValue(fieldname, out result))
            return result;
        else
            return -1;
    }

    /// <summary>
    /// フィールド名を指定して、空のStringTableを作成する。
    /// </summary>
    public StringTable(string[] fieldnames, int capacity = 100)
    {
        Init(fieldnames, capacity);
    }

    /// <summary>
    /// コピーコンストラクタ
    /// </summary>
    public StringTable(StringTable src)
    {
        Init(src.GetFieldNames(), src.Count());
        for (int i = 0; i < src.Count(); i++)
            Add(src.GetStringsFromRowNumber(i));
    }

    /// <summary>
    /// StringTableの初期設定をする
    /// </summary>
    private void Init(string[] fieldnames, int capacity)
    {
        if(fieldnames == null) throw new ArgumentException("StringTableの初期化に失敗しました。fieldnamesがNULLです。");

        /////フィールド/////
        _fields = new List<string>(fieldnames.Count());
        _fielddic = new Dictionary<string, int>(StringComparer.CurrentCultureIgnoreCase);
        foreach (string s in fieldnames)
        {
            string tmp = s.Trim();
            if (tmp == "")
                throw new ArgumentException("StringTableの初期化に失敗しました。フィールド名に空白を指定することはできません。");
            _fields.Add(s);
        }
        try
        {
            SetDic();
        }
        catch (ArgumentException e)
        {
            throw new ArgumentException(e.Message + "\nフィールド名が重複しています。「" + string.Join(",", _fields.ToArray()) + "」");
        }
        /////本体/////
        _value = new List<List<string>>();
        int c = (int)capacity;
        for (int i = 0; i < fieldnames.Count(); i++)
            _value.Add(new List<string>(c));
    }

    /// <summary>
    /// フィールド名とその添字番号を辞書型に格納する。
    /// フィールド配列にNULLが存在する場合、「NullReferenceException」が発生する。
    /// フィールド名が空文字列または空白文字の場合、「InvalidDataException」が発生する。
    /// フィールド名が重複する場合、「ArgumentException」が発生する。
    /// </summary>
    private void SetDic()
    {
        for(int i = 0; i < _fields.Count(); i++)
        {
            if (_fields[i] == null)
                throw new NullReferenceException(i.ToString());
            else if (_fields[i].Trim() == "")
                throw new InvalidDataException(i.ToString());
        }

        _fielddic.Clear();
        for (int i = 0; i < _fields.Count(); i++)
            try
            {
                _fielddic.Add(_fields[i], i);
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException(i.ToString());
            }            
    }

    /// <summary>
    /// 引数fieldnameをチェックする。有効なフィールド名なら、フィールドのインデックス番号が返る
    /// </summary>
    private int CheckFieldName(string functionname, string fieldname)
    {
        if(string.IsNullOrEmpty(fieldname))
            throw new ArgumentException("StringTable." + functionname + "の引数「fieldname」が空文字列かnullを受け取りました。");

        int index = IndexOf(fieldname);
        if(index == -1) throw new ArgumentException("StringTable." + functionname + "の引数「fieldname("+ fieldname +")」は存在しません。");

        return index;
    }

    private void CheckRow(string functionname, int row)
    {
        if (row < 0)
            throw new ArgumentOutOfRangeException("StringTable." + functionname + "の引数「row」が負数「" + row.ToString() + "」を受け取りました。");
        if (row >= Count())
            throw new ArgumentOutOfRangeException("StringTable." + functionname + "の引数「row(" + row.ToString() + ")」が最大行数「" + Count().ToString() + "」以上です。");
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestStringTable
{
    static public void InsertField(System.Windows.Forms.RichTextBox memo)
    {
        StringTable st = new StringTable(new string[] { "test1", "test2", "test3" });
        st.Add(new string[] { "value1", "value2", "value3" });
        st.Add(new string[] { "value4", "value5", "value6" });
        st.Add(new string[] { "value7", "value8", "value9" });
        memo.AppendText(st.ToString() + "\n----------------------------------------\n");

        StringTable copy_st = null;
        copy_st = new StringTable(st); copy_st.InsertField(-1, "test4"); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertField(0, "test4"); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertField(1, "test4"); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertField(2, "test4"); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertField(3, "test4"); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertField(4, "test4"); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");

        try { copy_st = new StringTable(st); copy_st.InsertField(1, null); } catch(Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.InsertField(1, ""); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.InsertField(1, " "); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.InsertField(1, "\t"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.InsertField(1, "test1"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.InsertField(1, "TesT1"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void ChangeField(System.Windows.Forms.RichTextBox memo)
    {
        StringTable st = new StringTable(new string[] { "test1", "test2", "test3" });
        st.Add(new string[] { "value1", "value2", "value3" });
        st.Add(new string[] { "value4", "value5", "value6" });
        st.Add(new string[] { "value7", "value8", "value9" });
        memo.AppendText(st.ToString() + "\n----------------------------------------\n");

        StringTable copy_st = null;

        copy_st = new StringTable(st);
        copy_st.ChangeField("test1", "test");
        memo.AppendText(copy_st.ToString() + "\n----------------------------------------\n");

        copy_st = new StringTable(st);
        copy_st.ChangeField("test1", "test1");
        memo.AppendText(copy_st.ToString() + "\n----------------------------------------\n");

        copy_st = new StringTable(st);
        copy_st.ChangeField("test1", "TesT1");
        memo.AppendText(copy_st.ToString() + "\n----------------------------------------\n");

        copy_st = new StringTable(st);
        copy_st.ChangeField("test1", "test１");
        memo.AppendText(copy_st.ToString() + "\n----------------------------------------\n");

        try { copy_st = new StringTable(st); copy_st.ChangeField("test1", null); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.ChangeField(null, "test"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.ChangeField("", "test"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.ChangeField("test1", ""); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void DeleteField(System.Windows.Forms.RichTextBox memo)
    {
        StringTable st = new StringTable(new string[] { "test1", "test2", "test3" });
        st.Add(new string[] { "value1", "value2", "value3" });
        st.Add(new string[] { "value4", "value5", "value6" });
        st.Add(new string[] { "value7", "value8", "value9" });
        memo.AppendText(st.ToString() + "\n----------------------------------------\n");

        StringTable copy_st = null;

        copy_st = new StringTable(st);
        copy_st.DeleteField("test1");
        memo.AppendText(copy_st.ToString() + "\n----------------------------------------\n");

        copy_st = new StringTable(st);
        copy_st.DeleteField("TesT1");
        memo.AppendText(copy_st.ToString() + "\n----------------------------------------\n");

        try { copy_st = new StringTable(st); copy_st.DeleteField(""); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.DeleteField(null); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.DeleteField("test"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.DeleteField("test１"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void InsertRow(System.Windows.Forms.RichTextBox memo)
    {
        StringTable st = new StringTable(new string[] { "test1", "test2", "test3" });
        st.Add(new string[] { "value1", "value2", "value3" });
        st.Add(new string[] { "value4", "value5", "value6" });
        st.Add(new string[] { "value7", "value8", "value9" });
        memo.AppendText(st.ToString() + "\n----------------------------------------\n");

        StringTable copy_st = null;
        copy_st = new StringTable(st); copy_st.InsertRow(-1, new string[] { "add" }); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertRow(0, new string[] { "add" }); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertRow(1, new string[] { "add" }); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertRow(2, new string[] { "add"}); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertRow(3, new string[] { "add" }); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.InsertRow(4, new string[] { "add" }); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
    }

    static public void DeleteRow(System.Windows.Forms.RichTextBox memo)
    {
        StringTable st = new StringTable(new string[] { "test1", "test2", "test3" });
        st.Add(new string[] { "value1", "value2", "value3" });
        st.Add(new string[] { "value4", "value5", "value6" });
        st.Add(new string[] { "value7", "value8", "value9" });
        memo.AppendText(st.ToString() + "\n----------------------------------------\n");

        StringTable copy_st = null;
        copy_st = new StringTable(st); copy_st.DeleteRow(0); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.DeleteRow(1); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");
        copy_st = new StringTable(st); copy_st.DeleteRow(2); memo.AppendText(copy_st.ToString() + "\n--------------------------------\n");

        try { copy_st = new StringTable(st); copy_st.DeleteRow(-1); } catch(Exception e) { memo.AppendText(e.Message + "\n"); }
        try { copy_st = new StringTable(st); copy_st.DeleteRow(3); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void Add(System.Windows.Forms.RichTextBox memo)
    {
        StringTable st = new StringTable(new string[] { "field1", "field2", "field3"});
        st.Add(new string[] { });
        st.Add(new string[] { "value1"});
        st.Add(new string[] { "value2", "value3"});
        st.Add(new string[] { "value4", null, "value5"});
        st.Add(new string[] { null, null, null });
        st.Add(null);
        st.Add(new string[] { "value6", "value7", "value8", "value9" });
        memo.AppendText(st.ToString());

    }
}