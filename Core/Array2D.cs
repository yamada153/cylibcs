﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

static class Array2D
{
    static public Type[] GetCol<Type>(Type[,] data, int col)
    {
        int n = Height(data);
        Type[] newdata = new Type[n];
        

        for(int i = 0; i < n; i++)
            newdata[i] = data[i, col];

        return newdata;
    }

    static public Type[] GetRow<Type>(Type[,] data, int row)
    {
        int n = Width(data);
        Type[] newdata = new Type[n];

        for (int i = 0; i < n; i++)
            newdata[i] = data[row, i + 1];

        return newdata;
    }

    /// <summary>
    /// 2次元配列のサイズを変更する。
    /// </summary>
    static public void ReSize<Type>(ref Type[,] data, int width, int height, Type value)
    {
        Type[,] newdata = Calloc<Type>(width, height, value);

        int h = Height(data);
        int w = Width(data);
        for (int i = 0; (i < height) && (i < h); i++)
            for (int j = 0; (j < width) && (j < w); j++)
                newdata[j, i] = data[j, i];

        data = newdata;
    }

    /// <summary>
    /// 指定された位置にdstを追加する。startx、startyは0スタート。
    /// NoData値は上書きしないので、上書き領域にデータがあっても消されず残る。
    /// </summary>
    static public void Append<Type>(ref Type[,] data, Type[,] dst, int startx, int starty, Type ignore)
    {
        int y = 0;
        int dataheight = Height(data);
        int dstheight = Height(dst);
        for (int i = starty; (i < dataheight) && (y < dstheight); i++, y++)
        {
            int x = 0;
            int datawidth = Width(data);
            int dstwidth = Width(dst);
            for (int j = startx; (j < datawidth) && (x < dstwidth); j++, x++)
                if (!dst[x, y].Equals(ignore))
                    data[j, i] = dst[x, y];
        }
    }

    /// <summary>
    /// 指定された位置の、指定されたサイズの2次元配列を返す。
    /// </summary>
    static public Type[,] Clip<Type>(Type[,] data, int startx, int starty, int width, int height, Type value)
    {
        if (Height(data) < (starty + height))
            height = Height(data) - starty;
        if (Width(data) < (startx + width))
            width = Width(data) - startx;

        Type[,] new2d = Calloc(width, height, value);
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                new2d[j, i] = data[j + startx, i + starty];
        return new2d;
    }

    /// <summary>
    /// 2次元配列の値を平行移動する。(しているように配置しなおす)
    /// </summary>
    static public void Trans(ref Type[,] data, int x, int y, Type value)
    {
        double[,] array = new double[3, 3] { { 1, 0, x }, { 0, 1, y }, { 0, 0, 1 } };
        ConvertCalc(ref data, array, value);
    }

    /// <summary>
    /// 2次元配列を拡大、縮小する。(しているように配置しなおす)
    /// </summary>
    static public void Scale<Type>(ref Type[,] data, double x, double y, Type value)
    {
        if (x <= 0) throw new ArgumentException("Real2DEx.Scaleの引数「x」が0以下の値「" + x.ToString() + "」を受け取りました。");
        if (y <= 0) throw new ArgumentException("Real2DEx.Scaleの引数「y」が0以下の値「" + y.ToString() + "」を受け取りました。");

        double[,] array = new double[3, 3] { { x, 0, 0 }, { 0, y, 0 }, { 0, 0, 1 } };
        ConvertCalc(ref data, array, value);
    }

    /// <summary>
    /// 2次元配列を上下反転させる。
    /// </summary>
    static public void MirrorTopUnder<Type>(ref Type[,] data, Type value)
    {
        double[,] array = new double[3, 3] { { 1, 0, 0 }, { 0, -1, Height(data) - 1 }, { 0, 0, 1 } };
        ConvertCalc(ref data, array, value);
    }

    /// <summary>
    /// 指定された変換行列を使って、2次元配列を変換する。
    /// </summary>
    static private void ConvertCalc<Type>(ref Type[,] data, double[,] array, Type value)
    {
        int height = Height(data);
        int width = Width(data);
        Type[,] new2d = Calloc<Type>(width, height, value);

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                double x = array[0, 0] * j + array[0, 1] * i + array[0, 2] * 1;
                double y = array[1, 0] * j + array[1, 1] * i + array[1, 2] * 1;

                if ((x < 0) || (x >= width)) continue;
                if ((y < 0) || (y >= height)) continue;
                new2d[(int)x, (int)y] = data[j, i];
            }
        data = new2d;
    }

    /// <summary>
    /// 2次元配列をnewして、valueで初期化して返す。
    /// </summary>
    static public Type[,] Calloc<Type>(int width, int height, Type value)
    {
        Type[,] d = new Type[width, height];

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                d[j, i] = value;
        return d;
    }

    static public int Width<Type>(Type[,] data)
    {
        return data.GetLength(0);
    }

    static public int Height<Type>(Type[,] data)
    {
        return data.GetLength(1);
    }

    static public string ToString(Type[,] data)
    {
        int width = Width(data);
        int height = Height(data);

        string result = "";
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
                result += data[j, i].ToString() + '\t';
            result = result.Substring(0, result.Length - 1) + "\n";
        }
        return result;
    }
}

