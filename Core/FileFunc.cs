/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

static class FileFunc
{
    /// <summary>
    /// 指定したfilenamesのファイル名だけを、拡張子抜きで取得する
    /// </summary>
    static public string[] GetFileNamesWithoutExtension(string[] filenames)
    {
        string[] result = new string[filenames.Length];
        for (int i = 0; i < filenames.Length; i++)
            result[i] = Path.GetFileNameWithoutExtension(filenames[i]);
        return result;
    }

    /// <summary>
    /// 指定したフォルダが空ならtrueを返す
    /// </summary>
    static public bool IsFolderEmpty(string folderpath)
    {
        if (Directory.EnumerateFileSystemEntries(folderpath).Any())
            return false;

        return true;
    }

    /// <summary>
    /// 指定したファイルセットが全て存在すればtrueを返す
    /// </summary>
    static public bool ExistFileSet(string filename, string[] exts)
    {
        if (!File.Exists(filename)) return false;

        for(int i = 0; i < exts.Length; i++)
        {
            string tmp = Path.ChangeExtension(filename, exts[i]);
            if (!File.Exists(tmp)) return false;
        }

        return true;
    }        

    /// <summary>
    /// 指定したフォルダ以下に存在するファイル名をフルパスで取得する
    /// </summary>
    static public string[] FileList(string dirname, string pattern, bool recursive = true)
    {
        if (!Directory.Exists(dirname))
            throw new DirectoryNotFoundException("FileFunc.FileListの引数「dirname」で指定したフォルダ「" + dirname + "」が存在しませんでした。");

        if (recursive)
            return Directory.GetFiles(dirname, pattern, SearchOption.AllDirectories);
        else
            return Directory.GetFiles(dirname, pattern, SearchOption.TopDirectoryOnly);
    }

    /// <summary>
    /// 指定したフォルダ以下に存在するフォルダ名をフルパスで取得する
    /// </summary>
    static public string[] FolderList(string dirname, string pattern, bool recursive = true)
    {
        if (!Directory.Exists(dirname))
            throw new DirectoryNotFoundException("FileFunc.FolderListの引数「dirname」で指定したフォルダ「" + dirname + "」が存在しませんでした。");

        if (recursive)
            return Directory.GetDirectories(dirname, pattern, SearchOption.AllDirectories);
        else
            return Directory.GetDirectories(dirname, pattern, SearchOption.TopDirectoryOnly);
    }

    /// <summary>
    /// 文字列の配列の内容をファイルに書き出す
    /// </summary>
    static public void SaveToFile(string[] strs, string filename)
    {
        string dirname = Path.GetDirectoryName(filename);
        if (!Directory.Exists(dirname))
            throw new DirectoryNotFoundException("FileFunc.SaveToFileの引数「filename」で指定したファイルの親フォルダ「" + dirname + "」が存在しませんでした。");

        if (strs == null)
            throw new ArgumentNullException("FileFunc.SaveToFileの引数「strs」がnullを受け取りました。\n");

        using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            foreach (string s in strs) sw.WriteLine(s);
        }
    }

    /// <summary>
    /// テキストファイルを読み込んで文字列の配列に書き出す
    /// </summary>
    static public string[] LoadFromFile(string filename)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("FileFunc.LoadFromFileの引数「filename」で指定されたファイル「" + filename + "」が存在しませんでした。");


        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            using (StreamReader sr = new StreamReader(fs, Encoding.GetEncoding("shift_jis")))
            {
                List<string> strs = new List<string>(512); //適当
                while (!sr.EndOfStream) strs.Add(sr.ReadLine());

                return strs.ToArray();
            }
        }
    }

    /// <summary>
    /// 指定したファイル名と拡張子だけが異なる同じファイル名のファイルを取得する
    /// </summary>
    static public string[] GetFileSet(string filename)
    {
        string noextractfilename = Path.GetFileNameWithoutExtension(filename);
        string dirname = Path.GetDirectoryName(filename);
        return FileList(dirname, noextractfilename + ".*", false);
    }

    /// <summary>
    /// テキストファイルを1行読み込む度にfuncを実行する
    /// stringには読み込んだ行、longには読み込んだバイト数が入る
    /// </summary>
    static public void WheneverExecFuncLoadFromFile<Type>(string filename, Action<Tuple<string, long, Type>> func, Type args)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("FileFunc.WheneverExecFuncLoadFromFileの引数「filename」で指定されたファイル「" + filename + "」が存在しませんでした。");

        using (StreamReader sr = new StreamReader(filename, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            long pos = 0;
            while (!sr.EndOfStream)
            {
                string stmp = sr.ReadLine();
                pos += stmp.ToCharArray().Length + 2; //2 = \r,\nの分
                func(new Tuple<string, long, Type>(stmp, pos, args));
            }
        }
    }

    /// <summary>
    /// 指定したフォルダからファイル名を一つ取得する度にfuncを実行する
    /// </summary>
    static public void WheneverExecFuncGetFileName<Type>(string dirname, string pattern, Action<Tuple<string, Type>> func, Type args, bool recursive = true)
    {
        if (!Directory.Exists(dirname))
            throw new DirectoryNotFoundException("FileFunc.WheneverExecFuncGetFileNameの引数「dirname」で指定したフォルダ「" + dirname + "」が存在しませんでした。");

        if (recursive)
        {
            foreach (string s in Directory.EnumerateFiles(dirname, pattern, SearchOption.AllDirectories))
                func(new Tuple<string, Type>(s, args));
        }
        else
        {
            foreach (string s in Directory.EnumerateFiles(dirname, pattern, SearchOption.TopDirectoryOnly))
                func(new Tuple<string, Type>(s, args));
        }
    }

    /// <summary>
    /// 指定されたファイルをコピーする。重複が無いように、取得先のフルパスをファイル名にしてコピーする
    /// </summary>
    static public void CopyOnePlace(string[] filenames, string savefolder, string drive_sep, string dir_sep)
    {
        foreach (string filename in filenames)
        {
            string copyfile = filename.Replace("\\", dir_sep).Replace(":", drive_sep);
            File.Copy(filename, savefolder + "\\" + copyfile);
        }
    }

    /// <summary>
    /// CopyOnePlaceでコピーしたファイルを元に戻す。loadfolderにはCopyOnePlace関数のsavefolderで指定した値を入れる
    /// </summary>
    static public void CopyForCopyOnePlaceFunction(string[] filenames, string loadfolder, string drive_sep, string dir_sep)
    {
        foreach (string filename in filenames)
        {
            string copyfile = filename.Replace (loadfolder + "\\", "").Replace(dir_sep, "\\").Replace(drive_sep, ":");
            string copydir = Path.GetDirectoryName(copyfile);
            if (!Directory.Exists(copydir))
                Directory.CreateDirectory(copydir);
            string movefile = copydir + "\\" + Path.GetFileName(copyfile);
            File.Copy(filename, movefile);
        }
    }

    /// <summary>
    /// 指定したファイルを、指定したフォルダにコピーする。delflg = trueなら、移動先に同名のファイルが存在した場合に削除して移動する dstfilenameの親フォルダがなければ作る
    /// </summary>
    static public void ForceCopy(string filename, string dstfilename, bool delflg = false)
    {
        string folder = Path.GetDirectoryName(dstfilename);
        if (!Directory.Exists(folder))
            Directory.CreateDirectory(folder);

        if (delflg && File.Exists(dstfilename))
            File.Delete(dstfilename);
        File.Copy(filename, dstfilename);        
    }

    /// <summary>
    /// 指定したファイルを、指定したフォルダに移動する。delflg = trueなら、移動先に同名のファイルが存在した場合に削除して移動する dstfilenameの親フォルダがなければ作る
    /// </summary>
    static public void ForceMove(string filename, string dstfilename, bool delflg = false)
    {
        string folder = Path.GetDirectoryName(dstfilename);
        if (!Directory.Exists(folder))
            Directory.CreateDirectory(folder);

        if (delflg && File.Exists(dstfilename))
            File.Delete(dstfilename);
        File.Move(filename, dstfilename);
    }

    /// <summary>
    /// ユーザーが実行しているプログラムが使用する一時フォルダを作成し、フォルダ名を返す。バックスラッシュで終わる。
    /// </summary>
    static public string CreateTempFolder(string applicationname)
    {
        string TmpFolder = Path.GetTempPath() + "\\" + applicationname + "\\";
        if (Directory.Exists(TmpFolder)) return TmpFolder;

        Directory.CreateDirectory(TmpFolder);
        return TmpFolder;
    }

    /// <summary>
    /// 指定したディレクトリを作成する。
    /// </summary>
    static public bool ForceDirectories(string dirname)
    {
        if (!Directory.Exists(dirname))
        {
            try
            {
                Directory.CreateDirectory(dirname);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// 指定したfilenameがフォルダならtrueを返す
    /// </summary>
    static public bool IsDirectory(string filename)
    {
        if ((File.GetAttributes(filename) & FileAttributes.Directory) == FileAttributes.Directory)
            return true;
        return false;
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestFileFunc
{
    static public void FileList(System.Windows.Forms.RichTextBox memo)
    {
        string dirname = "";

        dirname = @"c:\";
        string[] filenames = FileFunc.FileList(dirname, "*.txt");
        foreach (string s in filenames) memo.AppendText(s + "\n");

        try { FileFunc.FileList("存在しないフォルダ", "*.txt"); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void WheneverExecFuncGetFileName(System.Windows.Forms.RichTextBox memo)
    {
        string dirname = "";

        dirname = @"d:\";
        FileFunc.WheneverExecFuncGetFileName<string>(dirname, "*", (x) => { memo.AppendText(new FileInfo(x.Item1).Length.ToString() + "\t" + x.Item1 + "\n"); }, "");

        try { FileFunc.WheneverExecFuncGetFileName<string>("存在しないフォルダ", "*", (x) => { }, ""); }catch(Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void WheneverExecFuncLoadFromFile(System.Windows.Forms.RichTextBox memo)
    {
        string loadfilename = "";
        int count = 0;

        loadfilename = @"D:\Git\Testデータ\空のファイル.txt";
        count = 0;
        FileFunc.WheneverExecFuncLoadFromFile<int>(loadfilename, (x) => { count++; memo.AppendText(count.ToString() + "\t" + x.Item1 + "\n"); }, count);

        loadfilename = @"D:\Git\Testデータ\多量の行のファイル.txt";
        count = 0;
        FileFunc.WheneverExecFuncLoadFromFile<int>(loadfilename, (x) => { count++; memo.AppendText(count.ToString() + "\t" + x.Item1 + "\n"); }, count);

        try { FileFunc.WheneverExecFuncLoadFromFile("存在しないファイル", (x) => { }, ""); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }
}