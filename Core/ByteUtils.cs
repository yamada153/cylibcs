﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Linq;

static class ByteUtils
{
    /// <summary>
    /// 指定された値のバイト列を逆順にする
    /// </summary>
    public static Int32 ReverseByte(Int32 i)
    {
        return BitConverter.ToInt32(BitConverter.GetBytes(i).Reverse().ToArray(), 0);
    }

    /// <summary>
    /// Wordサイズに変換する
    /// </summary>
    public static byte[] ToWord(Int32 i)
    {
        Byte[] result = new Byte[2];
        result[0] = BitConverter.GetBytes(i)[0];
        result[1] = BitConverter.GetBytes(i)[1];
        return result;
    }
}
