﻿/******************************************************************************************
 *  Copyright (c) 2019 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// 名前は大文字小文字を区別しません。
/// 比較しなければならないとき(辿るとき等)は一旦小文字にします。
/// </summary>
class NameTree<type>
{
    class TreeEelement<type>
    {
        public type Value { set; get; }
        public string Name { get; }
        public List<TreeEelement<type>> Children { set; get; }
        public TreeEelement<type> Parent { get; }

        public TreeEelement(string name, type value, TreeEelement<type> parent)
        {
            Name = name;
            Value = value;
            Children = new List<TreeEelement<type>>();
            Parent = parent;
        }
    }

    private TreeEelement<type> _root;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public NameTree(string name, type value)
    {
        if (name.Contains('/'))
            new Exception("name「" + name + "」に/が入っています。受付できませんでした");

        _root = new TreeEelement<type>(name, value, null); //親を持たない = Root       
    }

    /// <summary>
    /// pathで指定したノードの直下に子を作成する。nameと同じ子が存在する場合、valueを置き換える。
    /// </summary>
    public bool AddChild(string path, string name, type value)
    {
        name = name.Trim('/');

        TreeEelement<type> target = GetElement(path);
        if (target == null)
            return false;

        for (int i = 0; i < target.Children.Count(); i++)
        {
            if (target.Children[i].Name.ToLower() != name.ToLower()) continue;
            target.Children[i].Value = value;
            return true;
        }

        target.Children.Add(new TreeEelement<type>(name, value, target));
        return true;
    }

    /// <summary>
    /// pathで指定した子を削除する。
    /// </summary>
    public void Delete(string path)
    {
        TreeEelement<type> target = GetElement(path);
        if (target == null)
            return;
        //子供削除
        DeleteRoutine(target);
        //自身を削除
        string[] splited = path.Trim('/').Split('/');
        for(int i = 0; i < target.Parent.Children.Count(); i++)
        {
            if(target.Parent.Children[i].Name.ToLower() == splited[splited.Length - 1].ToLower())
            {
                target.Parent.Children.RemoveAt(i);
                break;
            }
        }
    }

    private void DeleteRoutine(TreeEelement<type> top)
    {
        for (int i = top.Children.Count() - 1; i >= 0; i--)
        {
            if (top.Children[i].Children.Count() != 0)
                DeleteRoutine(top.Children[i]);

            top.Children.RemoveAt(i);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    override public string ToString()
    {
        List<string[]> strslist = new List<string[]>(100); //適当
        StringRoutine(_root, 2, ref strslist);

        string[] result = new string[strslist.Count + 1];
        for (int i = 0; i < strslist.Count(); i++)
            result[i + 1] = string.Join("", strslist[i]);
        result[0] = _root.Name;

        return string.Join("\n", result.ToArray());
    }

    private void StringRoutine(TreeEelement<type> target, int layer, ref List<string[]> strslist)
    {
        for (int i = 0; i < target.Children.Count(); i++)
        {
            string[] strs = new string[layer];

            if (strslist.Count() != 0)
            {
                string[] stmps = strslist.Last();
                for (int j = 0; j < stmps.Length; j++)
                {
                    if ((stmps[j] == "┣") || stmps[j] == "┃")
                        strs[j] = "┃";
                    else
                    {
                        if((strs.Length - 1) >= j)
                            strs[j] = " ";
                    }
                }
            }

            if (i == target.Children.Count() - 1)
                strs[layer - 2] = "┗";
            else
                strs[layer - 2] = "┣";

            strs[layer - 1] = target.Children[i].Name;
            strslist.Add(strs);
            StringRoutine(target.Children[i], layer + 1, ref strslist);
        }
    }

    private TreeEelement<type> GetElement(string path)
    {
        path = path.Trim('/');
        string[] splited = path.Split('/');

        TreeEelement<type> tmp = _root;
        if (tmp.Name.ToLower() != splited[0].ToLower())
        {
            return null;
        }

        for (int i = 1; i < splited.Length; i++)
        {
            bool isfind = false;
            for (int j = 0; j < tmp.Children.Count; j++)
            {
                if (tmp.Children[j].Name.ToLower() == splited[i].ToLower())
                {
                    tmp = tmp.Children[j];
                    isfind = true;
                    break;
                }
            }
            if (!isfind)
            {
                return null;
            }
        }

        return tmp;
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestNameTree
{
    static private Person[] persons = new Person[] {
        new Person("佐藤", 7),
        new Person("山田", 18),
        new Person("鈴木", 4),
        new Person("斉藤", 0),
        new Person("石田", 18),
        new Person("田中", 35),
        new Person("渡辺", 0),
        new Person("伊藤", 9),
        new Person("鈴木", 4),
        new Person("石川", 18),
        new Person("安部", 59)
    };

    class Person
    {
        public string name;
        public int age;

        public Person(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        override public string ToString()
        {
            return name + ", " + age.ToString();
        }
    }

    class IPersonEquality : IEqualityComparer<Person>
    {
        public int GetHashCode(Person p)
        {
            return p.name.GetHashCode() ^ p.age.GetHashCode();
        }

        public bool Equals(Person x, Person y)
        {
            return (x.age == y.age) && (x.name == y.name);
        }
    }

    class IPersonComparer : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            if (x.age > y.age) return 1;
            else if (x.age < y.age) return -1;
            else return 0;
        }
    }
}