﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections.Generic;

static class StrUtils
{
    /// <summary>
    /// numで指定した文字数でstrを分ける
    /// </summary>
    static public string[] Split(string str, int num)
    {
        char[] strarray = str.ToCharArray();
        List<string> result = new List<string>(100); //適当
        StringBuilder sb = new StringBuilder(num);
        for(int i = 0; i < strarray.Length; i++)
        {
            sb.Append(strarray[i]);
            if (sb.Length == num)
            {
                result.Add(sb.ToString());
                sb.Clear();
            }
        }

        return result.ToArray();
    }

    /// <summary>
    /// strが日付を表す文字列か判定する
    /// </summary>
    static public bool IsDateString(string str)
    {
        bool result = true;

        DateTime dttmp;
        if (!DateTime.TryParse(str, out dttmp))
        {
            if (str.Length == 6) // 790603
            {
                str = str.Insert(2, "/"); // 79/0603
                str = str.Insert(5, "/"); // 79/06/03
            }
            if (str.Length == 8) // 19790603
            {
                str = str.Insert(4, "/"); // 1979/0603
                str = str.Insert(7, "/"); // 1979/06/03
            }

            if (!DateTime.TryParse(str, out dttmp))
                result = false;
        }

        return result;
    }

    /// <summary>
    /// 文字列配列の要素から、この配列全体はFLOAT、DATE, INTEGER、CHARのいずれかを判断する。
    /// </summary>
    static public string TypeFromString(string[] strs)
    {
        bool flg_date = true;
        bool flg_double = true;
        bool flg_integer = true;
        bool flg_none = true;
        int str_length = 0;

        Encoding sjis = Encoding.GetEncoding("Shift_JIS");

        foreach (string str in strs)
        {
            if (str == "") continue;

            flg_none = false;

            int itmp = 0;
            if (!int.TryParse(str, out itmp)) flg_integer = false;

            if (!IsDateString(str)) flg_date = false;

            double dtmp = 0;
            if (!double.TryParse(str, out dtmp)) flg_double = false;
            if (flg_integer && flg_double && (itmp != 0) && (str[0] == '0') && (str[1] != '.'))
            {
                flg_integer = false;
                flg_double = false;
            }

            str_length = Math.Max(str_length, sjis.GetByteCount(str));
        }

        if (flg_none) return "CHAR(40)";
        if (flg_date) return "DATE";
        if (flg_double) return "FLOAT";
        if (flg_integer) return "INTEGER";

        return string.Format("CHAR({0})", str_length.ToString());
    }

    /// <summary>
    /// 文字列の中で連続して重複している文字を1つにする
    /// </summary>
    static public string DuplicateToOne(string str, string dup)
    {
        string pattern = dup + "+";
        Regex r = new Regex(pattern, RegexOptions.Singleline);

        return r.Replace(str, dup);
    }

    /// <summary>
    /// 標準メソッドでは末尾のtarget一文字だけを削除できないので用意した関数
    /// </summary>
    public static string TrimEnd(string str, char target)
    {
        CheckNullStr("TrimEnd", str);
        if (str == "") return str;

        char[] strarray = str.ToCharArray();
        if (strarray.Last() == target)
            Array.Resize(ref strarray, strarray.Length - 1);
        return new string(strarray);
    }

    /// <summary>
    /// 標準メソッドでは先頭のtarget一文字だけを削除できないので用意した関数
    /// </summary>
    public static string TrimStart(string str, char target)
    {
        CheckNullStr("TrimStart", str);
        if (str == "") return str;

        char[] strarray = str.ToCharArray();
        if (strarray.First() == target)
            return str.Substring(1);
        return str;
    }

    /// <summary>
    /// 全角半角、ひらがなカタカナ、大文字小文字をそれぞれ同じものとして、比較する
    /// </summary>
    static public bool SameText(string a, string b)
    {
        if (a == null) throw new ArgumentNullException("StrUtils.SameTextの引数「a」がnullを受け取りました。");
        if (b == null) throw new ArgumentNullException("StrUtils.SameTextの引数「b」がnullを受け取りました。");
        CultureInfo ci = new CultureInfo("ja-JP");

        CompareOptions opt;
        opt = CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase; //全角/半角無視|ひらがな/カタカナ無視|大文字/小文字無視

        return string.Compare(a, b, ci, opt) == 0 ? true : false;
    }

    private static void CheckNullStr(string functionname, string str)
    {
        if (str == null)
            throw new ArgumentNullException("StrUtils." + functionname + "の引数「str」がnullを受け取りました。");
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestStrUtils
{
    static public void IsDateString(RichTextBox memo)
    {
        string s = "";
        s = "1959/1/1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959/01/1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959/1/01"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959/01/01"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959/1/"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959/1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959/"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "59/1/1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "59/1/"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "59/1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-1-1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-01-1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-1-01"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-01-01"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-1-"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959-"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "59-1-1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "59-1-"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "59-1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959.1.1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959.1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959年1月1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959年1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "1959年"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "明治44年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "明治45年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "明治46年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "大正14年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "大正15年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "大正16年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "昭和63年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "昭和64年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "昭和65年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "昭59年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "小59年1月1日"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "s59.1.1."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "s59.1.1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "s59.1."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "s59.1"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "s59."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "s59"; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "m10.1.1."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "t10.1.1."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "h10.1.1."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
        s = "v10.1.1."; memo.AppendText(s + ": " + StrUtils.IsDateString(s) + "\n");
    }

    static public void SameText(RichTextBox memo)
    {
        string a = "Test";
        string b = "TesT";
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = "テスト";
        b = "てすと";
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = "テスト";
        b = "ﾃｽﾄ";
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = "テすﾄ";
        b = "てｽト";
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = "テすﾄ１";
        b = "てｽト1";
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = "テス　ト";//全角スペース
        b = "てす と"; //半角スペース
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = "テス ト";
        b = "てす\tと";
        memo.AppendText("「" + a + "」「" + b + "」 -> " + StrUtils.SameText(a, b).ToString() + "\n");

        a = null;
        b = "てすと";
        try { StrUtils.SameText(a, b); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }

        b = null;
        a = "てすと";
        try { StrUtils.SameText(a, b); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    static public void TrimEnd(RichTextBox memo)
    {
        string s = ",,,test,,,";
        char c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimEnd(s, c) + "\n");
        c = '_';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimEnd(s, c) + "\n");

        s = "";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimEnd(s, c) + "\n");

        s = ",";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimEnd(s, c) + "\n");

        s = "te,st";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimEnd(s, c) + "\n");

        s = null;
        c = ',';
        try
        {
            StrUtils.TrimEnd(s, c);
        }
        catch (Exception e)
        {
            memo.AppendText(e.Message + "\n");
        }
    }

    static public void TrimStart(RichTextBox memo)
    {
        string s = ",,,test,,,";
        char c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimStart(s, c) + "\n");
        c = '_';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimStart(s, c) + "\n");

        s = "";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimStart(s, c) + "\n");

        s = ",";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimStart(s, c) + "\n");

        s = "te,st";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimStart(s, c) + "\n");

        s = "test,";
        c = ',';
        memo.AppendText("「" + s + "」「" + c + "」 -> " + StrUtils.TrimStart(s, c) + "\n");

        s = null;
        c = ',';
        try
        {
            StrUtils.TrimStart(s, c);
        }
        catch (Exception e)
        {
            memo.AppendText(e.Message + "\n");
        }
    }
}