﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

static class ArrayFunc
{
    static public string ToString<Type>(Type[] datas, char separator = ',')
    {
        string[] tmp = new string[datas.Length];
        for (int i = 0; i < datas.Length; i++)
            tmp[i] = datas[i].ToString();
        return string.Join(separator.ToString(), tmp);
    }

    /// <summary>
    /// 2次元配列を1次元配列に変換する。
    /// </summary>
    static public Type[] ArrayFromArray2D<Type>(Type[,] data)
    {
        int height = data.GetLength(1);
        int width = data.GetLength(0);

        int cnt = 0;
        Type[] result = new Type[height * width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                result[cnt++] = data[j, i];

        return result;
    }

    /// <summary>
    /// 配列の先頭を変更する
    /// </summary>
    static public Type[] Rotate<Type>(Type[] datas, int newtop)
    {
        Type[] top = datas.Skip(newtop).Take(datas.Length - newtop).ToArray();
        Type[] under = datas.Skip(0).Take(newtop).ToArray();
        return Concat(top, under);
    }

    /// <summary>
    /// 配列からnullを取り除く
    /// </summary>
    static public Type[] Compaction<Type>(Type[] datas)
    {
        List<Type> tmp = new List<Type>(datas.Length);

        for (int i = 0; i < datas.Length; i++)
        {
            if (datas[i] == null) continue;
            tmp.Add(datas[i]);
        }
        return tmp.ToArray();
    }

    /// <summary>
    /// 配列から指定した要素を取り除く
    /// 例）配列から空文字列削除したいとき  string[] strs = ArrayFunc.Compaction(strarray, "", (x, y) => { return x == y; });
    /// </summary>
    static public Type[] Compaction<Type>(Type[] datas, Type del, Func<Type, Type, bool> f)
    {
        List<Type> tmp = new List<Type>(datas.Length);

        for (int i = 0; i < datas.Length; i++)
        {
            if (f(datas[i], del)) continue;
            tmp.Add(datas[i]);
        }
        return tmp.ToArray();
    }

    /// <summary>
    /// 配列から指定した要素を取り除く
    /// </summary>
    static public Type[] Compaction<Type>(Type[] datas, Func<Type, bool> f)
    {
        List<Type> tmp = new List<Type>(datas.Length);

        for (int i = 0; i < datas.Length; i++)
        {
            if (f(datas[i])) continue;
            tmp.Add(datas[i]);
        }
        return tmp.ToArray();
    }

    /// <summary>
    /// 配列を連結する
    /// </summary>
    static public Type[] Concat<Type>(Type[] a, Type[] b)
    {
        CheckNullArgument("Concat", "a", a);
        CheckNullArgument("Concat", "b", b);

        Type[] result = new Type[a.Length + b.Length];
    
        Array.Copy(a, result, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);

        return result;
    }

    /// <summary>
    /// 配列から重複を取り除く。元の順番は保たれる
    /// </summary>
    static public Type[] Distinct<Type>(Type[] datas)
    {
        CheckNullArgument("Distinct", "datas", datas);

        List<Type> tmp = new List<Type>(datas.Length);

        for (int i = 0; i < datas.Length; i++)
        {
            if (tmp.Contains(datas[i])) continue;
            tmp.Add(datas[i]);
        }
        return tmp.ToArray();
    }

    /// <summary>
    /// 配列から重複を取り除く。元の順番は保たれる
    /// </summary>
    static public Type[] Distinct<Type>(Type[] datas, IEqualityComparer<Type> eq)
    {
        CheckNullArgument("Distinct", "datas", datas);

        List<Type> tmp = new List<Type>(datas.Length);

        for (int i = 0; i < datas.Length; i++)
        {
            if (tmp.Contains(datas[i], eq)) continue;
            tmp.Add(datas[i]);
        }
        return tmp.ToArray();
    }

    /// <summary>
    /// aにもbにも存在する要素の配列を返す
    /// </summary>
    static public Type[] And<Type>(Type[] a, Type[] b, IEqualityComparer<Type> eq)
    {
        CheckNullArgument("And", "a", a);
        CheckNullArgument("And", "b", b);

        List<Type> result = new List<Type>(a.Length);
        for (int i = 0; i < a.Length; i++)
            if (b.Contains(a[i], eq)) result.Add(a[i]);

        return Distinct(result.ToArray(), eq);
    }

    /// <summary>
    /// aにもbにも存在する要素の配列を返す
    /// </summary>
    static public Type[] And<Type>(Type[] a, Type[] b)
    {
        CheckNullArgument("And", "a", a);
        CheckNullArgument("And", "b", b);

        List<Type> result = new List<Type>(a.Length);
        for (int i = 0; i < a.Length; i++)
            if (b.Contains(a[i])) result.Add(a[i]);

        return Distinct(result.ToArray());
    }

    /// <summary>
    /// bに存在しないaの要素の配列を返す
    /// </summary>
    static public Type[] Sub<Type>(Type[] a, Type[] b)
    {
        CheckNullArgument("Sub", "a", a);
        CheckNullArgument("Sub", "b", b);

        List<Type> result = new List<Type>(a.Length);
        for (int i = 0; i < a.Length; i++)
            if (!b.Contains(a[i])) result.Add(a[i]);
        return Distinct(result.ToArray());
    }

    /// <summary>
    /// bに存在しないaの要素の配列を返す
    /// </summary>
    static public Type[] Sub<Type>(Type[] a, Type[] b, IEqualityComparer<Type> eq)
    {
        CheckNullArgument("Sub", "a", a);
        CheckNullArgument("Sub", "b", b);

        List<Type> result = new List<Type>(a.Length);
        for (int i = 0; i < a.Length; i++)
            if (!b.Contains(a[i], eq)) result.Add(a[i]);
        return Distinct(result.ToArray(), eq);
    }

    /// <summary>
    /// 配列を指定した条件に合う要素で区切る
    /// </summary>
    static public Type[][] Split<Type>(Type[] ar,  Func<Type, bool> f)
    {
        CheckNullArgument("Split", "ar", ar);

        List<Type[]> result = new List<Type[]>(100); // 適当
        List<Type> tmp = new List<Type>(100); //適当
        for (int i = 0; i < ar.Length; i++)
        {
            if (f(ar[i]))
            {
                if(tmp.Count != 0)
                    result.Add(tmp.ToArray());

                tmp.Clear();
                tmp.Add(ar[i]);
            }
            else
            {
                tmp.Add(ar[i]);
            }
        }
        if (tmp.Count != 0)
            result.Add(tmp.ToArray());

        return result.ToArray();
    }

    /// <summary>
    /// 配列を指定した要素で区切る
    /// </summary>
    static public Type[][] Split<Type>(Type[] ar, Type delimiter, IEqualityComparer<Type> eq)
    {
        CheckNullArgument("Split", "ar", ar);

        int count = ar.Count(n => eq.Equals(n, delimiter));

        List<Type>[] tmp = new List<Type>[count + 1];
        for (int i = 0; i <= count; i++)
            tmp[i] = new List<Type>();

        int set_count = 0;
        for(int i = 0; i < ar.Length; i++)
        {
            if (eq.Equals(ar[i], delimiter))
            {
                set_count++;
                continue;
            }
            tmp[set_count].Add(ar[i]);
        }

        Type[][] result = new Type[count + 1][];
        for (int i = 0; i <= count; i++)
            result[i] = tmp[i].ToArray();

        return result;
    }

    /// <summary>
    /// 配列を指定した要素で区切る
    /// </summary>
    static public Type[][] Split<Type>(Type[] ar, Type delimiter)
    {
        CheckNullArgument("Split", "ar", ar);

        int count = ar.Count(n => n.Equals(delimiter));

        List<Type>[] tmp = new List<Type>[count + 1];
        for (int i = 0; i <= count; i++)
            tmp[i] = new List<Type>();

        int set_count = 0;
        for (int i = 0; i < ar.Length; i++)
        {
            if (ar[i].Equals(delimiter))
            {
                set_count++;
                continue;
            }
            tmp[set_count].Add(ar[i]);
        }

        Type[][] result = new Type[count + 1][];
        for (int i = 0; i <= count; i++)
            result[i] = tmp[i].ToArray();

        return result;
    }

    /// <summary>
    /// 配列から重複する値を取得する
    /// </summary>
    static public Type[] Double<Type>(Type[] datas)
    {
        CheckNullArgument("Double", "datas", datas);

        List<Type> tmp = new List<Type>(datas.Length);

        for (int i = 0; i < datas.Length; i++)
        {
            var a = datas.Where(x => x.Equals(datas[i]));
            foreach(var s in a)
                tmp.Add(s);            
        }
        return tmp.Distinct().ToArray();
    }

    /// <summary>
    /// 配列の要素が同じなら1を返す。逆にすれば一致するなら2を返す。一致しなければ-1を返す
    /// </summary>
    static public int Equal<Type>(Type[] a, Type[] b)
    {
        if (a.Length != b.Length) return -1;

        //そのままで一致する？
        int result = 1;
        for (int i = 0; i < a.Length; i++)
            if (!a[i].Equals(b[i])) result = -1;
        if (result == 1) return 1;
        //逆にすれば一致する？
        a.Reverse();
        result = 2;
        for (int i = 0; i < a.Length; i++)
            if (!a[i].Equals(b[i])) result = -1;
        if (result == 2) return 2;

        return result;
    }

    static private void CheckNullArgument<Type>(string functionname, string argname, Type[] a)
    {
        if (a == null)
            throw new ArgumentNullException("ArrayFunc." + functionname + "の引数「" + argname + "」がnullを受け取りました。");
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestArrayFunc
{
    static private Person[] persons_a = new Person[] {
        new Person("佐藤", 7),
        new Person("山田", 18),
        new Person("鈴木", 4),
        new Person("斉藤", 0),
        new Person("山田", 18)
    };

    static private Person[] persons_b = new Person[] {
        new Person("田中", 35),
        new Person("渡辺", 0),
        new Person("伊藤", 9),
        new Person("鈴木", 4),
        new Person("山田", 18),
        new Person("安部", 59)
    };

    static private Person[] persons_c = new Person[] {
    };

    static private Person[] persons_d = new Person[] {
        new Person("長谷川", 45),
        null,
        new Person("田辺", 11)
    };

    static private Person[] persons_e = new Person[] {
        new Person("長谷川", 45),
        new Person("長谷川", 45),
        new Person("長谷川", 45)
    };

    static private Person[] persons_f = new Person[] {
        null,
        null,
        null
    };

    public static void Rotate(System.Windows.Forms.RichTextBox memo)
    {
        Person[] result = null;

        result = ArrayFunc.Rotate(persons_a, 3);
        for (int i = 0; i < result.Length; i++) memo.AppendText(result[i].ToString() + "\n");
    }

    public static void Split(System.Windows.Forms.RichTextBox memo)
    {
        Person[][] result = null;

        result = ArrayFunc.Split(persons_a, new Person("山田", 18), new IPersonEquality());
        for (int i = 0; i < result.Length; i++)
        {
            for (int j = 0; j < result[i].Length; j++)
                memo.AppendText(result[i][j].ToString() + ", ");
            memo.AppendText("\n");
        }
    }

    public static void Split1(System.Windows.Forms.RichTextBox memo)
    {
        Person[][] result = null;

        result = ArrayFunc.Split(persons_a, (x) => { return x.name.Last() == '藤'; });
        for (int i = 0; i < result.Length; i++)
        {
            for (int j = 0; j < result[i].Length; j++)
                memo.AppendText(result[i][j].ToString() + ", ");
            memo.AppendText("\n");
        }
    }

    public static void Sub(System.Windows.Forms.RichTextBox memo)
    {
        Person[] result = null;

        result = ArrayFunc.Sub(persons_a, persons_b, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        try { ArrayFunc.Sub(persons_a, null, new IPersonEquality()); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { ArrayFunc.Sub(null, persons_b, new IPersonEquality()); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    public static void And(System.Windows.Forms.RichTextBox memo)
    {
        Person[] result = null;

        result = ArrayFunc.And(persons_a, persons_b, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        try { ArrayFunc.And(persons_a, null, new IPersonEquality()); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { ArrayFunc.And(null, persons_b, new IPersonEquality()); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    public static void Distinct(System.Windows.Forms.RichTextBox memo)
    {
        Person[] result = null;

        result = ArrayFunc.Distinct(persons_c, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        result = ArrayFunc.Distinct(persons_a, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        result = ArrayFunc.Distinct(persons_b, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        result = ArrayFunc.Distinct(persons_e, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");
        //Equalの実装によっては例外になるパターン
        result = ArrayFunc.Distinct(persons_f, new IPersonEquality());
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        try { ArrayFunc.Distinct(null, new IPersonEquality()); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    public static void Concat(System.Windows.Forms.RichTextBox memo)
    {
        Person[] result = null;

        result = ArrayFunc.Concat(persons_a, persons_b);
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        result = ArrayFunc.Concat(persons_c, persons_a);
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        result = ArrayFunc.Concat(persons_c, persons_d);
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        //連結元の配列の要素の値を変更した場合、連結後の配列の要素まで変わってしまうかな？
        result = ArrayFunc.Concat(persons_c, persons_a);
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("*************************************\n");
        persons_a[0].name = "てすとねーむ"; //連結前の配列の要素を変更する
        foreach (Person p in result) if (p == null) memo.AppendText("NULL\n"); else memo.AppendText(p.ToString() + "\n");
        memo.AppendText("-----------------------------------------\n");

        try { ArrayFunc.Concat(null, persons_b); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
        try { ArrayFunc.Concat(persons_a, null); } catch (Exception e) { memo.AppendText(e.Message + "\n"); }
    }

    class Person
    {
        public string name;
        public int age;

        public Person(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        override public string ToString()
        {
            return name + ", " + age.ToString();
        }
    }

    class IPersonEquality : IEqualityComparer<Person>
    {
        public int GetHashCode(Person p)
        {
            return p.name.GetHashCode() ^ p.age.GetHashCode();
        }

        public bool Equals(Person x, Person y)
        {
            if ((x == null) && (y == null)) return true;
            if (x == null) return false;
            if (y == null) return false;
             
            return (x.age == y.age) && (x.name == y.name);
        }
    }

}
