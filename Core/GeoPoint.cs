/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/
 //構造体よりクラスの方がいいかな...
using System;
using System.Collections.Generic;

struct GeoPoint : IEquatable<GeoPoint>, IComparable<GeoPoint>, IEqualityComparer<GeoPoint>
{
    public double X { get; set; } 
    public double Y { get; set; } 
    public double Z { get; set; } 

    public GeoPoint(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public GeoPoint(double x, double y)
    {
        X = x;
        Y = y;
        Z = 0;
    }

    override public string ToString()
    {
        return X.ToString() + ',' + Y.ToString() + ',' + Z.ToString();
    }

    public override int GetHashCode()
    {
        return (X * 2).GetHashCode() ^ (Y * 3).GetHashCode() ^ (Z * 5).GetHashCode(); //2,3,5は適当。これが無いと、例えば「X=1,Y=0,Z=-1」「X=0,Y=1,Z=-1」が同じコードになる
    }

    public int GetHashCode(GeoPoint pnt)
    {
        return (pnt.X * 2).GetHashCode() ^ (pnt.Y * 3).GetHashCode() ^ (pnt.Z * 5).GetHashCode(); //2,3,5は適当。これが無いと、例えば「X=1,Y=0,Z=-1」「X=0,Y=1,Z=-1」が同じコードになる
    }

    public override bool Equals(object obj)
    {
        if (!(obj is GeoPoint))
            return false;

        return Equals((GeoPoint)obj);
    }

    public bool Equals(GeoPoint pnt)
    {
        if (!EqualsEx(this, pnt)) return false;
        return true;
    }

    public bool Equals(GeoPoint a, GeoPoint b)
    {
        if (!EqualsEx(a, b)) return false;
        return true;
    }

    public int CompareTo(GeoPoint other)
    {
        if(X > other.X)
            return 1;
        else if (X < other.X)
            return -1;
        else
        {
            if (Y > other.Y) return 1;
            else if (Y < other.Y) return -1;
            else
            {
                if (Z > other.Z) return 1;
                else if (Z < other.Z) return -1;
                else return 0;
            }
        }
    }

    public static bool operator ==(GeoPoint a, GeoPoint b)
    {
        return EqualsEx(a, b);
    }

    public static bool operator !=(GeoPoint a, GeoPoint b)
    {
        return !(a == b);
    }

    public static GeoPoint operator +(GeoPoint a, GeoPoint b)
    {
        return new GeoPoint(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
    }

    public static GeoPoint operator -(GeoPoint a, GeoPoint b)
    {
        return new GeoPoint(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
    }

    public static GeoPoint operator *(GeoPoint a, double b)
    {
        return new GeoPoint(a.X * b, a.Y * b, a.Z * b);
    }

    public static GeoPoint operator *(double a, GeoPoint b)
    {
        return new GeoPoint(b.X * a, b.Y * a, b.Z * a);
    }

    public static GeoPoint operator /(GeoPoint a, double b)
    {
        return new GeoPoint(a.X / b, a.Y / b, a.Z / b);
    }

    /// <summary>
    /// X、Yを入れ替える。
    /// </summary>
    public void SwapXY()
    {
        double tmp = this.X;
        this.X = this.Y;
        this.Y = tmp;
    }

    /// <summary>
    /// 絶対誤差と相対誤差を使ってaとbを等しいとみなせるか判定する
    /// </summary>
    static private bool EqualsEx(GeoPoint a, GeoPoint b)
    {
        double[] arr = { a.X, a.Y, a.Z, b.X, b.Y, b.Z };

        for(int i = 0; i < 3; i++)
        {
            double m = arr[i];
            double n = arr[i + 3];
            if (double.IsNaN(m) && double.IsNaN(n)) continue;
            if (double.IsNegativeInfinity(m) && double.IsNegativeInfinity(n)) continue;
            if (double.IsPositiveInfinity(m) && double.IsPositiveInfinity(n)) continue;
            if (Math.Abs(m - n) <= Global.Epsilon) continue;
            return false;
        }
        return true;
    }
}
