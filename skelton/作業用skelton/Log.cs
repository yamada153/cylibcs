﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

/*使い方
 * Program.cs

    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()        
        {
            Log.Init();         //初期化
            try
            {
                略
            }
            finally
            {
                Log.Close();    //終了
            }
        }
    }
*/

using System.IO;
using System.Windows.Forms;

/// <summary>
/// ログファイルを作成してログを書き込む。
/// </summary>
static class Log
{
    static bool _haserror;
    static bool _debugmode;
    static string _filename;
    static StreamWriter _sw;

    /// <summary>
    /// コンストラクタの代わり。ログファイルを実行ファイルと同じフォルダに作成する。最初に呼ばなければならない。
    /// </summary>
    static public void Init(bool debug = false)
    {
        string dir = Path.GetDirectoryName(Application.ExecutablePath) + "\\Logs\\";
        Directory.CreateDirectory(dir);

        _filename = dir + System.DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";
        _sw = new StreamWriter(_filename, true, System.Text.Encoding.GetEncoding("shift_jis"));
        _haserror = false;
        _debugmode = debug;
    }

    /// <summary>
    /// メッセージを出力する。
    /// </summary>
    static public void MessageLog(string msg)
    {
        _sw.WriteLine(System.DateTime.Now.ToString() + "\t【メッセージ】\t" + msg);
    }

    /// <summary>
    /// デバッグ用メッセージを出力する。
    /// </summary>
    static public void DebugLog(string msg)
    {
        if(_debugmode)
            _sw.WriteLine(System.DateTime.Now.ToString() + "\t【デバッグ】\t" + msg);
    }

    /// <summary>
    /// 警告メッセージを出力する。
    /// </summary>
    static public void WarningLog(string msg)
    {
        _sw.WriteLine(System.DateTime.Now.ToString() + "\t【警告】\t" + msg);
    }

    /// <summary>
    /// エラーメッセージを出力する。この関数が一回でも呼ばれると、アプリケーション終了時にログが表示される。
    /// </summary>
    static public void ErrorLog(string msg)
    {
        _haserror = true;
        _sw.WriteLine(System.DateTime.Now.ToString() + "\t【エラー】\t" + msg);
    }

    /// <summary>
    /// アプリケーション終了時に忘れずにこの関数を呼ぶこと!!。
    /// </summary>
    static public void Close()
    {
        _sw.Close();

        if (_haserror)
        {
            MessageBox.Show("ログがあります。");
            System.Diagnostics.Process.Start("notepad.exe", _filename);
        }

        FileInfo fi = new FileInfo(_filename);
        if(fi.Length == 0)
            File.Delete(_filename);
    }
}

