﻿using System.Configuration;

//各ユーザー毎にアプリケーションの設定を保存するクラス。iniダメ「http://d.hatena.ne.jp/paz3/20091127/1259303402」
class Setting : ApplicationSettingsBase
{
    [UserScopedSetting()]
    public string ExeName
    {
        get { return (string)this["ExeName"]; }
        set { this["ExeName"] = value; }
    }

    [UserScopedSetting()]
    public string LoadFilePath
    {
        get { return (string)this["LoadFilePath"]; }
        set { this["LoadFilePath"] = value; }
    }

    [UserScopedSetting()]
    public string SaveFilePath
    {
        get { return (string)this["SaveFilePath"]; }
        set { this["SaveFilePath"] = value; }
    }

    [UserScopedSetting()]
    public string LoadFolderPath
    {
        get { return (string)this["LoadFolderPath"]; }
        set { this["LoadFolderPath"] = value; }
    }

    [UserScopedSetting()]
    public string SaveFolderPath
    {
        get { return (string)this["SaveFolderPath"]; }
        set { this["SaveFolderPath"] = value; }
    }

}

static class Global
{
    static private double _LLEpsilon = 0.000000005; //緯度経度でおおよそ0.5mmの誤差は無視しよう
    static private double _XYEpsilon = 0.0005; //平面直角でおおよそ0.5mmの誤差は無視しよう
    /// <summary>
    /// 
    /// </summary>
    static public double XYEpsilon
    {
        get
        {
            return _XYEpsilon;
        }
    }
    static public double LLEpsilon
    {
        get
        {
            return _LLEpsilon;
        }
    }

    /// <summary>
    /// 浮動小数点同士の比較は出来ないため、どの程度の差であれば同値とするかを設定する。
    /// </summary>
    static public double Epsilon
    {
        get; set;
    }

    /// <summary>
    /// シェープファイルでは、長さ0のポリラインは認められていない。その規約を無視するかどうかを設定する。
    /// </summary>
    static public bool ShapeStructAddPolyLineIgnoreError
    {
        get; set;
    }

    /// <summary>
    /// 区切りテキストを保存するとき、空文字列をダブルクォーテーションで囲むかどうかを設定する。
    /// </summary>
    static public bool EmptyStringSurroundAtSaveStringTable
    {
        get; set;
    }
}
