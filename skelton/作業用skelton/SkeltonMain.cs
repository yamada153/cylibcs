﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

public partial class SkeltonForm : Form
{
    private Setting _setting;
    private bool _exec;
    private CancellationTokenSource _CancellationTokenSource;

    private async Task Start()
    {
        using (_CancellationTokenSource = new CancellationTokenSource())
        {
            _exec = true; //処理実行中
            CloseButton.Text = "キャンセル";
            try
            {
                await Task.Run(() =>
                {
                    Tejun1();
//                    Tejun2();
                }, _CancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                MessageBox.Show("キャンセルされました。");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _exec = false;
                CloseButton.Text = "閉じる";
            }
        }
    }

    void Tejun1()
    {
        int min = 0;
        int max = 3000;

        for (int i = min; i <= max; i++)
        {
            _CancellationTokenSource.Token.ThrowIfCancellationRequested(); //キャンセルされたら例外を投げる
            UpdateLabel(i, max);
            Thread.Sleep(1);
        }
    }

    void Tejun2()
    {
        int min = 0;
        int max = 3000;

        for (int i = min; i <= max; i++)
        {
            _CancellationTokenSource.Token.ThrowIfCancellationRequested(); //キャンセルされたら例外を投げる
            UpdateLabel(i, max);
            Thread.Sleep(1);
        }
    }


    /// <summary>
    /// 
    /// </summary>
    private async void ExecButton_Click(object sender, EventArgs e)
    {
        /*
        if(!Directory.Exists(LoadFolderPath.Text))
        {
            MessageBox.Show("指定した読み込みフォルダ「" +LoadFolderPath.Text+ "」が見つかりませんでした");
            return;
        }
        
        if (!File.Exists(LoadFilePath.Text))
        {
            MessageBox.Show("指定した読み込みファイル「" + LoadFilePath.Text + "」が見つかりませんでした");
            return;
        }
        
        string savedir = Path.GetDirectoryName(SaveFilePath.Text);
        if (!Directory.Exists(savedir))
        {
            if(!FileFunc.ForceDirectories(savedir))
            {
                MessageBox.Show("指定した保存先ファイル「" + SaveFilePath.Text + "」の親ディレクトリの作成に失敗しました。");
                return;
            }
        }
        
        if (!Directory.Exists(SaveFolderPath.Text))
        {
            if (!FileFunc.ForceDirectories(SaveFolderPath.Text))
            {
                MessageBox.Show("指定した保存先フォルダ「" + SaveFolderPath.Text + "」の作成に失敗しました。");
                return;
            }
        }
        */

        /////ストップウォッチ開始/////
        ControlEnable(false);
        Cursor.Current = Cursors.WaitCursor;
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        try
        {
            ///////////////////ここから処理記述//////////////////////////
            await Start();
        }
        /////ストップウォッチ終了/////
        finally
        {
            ControlEnable(true);
            Cursor.Current = Cursors.Default;
            sw.Stop();
            MessageBox.Show(sw.Elapsed.ToString() + " 終了しました");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void LoadSetting()
    {
        //アプリケーション設定読み出し
        _setting = new Setting();
        try
        {
            this.Text = _setting.ExeName;
            this.LoadFilePath.Text = _setting.LoadFilePath;
            this.SaveFilePath.Text = _setting.SaveFilePath;
            this.LoadFolderPath.Text = _setting.LoadFolderPath;
            this.SaveFolderPath.Text = _setting.SaveFolderPath;
        }
        catch
        {
            //デフォルト値
            this.Text = "サンプルプログラム";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void SaveSetting()
    {
        _setting.ExeName = this.Text;
        _setting.LoadFilePath = this.LoadFilePath.Text;
        _setting.SaveFilePath = this.SaveFilePath.Text;
        _setting.LoadFolderPath = this.LoadFolderPath.Text;
        _setting.SaveFolderPath = this.SaveFolderPath.Text;
        _setting.Save();
    }

    /// <summary>
    /// 
    /// </summary>
    private void LoadFileSelect_Click(object sender, EventArgs e)
    {
        openFileDialog1.Multiselect = false;
        openFileDialog1.CheckFileExists = true;
        openFileDialog1.CheckPathExists = true;
        openFileDialog1.RestoreDirectory = true;
        openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"; //filterの設定「https://msdn.microsoft.com/ja-jp/library/system.windows.forms.filedialog.filter%28v=vs.110%29.aspx」

        if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
        LoadFilePath.Text = openFileDialog1.FileName;
    }

    /// <summary>
    /// 
    /// </summary>
    private void SaveFileSelect_Click(object sender, EventArgs e)
    {
        saveFileDialog1.DefaultExt = "txt";
        saveFileDialog1.OverwritePrompt = true;
        saveFileDialog1.AddExtension = true;
        saveFileDialog1.RestoreDirectory = true;

        if (saveFileDialog1.ShowDialog() != DialogResult.OK) return;
        SaveFilePath.Text = saveFileDialog1.FileName;
    }

    /// <summary>
    /// 
    /// </summary>
    public SkeltonForm()
    {
        InitializeComponent();
        LoadSetting();
        ProgressLabel.Visible = false;
        _exec = false;
    }

    /// <summary>
    /// 
    /// </summary>
    private void SkeltonForm_FormClosing(object sender, FormClosingEventArgs e)
    {
        SaveSetting();
    }

    /// <summary>
    /// 
    /// </summary>
    private void LoadFolderSelect_Click(object sender, EventArgs e)
    {
        folderBrowserDialog1.ShowNewFolderButton = false;

        if (folderBrowserDialog1.ShowDialog() != DialogResult.OK) return;
        LoadFolderPath.Text = folderBrowserDialog1.SelectedPath;
    }

    /// <summary>
    /// 
    /// </summary>
    private void SaveFolderSelect_Click(object sender, EventArgs e)
    {
        folderBrowserDialog1.ShowNewFolderButton = true;

        if (folderBrowserDialog1.ShowDialog() != DialogResult.OK) return;
        SaveFolderPath.Text = folderBrowserDialog1.SelectedPath;
    }

    /// <summary>
    /// 終了ボタンが押された
    /// </summary>
    private void CloseButton_Click(object sender, EventArgs e)
    {
        if (_exec)
            _CancellationTokenSource.Cancel();
        else
            Close();
    }

    /// <summary>
    /// プログラム実行中にEnableをfalseするかどうか設定する
    /// </summary>
    private void ControlEnable(bool enable)
    {
        SaveFileSelect.Enabled = enable;
        SaveFilePath.Enabled = enable;
        SaveFolderSelect.Enabled = enable;
        SaveFolderPath.Enabled = enable;
        LoadFileSelect.Enabled = enable;
        LoadFilePath.Enabled = enable;
        LoadFolderSelect.Enabled = enable;
        LoadFolderPath.Enabled = enable;
        ExecButton.Enabled = enable;
        InputBox.Enabled = enable;
        ProgressLabel.Visible = !enable;
    }

    private delegate void DelegateMemo(string str);
    /// <summary>
    /// メモボックスに文字列を表示させる
    /// </summary>
    private void MemoAppend(string str)
    {
        if (MemoBox.InvokeRequired)
        {
            var d = new DelegateMemo(MemoAppend);
            Invoke(d, new object[] { str });
        }
        else
        {
            MemoBox.AppendText(str);
        }
    }

    private delegate void DelegateUpdate(long value, long max);
    /// <summary>
    /// 進捗を更新する
    /// </summary>
    private void UpdateLabel(long value, long max)
    {
        if (ProgressLabel.InvokeRequired)
        {
            var d = new DelegateUpdate(UpdateLabel);
            Invoke(d, new object[] { value, max });
        }
        else
        {
            ProgressLabel.Text = value.ToString() + "/" + max.ToString();
        }
    }
}
