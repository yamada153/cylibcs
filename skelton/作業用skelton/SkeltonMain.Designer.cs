﻿    partial class SkeltonForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LoadFilePath = new System.Windows.Forms.TextBox();
            this.LoadFileSelect = new System.Windows.Forms.Button();
            this.SaveFileSelect = new System.Windows.Forms.Button();
            this.SaveFilePath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SaveFolderSelect = new System.Windows.Forms.Button();
            this.SaveFolderPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LoadFolderSelect = new System.Windows.Forms.Button();
            this.LoadFolderPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.MemoBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.InputBox = new System.Windows.Forms.TextBox();
            this.ProgressLabel = new System.Windows.Forms.Label();
            this.ExecButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "読み込みファイル";
            // 
            // LoadFilePath
            // 
            this.LoadFilePath.Location = new System.Drawing.Point(17, 31);
            this.LoadFilePath.Name = "LoadFilePath";
            this.LoadFilePath.Size = new System.Drawing.Size(315, 19);
            this.LoadFilePath.TabIndex = 1;
            // 
            // LoadFileSelect
            // 
            this.LoadFileSelect.Location = new System.Drawing.Point(338, 29);
            this.LoadFileSelect.Name = "LoadFileSelect";
            this.LoadFileSelect.Size = new System.Drawing.Size(20, 23);
            this.LoadFileSelect.TabIndex = 2;
            this.LoadFileSelect.UseVisualStyleBackColor = true;
            this.LoadFileSelect.Click += new System.EventHandler(this.LoadFileSelect_Click);
            // 
            // SaveFileSelect
            // 
            this.SaveFileSelect.Location = new System.Drawing.Point(338, 72);
            this.SaveFileSelect.Name = "SaveFileSelect";
            this.SaveFileSelect.Size = new System.Drawing.Size(20, 23);
            this.SaveFileSelect.TabIndex = 5;
            this.SaveFileSelect.UseVisualStyleBackColor = true;
            this.SaveFileSelect.Click += new System.EventHandler(this.SaveFileSelect_Click);
            // 
            // SaveFilePath
            // 
            this.SaveFilePath.Location = new System.Drawing.Point(17, 74);
            this.SaveFilePath.Name = "SaveFilePath";
            this.SaveFilePath.Size = new System.Drawing.Size(315, 19);
            this.SaveFilePath.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "保存ファイル";
            // 
            // SaveFolderSelect
            // 
            this.SaveFolderSelect.Location = new System.Drawing.Point(338, 161);
            this.SaveFolderSelect.Name = "SaveFolderSelect";
            this.SaveFolderSelect.Size = new System.Drawing.Size(20, 23);
            this.SaveFolderSelect.TabIndex = 11;
            this.SaveFolderSelect.UseVisualStyleBackColor = true;
            this.SaveFolderSelect.Click += new System.EventHandler(this.SaveFolderSelect_Click);
            // 
            // SaveFolderPath
            // 
            this.SaveFolderPath.Location = new System.Drawing.Point(17, 163);
            this.SaveFolderPath.Name = "SaveFolderPath";
            this.SaveFolderPath.Size = new System.Drawing.Size(315, 19);
            this.SaveFolderPath.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "保存フォルダ";
            // 
            // LoadFolderSelect
            // 
            this.LoadFolderSelect.Location = new System.Drawing.Point(338, 118);
            this.LoadFolderSelect.Name = "LoadFolderSelect";
            this.LoadFolderSelect.Size = new System.Drawing.Size(20, 23);
            this.LoadFolderSelect.TabIndex = 8;
            this.LoadFolderSelect.UseVisualStyleBackColor = true;
            this.LoadFolderSelect.Click += new System.EventHandler(this.LoadFolderSelect_Click);
            // 
            // LoadFolderPath
            // 
            this.LoadFolderPath.Location = new System.Drawing.Point(17, 120);
            this.LoadFolderPath.Name = "LoadFolderPath";
            this.LoadFolderPath.Size = new System.Drawing.Size(315, 19);
            this.LoadFolderPath.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "読み込みフォルダ";
            // 
            // MemoBox
            // 
            this.MemoBox.Location = new System.Drawing.Point(374, 31);
            this.MemoBox.Name = "MemoBox";
            this.MemoBox.Size = new System.Drawing.Size(191, 183);
            this.MemoBox.TabIndex = 12;
            this.MemoBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(372, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "メモ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "入力";
            // 
            // InputBox
            // 
            this.InputBox.Location = new System.Drawing.Point(43, 195);
            this.InputBox.Name = "InputBox";
            this.InputBox.Size = new System.Drawing.Size(289, 19);
            this.InputBox.TabIndex = 15;
            // 
            // ProgressLabel
            // 
            this.ProgressLabel.AutoSize = true;
            this.ProgressLabel.Location = new System.Drawing.Point(279, 231);
            this.ProgressLabel.Name = "ProgressLabel";
            this.ProgressLabel.Size = new System.Drawing.Size(53, 12);
            this.ProgressLabel.TabIndex = 16;
            this.ProgressLabel.Text = "進捗状況";
            // 
            // ExecButton
            // 
            this.ExecButton.Location = new System.Drawing.Point(385, 226);
            this.ExecButton.Name = "ExecButton";
            this.ExecButton.Size = new System.Drawing.Size(75, 23);
            this.ExecButton.TabIndex = 18;
            this.ExecButton.Text = "実行";
            this.ExecButton.UseVisualStyleBackColor = true;
            this.ExecButton.Click += new System.EventHandler(this.ExecButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(479, 226);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 19;
            this.CloseButton.Text = "閉じる";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SkeltonForm
            // 
            this.ClientSize = new System.Drawing.Size(572, 262);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ExecButton);
            this.Controls.Add(this.ProgressLabel);
            this.Controls.Add(this.InputBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.MemoBox);
            this.Controls.Add(this.SaveFolderSelect);
            this.Controls.Add(this.SaveFolderPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LoadFolderSelect);
            this.Controls.Add(this.LoadFolderPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SaveFileSelect);
            this.Controls.Add(this.SaveFilePath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LoadFileSelect);
            this.Controls.Add(this.LoadFilePath);
            this.Controls.Add(this.label1);
            this.Name = "SkeltonForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SkeltonForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LoadFilePath;
        private System.Windows.Forms.Button LoadFileSelect;
        private System.Windows.Forms.Button SaveFileSelect;
        private System.Windows.Forms.TextBox SaveFilePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SaveFolderSelect;
        private System.Windows.Forms.TextBox SaveFolderPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button LoadFolderSelect;
        private System.Windows.Forms.TextBox LoadFolderPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox MemoBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox InputBox;
        private System.Windows.Forms.Label ProgressLabel;
        private System.Windows.Forms.Button ExecButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
}
