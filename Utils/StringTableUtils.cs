/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

static class StringTableUtils
{
    /// <summary>
    /// fieldnameで指定した列の重複した値を削除する
    /// </summary>
    static public void Uniq(StringTable a, string fieldname)
    {
        for (int i = 0; i < a.Count(); i++)
        {
            string target = a[i, fieldname];
            int[] indexs = Search(a, fieldname, target);
            if (indexs.Length == 1) continue;

            for (int j = indexs.Length - 1; j > 0; j--)
                a.DeleteRow(indexs[j]);
        }
    }

    /// <summary>
    /// fieldname列から、valueと同じ値を持つ行の番号を返す
    /// </summary>
    static public int[] Search(StringTable a, string fieldname, string value)
    {
        SSIndex<string> ss = new SSIndex<string>(a.GetStringsFromColumnName(fieldname));
        return ss.Search(value);
    }

    /// <summary>
    /// StringTableのa, bの主キーを比較して、aに無いもの、bに無いものを取り出す。
    /// 配列0にはaに存在してbに存在しないもの、配列1にはbに存在してaに存在しないものを格納する
    /// </summary>
    /// <param name="primary_key">重複しないフィールド名を指定する</param>
    static public string[][] NothingReport(StringTable a, StringTable b, string primary_key)
    {
        string[][] result = new string[2][];

        //aに存在してbに存在しない
        List<string> tmp = new List<string>(a.Count());
        SSIndex<string> ss = new SSIndex<string>(b.GetStringsFromColumnName(primary_key));
        for (int i = 0; i < a.Count(); i++)
        {
            string pkey = a[i, primary_key];
            int[] indexs = ss.Search(pkey);
            if (indexs == null || indexs.Length == 0)
                tmp.Add(pkey);
        }
        result[0] = tmp.ToArray();
        //bに存在してaに存在しない
        tmp = new List<string>(b.Count());
        ss = new SSIndex<string>(a.GetStringsFromColumnName(primary_key));
        for (int i = 0; i < b.Count(); i++)
        {
            string pkey = b[i, primary_key];
            int[] indexs = ss.Search(pkey);
            if (indexs == null || indexs.Length == 0)
                tmp.Add(pkey);
        }
        result[1] = tmp.ToArray();

        return result;
    }

    /// <summary>
    /// 指定したフィールド名の値で、targetsと付き合う行を抜き出す。
    /// </summary>
    static public StringTable Extract(StringTable st, string fieldname, string[] targets)
    {
        StringTable result = new StringTable(st.GetFieldNames());

        SSIndex<string> ss = new SSIndex<string>(st.GetStringsFromColumnName(fieldname));
        foreach(string s in targets)
        {
            int[] indexs = ss.Search(s);
            if (indexs == null || indexs.Length == 0) continue;
            foreach (int i in indexs)
                result.Add(st.GetStringsFromRowNumber(i));
        }

        return result;
    }


    static public string[] CompareReport(StringTable a, StringTable b, string primary_key)
    {
        string[] fieldnames = a.GetFieldNames();
        if (ArrayFunc.Equal<string>(fieldnames, b.GetFieldNames()) != 1)
            throw new ArgumentException("フィールド構成が違うので、比較に不適です。");

        List<string> result = new List<string>(a.Count());

        SSIndex<string> ss = new SSIndex<string>(b.GetStringsFromColumnName(primary_key));
        for(int i = 0; i < a.Count(); i++)
        {
            string pkey = a[i, primary_key];
            int[] indexs = ss.Search(pkey);
            if (indexs == null) continue;
            
            for(int j = 0; j < fieldnames.Length; j++)
            {
                string astr = a[i, fieldnames[j]];
                string bstr = b[indexs[indexs.Length - 1], fieldnames[j]];
                if (astr != bstr)
                {
                    if (astr == "") astr = "\"\"";
                    if (bstr == "") bstr = "\"\"";
                    result.Add(a[i, primary_key] + "\t" + fieldnames[j] + "\t" + astr + "\t" + bstr);
                }
            }
        }

        return result.ToArray();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>
/// テスト用クラス
/// </summary>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static class TestStringTableUtils
{
}
