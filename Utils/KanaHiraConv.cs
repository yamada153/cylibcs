﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

// 「http://pgcafe.moo.jp/JAVA/string/main_1.htm」を参考に作成しました 

using System;
using System.Linq;
using System.Windows.Forms;

static class KanaHiraConv
{
    private static string[][] KanaHiraTable = new string[][]{
      // 2文字構成の濁点付き半角カナ
      // 必ずテーブルに先頭に置いてサーチ順を優先すること
      new string[] { "ｶﾞ", "ガ", "が" }, new string[] { "ｷﾞ", "ギ", "ぎ" }, new string[] { "ｸﾞ", "グ", "ぐ" }, new string[] { "ｹﾞ", "ゲ", "げ" }, new string[] { "ｺﾞ", "ゴ", "ご" },
      new string[] { "ｻﾞ", "ザ", "ざ" }, new string[] { "ｼﾞ", "ジ", "じ" }, new string[] { "ｽﾞ", "ズ", "ず" }, new string[] { "ｾﾞ", "ゼ", "ぜ" }, new string[] { "ｿﾞ", "ゾ", "ぞ" },
      new string[] { "ﾀﾞ", "ダ", "だ" }, new string[] { "ﾁﾞ", "ヂ", "ぢ" }, new string[] { "ﾂﾞ", "ヅ", "づ" }, new string[] { "ﾃﾞ", "デ", "で" }, new string[] { "ﾄﾞ", "ド", "ど" },
      new string[] { "ﾊﾞ", "バ", "ば" }, new string[] { "ﾋﾞ", "ビ", "び" }, new string[] { "ﾌﾞ", "ブ", "ぶ" }, new string[] { "ﾍﾞ", "ベ", "べ" }, new string[] { "ﾎﾞ", "ボ", "ぼ" },
      new string[] { "ﾊﾟ", "パ", "ぱ" }, new string[] { "ﾋﾟ", "ピ", "ぴ" }, new string[] { "ﾌﾟ", "プ", "ぷ" }, new string[] { "ﾍﾟ", "ペ", "ぺ" }, new string[] { "ﾎﾟ", "ポ", "ぽ" },
      new string[] { "ｳﾞ", "ヴ", "ヴ" },
      // 1文字構成の半角カナ
      new string[] { "ｱ", "ア", "あ" }, new string[] { "ｲ", "イ", "い" }, new string[] { "ｳ", "ウ", "う" }, new string[] { "ｴ", "エ", "え" }, new string[] { "ｵ", "オ", "お" },
      new string[] { "ｶ", "カ", "か" }, new string[] { "ｷ", "キ", "き" }, new string[] { "ｸ", "ク", "く" }, new string[] { "ｹ", "ケ", "け" }, new string[] { "ｺ", "コ", "こ" },
      new string[] { "ｻ", "サ", "さ" }, new string[] { "ｼ", "シ", "し" }, new string[] { "ｽ", "ス", "す" }, new string[] { "ｾ", "セ", "せ" }, new string[] { "ｿ", "ソ", "そ" },
      new string[] { "ﾀ", "タ", "た" }, new string[] { "ﾁ", "チ", "ち" }, new string[] { "ﾂ", "ツ", "つ" }, new string[] { "ﾃ", "テ", "て" }, new string[] { "ﾄ", "ト", "と" },
      new string[] { "ﾅ", "ナ", "な" }, new string[] { "ﾆ", "ニ", "に" }, new string[] { "ﾇ", "ヌ", "ぬ" }, new string[] { "ﾈ", "ネ", "ね" }, new string[] { "ﾉ", "ノ", "の" },
      new string[] { "ﾊ", "ハ", "は" }, new string[] { "ﾋ", "ヒ", "ぴ" }, new string[] { "ﾌ", "フ", "ふ" }, new string[] { "ﾍ", "ヘ", "へ" }, new string[] { "ﾎ", "ホ", "ほ" },
      new string[] { "ﾏ", "マ", "ま" }, new string[] { "ﾐ", "ミ", "み" }, new string[] { "ﾑ", "ム", "む" }, new string[] { "ﾒ", "メ", "め" }, new string[] { "ﾓ", "モ", "も" },
      new string[] { "ﾔ", "ヤ", "や" }, new string[] { "ﾕ", "ユ", "ゆ" }, new string[] { "ﾖ", "ヨ", "よ" },
      new string[] { "ﾗ", "ラ", "ら" }, new string[] { "ﾘ", "リ", "り" }, new string[] { "ﾙ", "ル", "る" }, new string[] { "ﾚ", "レ", "れ" }, new string[] { "ﾛ", "ロ", "ろ" },
      new string[] { "ﾜ", "ワ", "わ" }, new string[] { "ｦ", "ヲ", "を" }, new string[] { "ﾝ", "ン", "ん" },
      new string[] { "ｧ", "ァ", "ぁ" }, new string[] { "ｨ", "ィ", "ぃ" }, new string[] { "ｩ", "ゥ", "ぅ" }, new string[] { "ｪ", "ェ", "ぇ" }, new string[] { "ｫ", "ォ", "ぉ" },
      new string[] { "ｬ", "ャ", "ゃ" }, new string[] { "ｭ", "ュ", "ゅ" }, new string[] { "ｮ", "ョ", "ょ" }, new string[] { "ｯ", "ッ", "っ" },
      new string[] { "｡", "。", "。" }, new string[] { "｢", "「", "「" }, new string[] { "｣", "」", "」" }, new string[] { "､", "、", "、" }, new string[] { "･", "・", "・" },
      new string[] { "ｰ", "ー", "ー" }, new string[] { "", "", "" }
    };

    private enum ConvertType
    {
        HanKana = 0,
        ZenKana = 1,
        ZenHira = 2
    }

    /// <summary> 
    /// 半角カナを全角カナに変換する 
    /// </summary>
    public static string HanKanaToZenKana(string str)
    {
        CheckNullStr("HanKanaToZenKana", str);
        return Converter(str, ConvertType.HanKana, ConvertType.ZenKana, (char)0xff61, (char)0xff9f);
    }

    /// <summary> 
    /// 半角カナを全角ひらに変換する 
    /// </summary>
    public static string HanKanaToZenHira(string str)
    {
        CheckNullStr("HanKanaToZenHira", str);
        return Converter(str, ConvertType.HanKana, ConvertType.ZenHira, (char)0xff61, (char)0xff9f);
    }

    /// <summary> 
    /// 全角カナを半角カナに変換する 
    /// </summary>
    public static string ZenKanaToHanKana(string str)
    {
        CheckNullStr("ZenKanaToHanKana", str);
        return Converter(str, ConvertType.ZenKana, ConvertType.HanKana, (char)0x30a1, (char)0x30fc);
    }

    /// <summary> 
    /// 全角カナを全角ひらに変換する 
    /// </summary>
    public static string ZenKanaToZenHira(string str)
    {
        CheckNullStr("ZenKanaToZenHira", str);
        return Converter(str, ConvertType.ZenKana, ConvertType.ZenHira, (char)0x30a1, (char)0x30fc);
    }

    /// <summary> 
    /// 全角ひらを半角カナに変換する 
    /// </summary>
    public static String ZenHiraToHanKana(string str)
    {
        CheckNullStr("ZenHiraToHanKana", str);
        return Converter(str, ConvertType.ZenHira, ConvertType.HanKana, (char)0x3041, (char)0x3096);
    }

    /// <summary> 全角ひらを全角カナに変換する </summary>
    public static String ZenHiraToZenKana(string str)
    {
        CheckNullStr("ZenHiraToZenKana", str);
        return Converter(str, ConvertType.ZenHira, ConvertType.ZenKana, (char)0x3041, (char)0x3096);
    }

    /// <summary>
    /// 文字列中の半角/全角を変換する
    /// </summary>
    private static string Converter(string str, ConvertType src, ConvertType dst, char range_under, char range_top)
    {
        string result = "";

        for (int i = 0; i < str.Length; i++)
        {
            Char ch = str[i];

            if (!(ch.CompareTo(range_under) >= 0 && ch.CompareTo(range_top) <= 0)) //変換対象文字がUnicodeのコード範囲かな？
                result += ch;
            else
            {
                //strのi文字目と一致する文字をKanaHiraTableのdst列から見つける。
                foreach (var s in KanaHiraTable.Where(x => str.Substring(i).StartsWith(x[(int)src])))
                {
                    if (s[(int)dst] == "")
                        result += s[(int)src]; //変換できなかった...
                    else
                        result += s[(int)dst]; //変換できた!
                    break;
                }
            }
        }
        return result;
    }

    private static void CheckNullStr(string functionname, string str)
    {
        if (str == null)
            throw new ArgumentNullException("KanaHiraConv." + functionname + "の引数「str」がnullを受け取りました。");
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestKanaHiraConv
{
    static public void Test(RichTextBox memo)
    {
        string s = "";
        memo.AppendText("HanKanaToZenHira: 「" + s + "」 -> " + KanaHiraConv.HanKanaToZenHira(s) + "\n");
        memo.AppendText("HanKanaToZenKana: 「" + s + "」 -> " + KanaHiraConv.HanKanaToZenKana(s) + "\n");
        memo.AppendText("ZenHiraToHanKana: 「" + s + "」 -> " + KanaHiraConv.ZenHiraToHanKana(s) + "\n");
        memo.AppendText("ZenHiraToZenKana: 「" + s + "」 -> " + KanaHiraConv.ZenHiraToZenKana(s) + "\n");
        memo.AppendText("ZenKanaToHanKana: 「" + s + "」 -> " + KanaHiraConv.ZenKanaToHanKana(s) + "\n");
        memo.AppendText("ZenKanaToZenHira: 「" + s + "」 -> " + KanaHiraConv.ZenKanaToZenHira(s) + "\n");

        s = "ヴィーナスﾌｫｰﾄでお買い物";
        memo.AppendText("HanKanaToZenHira: 「" + s + "」 -> " + KanaHiraConv.HanKanaToZenHira(s) + "\n");
        memo.AppendText("HanKanaToZenKana: 「" + s + "」 -> " + KanaHiraConv.HanKanaToZenKana(s) + "\n");
        memo.AppendText("ZenHiraToHanKana: 「" + s + "」 -> " + KanaHiraConv.ZenHiraToHanKana(s) + "\n");
        memo.AppendText("ZenHiraToZenKana: 「" + s + "」 -> " + KanaHiraConv.ZenHiraToZenKana(s) + "\n");
        memo.AppendText("ZenKanaToHanKana: 「" + s + "」 -> " + KanaHiraConv.ZenKanaToHanKana(s) + "\n");
        memo.AppendText("ZenKanaToZenHira: 「" + s + "」 -> " + KanaHiraConv.ZenKanaToZenHira(s) + "\n");

        s = null;
        try { KanaHiraConv.HanKanaToZenHira(s); } catch (Exception e) { memo.AppendText("HanKanaToZenHira: " + e.Message + "\n"); }
        try { KanaHiraConv.HanKanaToZenKana(s); } catch (Exception e) { memo.AppendText("HanKanaToZenKana: " + e.Message + "\n"); }
        try { KanaHiraConv.ZenHiraToHanKana(s); } catch (Exception e) { memo.AppendText("ZenHiraToHanKana: " + e.Message + "\n"); }
        try { KanaHiraConv.ZenHiraToZenKana(s); } catch (Exception e) { memo.AppendText("ZenHiraToZenKana: " + e.Message + "\n"); }
        try { KanaHiraConv.ZenKanaToHanKana(s); } catch (Exception e) { memo.AppendText("ZenKanaToHanKana: " + e.Message + "\n"); }
        try { KanaHiraConv.ZenKanaToZenHira(s); } catch (Exception e) { memo.AppendText("ZenKanaToZenHira: " + e.Message + "\n"); }
    }
}