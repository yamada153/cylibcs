﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing.Imaging;

static class WindowsUtils
{
    ///スクリーンショットのための関数。下記URLのコードを使いました。
    ///http://dobon.net/vb/dotnet/graphics/screencapture.html
    ///

    [DllImport("user32.dll")]
    private static extern IntPtr GetDC(IntPtr hwnd);

    [DllImport("gdi32.dll")]
    private static extern int BitBlt(IntPtr hDestDC, int x, int y, int nWidth, int nHeight, IntPtr hSrcDC, int xSrc, int ySrc, int dwRop);

    [DllImport("user32.dll")]
    private static extern IntPtr ReleaseDC(IntPtr hwnd, IntPtr hdc);
    private const int SRCCOPY = 13369376;

    /// <summary>
    /// プライマリスクリーンの画像を取得する
    /// </summary>
    public static Bitmap CaptureScreen(int x, int y, int width, int height)
    {
        Bitmap bmp = null;

        //プライマリモニタのデバイスコンテキストを取得
        IntPtr disDC = GetDC(IntPtr.Zero);
        try
        {
            bmp = new Bitmap(width, height);

            Graphics g = Graphics.FromImage(bmp);
            try
            {
                //Graphicsのデバイスコンテキストを取得
                IntPtr hDC = g.GetHdc();
                try
                {
                    //Bitmapに画像をコピーする(ビットブロック転送)
                    BitBlt(hDC, 0, 0, bmp.Width, bmp.Height, disDC, x, y, SRCCOPY);
                }
                finally
                {
                    g.ReleaseHdc(hDC);
                }
            }
            finally
            {
                g.Dispose();
            }
        }
        finally
        {
            ReleaseDC(IntPtr.Zero, disDC);
        }
        return bmp;
    }

    /// <summary>
    /// 画像ファイルをクリップボードに書き込む。
    /// </summary>
    static public void LoadImageToClipBoard(string loadfilename)
    {
        using (FileStream f = new FileStream(loadfilename, FileMode.Open))
        {
            System.Windows.Forms.Clipboard.SetImage(new Bitmap(f));
        }
    }

    /// <summary>
    /// クリップボードの画像を保存する。失敗したらfalseを返す
    /// </summary>http://dobon.net/vb/dotnet/graphics/encoderparameters.html
    static public bool SaveImageFromClipBoardImage(string savefilename, int q)
    {
        Image img = System.Windows.Forms.Clipboard.GetImage();
        if (img == null) return false;

        Bitmap bmp = new Bitmap(img);

        EncoderParameters eps = new EncoderParameters(1);
        EncoderParameter ep = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, q);
        eps.Param[0] = ep;

        ImageCodecInfo ic = GetEncoderInfo(ImageFormat.Jpeg);
        if (ic == null)
            return false;

        bmp.Save(savefilename, ic, eps);
        return true;
    }

    //ImageFormatで指定されたImageCodecInfoを探して返す
    private static ImageCodecInfo GetEncoderInfo(ImageFormat f)
    {
        ImageCodecInfo[] encs = ImageCodecInfo.GetImageEncoders();
        foreach (ImageCodecInfo enc in encs)
            if (enc.FormatID == f.Guid)
                return enc;
        return null;
    }
}
