/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;

static class Geo2DIntersect
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //以下の関数の「GeoPoint[][] pol」は、インデックス0が外側、それ以外は内側を表す。PolygonToMultiPolygonに予め通しておくと良い
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    ///  折れ線が交差している点を返す。折れ線の始点と終点が同じ場合、この点は交点とはしない
    /// </summary>
    static public GeoPoint[] IntersectionSelf(GeoPoint[] pline)
    {
        List<GeoPoint> result = new List<GeoPoint>(pline.Length);

        for (int i = 1; i < pline.Length; i++)
            for (int j = i + 2; j < pline.Length; j++)
            {
                if (i == j) continue;

                GeoPoint[] tmp = Intersection(pline[i - 1], pline[i], pline[j - 1], pline[j]);
                if (tmp == null) continue;

                foreach (GeoPoint p in tmp)
                    if(!(p == pline[0] && p == pline.Last()))
                        result.Add(p);
            }
        return result.ToArray();
    }

    /// <summary>
    ///  線分a、線分bの交点を返す
    /// </summary>
    /// <see cref=">http://mf-atelier.sakura.ne.jp/mf-atelier/modules/tips/program/algorithm/a1.html"/>
    /// <see cref=">http://www.hiramine.com/programming/graphics/2d_segmentintersection.html"/>
    static public GeoPoint[] Intersection(GeoPoint a_start, GeoPoint a_end, GeoPoint b_start, GeoPoint b_end)
    {
        GeoPoint A = a_end - a_start;
        GeoPoint B = b_end - b_start;

        double ksi = (B.Y) * (b_end.X - a_start.X) - (B.X) * (b_end.Y - a_start.Y);
        double eta = (A.X) * (b_end.Y - a_start.Y) - (A.Y) * (b_end.X - a_start.X);
        double delta = (A.X) * (B.Y) - (A.Y) * (B.X);

        if (MathEx.EqualsEx(delta, 0) && MathEx.EqualsEx(eta, 0) && MathEx.EqualsEx(ksi, 0)) //平行
        {
            List<GeoPoint> result = new List<GeoPoint>();
            //線分aと線分bが重なっている場合、線分間に存在する端点を交点としている
            if (Geo2DExtra.IsPointOnLine(b_start, a_start, a_end)) result.Add(b_start);
            if (Geo2DExtra.IsPointOnLine(a_start, b_start, b_end)) result.Add(a_start);
            if (Geo2DExtra.IsPointOnLine(b_end, a_start, a_end)) result.Add(b_end);
            if (Geo2DExtra.IsPointOnLine(a_end, b_start, b_end)) result.Add(a_end);
            if (result.Count == 0)
                return null;
            return result.Distinct().ToArray();
        }
        else if (!MathEx.EqualsEx(delta, 0))
        {
            double ramda = ksi / delta;
            double mu = eta / delta;

            if (((ramda >= 0) && (ramda <= 1)) && ((mu >= 0) && (mu <= 1)))
                return new GeoPoint[] { a_start + ramda * A };
        }

        return null;
    }

    /// <summary>
    /// 2線分の交点を返す。実際には交点が無くても、線分を延ばした時に交差する点を返す
    /// </summary>
    static public GeoPoint IntersectionVirtual(GeoPoint a_start, GeoPoint a_end, GeoPoint b_start, GeoPoint b_end)
    {
        GeoPoint koten = new GeoPoint();
        double dev = (a_end.Y - a_start.Y) * (b_end.X - b_start.X) - (a_end.X - a_start.X) * (b_end.Y - b_start.Y);

        if (Math.Abs(dev - 0) <= Global.Epsilon) //平行
        {
            koten.X = double.NaN;
            koten.Y = double.NaN;
            return koten;
        }

        double d1, d2;
        d1 = (b_start.Y * b_end.X - b_start.X * b_end.Y);
        d2 = (a_start.Y * a_end.X - a_start.X * a_end.Y);

        koten.X = d1 * (a_end.X - a_start.X) - d2 * (b_end.X - b_start.X);
        koten.X /= dev;
        koten.Y = d1 * (a_end.Y - a_start.Y) - d2 * (b_end.Y - b_start.Y);
        koten.Y /= dev;

        return koten;
    }

    /// <summary>
    /// 線分が交差していればtrueを返す
    /// </summary>
    static public bool Intersect(GeoPoint a_start, GeoPoint a_end, GeoPoint b_start, GeoPoint b_end)
    {
        double aminx, aminy, amaxx, amaxy;
        double bminx, bminy, bmaxx, bmaxy;

        if (a_start.X < a_end.X)
        {
            amaxx = a_end.X;
            aminx = a_start.X;
        }
        else
        {
            amaxx = a_start.X;
            aminx = a_end.X;
        }
        if (b_start.X < b_end.X)
        {
            bmaxx = b_end.X;
            bminx = b_start.X;
        }
        else
        {
            bmaxx = b_start.X;
            bminx = b_end.X;
        }
        if (amaxx < bminx) return false;
        if (bmaxx < aminx) return false;
        if (aminx > bmaxx) return false;
        if (bminx > amaxx) return false;

        if (a_start.Y < a_end.Y)
        {
            amaxy = a_end.Y;
            aminy = a_start.Y;
        }
        else
        {
            amaxy = a_start.Y;
            aminy = a_end.Y;
        }
        if (b_start.Y < b_end.Y)
        {
            bmaxy = b_end.Y;
            bminy = b_start.Y;
        }
        else
        {
            bmaxy = b_start.Y;
            bminy = b_end.Y;
        }
        if (amaxy < bminy) return false;
        if (bmaxy < aminy) return false;
        if (aminy > bmaxy) return false;
        if (bminy > amaxy) return false;

        if (Geo2DExtra.IsPointOnLine(a_start, b_start, b_end)) return true;
        if (Geo2DExtra.IsPointOnLine(a_end, b_start, b_end)) return true;
        if (Geo2DExtra.IsPointOnLine(b_start, a_start, a_end)) return true;
        if (Geo2DExtra.IsPointOnLine(b_end, a_start, a_end)) return true;

        if (Intersection(a_start, a_end, b_start, b_end) != null) return true;
        return false;
//        return (Geo2DBase.Side(a_start, b_start, b_end) * Geo2DBase.Side(a_end, b_start, b_end) < Global.Epsilon) &&
//               (Geo2DBase.Side(b_start, a_start, a_end) * Geo2DBase.Side(b_end, a_start, a_end) < Global.Epsilon);
    }

    /// <summary>
    ///  ポイント、折れ線が交差していればtrueを返す
    /// </summary>
    static public bool Intersect(GeoPoint pnt, GeoPoint[] pline)
    {
        for (int i = 1; i < pline.Length - 1; i++)
            if (Intersect(pnt, pnt, pline[i - 1], pline[i]))
                return true;
        return false;
    }

    /// <summary>
    ///  折れ線a、bが交差していればtrueを返す
    /// </summary>
    static public bool Intersect(GeoPoint[] pline_a, GeoPoint[] pline_b)
    {
        for (int i = 0; i < pline_a.Length - 1; i++)
        {
            GeoPoint pline_a_start = pline_a[i];
            GeoPoint pline_a_end = pline_a[i + 1];

            for (int j = 0; j < pline_b.Length - 1; j++)
            {
                GeoPoint pline_b_start = pline_b[j];
                GeoPoint pline_b_end = pline_b[j + 1];

                if (Intersect(pline_a_start, pline_a_end, pline_b_start, pline_b_end))
                    return true;
            }
        }
        return false;
    }

    /// <summary>
    ///  ポイント、ポリゴンが交差していればtrueを返す
    /// </summary>
    static public bool Intersect(GeoPoint pnt, GeoPoint[][] pol)
    {
        for (int i = 0; i < pol.Length - 1; i++)
            if (Intersect(pnt, pol[i]))
                return true;

        return WithIn(pnt, pol);
    }

    /// <summary>
    ///  折れ線とポリゴンが交差していればtrueを返す
    /// </summary>
    static public bool Intersect(GeoPoint[] pline, GeoPoint[][] pol)
    {
        for (int i = 0; i < pol.Length; i++)
            if (Intersect(pline, pol[i])) return true;
        return false;
    }

    /// <summary>
    ///  ポリゴンa、bが交差していればtrueを返す
    /// </summary>
    static public bool Intersect(GeoPoint[][] pola, GeoPoint[][] polb)
    {
        for (int i = 0; i < pola.Length; i++)
            if (Intersect(pola[i], polb)) return true;
        return false;
    }

    /// <summary>
    /// 点がポリゴン内に存在するならtrueを返す。ポリゴンの辺上に存在する場合はfalseを返す
    /// </summary>    
    static public bool WithIn(GeoPoint pnt, GeoPoint[] pol)
    {
        if (Geo2DExtra.IsPointOnLine(pnt, pol)) return false; //辺上に存在する
        return ((CrossingNumber(pol, pnt) % 2) == 1);
    }

    /// <summary>
    /// 点が内側ポリゴン内に存在しないで、外側ポリゴン内に存在するならtrueを返す。
    /// </summary>    
    static public bool WithIn(GeoPoint pnt, GeoPoint[][] pol)
    {
        for (int i = 1; i < pol.Length; i++)
            if (WithIn(pnt, pol[i])) return false;

        return WithIn(pnt, pol[0]);
    }

    /// <summary>
    /// 折れ線がポリゴン内に存在すればtrueを返す。
    /// </summary>    
    static public bool WithIn(GeoPoint[] pline, GeoPoint[] pol)
    {
        if (Intersect(pline, pol)) return false;

        for (int i = 0; i < pline.Length - 1; i++)
            if (!WithIn(pline[i], pol)) return false;
        return true;
    }

    /// <summary>
    /// 折れ線がポリゴン内に存在すればtrueを返す。
    /// </summary>    
    static public bool WithIn(GeoPoint[] pline, GeoPoint[][] pol)
    {
        for (int i = 1; i < pol.Length; i++)
            if (WithIn(pline, pol[i])) return false;
        return WithIn(pline, pol[0]);
    }

    /// <summary>
    /// ポリゴンがポリゴン内に存在すればtrueを返す。
    /// </summary>    
    static public bool WithIn(GeoPoint[][] pola, GeoPoint[] polb)
    {
        //polaの外側ポリゴンがpolbに含まれているかどうかだけチェックすればいい
        return WithIn(pola[0], polb);
    }

    /// <summary>
    /// ポリゴンがポリゴン内に存在すればtrueを返す。
    /// </summary>    
    static public bool WithIn(GeoPoint[][] pola, GeoPoint[][] polb)
    {
        //falseにならなければ、polaの外側は（つまりpola全体）、polbの内側に含まれていない
        for (int i = 1; i < polb.Length; i++)
            if (WithIn(pola[0], polb[i])) return false;
        //falseにならなければ、polaの内側は、polbの外側に含まれる
        for (int i = 1; i < pola.Length; i++)
            if (WithIn(pola[i], polb[0])) return false;

        return WithIn(pola[0], polb[0]);
    }

    /// <summary>
    ///  折れ線a,bの交点を返す。
    /// </summary>
    static public GeoPoint[] Intersection(GeoPoint[] a, GeoPoint[] b)
    {
        List<GeoPoint> result = new List<GeoPoint>(100);

        for (int i = 1; i < a.Length; i++)
            for (int j = 1; j < b.Length; j++)
            {
                GeoPoint[] tmp = Intersection(a[i - 1], a[i], b[j - 1], b[j]);
                if (tmp == null) continue;
                foreach (GeoPoint p in tmp)
                    result.Add(p);
            }

        if (result.Count == 0)
            return new GeoPoint[] { new GeoPoint(double.NaN, double.NaN) };
        return ArrayFunc.Distinct(result.ToArray());
    }

    /// <summary>
    ///  折れ線aと,ポリゴンbの交点を返す。
    /// </summary>
    static public GeoPoint[] Intersection(GeoPoint[] a, GeoPoint[][] b)
    {
        List<GeoPoint> result = new List<GeoPoint>(100);

        for (int i = 0; i < b.Length; i++)
        {
            GeoPoint[] tmp = Intersection(a, b[i]);
            if (tmp == null) continue;
            for (int j = 0; j < tmp.Length; j++)
                result.Add(tmp[j]);
        }

        if (result.Count == 0)
            return new GeoPoint[] { new GeoPoint(double.NaN, double.NaN) };
        return ArrayFunc.Distinct(result.ToArray());
    }

    /// <summary>
    /// ポリゴンa,bの交点を返す。
    /// </summary>
    static public GeoPoint[] Intersection(GeoPoint[][] a, GeoPoint[][] b)
    {
        List<GeoPoint> result = new List<GeoPoint>(100);

        for (int i = 0; i < b.Length; i++)
            for (int j = 0; j < a.Length; j++)
            {
                GeoPoint[] tmp = Intersection(a[j], b[i]);
                if (tmp == null) continue;
                for (int k = 0; k < tmp.Length; k++)
                    result.Add(tmp[k]);
            }

        if (result.Count == 0)
            return new GeoPoint[] { new GeoPoint(double.NaN, double.NaN) };
        return ArrayFunc.Distinct(result.ToArray());
    }

    /// <summary>
    /// CrossingNumberアルゴリズムを使ったポイント in ポリゴン判定関数
    /// 「MATH01_Various-4slides.pdf」の6ページ
    /// </summary>
    static private int CrossingNumber(GeoPoint[] pol, GeoPoint q)
    {
        Func<GeoPoint, GeoPoint, GeoPoint, bool> fo = (p1, p2, other) =>
        {
            if (((p1.Y > other.Y) == (p2.Y > other.Y))) return false;

            //p1p2が成す線分Pと、点otherを通る水平線との交点をother'(xc, other.y)とする
            //線分Pの傾きと、p1other'が成す線分の傾きは同じだから(p2.Y-p1.Y)/(p2.X-p1.X) = (other.Y-p1.Y)/(xc-p1.X)
            //上記式を変形して下記式を得る
            double xc = p1.X + ((other.Y - p1.Y) * (p2.X - p1.X)) / (p2.Y - p1.Y);

            return (xc > other.X);
        };

        int ncross = 0;
        for (int i = 0; i < pol.Length - 1; i++)
            if (fo(pol[i], pol[i + 1], q))
                ncross++;

        return ncross;
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestGeo2DIntersect
{
    static public void IntersectSelf(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pol = new GeoPoint[]
        {
            new GeoPoint(0, 0),
            new GeoPoint(100, 0),
            new GeoPoint(100, 100),
            new GeoPoint(0, 100)
        };

        GeoPoint[] tmp = Geo2DIntersect.IntersectionSelf(pol);
        foreach(GeoPoint p in tmp) memo.AppendText(p.ToString() + "\n");
    }

    static public void Intersect1(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint pnt = new GeoPoint(0, 0);
        //点が線分(0, 10),(45, 45)に重なるかな？
        pnt = new GeoPoint(0, 0); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(45, 45); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(10, 10); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(-10, 10); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(10, -10); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(-10, -10); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(10.00000001, 10); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
        pnt = new GeoPoint(10.00000000000000000001, 10); memo.AppendText(Geo2DIntersect.Intersect(pnt, pnt, new GeoPoint(0, 0), new GeoPoint(45, 45)).ToString() + "\n");
    }

    static public void WithIn1(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint pnt = new GeoPoint(0, 0);
        GeoPoint[] pol = new GeoPoint[] {
            new GeoPoint(0, 0),
            new GeoPoint(0, 100),
            new GeoPoint(100,100),
            new GeoPoint(100, 0),
            new GeoPoint(0, 0)
        };

        memo.AppendText("ノードと同じ位置に点が存在する場合 falseが正しい\n");
        pnt.X = 0; pnt.Y = 0; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 0; pnt.Y = 100; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 100; pnt.Y = 100; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 100; pnt.Y = 0; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        memo.AppendText("辺上に点が存在する場合 falseが正しい\n");
        pnt.X = 50; pnt.Y = 0; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 100; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 50; pnt.Y = 100; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 0; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        memo.AppendText("内に点が存在する場合 trueが正しい\n");
        pnt.X = 50; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 50; pnt.Y = 0.00000000000000000001; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        memo.AppendText("外に点が存在する場合 falseが正しい\n");
        pnt.X = 50; pnt.Y = -10; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 50; pnt.Y = -0.00000000000000000001; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 200; pnt.Y = 0; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = -200; pnt.Y = 0; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");

        memo.AppendText("違うオブジェクトでWithIn\n");
        pol = new GeoPoint[] {
            new GeoPoint(0, 0),
            new GeoPoint(0, 100),
            new GeoPoint(50, 50),
            new GeoPoint(100,100),
            new GeoPoint(150, 50),
            new GeoPoint(200, 100),
            new GeoPoint(200, 0),
            new GeoPoint(0, 0)
        };
        memo.AppendText("内に点が存在する場合 trueが正しい\n");
        pnt.X = 60; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 40; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 60; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 40; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        memo.AppendText("外に点が存在する場合 falseが正しい\n");
        pnt.X = 200; pnt.Y = 100; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = -200; pnt.Y = 100; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = 200; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
        pnt.X = -200; pnt.Y = 50; memo.AppendText(pnt.ToString() + " : " + Geo2DIntersect.WithIn(pnt, pol).ToString() + "\n");
    }

    static public void Intersection(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] result = null;

        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(0, 10), new GeoPoint(0, -10));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");

        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(0, -0.000000000000005), new GeoPoint(0, -10));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");
        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(0, -0.0000000000000005), new GeoPoint(0, -10));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");

        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(-5, 0), new GeoPoint(5, 0));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");

        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(-11, 0), new GeoPoint(5, 0));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");

        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(-5, 0), new GeoPoint(11, 0));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");

        result = Geo2DIntersect.Intersection(new GeoPoint(-10, 0), new GeoPoint(10, 0), new GeoPoint(-10, 0), new GeoPoint(10, 0));
        if (result == null) memo.AppendText("NULL\n");
        else foreach (GeoPoint g in result) memo.AppendText(g.ToString() + "\n");

    }
}