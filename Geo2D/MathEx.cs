/******************************************************************************************
 *  Copyright (c) 2016,2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;

static class MathEx
{
    /// <summary>
    /// ベクトル
    /// </summary>
    static public GeoPoint Vector(GeoPoint a, GeoPoint b)
    {
        return new GeoPoint(b.X - a.X, b.Y - a.Y, b.Z - a.Z);
    }

    /// <summary>
    /// ベクトルの大きさを返す
    /// </summary>
    static public double Abs(GeoPoint a)
    {
        return Math.Sqrt(Math.Pow(a.X, 2) + Math.Pow(a.Y, 2) + Math.Pow(a.Z, 2));
    }

    /// <summary>
    /// 内積
    /// </summary>
    static public double Dot(GeoPoint a, GeoPoint b)
    {
        return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
    }

    /// <summary>
    /// 外積
    /// </summary>
    static public GeoPoint Cross(GeoPoint a, GeoPoint b)
    {
        return new GeoPoint(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
    }

    /// <summary>
    /// 列毎に合計値を取得する
    /// </summary>
    static public double[] SumCols(double[,] data)
    {
        int width = data.GetLength(0);
        int height = data.GetLength(1);

        double[] results = new double[width];

        for (int i = 0; i < width; i++)
        {
            results[i] = 0;
            for (int j = 0; j < height; j++)
                results[i] += data[i, j];
        }

        return results;
    }

    /// <summary>
    /// 行毎に合計値を取得する
    /// </summary>
    static public double[] SumRows(double[,] data)
    {
        int width = data.GetLength(0);
        int height = data.GetLength(1);

        double[] results = new double[height];

        for (int i = 0; i < height; i++)
        {
            results[i] = 0;
            for (int j = 0; j < width; j++)
                results[i] += data[j, i];
        }

        return results;
    }

    /// <summary>
    /// 与えられたノードの近似直線を返す
    /// </summary>
    static public GeoPoint[] NearLine(GeoPoint[] pnts)
    {
        double sigma_x_pow = 0;
        for (int i = 0; i < pnts.Length; i++)
            sigma_x_pow += Math.Pow(pnts[i].X, 2);

        double sigma_x = 0;
        for (int i = 0; i < pnts.Length; i++)
            sigma_x += pnts[i].X;

        double sigma_xy = 0;
        for (int i = 0; i < pnts.Length; i++)
            sigma_xy += pnts[i].X * pnts[i].Y;

        double sigma_y = 0;
        for (int i = 0; i < pnts.Length; i++)
            sigma_y += pnts[i].Y;

        double n = pnts.Length;

        double a = (n * sigma_xy - sigma_x * sigma_y) / (n * sigma_x_pow - Math.Pow(sigma_x, 2));
        double b = (sigma_x_pow * sigma_y - sigma_xy * sigma_x) / (n * sigma_x_pow - Math.Pow(sigma_x, 2));

        GeoPoint[] result = new GeoPoint[2];
        result[0].X = pnts[0].X;
        result[0].Y = a * pnts[0].X + b;
        result[1].X = pnts[pnts.Length - 1].X;
        result[1].Y = a * pnts[pnts.Length - 1].X + b;
        return result;
    }

    /// <summary>
    /// 絶対誤差を使ってaとbを等しいとみなせるか判定する
    /// </summary>
    static public bool EqualsEx(double a, double b)
    {
        if (double.IsNaN(a) && double.IsNaN(b)) return true;
        if (double.IsNegativeInfinity(a) && double.IsNegativeInfinity(b)) return true;
        if (double.IsPositiveInfinity(a) && double.IsPositiveInfinity(b)) return true;

        if (Math.Abs(a - b) <= Global.Epsilon)
            return true;

        return false;
    }

    /// <summary>
    /// 指定した小数点以下の桁で四捨五入する
    /// </summary>
    static public double RoundOff(double f, int dec)
    {
        return Math.Round(f, Math.Min(dec, 15), MidpointRounding.AwayFromZero);
    }
}