/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;

static class Geo2DExtra
{
    /// <summary>
    /// 線分上にpntが存在すればtrueを返す。線分の始点、終点と同じでもtrueを返す
    /// </summary>
    static public bool IsPointOnLine(GeoPoint pnt, GeoPoint start, GeoPoint end)
    {
        //端点に一致するかな？
//        if (pnt == start) return true;
//        if (pnt == end) return true;

        double maxx, maxy, minx, miny;

        if (start.X < end.X)
        {
            maxx = end.X;
            minx = start.X;
        }
        else
        {
            maxx = start.X;
            minx = end.X;
        }

        if (start.Y < end.Y)
        {
            maxy = end.Y;
            miny = start.Y;
        }
        else
        {
            maxy = start.Y;
            miny = end.Y;
        }

        if (pnt.X > maxx) return false;
        if (pnt.X < minx) return false;
        if (pnt.Y > maxy) return false;
        if (pnt.Y < miny) return false;

        return MathEx.EqualsEx((end.Y - start.Y) * (pnt.X - start.X), (pnt.Y - start.Y) * (end.X - start.X));
    }

    /// <summary>
    /// 折れ線上にpntが存在すればtrueを返す
    /// </summary>
    static public bool IsPointOnLine(GeoPoint pnt, GeoPoint[] pnts)
    {
        for(int i = 1; i < pnts.Length; i++)
            if (IsPointOnLine(pnt, pnts[i - 1], pnts[i]))
                return true;
        return false;
    }

    /// <summary>
    /// 折れ線上にpntが存在すればtrueを返す
    /// </summary>
    static public bool IsPointOnLine(GeoPoint pnt, GeoPoint[][] pnts)
    {
        for (int i = 0; i < pnts.Length; i++)
            for (int j = 1; j < pnts[i].Length; j++)
                if (IsPointOnLine(pnt, pnts[i][j - 1], pnts[i][j]))
                    return true;
        return false;
    }

    /// <summary>
    /// 線分が他の線分上に存在すればtrueを返す。線分の始点、終点と同じでもtrueを返す
    /// </summary>
    static public bool IsLineOnLine(GeoPoint start, GeoPoint end, GeoPoint otherstart, GeoPoint otherend)
    {
        if (!IsPointOnLine(start, otherstart, otherend)) return false;
        if (!IsPointOnLine(end, otherstart, otherend)) return false;
        return true;
    }

    /// <summary>
    /// 点と線分の距離を返す
    /// </summary>
    /// <returns></returns>
    static public double Distance(GeoPoint pnt, GeoPoint sp, GeoPoint ep)
    {
        GeoPoint p = Geo2DBase.ClosestPoint(pnt, sp, ep);
        return Geo2DBase.Distance(p, pnt);
    }

    /// <summary>
    /// 始点から終点までnずつ進んだ位置の配列を返す
    /// </summary>
    static public GeoPoint[] Destinations(GeoPoint[] pline, double len)
    {
        List<GeoPoint> result = new List<GeoPoint>();
        result.Add(pline[0]);

        double current_position = len;
        for (int i = 1; i < pline.Length; i++)
        {
            double dis = Geo2DBase.Distance(pline[i - 1], pline[i]);
            //線分上にlen間隔で点を打っていく
            while (current_position <= dis)
            {
                double deg = Geo2DBase.Angle(pline[i - 1], pline[i]);
                GeoPoint pnt = Geo2DBase.Destination(pline[i - 1], deg, current_position);
                result.Add(pnt);
                current_position += len;
            }
            //最後に点を打った位置+lenから、線分の長さを引くと、次の線分上に最初に打つ点の位置を得る
            current_position -= dis;
        }

        return result.ToArray();
    }

    /// <summary>
    /// pntとの距離が短い順に並べる
    /// </summary>
    static public GeoPoint[] NearSort(GeoPoint[] pnts, GeoPoint pnt)
    {
        SSIndex<double> lengths = new SSIndex<double>(pnts.Length);
        for (int i = 0; i < pnts.Length; i++)
            lengths.Add(Geo2DBase.Distance(pnt, pnts[i]), i);

        List<GeoPoint> result = new List<GeoPoint>(pnts.Length);
        foreach (int idx in lengths.Sort())
            result.Add(pnts[idx]);

        return result.ToArray();
    }

    /// <summary>
    /// ポイントを道順に並び替える
    /// </summary>
    static public GeoPoint[] DirectionSort(GeoPoint[] pnts, GeoPoint[] directionline)
    {
        GeoPoint[] pntsc = (GeoPoint[])pnts.Clone();

        List<GeoPoint> result = new List<GeoPoint>(pntsc.Length);

        for (int i = 1; i < directionline.Length; i++)
        {
            List<GeoPoint> onlinepnts = new List<GeoPoint>(pntsc.Length);
            //線分(directionline[i - 1], directionline[i])上に存在する点を集めて...
            for (int j = 0; j < pntsc.Length; j++)
            {
                if (double.IsNaN(pntsc[j].X)) continue;
                if (IsPointOnLine(pntsc[j], directionline[i - 1], directionline[i]))
                {
                    GeoPoint tmp = new GeoPoint(pntsc[j].X, pntsc[j].Y);
                    onlinepnts.Add(tmp);
                    pntsc[j].X = double.NaN;
                    pntsc[j].Y = double.NaN;
                }
            }
            if ((onlinepnts == null) || (onlinepnts.Count == 0)) continue;
            //線分(directionline[i - 1], directionline[i])の始点に近い順に並べる
            GeoPoint[] sorted = NearSort(onlinepnts.ToArray(), directionline[i - 1]);
            for (int j = 0; j < sorted.Length; j++)
            {
                if ((i != 1) && (sorted[j] == directionline[i - 1])) continue; //前の線分の終点と同じ位置の点なら既に取得しているから不要になる
                result.Add(sorted[j]);
            }
        }

        return result.ToArray();
    }

    /// <summary>
    /// 折れ線を或る点で分割する。
    /// </summary>
    static public GeoPoint[][] DivideLine(GeoPoint[] pline, GeoPoint[] pnts, bool ispolygon = false)
    {
        //折れ線のノードと切断点を統合する
        GeoPoint[] concated = ArrayFunc.Concat(pline, pnts);
        GeoPoint[] uniq = ArrayFunc.Distinct(concated);
        GeoPoint[] dsorted = DirectionSort(uniq, pline);

        //もしポリゴンをplineに渡しているなら、終点がDistinctで消されているだろうから、戻してあげないといけない
        if (ispolygon)
            dsorted = ArrayFunc.Concat(dsorted, new GeoPoint[] { dsorted[0] });
        
        List<GeoPoint[]> result = new List<GeoPoint[]>();
        result.Add(dsorted);
        for(int i = 0; i < pnts.Length; i++)
        {
            int n = result.Count;
            for (int j = 0; j < n; j++)
            {
                if (result[j] == null) continue;
                GeoPoint[][] tmp = ArrayFunc.Split(result[j], pnts[i]); //2つに分かれる（分かれるとすれば）
                if (tmp.Length != 2) continue;
                result[j] = null;
                result.Add(ArrayFunc.Concat(tmp[0], new GeoPoint[] { pnts[i] }));
                result.Add(ArrayFunc.Concat(new GeoPoint[] { pnts[i] }, tmp[1]));
            }
        }
        return ArrayFunc.Compaction(result.ToArray());
    }

    /// <summary>
    /// ポリゴンを線分で分割する
    /// </summary>
    static public GeoPoint[][] DividePolygon(GeoPoint[] pol, GeoPoint start, GeoPoint end)
    {
        GeoPoint[] poltmp = (GeoPoint[])pol.Clone();

        ////////////切断線とポリゴンの交点を調べる////////////
        List<GeoPoint> kotenlist = new List<GeoPoint>();
        for (int i = 1; i < poltmp.Length; i++)
        {
            GeoPoint[] tmp = Geo2DIntersect.Intersection(poltmp[i - 1], poltmp[i], start, end);
            if (tmp == null) continue;
            foreach (GeoPoint p in tmp) kotenlist.Add(p);
        }
        GeoPoint[] koten = ArrayFunc.Distinct(DirectionSort(kotenlist.ToArray(), new GeoPoint[] { start, end }));

        ////////////ポリゴンを分割する切断線の部分線を取得する////////////
        List<GeoPoint[]> parts = new List<GeoPoint[]>();
        for (int i = 1; i < koten.Length; i++)
        {
            GeoPoint center = Geo2DBase.Center(koten[i - 1], koten[i]);
            if (!Geo2DIntersect.WithIn(center, poltmp)) continue;
            parts.Add(new GeoPoint[] { koten[i - 1], koten[i] });
        }
        if (parts.Count == 0) return new GeoPoint[][] { poltmp };
        ////////////ポリゴンを切断する////////////
        GeoPoint[][] cut_polygons = DivideLine(poltmp, koten, true);
        if (!koten.Contains(pol[0])) //線分が交点以外の点（ポリゴンの最初の線分と最後の線分等）で切れているなら、結合させる
        {
            GeoPoint ketugo_ten = pol[0];
            if ((cut_polygons.First().First() == pol[0]) && (cut_polygons.Last().Last() == pol[0]))
            {
                cut_polygons[0] = ArrayFunc.Distinct(ArrayFunc.Concat(cut_polygons.Last(), cut_polygons.First()));
                cut_polygons[cut_polygons.Length - 1] = null;
                cut_polygons = ArrayFunc.Compaction(cut_polygons);
            }
            else
            {
                for (int i = 1; i < cut_polygons.Length; i++)
                {
                    if (cut_polygons[i - 1].Last() != pol[0]) continue;
                    if (cut_polygons[i].First() != pol[0]) continue;
                    cut_polygons[i - 1] = ArrayFunc.Distinct(ArrayFunc.Concat(cut_polygons[i - 1], cut_polygons[i]));
                    cut_polygons[i - 1] = ArrayFunc.Distinct(cut_polygons[i - 1]);
                    cut_polygons[i] = null;
                    cut_polygons = ArrayFunc.Compaction(cut_polygons);
                    break;
                }
            }
        }
        ////////////ポリゴンの切断辺と部分線を結合する////////////
        for (int i = 0; i < cut_polygons.Length; i++)
            for (int j = 0; j < parts.Count; j++)
                if (cut_polygons[i][0] == parts[j][0])
                {
                    cut_polygons[i] = ArrayFunc.Concat(cut_polygons[i], new GeoPoint[] { parts[j][0] });
                    break;
                }
                else if (cut_polygons[i][0] == parts[j][1])
                {
                    cut_polygons[i] = ArrayFunc.Concat(new GeoPoint[] { parts[j][0] }, cut_polygons[i]);
                    break;
                }
        ////////////切断しきれたポリゴンと、そうでないポリゴンを仕分ける////////////
        List<GeoPoint[]> result = new List<GeoPoint[]>();
        for (int i = 0; i < cut_polygons.Length; i++)
        {
            if (cut_polygons[i].First() == cut_polygons[i].Last())
            {
                result.Add(cut_polygons[i]);
                cut_polygons[i] = null;
            }
        }
        cut_polygons = ArrayFunc.Compaction(cut_polygons);
        ////////////切断しきれなかったポリゴンを結合する////////////
        for (int i = 0; i < cut_polygons.Length; i++)
        {
            for (int j = 0; j < cut_polygons.Length; j++)
            {
                if (i == j) continue;
                if (cut_polygons[i] == null) continue;
                if (cut_polygons[j] == null) continue;
                if (cut_polygons[i].First() == cut_polygons[j].Last())
                {
                    cut_polygons[j] = cut_polygons[j].Skip(1).Take(cut_polygons[j].Length - 1).ToArray();
                    result.Add(ArrayFunc.Concat(cut_polygons[i], cut_polygons[j]));
                    cut_polygons[i] = null;
                    cut_polygons[j] = null;
                }
            }
        }

        return result.ToArray();
    }

    /// <summary>
    /// targetsで指定した点を、折れ線の各線分上に挿入する
    /// </summary>
    static public GeoPoint[] Insert(GeoPoint[] pline, GeoPoint[] targets)
    {
        List<GeoPoint> result = pline.ToList();

        for (int i = 1; i < pline.Length; i++)
        {
            for (int j = 0; j < targets.Length; j++)
            {
                if (double.IsNaN(targets[j].X)) continue;
                if (!IsPointOnLine(targets[j], pline[i - 1], pline[i])) continue;
                if (targets[j] == pline[i - 1] || targets[j] == pline[i]) continue;

                result.Insert(i, targets[j]);
                targets[j].X = double.NaN;
            }
        }

        return result.ToArray();
    }

    /// <summary>
    /// 同じノードを持つラインの重複を削除する
    /// </summary>
    static public GeoPoint[] PolyLineFix(GeoPoint[] pline)
    {
        GeoPoint[] uniq = ArrayFunc.Distinct<GeoPoint>(pline);
        return DirectionSort(uniq, pline);
    }

    /// <summary>
    /// 指定したポリゴンを閉じる。outsideがtrueなら時計回り、falseなら反時計回りに直す。
    /// </summary>
    static public GeoPoint[] PolygonFixClockwise(GeoPoint[] pol, bool outside)
    {
        //必要なら回転方向を修正
        double area = Geo2DBase.Area(pol);
        if ((area > 0 && outside) || (area < 0 && (!outside)))
            pol.Reverse();

        return pol;
    }

    /// <summary>
    /// 指定したポリゴンを閉じる。
    /// </summary>
    static public GeoPoint[] PolygonFixClose(GeoPoint[] pol)
    {
        if (pol.First() != pol.Last())
        {
            Array.Resize(ref pol, pol.Length + 1);
            pol[pol.Length - 1] = pol[0];
        }
        return pol;
    }

    /// <summary>
    /// マルチポリゴンに変換する。返るポリゴンは内外に分けられているはず（テスト不十分）
    /// </summary>
    static public GeoPoint[][][] PolygonToMultiPolygon(GeoPoint[][] pntss)
    {
        double[,] in_out_table = Array2D.Calloc<double>(pntss.Length, pntss.Length, 0);

        for (int i = 0; i < pntss.Length; i++) pntss[i] = PolygonFixClose(pntss[i]); //閉じているだろうけど、念のため

        /////////包含関係調査//////////
        for (int i = 0; i < pntss.Length; i++)
            for (int j = 0; j < pntss.Length; j++)
                if ((i != j) && Geo2DIntersect.WithIn(pntss[i], pntss[j]))
                    in_out_table[i, j] = 1;
        ////////ポリゴンの回転方向を修正//////////////
        double[] within_counts = MathEx.SumCols(in_out_table);
        for (int i = 0; i < within_counts.Length; i++)
            if ((within_counts[i] % 2) == 0)
                pntss[i] = PolygonFixClockwise(pntss[i], true); //外側のポリゴンになるポリゴンは時計回り
            else
                pntss[i] = PolygonFixClockwise(pntss[i], false);//内側になるポリゴンは反時計回り
        /////////ポリゴンを内、外に振り分ける/////////
        List<GeoPoint[]>[] result = new List<GeoPoint[]>[pntss.Length];
        //外側
        for (int i = 0; i < within_counts.Length; i++)
        {
            if ((within_counts[i] % 2) != 0) continue;
            result[i] = new List<GeoPoint[]>();
            result[i].Add(pntss[i]);
        }
        //内側
        for (int i = 0; i < within_counts.Length; i++)
        {
            if ((within_counts[i] % 2) == 0) continue;
            //内側のポリゴンpntss[j]を含むポリゴンの中で、一番他のポリゴンに含まれるポリゴンを探す
            double max_count = 0;
            int max_index = -1;
            for (int j = 0; j < pntss.Length; j++)
                if ((in_out_table[i, j] == 1) && (max_count <= within_counts[j]))
                {
                    max_index = j;
                    max_count = within_counts[j];
                }
            if (max_index == -1)   //見つからなかった
                throw new Exception("処理できないポリゴンです");
            result[max_index].Add(pntss[i]);
        }

        result = ArrayFunc.Compaction(result.ToArray());
        GeoPoint[][][] tmp = new GeoPoint[result.Length][][];
        for (int i = 0; i < result.Length; i++)
            tmp[i] = result[i].ToArray();
        return tmp;
    }

    /// <summary>
    /// マルチポリゴン型をポリゴン型に変換する
    /// </summary>
    static public GeoPoint[][] MultiPolygonsToPolygons(GeoPoint[][][] mpol)
    {
        List<GeoPoint[]> result = new List<GeoPoint[]>(1000); //適当

        for (int i = 0; i < mpol.Length; i++)
            for (int j = 0; j < mpol[i].Length; j++)
                result.Add(mpol[i][j]);
        return result.ToArray();
    }

    /// <summary>
    /// バラバラの線分を結合する。同じ始点または終点を持つ線分が複数あると、正常に動作しない
    /// </summary>
    static public GeoPoint[][] Join(GeoPoint[][] mline)
    {
        List<GeoPoint[]> pol = new List<GeoPoint[]>(100);
        for (int i = 0; i < mline.Length; i++)
        {
            if (mline[i] == null) continue;

            List<GeoPoint> pnts = new List<GeoPoint>(100);
            pnts.Add(mline[i][0]); //最初の線分の始点
            pnts.Add(mline[i][1]); //最初の線分の終点
            mline[i] = null;
            for (int j = 0; j < mline.Length; j++)
            {
                if (mline[j] == null) continue;
                if (i == j) continue;
                //連結
                if (pnts.Last() == mline[j][0])
                {
                    pnts.Add(mline[j][1]);
                    mline[j] = null;
                    j = 0;
                }
                else if(pnts.Last() == mline[j][1])
                {
                    pnts.Add(mline[j][0]);
                    mline[j] = null;
                    j = 0;
                }
                else if(pnts.First() == mline[j][0])
                {
                    pnts.Insert(0, mline[j][1]);
                    mline[j] = null;
                    j = 0;
                }
                else if (pnts.First() == mline[j][1])
                {
                    pnts.Insert(0, mline[j][0]);
                    mline[j] = null;
                    j = 0;
                }
            }
            pol.Add(pnts.ToArray());
        }
        return pol.ToArray();
    }

    /// <summary>
    /// バラバラの折れ線の中で、指定した点を始点または終点に持つ折れ線同士を結合する。同じ始点または終点を持つ線分が複数あると、正常に動作しない
    /// </summary>
    static public GeoPoint[][] Join(GeoPoint[][] plines, GeoPoint junction)
    {
        GeoPoint[][] plinesc = (GeoPoint[][])plines.Clone();

        List<GeoPoint[]> tmp = new List<GeoPoint[]>(100);
        for (int i = 0; i < plinesc.Length; i++)
        {
            for (int j = 0; j < plinesc.Length; j++)
            {
                if (i == j) continue;
                if (plinesc[i] == null) continue;
                if (plinesc[j] == null) continue;

                if (!((plinesc[i][0] == junction) && (plines[j][plines[j].Length - 1] == junction))) continue;
                tmp.Add(ArrayFunc.Distinct(ArrayFunc.Concat(plinesc[i], plinesc[j])));
                plinesc[i] = null;
                plinesc[j] = null;
            }
        }

        for(int i= 0; i < plinesc.Length; i++)
        {
            if (plinesc[i] == null) continue;
            tmp.Add(plinesc[i]);
        }
        return tmp.ToArray();
    }

    /// <summary>
    /// ポリゴンに重なる線分を取得する。ドーナツには未対応
    /// </summary>
    static public GeoPoint[][] Clip(GeoPoint[] pnts, GeoPoint[][] pol)
    {
        List<GeoPoint[]> line = GetWithInOrOutPointAndCrossPoint(pnts, pol, true);
        return Join(line.ToArray());
    }

    /// <summary>
    /// ポリゴンを合併する。一点で交わるポリゴンの合併は不可
    /// </summary>
    static public GeoPoint[][][] Union(GeoPoint[][] pol1, GeoPoint[][] pol2)
    {
        /////順番バラバラの線分を一か所にまとめる/////
        List<GeoPoint[]> line = GetWithInOrOutPointAndCrossPoint(pol1[0], pol2, false);
        for (int i = 1; i < pol1.Length; i++)
        {
            List<GeoPoint[]> f = GetWithInOrOutPointAndCrossPoint(pol1[i], pol2, false);
            foreach (GeoPoint[] t in f)
                line.Add(t);
        }
        List<GeoPoint[]> addline = GetWithInOrOutPointAndCrossPoint(pol2[0], pol1, false);
        for (int i = 1; i < pol2.Length; i++)
        {
            List<GeoPoint[]> f = GetWithInOrOutPointAndCrossPoint(pol2[i], pol1, false);
            foreach (GeoPoint[] t in f)
                addline.Add(t);
        }
        for (int i = 0; i < addline.Count; i++)
            line.Add(addline[i]);
        List<int> indexslist = new List<int>(100); //適当
        for (int i = 0; i < line.Count; i++)
        {
            for (int j = i + 1; j < line.Count; j++)
            {
                //始点、終点が同じ線があれば1本にする
                if (ExistPart(line[j], line[i]) == 1)
                {
                    indexslist.Add(j);
                }
                //同じ線があれば両方捨てる
                else if (ExistPart(line[j], line[i]) == 2)
                {
                    indexslist.Add(j);
                    indexslist.Add(i);
                }
                ////重なっている線を削除する/////
                else if (Geo2DExtra.IsLineOnLine(line[j][0], line[j][1], line[i][0], line[i][1]))
                {
                    indexslist.Add(j);
                }
                else if (IsLineOnLine(line[i][0], line[i][1], line[j][0], line[j][1]))
                {
                    indexslist.Add(i);
                }
            }
        }
        indexslist.Sort();
        int[] indexs = indexslist.Distinct().ToArray();
        for (int i = indexs.Length - 1; i >= 0; i--)
            line.RemoveAt(indexs[i]);
        /////分割線を削除する/////        
        for (int i = line.Count - 1; i >= 0; i--)
            if (IsBunkatsuLine(line[i], pol1))
                line.RemoveAt(i);
        for (int i = line.Count - 1; i >= 0; i--)
            if (IsBunkatsuLine(line[i], pol2))
                line.RemoveAt(i);
        ////重なっている線を削除する/////
                for (int i = line.Count - 1; i >= 0; i--)
                    for (int j = line.Count - 1; j >= 0; j--)
                    {
                        if (i == j) continue;
                        if (Geo2DExtra.IsLineOnLine(line[j][0], line[j][1], line[i][0], line[i][1]))
                        {
                            line.RemoveAt(j);
                            break;
                        }
                        else if (IsLineOnLine(line[i][0], line[i][1], line[j][0], line[j][1]))
                        {
                            line.RemoveAt(i);
                            break;
                        }
                    }
        /////線分を連結する/////
        GeoPoint[][] pol = Join(line.ToArray());//一点で交わるポリゴンの合併の場合、ここで不具合が出る
        return PolygonToMultiPolygon(pol);
    }

    /// <summary>
    /// 折れ線を線分に分割する
    /// </summary>
    static public GeoPoint[][] Split(GeoPoint[] pline)
    {
        List<GeoPoint[]> result = new List<GeoPoint[]>(100); //適当

        for (int i = 1; i < pline.Length; i++)
            result.Add(new GeoPoint[] { pline[i - 1], pline[i] });

        return result.ToArray();
    }

    /// <summary>
    /// ポリゴンを合併する。一点で交わるポリゴンの合併は不可
    /// </summary>
    static public GeoPoint[][][] Union(GeoPoint[][][] pol1, GeoPoint[][][] pol2)
    {
        GeoPoint[][][] pol1c = (GeoPoint[][][])pol1.Clone();
        GeoPoint[][][] pol2c = (GeoPoint[][][])pol2.Clone();

        //マルチポリゴンを独立したポリゴンにする
        List<GeoPoint[][]> polygonslist = new List<GeoPoint[][]>(pol1c.Length + pol2c.Length);
        for (int i = 0; i < pol1c.Length; i++)
            polygonslist.Add(pol1c[i]);
        for (int i = 0; i < pol2c.Length; i++)
            polygonslist.Add(pol2c[i]);
        GeoPoint[][][] polygons = polygonslist.ToArray();
        //どのポリゴンとも重ならないポリゴンは他の場所に退避しておく
        List<GeoPoint[][]> unipol = new List<GeoPoint[][]>();
        for(int i = 0; i < polygons.Length; i++)
        {
            if (polygons[i] == null) continue;
            bool intersect = false;
            for (int j = i + 1; j <  polygons.Length; j++)
            {
                if (polygons[j] == null) continue;
                if(Geo2DIntersect.Intersect(polygons[i], polygons[j]))
                {
                    GeoPoint[][] tmppol = Union(polygons[i], polygons[j])[0]; //返るポリゴンは1つしかない。
                    polygons[j] = tmppol;
                    polygons[i] = null;
                    intersect = true;
                    break;
                }
            }
            if(!intersect)
            {
                unipol.Add(polygons[i]);
                polygons[i] = null;
            }
        }
        //待避されずに残ったポリゴンをまとめて、返す
        for (int i = 0; i < polygons.Length; i++)
            unipol.Add(polygons[i]);
        return ArrayFunc.Compaction(unipol.ToArray());
    }

    /// <summary>
    /// 線分がポリゴンを分割するならTrueを返す。線分の端点がポリゴンの線分上に載っていなければfalseを返す
    /// </summary>
    static public bool IsBunkatsuLine(GeoPoint[] senbun, GeoPoint[][] pol)
    {
        //この線分はポリゴンを分割しない
        if (!IsPointOnLine(senbun[0], pol)) return false;
        if (!IsPointOnLine(senbun[1], pol)) return false;
        GeoPoint pnt = Geo2DBase.Center(senbun);
        if (!Geo2DIntersect.Intersect(pnt, pol))
            return false;

        GeoPoint[] koten = Geo2DIntersect.Intersection(senbun, pol);
        if (koten.Length != 2)
            return false;
        //得られた交点が成す線分がポリゴンの辺に重なっているなら、線分はポリゴンを分割しないとする
        for (int i = 0; i < pol.Length; i++)
            for (int j = 1; j < pol[i].Length; j++)
                if (Geo2DIntersect.WithIn(koten, new GeoPoint[] { pol[i][j - 1], pol[i][j] }))
                    return false;
        //得られた交点が成す線分が引数の線分と違う線なら、引数の線分はポリゴンを横切っているということ
        if (ExistPart(senbun, koten) == -1)
            return false;

        return true;

        /*
                GeoPoint[] koten = Geo2DIntersect.Intersection(senbun, pol);
                if (koten.Length != 2)
                    return false;
                //得られた交点が成す線分がポリゴンの辺に重なっているなら、線分はポリゴンを分割しないとする
                for (int i = 0; i < pol.Length; i++)
                    for (int j = 1; j < pol[i].Length; j++)
                        if (Geo2DIntersect.WithIn(koten, new GeoPoint[] { pol[i][j - 1], pol[i][j] }))
                            return false;
                //得られた交点が成す線分が引数の線分と違う線なら、引数の線分はポリゴンを横切っているということ
                if (ExistPart(senbun, koten) == -1)
                    return false;
                //この線分はポリゴンを分割しない
                GeoPoint pnt = Geo2DBase.Center(senbun);
                if (!Geo2DIntersect.Intersect(pnt, pol))
                    return false;

                return true;
        */
    }

    /// <summary>
    /// ポリゴン外の折れ線のノードや、ポリゴンと線分の交点が成す線分を返す
    /// </summary>
    static private List<GeoPoint[]> GetWithInOrOutPointAndCrossPoint(GeoPoint[] pnts, GeoPoint[][] pol, bool _in)
    {
        List<GeoPoint[]> result = new List<GeoPoint[]>(100); //適当
        List<GeoPoint> tmp = new List<GeoPoint>(10); //適当

        //線分をリストに追加していく。線分の始点終点は、ポリゴン外の点か、交点
        for (int i = 1; i < pnts.Length; i++)
        {
            tmp.Clear();

            //始点
            bool isintersect = Geo2DIntersect.Intersect(pnts[i - 1], pol);
            if ((_in && isintersect) || (!_in && !isintersect))
                tmp.Add(pnts[i - 1]);
            //始点かもしれないし、終点かもしれない
            for (int j = 0; j < pol.Length; j++)
                for (int k = 1; k < pol[j].Length; k++)
                {
                    GeoPoint[] p = Geo2DIntersect.Intersection(pnts[i - 1], pnts[i], pol[j][k - 1], pol[j][k]);
                    if (p == null) continue;
                    foreach (GeoPoint pnt in p)
                        if (!pnts.Contains(pnt))
                            tmp.Add(pnt);
                }
            //終点
            isintersect = Geo2DIntersect.Intersect(pnts[i], pol);
            if ((_in && isintersect) || (!_in && !isintersect))
                tmp.Add(pnts[i]);
            //取得した点を、始点に近い順に並べて線分を作成する
            GeoPoint[] line = NearSort(tmp.ToArray(), pnts[i - 1]);
            for (int j = 1; j < line.Length; j++)
            {
                GeoPoint[] senbun = new GeoPoint[] { line[j - 1], line[j] };
                result.Add(senbun);
            }
        }

        return result;
    }

    /// <summary>
    /// ポリゴンを平行移動する
    /// </summary>
    static public GeoPoint[][] Trans(GeoPoint[][] pol, double x, double y)
    {
        return ConvertCalc(pol, new double[3, 3] { { 1, 0, x }, { 0, 1, y }, { 0, 0, 1 } });
    }

    /// <summary>
    /// ポリゴンを拡大、縮小する
    /// </summary>
    static public GeoPoint[][] Scale(GeoPoint[][] pol, double x, double y)
    {
        return ConvertCalc(pol, new double[3, 3] { { x, 0, 0 }, { 0, y, 0 }, { 0, 0, 1 } });
    }

    static public GeoPoint[][] Scale(GeoPoint[][] pol, double x, double y, GeoPoint center)
    {
        pol = Trans(pol, 0 - center.X, 0 - center.Y);
        pol = Scale(pol, x, y);
        return Trans(pol, center.X, center.Y);
    }

    /// <summary>
    /// ポリゴンを回転する
    /// </summary>
    static public GeoPoint[][] Rotate(GeoPoint[][] pol, double deg)
    {
        double rag = deg * Math.PI / 180d;
        double sin = Math.Sin(rag);
        double cos = Math.Cos(rag);
        //      return ConvertCalc(pol, new double[3, 3] { { cos, sin, 0 }, { -1*sin, cos, 0 }, { 0, 0, 1 } });
        return ConvertCalc(pol, new double[3, 3] { { cos, -1 * sin, 0 }, { sin, cos, 0 }, { 0, 0, 1 } });
    }

    /// <summary>
    /// Scale,Transで使用する関数
    /// </summary>
    static private GeoPoint[][] ConvertCalc(GeoPoint[][] pol, double[,] calArray)
    {
        GeoPoint[][] dst = new GeoPoint[pol.Length][];
        for (int i = 0; i < pol.Length; i++)
        {
            dst[i] = new GeoPoint[pol[i].Length];
            for (int j = 0; j < pol[i].Length; j++)
            {
                dst[i][j].X = calArray[0, 0] * pol[i][j].X + calArray[0, 1] * pol[i][j].Y + calArray[0, 2] * 1;
                dst[i][j].Y = calArray[1, 0] * pol[i][j].X + calArray[1, 1] * pol[i][j].Y + calArray[1, 2] * 1;
            }
        }
        return dst;
    }

    /// <summary>
    /// 線分が、折れ線の要素と同じなら1を返す。始点終点を逆にすれば一致するなら2を返す。一致しなければ-1を返す
    /// </summary>
    static public int ExistPart(GeoPoint[] senbun, GeoPoint[] line)
    {
        GeoPoint[] senbunrev = new GeoPoint[2];
        senbunrev[0] = senbun[1];
        senbunrev[1] = senbun[0];

        for (int i = 1; i < line.Length; i++)
        {
            if ((senbun[0] == line[i - 1]) && (senbun[1] == line[i]))
                return 1;
            if ((senbunrev[0] == line[i - 1]) && (senbunrev[1] == line[i]))
                return 2;
        }
        return -1;
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestGeo2DExtra
{
    static public void DividePolygon(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pnts = new GeoPoint[]
        {
            new GeoPoint(0, 0),
            new GeoPoint(100, 0),
            new GeoPoint(100, 20),
            new GeoPoint(50, 20),
            new GeoPoint(50, 80),
            new GeoPoint(100, 80),
            new GeoPoint(100, 100),
            new GeoPoint(0, 100),
            new GeoPoint(0, 0)
        };

        GeoPoint[][] result = null;

        result = Geo2DExtra.DividePolygon(pnts, new GeoPoint(60, -10), new GeoPoint(60, 110));
        for (int i = 0; i < result.Length; i++)
            memo.AppendText(ArrayFunc.ToString(result[i], '|') + "\n");
        memo.AppendText("-------------------------------------------------------\n");

        result = Geo2DExtra.DividePolygon(pnts, new GeoPoint(100, -10), new GeoPoint(100, 110));
        for (int i = 0; i < result.Length; i++)
            memo.AppendText(ArrayFunc.ToString(result[i], '|') + "\n");
        memo.AppendText("-------------------------------------------------------\n");

        result = Geo2DExtra.DividePolygon(pnts, new GeoPoint(50, -10), new GeoPoint(50, 110));
        for (int i = 0; i < result.Length; i++)
            memo.AppendText(ArrayFunc.ToString(result[i], '|') + "\n");
        memo.AppendText("-------------------------------------------------------\n");

        result = Geo2DExtra.DividePolygon(pnts, new GeoPoint(0, 0), new GeoPoint(50, 20));
        for (int i = 0; i < result.Length; i++)
            memo.AppendText(ArrayFunc.ToString(result[i], '|') + "\n");
        memo.AppendText("-------------------------------------------------------\n");

        result = Geo2DExtra.DividePolygon(pnts, new GeoPoint(0, 20), new GeoPoint(100, 100));
        for (int i = 0; i < result.Length; i++)
            memo.AppendText(ArrayFunc.ToString(result[i], '|') + "\n");
        memo.AppendText("-------------------------------------------------------\n");
    }

    static public void Insert(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pnts = new GeoPoint[]
        {
            new GeoPoint(0, 0),
            new GeoPoint(100, 0),
            new GeoPoint(100, 20),
            new GeoPoint(50, 20),
            new GeoPoint(50, 80),
            new GeoPoint(100, 80),
            new GeoPoint(100, 100),
            new GeoPoint(0, 100),
            new GeoPoint(0, 0)
        };

        Geo2DExtra.Insert(pnts, new GeoPoint[] { new GeoPoint(10, 0), new GeoPoint(100, 0), new GeoPoint(100, 20) });
    }

    static public void IsLineOnLine(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] other = new GeoPoint[] { new GeoPoint(0, 0), new GeoPoint(0, 100) };

        memo.AppendText(Geo2DExtra.IsLineOnLine(new GeoPoint(0, 90), new GeoPoint(0, 10), new GeoPoint(0, 0), new GeoPoint(0, 100)).ToString() + "\n");
        memo.AppendText(Geo2DExtra.IsLineOnLine(new GeoPoint(0, 90), new GeoPoint(0, 100), new GeoPoint(0, 0), new GeoPoint(0, 100)).ToString() + "\n");
        memo.AppendText(Geo2DExtra.IsLineOnLine(new GeoPoint(0, 0), new GeoPoint(0, 100), new GeoPoint(0, 0), new GeoPoint(0, 100)).ToString() + "\n");
    }

    static public void Join(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[][] pntss = new GeoPoint[][]
        {
            new GeoPoint[]{ new GeoPoint(0, 0), new GeoPoint(0, 10) },
            new GeoPoint[]{ new GeoPoint(-10, -10), new GeoPoint(-5, -5), new GeoPoint(0, 0) },
            new GeoPoint[]{ new GeoPoint(0, 10), new GeoPoint(30, 30) },
            new GeoPoint[]{ new GeoPoint(100, 20), new GeoPoint(180, 20) },
            new GeoPoint[]{ new GeoPoint(30, 30), new GeoPoint(35, 35), new GeoPoint(40, 40) },
            new GeoPoint[]{ new GeoPoint(30, 10), new GeoPoint(40, 40) }
        };

        GeoPoint[][] result = Geo2DExtra.Join(pntss);
        foreach (GeoPoint[] a in result)
        {
            memo.AppendText("(");
            foreach (GeoPoint b in a)
                memo.AppendText(b.ToString() + "|");
            memo.AppendText(")");
            memo.AppendText("\n--------------------------------------------\n");
        }
    }

    static public void PolygonToMultiPolygon(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[][] pntss = new GeoPoint[][]
        {
            new GeoPoint[]{ new GeoPoint(0, 0), new GeoPoint(0, 200), new GeoPoint(200, 200), new GeoPoint(200, 0), new GeoPoint(0, 0) },
            new GeoPoint[]{ new GeoPoint(10, 10), new GeoPoint(10, 190), new GeoPoint(190, 190), new GeoPoint(190, 10), new GeoPoint(10, 10) },
            new GeoPoint[]{ new GeoPoint(20, 20), new GeoPoint(20, 180), new GeoPoint(50, 180), new GeoPoint(50, 20), new GeoPoint(20, 20)},
            new GeoPoint[]{ new GeoPoint(100, 20), new GeoPoint(180, 20), new GeoPoint(180, 180), new GeoPoint(100, 180), new GeoPoint(100, 20)},
            new GeoPoint[]{ new GeoPoint(120, 40), new GeoPoint(160, 40), new GeoPoint(160,160), new GeoPoint(120, 160), new GeoPoint(120, 40)},
            new GeoPoint[]{ new GeoPoint(140, 50), new GeoPoint(150, 50), new GeoPoint(150, 60), new GeoPoint(140, 60), new GeoPoint(140, 50)}
        };

        GeoPoint[][][] result = Geo2DExtra.PolygonToMultiPolygon(pntss);
        foreach(GeoPoint[][] a in result)
        {
            foreach (GeoPoint[] b in a)
            {
                memo.AppendText("(");
                foreach (GeoPoint c in b)
                    memo.AppendText(c.ToString() + "|");
                memo.AppendText(")");
            }
            memo.AppendText("\n--------------------------------------------\n");
        }
    }

    static public void DivideLine(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pline = new GeoPoint[] { new GeoPoint(0, 0), new GeoPoint(100, 0), new GeoPoint(100, 100) };

        GeoPoint[][] result = Geo2DExtra.DivideLine(pline, new GeoPoint[] { new GeoPoint(90, 0), new GeoPoint(100, 10) });
        foreach (GeoPoint[] pnts in result)
        {
            foreach (GeoPoint pnt in pnts)
                memo.AppendText(pnt.ToString() + "\n");
            memo.AppendText("------------------------------------------------------\n");
        }
    }

    static public void DirectionSort(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pline = new GeoPoint[] { new GeoPoint(0, 0), new GeoPoint(100, 0), new GeoPoint(100, 100) };

        GeoPoint[] pnts = new GeoPoint[]
        {
            new GeoPoint(10, 0),
            new GeoPoint(0, 0.00000000000000001),
            new GeoPoint(50, 0),
            new GeoPoint(0, 50),
            new GeoPoint(100, 100),
            new GeoPoint(100, 101),
            new GeoPoint(0, 0),
            new GeoPoint(100, 0),
            new GeoPoint(100, 0),
            new GeoPoint(70, 100.000000000000000000000000000000000001)
        };

        GeoPoint[] result = Geo2DExtra.DirectionSort(pnts, pline);
        foreach (GeoPoint p in result) memo.AppendText(p.ToString() + "\n");
    }

    static public void NearSort(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pnts = new GeoPoint[]
        {
            new GeoPoint(0.000000000000000002, 0.000000000000000002),
            new GeoPoint(0, 0),
            new GeoPoint(0.000000000000000001, 0.000000000000000001)
        };

        GeoPoint[] result = Geo2DExtra.NearSort(pnts, new GeoPoint(0, 0));
        foreach (GeoPoint p in result) memo.AppendText(p.ToString() + "\n");
    }

    static public void Destinations(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pline = new GeoPoint[]
        {
            new GeoPoint(0, 0),
            new GeoPoint(100, 0),
            new GeoPoint(100, 100)
        };

        GeoPoint[] result = Geo2DExtra.Destinations(pline, 120);
        foreach (GeoPoint p in result) memo.AppendText(p.ToString() + "\n");
    }

    static public void IsPointOnLine(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint start = new GeoPoint(0, 0);
        GeoPoint end = new GeoPoint(100, 100);

        memo.AppendText(Geo2DExtra.IsPointOnLine(new GeoPoint(45.0000000000000001, 45), start, end).ToString() + "\n");
        memo.AppendText(Geo2DExtra.IsPointOnLine(new GeoPoint(45.000000000000001, 45), start, end).ToString() + "\n");
        memo.AppendText(Geo2DExtra.IsPointOnLine(new GeoPoint(45.00000000000001, 45), start, end).ToString() + "\n");
        memo.AppendText(Geo2DExtra.IsPointOnLine(new GeoPoint(45.0000000000001, 45), start, end).ToString() + "\n");
    }
}