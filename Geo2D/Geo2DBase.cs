﻿/******************************************************************************************
 *  Copyright (c) 2016,2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
 
static class Geo2DBase
{
    /// <summary>
    /// ポイントの各座標値を合計する 
    /// </summary>
    static public GeoPoint Sum(GeoPoint[] pnts)
    {
        GeoPoint pnt = new GeoPoint(0, 0, 0);

        for (int i = 0; i < pnts.Length; i++)
            pnt += pnts[0];

        return pnt;
    }

    /// <summary>
    /// ポイントのXY座標を入れ替える 
    /// </summary>
    static public void SwapXY(GeoPoint[] pnts)
    {
        for (int i = 0; i < pnts.Length; i++)
            pnts[i].SwapXY();
    }

    /// <summary>
    /// ポイントのXY座標を入れ替える 
    /// </summary>
    static public void SwapXY(GeoPoint[][] pol)
    {
        for (int i = 0; i < pol.Length; i++)
            SwapXY(pol[i]);
    }

    /// <summary>
    /// ポイントのXY座標を入れ替える 
    /// </summary>
    static public void SwapXY(GeoPoint[][][] mpol)
    {
        for (int i = 0; i < mpol.Length; i++)
            SwapXY(mpol[i]);
    }

    /// <summary>
    /// 線分の中点を返す
    /// </summary>
    static public GeoPoint Center(GeoPoint start, GeoPoint end)
    {
        return new GeoPoint((end.X + start.X) / 2, (end.Y + start.Y) / 2);
    }

    /// <summary>
    /// 点群の最小矩形の中心を返す
    /// </summary>
    static public GeoPoint Center(GeoPoint[] pnts)
    {
        double[] bnd = Bound(pnts);
        return new GeoPoint((bnd[0] + bnd[2]) / 2, (bnd[1] + bnd[3]) / 2);
    }


    /// <summary>
    /// 図形の最小矩形範囲を返す。
    /// 戻り値の配列のインデックス0 = 最大X、1 = 最大Y、2 = 最小X、3 = 最小Y
    /// </summary>
    static public double[] Bound(GeoPoint[] pnts)
    {
        double[] result = new double[4];
        result[0] = double.NegativeInfinity;
        result[1] = double.NegativeInfinity;
        result[2] = double.PositiveInfinity;
        result[3] = double.PositiveInfinity;

        for (int i = 0; i < pnts.Length; i++)
        {
            result[0] = Math.Max(result[0], pnts[i].X);
            result[1] = Math.Max(result[1], pnts[i].Y);
            result[2] = Math.Min(result[2], pnts[i].X);
            result[3] = Math.Min(result[3], pnts[i].Y);
        }

        return result;
    }

    /// <summary>
    /// 図形の最小矩形範囲を返す。
    /// 戻り値の配列のインデックス0 = 最大X、1 = 最大Y、2 = 最小X、3 = 最小Y
    /// </summary>
    static public double[] Bound(GeoPoint[][] pol)
    {
        double[] result = new double[4];
        result[0] = double.NegativeInfinity;
        result[1] = double.NegativeInfinity;
        result[2] = double.PositiveInfinity;
        result[3] = double.PositiveInfinity;

        for (int i = 0; i < pol.Length; i++)
        {
            double[] tmp = Bound(pol[i]);
            result[0] = Math.Max(result[0], tmp[0]);
            result[1] = Math.Max(result[1], tmp[1]);
            result[2] = Math.Min(result[2], tmp[2]);
            result[3] = Math.Min(result[3], tmp[3]);
        }

        return result;
    }

    /// <summary>
    /// 図形の最小矩形範囲を返す。
    /// 戻り値の配列のインデックス0 = 最大X、1 = 最大Y、2 = 最小X、3 = 最小Y
    /// </summary>
    static public double[] Bound(GeoPoint[][][] mpol)
    {
        double[] result = new double[4];
        result[0] = double.NegativeInfinity;
        result[1] = double.NegativeInfinity;
        result[2] = double.PositiveInfinity;
        result[3] = double.PositiveInfinity;

        for (int i = 0; i < mpol.Length; i++)
        {
            double[] tmp = Bound(mpol[i]);
            result[0] = Math.Max(result[0], tmp[0]);
            result[1] = Math.Max(result[1], tmp[1]);
            result[2] = Math.Min(result[2], tmp[2]);
            result[3] = Math.Min(result[3], tmp[3]);
        }

        return result;
    }

    /// <summary>
    /// 2点を結ぶ線分の距離を返す
    /// </summary>
    static public double Distance(GeoPoint a, GeoPoint b)
    {
        return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
    }

    /// <summary>
    /// 折れ線の長さを返す
    /// </summary>
    static public double Distance(GeoPoint[] pnts)
    {
        double sum = 0;
        for(int i = 1; i < pnts.Length; i++)
            sum += Distance(pnts[i - 1], pnts[i]);
        return sum;
    }

    /// <summary>
    /// 折れ線の長さを返す
    /// </summary>
    static public double Distance(GeoPoint[][] pntss)
    {
        double sum = 0;
        for (int i = 0; i < pntss.Length; i++)
            sum += Distance(pntss[i]);
        return sum;
    }

    /// <summary>
    /// 始点からdeg方向にn進んだ位置を返す
    /// </summary>
    static public GeoPoint Destination(GeoPoint start, double deg, double n)
    {
        double rad = deg * Math.PI / 180d;
        return new GeoPoint(n * Math.Cos(rad) + start.X, n * Math.Sin(rad) + start.Y);
    }
    
    /// <summary>
    /// 線分abの角度を返す
    /// </summary>
    static public double Angle(GeoPoint a, GeoPoint b)
    {
        double radian = Math.Atan2(b.Y - a.Y, b.X - a.X);
        radian = radian * (180d / Math.PI);
        return radian;
    }

    /// <summary>
    /// 捻じれていない閉じたポリゴンの面積を返す。ポリゴンが時計回りなら負、反時計回りなら正の値が返る
    /// </summary>
    static public double Area(GeoPoint[] pol)
    {
        double result = 0;
        for (int i = 0; i < pol.Length - 1; i++)
            result += pol[i].X * pol[i + 1].Y - pol[i].Y * pol[i + 1].X;
        result += pol[pol.Length - 1].X * pol[0].Y - pol[pol.Length - 1].Y * pol[0].X;

        return result * 0.5;
    }

    /// <summary>
    /// 点と最も近い線分上の点を取得する。
    /// </summary>
    static public GeoPoint ClosestPoint(GeoPoint pnt, GeoPoint start, GeoPoint end)
    {
        if (start == end)
            throw new ArgumentException("Geo2DBase.ClosestPointの引数「start」「end」が同じ点です。"); //このまま進めると0除算になるから

        GeoPoint vbase = MathEx.Vector(start, end);
        GeoPoint vany = MathEx.Vector(start, pnt);

        // vbase・vany = |vbase||vany|cosθ
        //(vbase・vany)/|vbase| = |vany|cosθ
        //ここで「|vany|cosθ」は線分(start end)上に下ろした交点までのstartからの距離を意味する(rcosθ=xを思い出そう)
        double r = MathEx.Dot(vbase, vany) / (Math.Pow(vbase.X, 2) + Math.Pow(vbase.Y, 2));

        if (r <= 0)
            return start;
        else if (r >= 1)
            return end;
        else
            return new GeoPoint(start.X + r * vbase.X, start.Y + r * vbase.Y);
    }

    /// <summary>
    /// 点と最も近い線分上の点を取得する。
    /// </summary>
    static public GeoPoint ClosestPoint(GeoPoint pnt, GeoPoint[] pnts)
    {
        GeoPoint[] ptmps = new GeoPoint[pnts.Length - 1];

        SSIndex<double> distances = new SSIndex<double>(pnts.Length);
        for (int i = 0; i < pnts.Length - 1; i++)
        {
            ptmps[i] = ClosestPoint(pnt, pnts[i], pnts[i + 1]);
            distances.Add(Distance(ptmps[i], pnt), i);
        }
        int[] indexs = distances.Sort();

        return ptmps[indexs.First()];
    }

    /// <summary>
    /// 或る点が線分のどの位置にあるか調べる。線分の進行方向からの左に点があれば1を、右なら-1を、線上にあれば0を返す
    /// http://imagingsolution.blog107.fc2.com/blog-entry-51.html
    /// </summary>
    static public int Side(GeoPoint pnt, GeoPoint start, GeoPoint end)
    {
        double tmp = MathEx.Cross(MathEx.Vector(start, pnt), MathEx.Vector(start, end)).Z;

        if (MathEx.EqualsEx(tmp, 0))
            return 0;
        else if (tmp > 0)
            return 1;  // 左
        else
            return -1; // 右
    }
}

/***********************************************************************************************************************/
/// <summary>
/// テスト用クラス
/// </summary>
/***********************************************************************************************************************/
static class TestGeo2DBase
{
    static public void Side(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] line = new GeoPoint[] { new GeoPoint(0, 0), new GeoPoint(100, 0) };
        GeoPoint pnt = new GeoPoint(0, 0);
        
        memo.AppendText(Geo2DBase.Side(new GeoPoint(0, 0), line[0], line[1]).ToString() + "\n");
        memo.AppendText(Geo2DBase.Side(new GeoPoint(50, 0.000000000000001), line[0], line[1]).ToString() + "\n");
        memo.AppendText(Geo2DBase.Side(new GeoPoint(50, -0.000000000000001), line[0], line[1]).ToString() + "\n");
        memo.AppendText(Geo2DBase.Side(new GeoPoint(50, 0.000000000000000000000001), line[0], line[1]).ToString() + "\n");
        memo.AppendText(Geo2DBase.Side(new GeoPoint(50, -0.000000000000000000000001), line[0], line[1]).ToString() + "\n");
        memo.AppendText(Geo2DBase.Side(new GeoPoint(50, 0.000000000000000000000000000000000001), line[0], line[1]).ToString() + "\n");
        memo.AppendText(Geo2DBase.Side(new GeoPoint(50, -0.000000000000000000000000000000000001), line[0], line[1]).ToString() + "\n");
    }

    static public void Destination(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint org = new GeoPoint(0, 0);
        double n = Math.Sqrt(2);

        memo.AppendText(Geo2DBase.Destination(org, 45, n).ToString() + "\n");
        memo.AppendText(Geo2DBase.Destination(org, 135, n).ToString() + "\n");
        memo.AppendText(Geo2DBase.Destination(org, 225, n).ToString() + "\n");
        memo.AppendText(Geo2DBase.Destination(org, 315, n).ToString() + "\n");
    }

    static public void Contains(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pnts = new GeoPoint[] {
            new GeoPoint(0, 0),
            new GeoPoint(0, 10),
            new GeoPoint(10, 10),
            new GeoPoint(10, 0)
        };

        Global.Epsilon = 0.000000000001;

        memo.AppendText(pnts.Contains(new GeoPoint(0, 0)).ToString() + "\n");
        memo.AppendText(pnts.Contains(new GeoPoint(10, 10)).ToString() + "\n");
        memo.AppendText(pnts.Contains(new GeoPoint(5, 5)).ToString() + "\n");
        memo.AppendText(pnts.Contains(new GeoPoint(0.0000001, 0.0000001)).ToString() + "\n");
        memo.AppendText(pnts.Contains(new GeoPoint(0.0000000000001, 0.0000000000001)).ToString() + "\n");
        memo.AppendText(pnts.Contains(new GeoPoint(0.0000000000000000001, 0.0000000000000000001)).ToString() + "\n");
        memo.AppendText(pnts.Contains(new GeoPoint(0.0000000000000000000000001, 0.0000000000000000000000001)).ToString() + "\n");
    }

    static public void ClosestPoint(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint start = new GeoPoint(0, 0);
        GeoPoint end = new GeoPoint(100, 0);

        memo.AppendText(Geo2DBase.ClosestPoint(new GeoPoint(45, 45), start, end).ToString() + "\n");
        memo.AppendText(Geo2DBase.ClosestPoint(new GeoPoint(-45, 45), start, end).ToString() + "\n");
        memo.AppendText(Geo2DBase.ClosestPoint(new GeoPoint(110, 45), start, end).ToString() + "\n");

        memo.AppendText(Geo2DBase.ClosestPoint(new GeoPoint(110, 45), start, start).ToString() + "\n");
    }

    static public void Area(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint[] pol1 = new GeoPoint[] {
            new GeoPoint(10, -10),
            new GeoPoint(10, 10),
            new GeoPoint(-10, 10),
            new GeoPoint(-10, -10),
            new GeoPoint(10, -10)
        };
        memo.AppendText(Geo2DBase.Area(pol1).ToString() + "\n");
        memo.AppendText(Geo2DBase.Area(pol1.Reverse().ToArray()).ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");

        GeoPoint[] pol2 = new GeoPoint[] {
            new GeoPoint(10, -10),
            new GeoPoint(10, -6), new GeoPoint(10, -2), new GeoPoint(10, 2), new GeoPoint(10, 6),
            new GeoPoint(10, 10),
            new GeoPoint(-10, 10),
            new GeoPoint(-10, -10),
            new GeoPoint(10, -10)
        };
        memo.AppendText(Geo2DBase.Area(pol2).ToString() + "\n");
        memo.AppendText(Geo2DBase.Area(pol2.Reverse().ToArray()).ToString() + "\n");
        memo.AppendText("----------------------------------------------------\n");
        //ねじれたポリゴン
        GeoPoint[] pol3 = new GeoPoint[] {
            new GeoPoint(10, -10),
            new GeoPoint(-10, 10),
            new GeoPoint(-10, -10),
            new GeoPoint(10, 10),
            new GeoPoint(10, -10),
        };
        memo.AppendText(Geo2DBase.Area(pol2).ToString() + "\n");
    }

    static public void Angle(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint org = new GeoPoint(0, 0, 0);

        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(10, 0, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(10, 10, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(0, 10, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(-10, 10, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(-10, 0, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(-10, -10, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(0, -10, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(10, -10, 0)).ToString() + "\n");
        memo.AppendText(Geo2DBase.Angle(org, new GeoPoint(0, 0, 0)).ToString() + "\n");

        memo.AppendText("----------------------------------------------------\n");
        memo.AppendText(Geo2DBase.Angle(org, org).ToString() + "\n");

        memo.AppendText("----------------------------------------------------\n");
        GeoPoint pnt = new GeoPoint(100, 100, 0);
        memo.AppendText(Geo2DBase.Angle(pnt, pnt).ToString() + "\n");

        memo.AppendText("----------------------------------------------------\n");
        memo.AppendText(Geo2DBase.Angle(new GeoPoint(0, 0, 0), new GeoPoint(1000, 0.00000000001, 0)).ToString() + "\n");
        memo.AppendText(MathEx.RoundOff(Geo2DBase.Angle(new GeoPoint(0, 0, 0), new GeoPoint(1000, 0.00000000001, 0)), 40).ToString());
    }

    static public void Distance(System.Windows.Forms.RichTextBox memo)
    {
        GeoPoint p1 = new GeoPoint(0, 0, -1);
        GeoPoint p2 = new GeoPoint(0, 0, 1);

        memo.AppendText(Geo2DBase.Distance(p1, p2).ToString());
    }
}
