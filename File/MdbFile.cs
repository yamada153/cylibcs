﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

//このクラスを使うには、ターゲットCPUをx64以外にしなければエラーになる
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;

class MdbFile
{
    static public string[] TableNames(string filename)
    {
        OleDbConnection conn = new OleDbConnection();
        OleDbCommand comm = new OleDbCommand();

        conn.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename;
        conn.Open();
        try
        {
            DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "TABLE" });
            string[] result = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
                result[i] = dt.Rows[i].ItemArray[2].ToString();
            return result;
        }
        finally
        {
            conn.Close();
        }
    }

    static public StringTable MdbToStringTable(string filename, string tablename)
    {
        OleDbConnection conn = new OleDbConnection();
        OleDbCommand comm = new OleDbCommand();

        conn.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename;
        conn.Open();
        try
        {
            comm.CommandText = "SELECT * FROM " + tablename;
            comm.Connection = conn;
            OleDbDataReader reader = comm.ExecuteReader();

            string[] fields = new string[reader.FieldCount];
            for (int i = 0; i < reader.FieldCount; i++)
                fields[i] = reader.GetName(i);
            StringTable result = new StringTable(fields);

            string[] contents = new string[reader.FieldCount];
            while (reader.Read())
            {
                for(int i = 0; i < reader.FieldCount; i++)
                    contents[i] = reader.GetValue(i).ToString();
                result.Add(contents);
            }
            return result;
        }
        finally
        {
            conn.Close();
        }
    }
}
