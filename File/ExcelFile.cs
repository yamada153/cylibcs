﻿/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

//プロジェクト->参照の追加->COM->microsoft excel15 object library
using MSExcel = Microsoft.Office.Interop.Excel;
using Range = Microsoft.Office.Interop.Excel.Range;
using Cells = Microsoft.Office.Interop.Excel.Range;
using Microsoft.Office.Core;

struct ExcelShapeAttr
{
    public double x;
    public double y;
    public double width;
    public double height;
    //図形のタイプhttps://msdn.microsoft.com/ja-jp/VBA/Office-Shared-VBA/articles/msoautoshapetype-enumeration-office
    public int type;
}

struct ExcelShape
{
    public int type;
    public int sub_type;
    public object value;
    public double x;
    public double y;
    public double width;
    public double height;
}

partial class ExcelFile : IDisposable
{
    private MSExcel.Application _xls; //エクセル

    public bool _lock { get; set; } //SetChangeEvent等のイベント中に、同じイベントが発生してループしないように使う

    /// <summary>
    /// 新規でエクセルファイルを開く
    /// </summary>
    public ExcelFile(bool visible = false)
    {
        _xls = new MSExcel.Application();

        Visible(visible);
        _xls.DisplayAlerts = false; // Excelの確認ダイアログ表示有無

        _lock = false;
    }

    /// <summary>
    /// Excel画面の表示、非表示を指定する
    /// </summary>
    public void Visible(bool flg)
    {
        if (flg)
        {
            _xls.Visible = true;
            _xls.ScreenUpdating = true; //エクセルの画面更新を表示
        }
        else
        {
            _xls.Visible = false;
            _xls.ScreenUpdating = false;
        }
    }

    /// <summary>
    /// リボンの表示、非表示を指定する
    /// </summary>
    public void Ribbon(bool flg)
    {
        if (flg)
            _xls.ExecuteExcel4Macro("SHOW.TOOLBAR(\"Ribbon\", \"true\")");
        else
            _xls.ExecuteExcel4Macro("SHOW.TOOLBAR(\"Ribbon\", \"false\")");
    }

    /// <summary>
    /// 集合棒グラフを作成する
    /// </summary>なかなか辛いネストだ...
    public void CreateGraphColumnClustered(int posx, int posy, int width, int height, string range)
    {
        Range r = _xls.get_Range(range);
        try
        {
            MSExcel.Worksheet sh = _xls.ActiveSheet;
            try
            {
                MSExcel.Shapes shp = sh.Shapes;
                try
                {
                    shp.AddChart(MSExcel.XlChartType.xlColumnClustered, posx, posy, width, height);
                    MSExcel.Chart ch = _xls.ActiveChart;
                    try
                    {
                        ch.SetSourceData(r);
                    }
                    finally
                    {
                        Marshal.FinalReleaseComObject(ch);
                    }
                }
                finally
                {
                    Marshal.FinalReleaseComObject(shp);
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(sh);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary> 現在のシートに格納されているデータの最大行数を返す </summary>
    public int NumRow() { return (int)GetCellData((r) => { return r.Row; }, ""); }
    /// <summary> 現在のシートに格納されているデータの最大列数を返す </summary>
    public int NumCol() { return (int)GetCellData((r) => { return r.Column; }, ""); }
    /// <summary> 現在のシートの最大行数を返す </summary>
    public int MaxNumRow() { return (int)GetCellData((r) => { return r.Rows.Count; }, 0, 0); }
    /// <summary> 現在のシートの最大列数を返す </summary>
    public int MaxNumCol() { return (int)GetCellData((r) => { return r.Columns.Count; }, 0, 0); }

    /// <summary>
    /// 現在のウィンドウの尺度を設定する
    /// </summary>
    public void SetZoom(int zoom)
    {
        MSExcel.Window w = _xls.ActiveWindow;
        try
        {
            w.Zoom = zoom;
        }
        finally
        {
            Marshal.FinalReleaseComObject(w);
        }
    }

    /// <summary>
    /// 現在のウィンドウの尺度を取得する
    /// </summary>
    public double GetZoom()
    {
        MSExcel.Window w = _xls.ActiveWindow;
        try
        {
            return w.Zoom;
        }
        finally
        {
            Marshal.FinalReleaseComObject(w);
        }
    }

    /********************************************************************************************
     *                               ブック操作
    ********************************************************************************************/
    /// <summary>
    /// ブックをアクティブにする。もし開いていないファイルなら開く。
    /// </summary>
    public void OpenBook(string filename)
    {
        string name = Path.GetFileName(filename);
        System.Threading.Thread.Sleep(100); //これがあるとOpenで止まることが少なくなる気がする

        //既に開いている
        if (ExistBook(name))
        {
            MSExcel.Workbooks wbs = _xls.Workbooks;
            try
            {
                MSExcel.Workbook w = wbs[name];
                try
                {
                    w.Activate();
                }
                finally
                {
                    Marshal.FinalReleaseComObject(w);
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(wbs);
            }
        }
        //まだ開いていない
        else
        {
            MSExcel.Workbooks wbs = _xls.Workbooks;
            try
            {
                _xls.Application.EnableEvents = false; //開いたとき、マクロを実行させないため
                MSExcel.Workbook b = wbs.Open(filename, false);
                try
                {

                }
                finally
                {
                    Marshal.FinalReleaseComObject(b);
                }
            }
            finally
            {
                _xls.Application.EnableEvents = true;
                Marshal.FinalReleaseComObject(wbs);
            }

        }
    }

    /// <summary>
    /// 指定したブックを開いているならtrueを返す。
    /// booknameにファイルのフルパスを指定しても、ファイル名を取り出して比較する。
    /// </summary>
    public bool ExistBook(string bookname)
    {
        string name = Path.GetFileName(bookname);
        MSExcel.Workbooks wbs = _xls.Workbooks;
        try
        {
            int count = wbs.Count;
            for(int i = 1; i <= count; i++)
            {
                MSExcel.Workbook w = wbs[i];
                try
                {
                    if (StrUtils.SameText(name, w.Name))
                        return true;
                }
                finally
                {
                    Marshal.FinalReleaseComObject(w);
                }
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(wbs);
        }

        return false;
    }

    /// <summary>
    /// 新規ブックを作成する。作成されたブックの名前が返る
    /// </summary>
    public string CreateBook()
    {
        MSExcel.Workbooks wbs = _xls.Workbooks;
        try
        {
            MSExcel.Workbook wb = wbs.Add();
            try
            {
                return wb.Name;
            }
            finally
            {
                Marshal.FinalReleaseComObject(wb);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(wbs);
        }
    }

    /// <summary>
    /// ワークブックを閉じる
    /// </summary>
    public void CloseBook(string filename)
    {
        MSExcel.Workbook wb = _xls.ActiveWorkbook;
        try
        {
            wb.Close(false, filename);
        }
        finally
        {
            Marshal.FinalReleaseComObject(wb);
        }
    }

    /********************************************************************************************
     *                               シート操作
    ********************************************************************************************/
    /// <summary> シートをアクティブにする。大文字小文字は関係無いようだ。非表示のシートは正しく開けない。先にシートを表示状態にしておくこと! </summary>
    public void OpenSheet(string sheetname) { SetSheetData((f, s) => { f[s].Activate(); }, sheetname); }
    /// <summary> シートを削除する </summary>
    public void DeleteSheet(string sheetname) { SetSheetData((f,s) => { f[s].Delete(); }, sheetname); }
    /// <summary> シート名を変更する </summary>
    public void RenameSheet(string src, string dst) { SetSheetData((f, s) => { f[s].Name = dst; }, src); }
    /// <summary> シート保護をOn/Offする </summary>
    public void SheetProtect(bool flg) { SetSheetData((f, s) => { if (flg) f[s].Protect(); else f[s].Unprotect(); }, ""); }
    /// <summary> シートを表示する </summary>
    public void SheetVisible(string sheetname) { SetSheetData((f, s) => { f[s].Visible = MSExcel.XlSheetVisibility.xlSheetVisible; }, sheetname); }
    /// <summary> シートを非表示する </summary>
    public void SheetHidden(string sheetname) { SetSheetData((f, s) => { f[s].Visible = MSExcel.XlSheetVisibility.xlSheetHidden; }, sheetname); }
    /// <summary> シートが非表示でなければtrueを返す </summary>
    public bool IsSheetVisible(string sheetname) { return (bool)GetSheetData((f, s) => { return f[s].Visible == MSExcel.XlSheetVisibility.xlSheetVisible ?  true :  false; }, sheetname); }
    /// <summary> 開いているブックのシート数を返す。 </summary>
    public int WorkSheetCount() { return (int)GetSheetsData((sh) => { return sh.Count; }, ""); }
    /// <summary> シートを作成する </summary>
    public void CreateWorkSheet(string sheetname) { SetSheetsData((sh) => { MSExcel.Worksheet newWorksheet = sh.Add(); newWorksheet.Name = sheetname; }, sheetname); }
    /// <summary> 指定したシートがワークシートならtrueを返す </summary>
    public bool IsWorkSheet(string sheetname) { return (bool)GetSheetData((f, s) => { return f[s].Type == (int)(MSExcel.XlSheetType.xlWorksheet) ? true : false; }, sheetname); }

    /// <summary>
    /// ブック内のすべてのワークシート名を取得する。
    /// </summary>
    public string[] SheetNames()
    {
        List<string> result = new List<string>();

        MSExcel.Sheets sheets = _xls.Sheets;
        try
        {
            int n = sheets.Count;
            for(int i = 0; i < n; i++)
            {
                var s = sheets[i + 1];
                try
                {
                    result.Add(s.Name);
                }
                finally
                {
                    Marshal.FinalReleaseComObject(s);
                }
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheets);
        }
        return result.ToArray();
    }

    /// <summary>
    /// 指定したシートが存在するならtrueを返す。
    /// booknameにファイルのフルパスを指定しても、ファイル名を取り出して比較する。
    /// </summary>
    public bool ExistSheet(string sheetname)
    {
        MSExcel.Sheets shts = _xls.Worksheets;
        try
        {
            int count = shts.Count;
            for (int i = 1; i <= count; i++)
            {
                MSExcel.Worksheet s = shts[i];
                try
                {
                    if (StrUtils.SameText(sheetname, s.Name))
                        return true;
                }
                finally
                {
                    Marshal.FinalReleaseComObject(s);
                }
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(shts);
        }

        return false;
    }

    /// <summary>
    /// 指定したブックのシートを、指定した別ブックにコピーする
    /// </summary>
    public void SheetCopy(string srcbook, string srcsheet, string dstbook)
    {
        if (!ExistBook(srcbook)) return;
        OpenBook(srcbook);
        MSExcel.Sheets srcshts = _xls.Worksheets;
        try
        {
            if (!ExistSheet(srcsheet)) return;
            MSExcel.Worksheet srcsht = srcshts[srcsheet];
            try
            {
                if (!ExistBook(dstbook)) return;
                OpenBook(dstbook);
                MSExcel.Sheets dstshts = _xls.Worksheets;
                try
                {
                    MSExcel.Worksheet dstsht = dstshts[1];
                    try
                    {
                        srcsht.Copy(dstsht);
                    }
                    finally
                    {
                        Marshal.FinalReleaseComObject(dstsht);
                    }
                }
                finally
                {
                    Marshal.FinalReleaseComObject(dstshts);
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(srcsht);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(srcshts);
        }
    }

    public string GetObjctAttr(string sheetname)
    {
        MSExcel.Sheets dstshts = _xls.Worksheets;
        try
        {
            MSExcel.Worksheet dstsht = dstshts[sheetname];
            try
            {
                MSExcel.Shapes shps = dstsht.Shapes;
                try
                {
                    List<string> objs = new List<string>(100); //適当
                    for(int i = 0; i < shps.Count; i++)
                    {
                        MSExcel.Shape shp = shps.Item(i + 1);
                        try
                        {
                            string topleftcell = "";
                            string bottomrightcell = "";
                            string width = shp.Width.ToString();
                            string height = shp.Height.ToString();
                            string type = "";
                            string value = "";
                            if (shp.Type == MsoShapeType.msoFormControl)
                            {
                                MSExcel.ControlFormat cnt = shp.ControlFormat;
                                try
                                {
                                    //チェックボックス
                                    if (shp.FormControlType == MSExcel.XlFormControl.xlCheckBox)
                                    {
                                        topleftcell = shp.TopLeftCell.Address.Replace("$", "");
                                        bottomrightcell = shp.BottomRightCell.Address.Replace("$", "");
                                        type = "チェックボックス";
                                        value = cnt.Value == 1 ? "On" : "Off";
                                    }
                                    //ラジオボタン
                                    else if (shp.FormControlType == MSExcel.XlFormControl.xlOptionButton)
                                    {
                                        topleftcell = shp.TopLeftCell.Address.Replace("$", "");
                                        bottomrightcell = shp.BottomRightCell.Address.Replace("$", "");
                                        type = "ラジオボタン";
                                        value = cnt.Value == 1 ? "True" : "False";
                                    }
                                    //グループボックス
                                    else if (shp.FormControlType == MSExcel.XlFormControl.xlGroupBox)
                                    {
                                        topleftcell = shp.TopLeftCell.Address.Replace("$", "");
                                        bottomrightcell = shp.BottomRightCell.Address.Replace("$", "");
                                        type = "グループボックス";
                                    }
                                    //ドロップダウン
                                    else if (shp.FormControlType == MSExcel.XlFormControl.xlDropDown)
                                    {
                                        type = "ドロップダウン";
                                    }
                                    else
                                    {
                                        type = "不明";
                                    }
                                }
                                finally
                                {
                                    Marshal.FinalReleaseComObject(cnt);
                                }
                            }
                            else if (shp.Type == MsoShapeType.msoPicture)
                            {
                                type = "ピクチャ";
                            }
                            else
                            {
                                type = shp.Type.ToString();
                            }

                            objs.Add(type + "," + value + "," + topleftcell + "," + bottomrightcell + "," + width + "," + height);
                        }
                        finally
                        {
                            Marshal.FinalReleaseComObject(shp);
                        }
                    }

                    return string.Join(";", objs.ToArray());
                }
                finally
                {
                    Marshal.FinalReleaseComObject(shps);
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(dstsht);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(dstshts);
        }
    }

    /********************************************************************************************
     *                               セル操作
    ********************************************************************************************/
    /// <summary>
    /// 指定した範囲から、アクティブなデータ範囲を選択する
    /// </summary>
    public void SelectCurrentRegion(string range)
    {
        string[] splited = range.Split(':');

        Range r = _xls.Range[splited[0], splited[1]];
        try
        {
            Range cr = r.CurrentRegion;
            try
            {
                cr.Select();
            }
            finally
            {
                Marshal.FinalReleaseComObject(cr);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>
    /// 指定した範囲を選択状態にする
    /// </summary>
    public void SelectCells(string range)
    {
        string[] splited = range.Split(':');

        Range r = _xls.Range[splited[0], splited[1]];
        try
        {
            r.Select();
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>
    /// 指定した範囲を選択状態にする
    /// </summary>
    public void SelectCells(int row1, int col1, int row2, int col2)
    {
        string range1 = ToAlpha(col1) + row1.ToString();
        string range2 = ToAlpha(col2) + row2.ToString();

        SelectCells(range1 + ':' + range2);
    }

    /// <summary> 指定したセルの上のセル番号を返す </summary>
    public string Up(string addr) { return CellOffsetFunction((c) => { return c.Address; }, addr, -1, 0); }
    /// <summary> 指定したセルの下のセル番号を返す </summary>
    public string Down(string addr) { return CellOffsetFunction((c) => { return c.Address; }, addr, 1, 0); }
    /// <summary> 指定したセルの左のセル番号を返す </summary>
    public string Left(string addr) { return CellOffsetFunction((c) => { return c.Address; }, addr, 0, -1); }
    /// <summary> 指定したセルの右のセル番号を返す </summary>
    public string Right(string addr) { return CellOffsetFunction((c) => { return c.Address; }, addr, 0, 1); }

    /// <summary>
    /// 指定したアドレスからのオフセットした先のセル番号を取得する
    /// </summary>
    public string Offset(int iRow, int iCol, int row_offset, int col_offset)
    {
        Cells cells = _xls.Cells[iRow, iCol];
        try
        {
            string addr = cells.Address.Replace("$", "");
            if (row_offset < 0)
            {
                for (int i = row_offset; i >= 0; i++)
                    addr = Up(addr);
            }
            if (row_offset > 0)
            {
                for (int i = 0; i < row_offset; i++)
                    addr = Down(addr);
            }
            if (col_offset < 0)
            {
                for (int i = col_offset; i >= 0; i++)
                    addr = Left(addr);
            }
            if (col_offset > 0)
            {
                for (int i = 0; i < col_offset; i++)
                    addr = Right(addr);
            }
            return addr;
        }
        finally
        {
            Marshal.FinalReleaseComObject(cells);
        }
    }

    /// <summary>
    /// 選択されている範囲に罫線を引く
    /// </summary>
    /// http://www.din.or.jp/~graywing/csharp_excel.html#CELL_LINE_INSIDEHORIZONTAl
    public void DrawBorderLine(string range, MSExcel.XlBordersIndex index, MSExcel.XlLineStyle style, MSExcel.XlBorderWeight bw)
    {
        Range r = _xls.Range[range];
        try
        {
            MSExcel.Borders br = r.Borders;
            try
            {
                br[index].LineStyle = style;
                br[index].Weight = bw;
            }
            finally
            {
                Marshal.FinalReleaseComObject(br);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>
    /// 選択されている範囲の背景色を設定する
    /// </summary>
    /// http://www.officepro.jp/excelvba/cell_interior/index1.html
    public void SetBackgroundColor(string range, int color_index)
    {
        Range r = _xls.Range[range];
        try
        {
            MSExcel.Interior ir = r.Interior;
            try
            {
                ir.ColorIndex = color_index;
            }
            finally
            {
                Marshal.FinalReleaseComObject(ir);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>
    /// 選択されている範囲の背景色を取得する
    /// </summary>
    /// http://www.officepro.jp/excelvba/cell_interior/index1.html
    public int GetBackgroundColor(string range)
    {
        Range r = _xls.Range[range];
        try
        {
            MSExcel.Interior ir = r.Interior;
            try
            {
                return ir.ColorIndex;
            }
            finally
            {
                Marshal.FinalReleaseComObject(ir);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>
    /// 選択されている範囲の背景色を取得する
    /// </summary>
    /// http://www.officepro.jp/excelvba/cell_interior/index1.html
    public int GetBackgroundColor(int row, int col)
    {
        MSExcel.Range r = _xls.Cells[row, col];
        try
        {
            MSExcel.Interior ir = r.Interior;
            try
            {
                return ir.ColorIndex;
            }
            finally
            {
                Marshal.FinalReleaseComObject(ir);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>
    /// セルにハイパーリンクを設定する。
    /// </summary>
    public void HyperLinks(int row, int col, string address)
    {
        MSExcel.Worksheet sh = _xls.ActiveSheet;
        try
        {
            MSExcel.Hyperlinks hyp = sh.Hyperlinks;
            try
            {
                string title = GetValue(row, col);
                Cells c = _xls.Cells[row, col];
                try
                {
                    hyp.Add(c, address, "", "", "");
                }
                finally
                {
                    Marshal.FinalReleaseComObject(c);
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(hyp);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sh);
        }
    }

    /// <summary> 指定したセルが結合されているか調べる </summary>
    public bool IsMergeCell(int row, int col) { return (bool)GetCellData((c) => { return c.MergeCells; }, row, col); }
    /// <summary> セルの結合を解除する。結合解除されたセルに、結合前の値が入る </summary>
    public void UnMergeCell(int row, int col) { SetCellData((c, s) => { c.UnMerge(); }, row, col, ""); }
    /// <summary> 列幅の値を返す </summary>
    public double GetCellColWidth(int row, int col) { return (double)GetCellData ((c) => { return c.ColumnWidth; }, row, col); }
    /// <summary> 行高さの値を返す </summary>
    public double GetCellRowHeight(int row, int col) { return (double)GetCellData((c) => { return c.RowHeight; }, row, col); }
    /// <summary>指定したセルの行数を返す。通常は1が返るが、セルが結合されている場合は結合されている分の行数が返る</summary>
    public int CellRowNum(int row, int col) { return (int)GetCellData((c) => { Range r = c.MergeArea; try { return r.Rows.Count; } finally { Marshal.FinalReleaseComObject(r); } }, row, col); }
    /// <summary>指定したセルの列数を返す。通常は1が返るが、セルが結合されている場合は結合されている分の列数が返る</summary>
    public int CellColNum(int row, int col) { return (int)GetCellData((c) => { Range r = c.MergeArea; try { return r.Columns.Count; } finally { Marshal.FinalReleaseComObject(r); } }, row, col); }
    /// <summary> 列幅の値を設定する </summary>
    public void SetCellColumnWidth(int row, int col, double value) { SetCellData((c, v) => { c.ColumnWidth = v; }, row, col, value); }
    /// <summary> 行高さの値を設定する </summary>
    public void SetCellRowHight(int row, int col, double value) { SetCellData((c, v) => { c.RowHeight = v; }, row, col, value); }

    /// <summary>
    /// 指定した範囲をコピーする
    /// </summary>
    public void Copy(string srange)
    {
        Cells r = _xls.Columns[srange];
        try
        {
            var sel = _xls.Selection;
            try
            {
                sel.Copy();
            }
            finally
            {
                Marshal.FinalReleaseComObject(sel);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(r);
        }
    }

    /// <summary>行を挿入する。(例)InsertRows("3:5"); //3行目から7行目まで、新しい行を挿入する</summary>    
    public void InsertRows(string range) { SetRowColData((r) => { object xldown = -4121; r.Insert(xldown); }, range, false); }
    /// <summary>列を挿入する。(例)xls.InsertCols("B:F"); //B列目からF列目まで、新しい列を挿入する</summary>    
    public void InsertCols(string range) { SetRowColData((r) => { object xlright = -4161; r.Insert(xlright); }, range, true); }
    /// <summary>行を削除する。(例)InsertRows("3:5"); //3行目から7行目まで、行を削除する</summary>    
    public void DeleteRows(string range) { SetRowColData((r) => { object xlup = -4162; r.Delete(xlup); }, range, false); }
    /// <summary>列を削除する。(例)xls.InsertCols("B:F"); //B列目からF列目まで、列を削除する</summary>    
    public void DeleteCols(string range) { SetRowColData((r) => { object xlleft = -4159; r.Delete(xlleft); }, range, true); }


    /// <summary> 指定したセルの数式を取得する </summary>
    public string GetFormula(int row, int col) { return (string)GetCellData((c) => { var tmp = c.Formula; return tmp == null ? "" :  tmp.ToString(); }, row, col); }
    /// <summary> 指定したセルの値を取得する </summary>
    public string GetValue(int row, int col) { return (string)GetCellData((c) => { var tmp = c.Value; return tmp == null ? "" : tmp.ToString(); } , row, col); }
    /// <summary> 指定したセルの値を見た目通りに取得する </summary>
    public string GetText(int row, int col) { return (string)GetCellData((c) => { var tmp = c.Text; if (tmp != null) return tmp.ToString(); else return ""; }, row, col); }
    /// <summary> 指定したセルの数式を取得する </summary>
    public string GetFormula(string address) { return (string)GetCellData((c) => { var tmp = c.Formula; return tmp == null ? "": tmp.ToString(); }, address); }
    /// <summary> 指定したセルの値を取得する </summary>
    public string GetValue(string address) { return (string)GetCellData((c) => { var tmp = c.Value; return tmp == null ? "" : tmp.ToString(); }, address); }
    /// <summary> 指定したセルの値を見た目通りに取得する </summary>
    public string GetText(string address) { return (string)GetCellData((c) => { var tmp = c.Text; return tmp == null ? "" : tmp.ToString(); }, address); }
    /// <summary> 指定した範囲の数式を取得する 3行目を取得するときGetFomulasFromRange("3:3")、B列を取得するときGetFomulasFromRange("B:B")</summary>
    public object[,] GetFomulasFromRange(string range) { return (object[,])GetCellData((r) => { return r.Formula; }, range); }
    /// <summary> 指定した範囲の値を取得する 3行目を取得するときGetValuesFromRange("3:3")、B列を取得するときGetValuesFromRange("B:B")</summary>
    public object[,] GetValuesFromRange(string range) { return (object[,])GetCellData((r) => { return r.Value; }, range); }
    /// <summary> 指定した範囲の値を見た目通りに取得する。3行目を取得するときGetTextsFromRange("3:3")、B列を取得するときGetTextsFromRange("B:B") 上手くいかない理由「https://stackoverflow.com/questions/40833124/read-excel-range-as-array-of-textstring-representing-what-is-displayed-on-the-s」 </summary>
    public object[,] GetTextsFromRange(string range) { return (object[,])GetCellData((r) => { return r.Text; }, range); }

    /// <summary> 指定したセルに値を設定する </summary>
    public void SetValue(string range, object value) { SetCellData((r, v) => { r.Value = v.ToString(); }, range, value); }
    /// <summary> 指定したセルに二次元配列の値を設定する 例 SetValues("B4: C6", v)</summary>
    public void SetValues(string range, object[,] value) { SetCellData((r, v) => { r.Value = v; }, range, value); }


    public string[] SearchFromSelect(string str)
    {
        List<string> result = new List<string>();

        var sel = _xls.Selection;
        try
        {
            Cells cells = _xls.Cells;
            try
            {
                Range find = cells.Find(str);
                try
                {

                    if (find == null)
                    {
                        return null;
                    }
                    else
                    {
                        Range first = find;
                        try
                        {
                            Range findnext = find;
                            try
                            {
                                result.Add(findnext.Address.Replace("$", ""));

                                while (true)
                                {
                                    findnext = cells.FindNext(findnext);
                                    if ((findnext == null) || (findnext.Address == first.Address)) break;
                                    result.Add(findnext.Address.Replace("$", ""));
                                }
                            }
                            finally
                            {
                                if (findnext != null)
                                    Marshal.FinalReleaseComObject(findnext);
                            }
                        }
                        finally
                        {
                            Marshal.FinalReleaseComObject(first);
                        }
                    }
                    return result.ToArray();
                }
                finally
                {
                    if (find != null)
                        Marshal.FinalReleaseComObject(find);
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(cells);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sel);
        }
    }

    /********************************************************************************************
    *                               列名、列番号変換関連関数
    ********************************************************************************************/
    /// <summary> 列番号を列名に変換する。取得に失敗したらnullを返す。 例 ToAlpha(27); //戻り値: "AA" </summary>
    public string ToAlpha(int number) { return _xls.ExecuteExcel4Macro("SUBSTITUTE(ADDRESS(1," + number.ToString() + ",4),1,)"); }
    /// <summary>セル番地の列番号を取得する。(例)ToColNumber("AA35"); //戻り値: 27</summary>
    public int ToColNumber(string address) { return (int)GetCellData((r) => { return r.Column; }, address); }
    /// <summary>セル番地の行番号を取得する(要る？)。(例)ToRowNumber("AA35"); //戻り値: 35</summary>
    public int ToRowNumber(string address) { return (int)GetCellData((r) => { return r.Row; }, address); }

    /// <summary>
    /// row1、col1、row2、col2を"A1:B8"のような形式に変換する
    /// 例
    /// NumToRange(25, 6, 27, 3); //戻り値: "Y6:AA3"
    /// </summary>
    public string NumToRange(int col1, int row1, int col2, int row2)
    {
        string res, tmp;

        tmp = ToAlpha(col1);
        res = tmp + row1.ToString() + ':';

        tmp = ToAlpha(col2);
        res += tmp + row2.ToString();

        return res;
    }

    /// <summary>
    /// "A1:B8"のような形式をintの配列に変換して返す
    /// 配列[0]...col1
    /// 配列[1]...row1
    /// 配列[2]...col2
    /// 配列[3]...row2
    /// 例
    /// RangeToNums("Y6:AA3"); //戻り値: {25, 6, 27, 3}
    /// </summary>
    public int[] RangeToNums(string range)
    {
        int[] res = new int[4];
        string[] splited = range.Split(':');

        res[0] = ToColNumber(splited[0]);
        res[1] = ToRowNumber(splited[0]);
        res[2] = ToColNumber(splited[1]);
        res[3] = ToRowNumber(splited[1]);

        return res;
    }

    /// <summary>
    /// "A1:C4"を"A1","A2", "A3","A4","B1", "B2","B3","B4", "C1","C2","C3", "C4"に分解する
    /// </summary>
    public string[] RangeExpload(string range)
    {
        int[] rnum = RangeToNums(range);

        if(rnum[0] > rnum[2])
        {
            int itmp = rnum[2];
            rnum[2] = rnum[0];
            rnum[0] = itmp;
        }

        if (rnum[1] > rnum[3])
        {
            int itmp = rnum[3];
            rnum[3] = rnum[1];
            rnum[1] = itmp;
        }

        List<string> result = new List<string>(100); //適当
        int x = rnum[2] - rnum[0];
        int y = rnum[3] - rnum[1];
        for (int j = rnum[1]; j <= rnum[3]; j++)
            for (int i = rnum[0]; i <= rnum[2]; i++)
            {
                string stmp = ToAlpha(i) + j.ToString();
                result.Add(stmp);
            }
        return result.ToArray();
    }

    /****************************************************************************
     * 図形関係関数
     ****************************************************************************/
    /// <summary>
    /// 画像をシートに張り付ける
    /// </summary>    
    public void InsertPicture(string imagepath, float left, float top, float width, float height)
    {
        MSExcel.Worksheet sh = _xls.ActiveSheet;
        try
        {
            MSExcel.Shapes shp = sh.Shapes;
            try
            {
                shp.AddPicture(imagepath, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoTrue, left, top, width, height);
            }
            finally
            {
                Marshal.FinalReleaseComObject(shp);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sh);
        }
    }

    /// <summary>
    /// 高圧縮版。チェックあまりしていない
    /// 戻り値について...falseが返された場合、シート内の画像に回転されているものがある。
    /// 正常に貼り付けられていない場合があるので、出力をチェックすること!!!
    /// </summary>
    public bool CompressPictures2(int comp_rate)
    {
        MSExcel.Worksheet sheet = _xls.ActiveSheet;
        try
        {
            MSExcel.Shapes shps = _xls.ActiveSheet.Shapes;
            try
            {
                foreach (MSExcel.Shape shp in shps)
                {
                    string tmp = shp.AutoShapeType.ToString();
                    try
                    {
                        if (shp.AutoShapeType != Microsoft.Office.Core.MsoAutoShapeType.msoShapeRectangle) continue; //画像かな?

                        string savefilename = Path.GetTempFileName();
                        float x = shp.Left;
                        float y = shp.Top;
                        float w = shp.Width;
                        float h = shp.Height;

                        shp.Cut();
                        WindowsUtils.SaveImageFromClipBoardImage(savefilename, comp_rate);
                        InsertPicture(savefilename, x, y, w, h);
                        File.Delete(savefilename);
                    }
                    finally
                    {
                        Marshal.FinalReleaseComObject(shp);
                    }
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(shps);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheet);
        }
        return false;
    }

    /// <summary>
    /// 現在のシートに張られている図形の数を返す
    /// </summary>
    public int ShapeCount()
    {
        MSExcel.Worksheet sheet = _xls.ActiveSheet;
        try
        {
            MSExcel.Shapes shps = _xls.ActiveSheet.Shapes;
            try
            {
                return shps.Count;
            }
            finally
            {
                Marshal.FinalReleaseComObject(shps);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheet);
        }
    }

    public ExcelShapeAttr GetShapeAttr(int index)
    {
        ExcelShapeAttr result = new ExcelShapeAttr();

        MSExcel.Worksheet sheet = _xls.ActiveSheet;
        try
        {
            MSExcel.Shapes shps = _xls.ActiveSheet.Shapes;
            try
            {
                result.width = shps.Item(index).Width;
                result.height = shps.Item(index).Height;
                result.x = shps.Item(index).Left;
                result.y = shps.Item(index).Top;
                result.type = (int)shps.Item(index).Type;
                return result;
            }
            finally
            {
                Marshal.FinalReleaseComObject(shps);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheet);
        }

    }

    /****************************************************************************
     * 保存関数
     ****************************************************************************/
    /// <summary>上書き保存する。</summary>
    public void Save() { _Save((wb, str) => { wb.Save(); }, ""); }
    /// <summary>名前をつけて保存する。</summary>
    public void Save(string filename) { _Save((wb, str) => { wb.SaveAs(str); }, filename); }
    /// <summary>XLSX形式で保存する。</summary>
    public void SaveXLSX(string filename) { _Save((wb, str) => { wb.SaveAs(str, MSExcel.XlFileFormat.xlOpenXMLWorkbook); }, filename); }
    /// <summary>CSV形式で保存する。</summary>
    public void SaveCSV(string filename) { _Save((wb, str) => { wb.SaveAs(str, MSExcel.XlFileFormat.xlCSV); }, filename); }
    /// <summary>HTML形式で保存する。</summary>
    public void SaveHTML(string filename) { _Save((wb, str) => { wb.SaveAs(str, MSExcel.XlFileFormat.xlHtml); }, filename); }

    /****************************************************************************
     * リソース解放処理
     ****************************************************************************/

    /// <summary>
    /// リソースを解放する
    /// </summary>
    public void Free()
    {
        if (_xls != null)
        {
            _xls.Quit();
            Marshal.FinalReleaseComObject(_xls);
            _xls = null;
        }
    }

    /// <summary>
    /// usingでnewした場合に、usingから出たら自動的に呼ばれる。
    /// </summary>
    void IDisposable.Dispose()
    {
        if (_xls != null)
        {
            _xls.Quit();
            Marshal.FinalReleaseComObject(_xls);
            _xls = null;
        }
    }

    /********************************************************************************************
    *                               イベント関連                            
    ********************************************************************************************/
    /*
        使用例
        _ex = new Excel();
        _ex.SetSaveAfterEvent((Microsoft.Office.Interop.Excel.Workbook Wb, bool Success) => { MessageBox.Show("保存しました"); });

    */
    public void SetSaveAfterEvent(Action<MSExcel.Workbook, bool> f)
    {
        _xls.WorkbookAfterSave += new MSExcel.AppEvents_WorkbookAfterSaveEventHandler(f);
    }

    /*
        使用例
        _ex = new Excel();
        _ex.SetBeforeCloseEvent((Microsoft.Office.Interop.Excel.Workbook Wb, ref bool Cancel) => { MessageBox.Show("終了しました"); });

    */
    public delegate void testfunc(MSExcel.Workbook a, ref bool b);
    public void SetBeforeCloseEvent(testfunc f)
    {
        _xls.WorkbookBeforeClose += new MSExcel.AppEvents_WorkbookBeforeCloseEventHandler(f);
    }

    /*
        使用例 4行目は編集が無効になるサンプル
        xls.SetChangeEvent((object a, Microsoft.Office.Interop.Excel.Range b) => 
        {
            if (xls._lock) return;

            string oldvalue = xls.GetValueFromCell(b.Column, b.Row);
            if (b.Row == 4)
            {
                xls._lock = true;
                xls.Undo();
                xls._lock = false;
            }
        });
    */
    public void SetChangeEvent(Action<object, MSExcel.Range> f)
    {
        MSExcel.Workbook b = _xls.ActiveWorkbook;
        try
        {
            b.SheetChange += new MSExcel.WorkbookEvents_SheetChangeEventHandler(f);
        }
        finally
        {
            Marshal.FinalReleaseComObject(b);
        }
    }
}

static class ExcelUtils
{
    /// <summary>
    /// 指定されたエクセルファイル内の画像ファイルを圧縮する
    /// </summary>
    static public void PictureCompress(string[] filenames, string sheetname, int comp_rate)
    {
        for (int i = 0; i < filenames.Length; i++)
        {
            try
            {
                using (ExcelFile xls = new ExcelFile(true))
                {
                    xls.OpenBook(filenames[i]);
                    xls.OpenSheet(sheetname);
                    xls.SheetProtect(false);
                    bool flg = xls.CompressPictures2(comp_rate);
                    if(!flg)
                        throw new Exception(filenames[i] + ":回転されている画像が[" + sheetname +"]にある");

                    xls.SheetProtect(true);
                    xls.Save();
                }
            }
            catch
            {
                MessageBox.Show(filenames[i]);
            }
            GC.Collect();
        }
    }

    /// <summary>
    /// 指定した範囲を抽出する。範囲は矩形で例えば"Q2:AK16"など。多重起動OK。
    /// </summary>
    static public void ExtractRangeValue(string[] filenames, string sheetname, string range, string savefilename, ProgressBar p)
    {
        StringTable st = null;


        p.Minimum = 0;
        p.Maximum = filenames.Length - 1;

        using (ExcelFile xls = new ExcelFile(false))
        {
            for (int i = 0; i < filenames.Length; i++)
            {
                p.Value = i;
                Application.DoEvents(); //本当は良くない
                xls.OpenBook(filenames[i]);

                if (i == 0)
                {
                    string[] addresses = xls.RangeExpload(range);
                    st = new StringTable(addresses);
                }

                bool visible = xls.IsSheetVisible(sheetname);
                xls.SheetVisible(sheetname);
                xls.OpenSheet(sheetname);
                string[,] s = null;// xls.GetRangeValue(range);

                var collection = s.Cast<string>();
                List<string> tmp = new List<string>(collection.Count());
                foreach (var c in collection)
                {
                    if (c == null)
                        tmp.Add("");
                    else
                        tmp.Add(c.ToString());
                }
                st.Add(tmp.ToArray());

                xls.SheetVisible(sheetname);
                xls.CloseBook(filenames[i]);
            }
        }
        SeparateText.SaveToFile(st, savefilename, ',');
    }
}

partial class ExcelFile : IDisposable
{
    private void _Save(Action<MSExcel.Workbook, string> func, string filename)
    {
        bool tmp = _xls.DisplayAlerts;
        _xls.DisplayAlerts = false;

        MSExcel.Workbook wb = _xls.ActiveWorkbook;
        try
        {
            func(wb, filename);
        }
        finally
        {
            _xls.DisplayAlerts = tmp;
            Marshal.FinalReleaseComObject(wb);
        }
    }

    private Object GetCellData(Func<Cells, Object> func, int row, int col)
    {
        Cells c = ((row == 0) && (col == 0)) ? _xls.Cells : _xls.Cells[row, col];
        try
        {
            return func(c);
        }
        finally
        {
            Marshal.FinalReleaseComObject(c);
        }
    }

    private Object GetCellData(Func<Cells, Object> func, string address)
    {
        Cells c = address == "" ? _xls.Cells.SpecialCells(MSExcel.XlCellType.xlCellTypeLastCell) : _xls.Range[address];
        try
        {
            return func(c);
        }
        finally
        {
            Marshal.FinalReleaseComObject(c);
        }
    }

    public void SetCellData(Action<Cells, object> func, string address, object value)
    {
        Cells c = _xls.Range[address];
        try
        {
            func(c, value);
        }
        finally
        {
            Marshal.FinalReleaseComObject(c);
        }
    }

    public void SetCellData(Action<Cells, object> func, int row, int col, object value)
    {
        Cells c = _xls.Cells[row, col];
        try
        {
            func(c, value);
        }
        finally
        {
            Marshal.FinalReleaseComObject(c);
        }
    }

    public void SetRowColData(Action<Cells> func, string address, bool blCol)
    {
        Cells c = blCol == true ? _xls.Columns[address] : _xls.Rows[address];
        try
        {
            func(c);
        }
        finally
        {
            Marshal.FinalReleaseComObject(c);
        }
    }

    private object GetSheetData(Func<MSExcel.Sheets, string, object> func, string sheetname)
    {
        MSExcel.Sheets sheets = _xls.Sheets;
        try
        {
            return func(sheets, sheetname);
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheets);
        }
    }

    private void SetSheetData(Action<MSExcel.Sheets, string> func, string sheetname)
    {
        MSExcel.Sheets sheets = _xls.Sheets;
        try
        {
            func(sheets, sheetname);
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheets);
        }
    }

    private object GetSheetsData(Func<MSExcel.Sheets, object> func, string sheetname)
    {
        MSExcel.Sheets sheets = _xls.Sheets;
        try
        {
            return func(sheets);
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheets);
        }
    }

    private void SetSheetsData(Action<MSExcel.Sheets> func, string sheetname, bool blSheet = true)
    {
        MSExcel.Sheets sheets = blSheet == true ? _xls.Sheets : _xls.Charts;
        try
        {
            func(sheets);
        }
        finally
        {
            Marshal.FinalReleaseComObject(sheets);
        }
    }

    private string CellOffsetFunction(Func<MSExcel.Range, string> func, string addr, int offset_row, int offset_col)
    {
        Cells cells = _xls.Range[addr];
        try
        {
            Range r = cells.Offset[offset_row, offset_col];
            try
            {
                return func(r).Replace("$", "");
            }
            finally
            {
                Marshal.FinalReleaseComObject(r);
            }
        }
        finally
        {
            Marshal.FinalReleaseComObject(cells);
        }
    }
}