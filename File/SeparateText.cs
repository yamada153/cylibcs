/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

/// <summary>
/// フィールド名について
/// 1.空白やタブだけのフィールド名は受け付けない
/// 2.重複するフィールド名は受け付けない
/// 3.フィールド名の前後のスペースは無視される
/// 4.フィールド名の大文字小文字は区別しない
/// 読み込むファイルは下記を満たしていること!!
/// 1.レコードは、CRLFで区切られる。
/// 2.フィールドは、delimiterで指定した1バイト文字で区切られる。
/// 3.delimiterの前後のスペース（タブを含む）は無視される。
/// 4.フィールドにdelimiterが含まれる場合、フィールドをダブルクォート（"）で囲まなければならない。
/// 5.フィールドにダブルクォートが含まれる場合、フィールドをダブルクォートで囲み、フィールド内のダブルクォートを2つの連続するダブルクォート（つまり、「""」）に置き換えなければならない。
/// 6.フィールドが改行文字を含む場合、フィールドをダブルクォートで囲まなければならない。
/// 7.フィールドの前後にスペースがある場合、フィールドをダブルクォートで囲まなければならない。
/// 8.すべてのフィールドがダブルクォートで囲まれているかもしれない。
/// 9.はじめのレコードは、ヘッダかもしれない。
/// ※「http://dobon.net/vb/dotnet/file/readcsvfile.html」を参考に作成しました。
/// </summary>
static class SeparateText
{
    /// <summary>
    /// delimiterで区切られたファイルを一行読み込む度にfuncを実行する。
    /// </summary>
    static public void LoadFromFile(string filename, char delimiter, Action<string[]> func, string[] fieldnames = null)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("SeparateText.LoadFromFileが失敗しました。指定したファイル「" + filename + "」が見つかりません。");

        string[] fields = null;
        if (fieldnames != null)
            fields = fieldnames;

        if (!File.Exists(filename))
            throw new FileNotFoundException("SeparateText.LoadFromFileが失敗しました。指定したファイル「" + filename + "」が見つかりません。");

        using (StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("shift_jis")))
        {
            while (!sr.EndOfStream)
            {
                string[] strs = GetCsvStringFromFile(sr, delimiter);
                if (fields != null)
                    func(strs);
                else
                    fields = strs;
            }
        }
    }

    /// <summary>
    /// delimiterで区切られたファイルを読み込む。フィールドが無い区切りテキストを読み込む場合はfieldnamesにフィールド名を設定しておくこと!
    /// </summary>
    static public StringTable LoadFromFile(string filename, char delimiter, string[] fieldnames = null, int capacity = 100)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("SeparateText.LoadFromFileが失敗しました。指定したファイル「" + filename + "」が見つかりません。");

        StringTable st = null;

        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            using (StreamReader sr = new StreamReader(fs, Encoding.GetEncoding("shift_jis")))
            {
                while (!sr.EndOfStream)
                {
                    string[] strs = GetCsvStringFromFile(sr, delimiter);
                    if (st == null)
                        try
                        {
                            if (fieldnames != null)
                            {
                                st = new StringTable(fieldnames, capacity);
                                st.Add(strs);
                            }
                            else
                                st = new StringTable(strs, capacity);
                        }
                        catch (Exception e)
                        {
                            throw new ArgumentException("SeparateText.LoadFromFileで失敗しました。\n詳細 : " + e.Message);
                        }
                    else
                        st.Add(strs);
                }
            }

            //一行も取得できなかった場合
            if(st == null)
                if (fieldnames != null)
                    st = new StringTable(fieldnames, capacity);

            return st;
        }
    }

    /// <summary>
    /// 指定したファイルにdelimiterで区切られたテキストを保存する。
    /// </summary>
    static public void SaveToFile(StringTable st, string filename, char delimiter, bool field = true)
    {
        if (string.IsNullOrEmpty(filename))
            throw new ArgumentException("SeparateText.SaveToFileが失敗しました。引数filenameが空文字列かnullです。");

        string dir = Path.GetDirectoryName(filename);
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
//            throw new DirectoryNotFoundException("SeparateText.SaveToFileが失敗しました。保存先ディレクトリ「" + dir + "」が存在しません。");
        }
        using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("shift_jis")))
        {
            if (field)
                sw.WriteLine(SeperateToString(st.GetFieldNames(), delimiter));
            for (int i = 0; i < st.Count(); i++)
                sw.WriteLine(SeperateToString(st.GetStringsFromRowNumber(i), delimiter));
        }
    }

    /// <summary>
    /// delimiterで区切られたファイルを読み込む。フィールドが無い区切りテキストを読み込む場合はfieldnamesにフィールド名を設定しておくこと!
    /// </summary>
    static private string[] GetCsvStringFromFile(StreamReader sr, char delimiter, string[] fieldnames = null, int capacity = 100)
    {
        bool infield = false; //ダブルクォーテーションから出入りする度に変わる。そのとき以外では使わない

        List<char> cellvalue = new List<char>(256); //適当
        List<string> cells = new List<string>(32); //適当
        while (sr.Peek() > -1)
        {
            char ch = Convert.ToChar(sr.Read());

            ////////////////////区切り文字////////////////////
            if (delimiter == ch)
            {
                if (infield)
                    cellvalue.Add(ch);
                else
                {
                    cells.Add(StrUtils.TrimStart(StrUtils.TrimEnd(new string(cellvalue.ToArray()).Trim(), '"'), '"'));
                    cellvalue.Clear();
                }
            }
            ///////////////////改行//////////////////////////
            else if (('\n' == ch) ||
                ('\r' == ch) && (sr.Peek() != '\n')) //改行が「\r」だけのテキストがあったので...
            {
                if (sr.Peek() <= -1) break; //ファイルの最後の改行

                if (infield)
                {
                    cellvalue.Add('\r');
                    cellvalue.Add('\n');
                    continue;
                }

                cells.Add(StrUtils.TrimStart(StrUtils.TrimEnd(new string(cellvalue.ToArray()).Trim(), '"'), '"'));
                return cells.ToArray();
            }
            ///////////////////ダブルクォーテーション///////////////////
            else if ('"' == ch)
            {
                //フィールドの最初が「"」のとき
                if (!infield && cellvalue.Count == 0)
                {
                    infield = true;
                    cellvalue.Add(ch);
                }
                //フィールド内に「"」が見つかり、次の文字も「"」の場合
                else if (infield && (sr.Peek() == '"'))
                {
                    cellvalue.Add(ch);
                    sr.Read(); //次のダブルクォーテーションは捨てる
                }
                //フィールド内に「"」が見つかり、前の文字が「\」の場合
                else if (cellvalue.Count != 0 && cellvalue[cellvalue.Count - 1] == '\\')
                {
                    cellvalue.Add(ch);
                }
                //フィールドの最後!?�@
                else if (infield && ((sr.Peek() == ' ') || (sr.Peek() == '\t') || (sr.Peek() == delimiter)))
                {
                    infield = false;
                    cellvalue.Add(ch);
                }
                //フィールドの最後!?�A
                else if (infield && (sr.Peek() == '\r'))
                {
                    sr.Read(); //\r捨てる
                    infield = false;
                    cellvalue.Add(ch);
                }
            }
            ///////////////////スペース///////////////////
            else if ((' ' == ch) || ('\t' == ch))
            {
                if (infield || (cellvalue.Count != 0))
                    cellvalue.Add(ch);
            }
            ///////////////////キャリッジリターン///////////////////
            else if ('\r' == ch) { }
            ///////////////////上記以外の文字///////////////////
            else
            {
                cellvalue.Add(ch);
            }
        }
        //最後のレコードが改行で終わっていない場合、未処理のセルが溜まっているので処理する
        if (cellvalue.Count == 0) return cells.ToArray(); //フィールドは全部出来てる

        cells.Add(StrUtils.TrimStart(StrUtils.TrimEnd(new string(cellvalue.ToArray()).Trim(), '"'), '"')); //フィールド途中でファイルが終わっている場合
        return cells.ToArray();
    }

    /// <summary>
    /// 指定した行の文字列を、delimiterで区切られた文字列に変える
    /// </summary>
    private static string SeperateToString(string[] strs, char delimiter)
    {
        string result = "";

        for (int i = 0; i < strs.Length; i++)
        {
            string tmp = strs[i];
            if (tmp.Contains("\r"))
                tmp = "\"" + tmp + "\"";
            else if (tmp.Contains("\n"))
                tmp = "\"" + tmp + "\"";
            else if (tmp.Contains(delimiter.ToString()))
                tmp = "\"" + tmp + "\"";
            else if (tmp.Contains(" "))
                tmp = "\"" + tmp + "\"";
            else if (tmp.Contains("\t"))
                tmp = "\"" + tmp + "\"";
            else if (tmp.Contains("\""))
            {
                tmp = tmp.Replace("\"", "\"\"");
                tmp = "\"" + tmp + "\"";
            }
            else if ((tmp == "") && (Global.EmptyStringSurroundAtSaveStringTable))
                tmp = "\"\"";

            result = result + tmp + delimiter;
        }

        return result.Substring(0, result.Length - 1);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>
/// テスト用クラス
/// </summary>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static class TestSeparateText
{
    static public void LoadFromFile(System.Windows.Forms.RichTextBox memo)
    {
        string filename = @"D:\Git\Testデータ\CSVサンプル.csv";
        SeparateText.LoadFromFile(filename, ',', (x) => { memo.AppendText(x[0] + "\n"); });
    }
}
