﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

class XMLFile
{
    private XDocument _doc = null;

    public XMLFile()
    {
    }

    /// <summary>
    /// filenameで指定したXMLファイルを読み込む。
    /// </summary>
    public void LoadFromFile(string filename)
    {
        if (File.Exists(filename) == false)
            throw new FileNotFoundException("「" + filename + "」が存在しません");

        _doc = XDocument.Load(filename);
    }

    /// <summary>
    /// filenameで指定したKMLファイルを読み込む。
    /// </summary>
    public void LoadFromKMLFile(string filename)
    {
        if (File.Exists(filename) == false)
            throw new FileNotFoundException("「" + filename + "」が存在しません");

        using (StreamReader sr = new StreamReader(filename, System.Text.Encoding.GetEncoding("utf-8")))
        {
            string stmp = sr.ReadToEnd();
            _doc = XDocument.Parse(stmp.Replace(" xmlns=\"http://www.opengis.net/kml/2.2\"", ""));
        }
    }

    /// <summary>
    /// filenameで指定したXMLファイルに書き込む。
    /// </summary>
    public void SaveToFile(string filename)
    {
        _doc.Save(filename);
    }

    /// <summary>
    /// pathで指定したパスの値を返す。pathが存在しない場合はexceptがtrueなら例外を発生させる。faseならnullが返る。
    /// 指定の仕方 例
    /// 「GetValue("//ボーリング情報/標題情報/調査基本情報/ボーリング名");」
    /// 「GetValue("//ボーリング情報/コア情報/岩石土区分[3]/岩石土区分_下端深度");」
    /// </summary>
    public string GetValue(string path, bool except = false)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
        {
            if (except)
                throw new ArgumentException("「" + path + "」は存在しません。");
            else
                return null;
        }
        return elm.Value;
    }

    /// <summary>
    /// pathで指定したパスに値を入れる。
    /// </summary>
    public void SetValue(string path, string value)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
            throw new ArgumentException("「" + path + "」は存在しません。");

        elm.Value =  value;
    }

    /// <summary>
    /// pathで指定したパスにタグを追加し、値を入れる。
    /// </summary>
    public void AddValue(string path, string tagname, string value)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
            throw new ArgumentException("「" + path + "」は存在しません。");
        
        elm.Add(new XElement(tagname, value));
    }

    /// <summary>
    /// pathで指定したパスの後にタグを追加し、値を入れる。
    /// </summary>
    public void AddValueAfter(string path, string tagname, string value)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
            throw new ArgumentException("「" + path + "」は存在しません。");


        elm.AddAfterSelf(new XElement(tagname, value));
    }

    /// <summary>
    /// 指定したパスの属性値を取得する。
    /// </summary>
    public string GetAttribute(string path, string attname)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
            throw new ArgumentException("「" + path + "」は存在しません。");

        XAttribute attr = elm.FirstAttribute;
        while(attr != null)
        {
            if (attr.Name == attname)
                return attr.Value;
            attr = attr.NextAttribute;
        }
        return null;
    }

    /// <summary>
    /// 指定したパスの属性値をセットする。
    /// </summary>
    public void SetAttribute(string path, string attname, string value)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
            throw new ArgumentException("「" + path + "」は存在しません。");
        elm.SetAttributeValue(attname, value);
    }

    /// <summary>
    /// 指定したパスの属性名を取得する。
    /// </summary>
    public string[] GetAttributeNames(string path)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
            throw new ArgumentException("「" + path + "」は存在しません。");

        List<string> result = new List<string>(100); //適当

        XAttribute attr = elm.FirstAttribute;
        while (attr != null)
        {
            result.Add(attr.Name.ToString());
            attr = attr.NextAttribute;
        }
        return result.ToArray();
    }

    /// <summary>
    /// pathで指定したパスの子の数を返す。pathが読めなかった場合、exceptがtrueなら例外を発生させる。faseなら-1を返す。
    /// 指定の仕方 例
    /// 「xml.GetChildItemName("//ボーリング情報/コア情報/岩石土区分");」　 //コア情報以下の全ての岩石土区分の数を取得
    /// 「xml.GetChildItemName("//ボーリング情報/コア情報/岩石土区分[1]");」//コア情報以下の最初の岩石土区分の数を取得。存在すれば1だろう。
    /// </summary>
    public int GetChildItemNum(string path, bool except = false)
    {
        XElement elm = _doc.XPathSelectElement(path);
        if (elm == null)
        {
            if (except)
                throw new ArgumentException("「" + path + "」は存在しません。");
            else
                return -1;
        }
        return _doc.XPathSelectElements(path).Count();
    }

    /// <summary>
    /// </summary>
    public string[] GetChildItemName(string path, bool except = false)
    {
        IEnumerable<XNode> nodes = _doc.XPathSelectElements(path).Nodes();
        if (nodes == null)
        {
            if (except)
                throw new ArgumentException("「" + path + "」は存在しません。");
            else
                return null;
        }
        List<string> result = new List<string>();
        foreach(XElement e in nodes)
            result.Add(e.Name.ToString());

        return result.ToArray();
    }

    /// <summary>
    /// XMLファイルのpathで指定した値を取得する。
    /// 失敗した場合は、exceptがtrueなら例外を発生させる。faseなら空文字列が返る。
    /// </summary>
    public string ToString(string path, bool except = false)
    {
        string result = GetValue(path);
        if (result == null)
        {
            if (except)
                throw new Exception("「" + path + "」で指定したパスの文字列の取得中にエラーが発生しました。");
            else
                return "";
        }
        return result;
    }

    public void InitDoc(string version, string encoding, string name, string systemid, string root)
    {
        if (_doc != null) _doc = null;
        _doc = new XDocument(new XDeclaration(version, encoding, null));
        if(name != "")
            _doc.Add(new XDocumentType(name, "", systemid, ""));

        _doc.Add(new XElement(root));
    }
}
