﻿/******************************************************************************************
 *  Copyright (c) 2016 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibcs.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

/// <summary>
/// Format24bppRgbのビットマップファイルを高速に読み書きするためのクラス
/// </summary>
class Bitmap24bppRgbEx
{
    byte[] _Pixels = null;
    int _stride;
    int _width;
    int _height;

    public int Width
    {
        get
        {
            return _width;
        }
    }

    public int Height
    {
        get
        {
            return _height;
        }
    }

    public Color this[int x, int y]
    {
        get
        {
            int position = x * 3 + _stride * y;
            byte b = _Pixels[position + 0];
            byte g = _Pixels[position + 1];
            byte r = _Pixels[position + 2];

            return Color.FromArgb(r, g, b);
        }
        set
        {
            int position = x * 3 + _stride * y;
            _Pixels[position + 0] = value.B;
            _Pixels[position + 1] = value.G;
            _Pixels[position + 2] = value.R;
        }
    }

    public double[,] ToArray()
    {
        double[,] newdata = new double[Width, Height];

        for (int i = 0; i < Height; i++)
            for (int j = 0; j < Width; j++)
                newdata[j, i] = this[j, i].ToArgb();

        return newdata;
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public Bitmap24bppRgbEx(int width, int height)
    {
        _width = width;
        _height = height;

        using (Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb))
        {
            BitmapData BitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
            try
            {
                _stride = BitmapData.Stride;
                _Pixels = new byte[_stride * bmp.Height];
                Marshal.Copy(BitmapData.Scan0, _Pixels, 0, _Pixels.Length);
            }
            finally
            {
                bmp.UnlockBits(BitmapData);
            }
        }
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public Bitmap24bppRgbEx(string filename)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("Bitmap24bppRgbExの初期化に失敗しました。「" + filename + "」が存在しません。");

        LoadFromFile(filename);
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public Bitmap24bppRgbEx(double[,] data)
    {
        _width = data.GetLength(0);
        _height = data.GetLength(1);

        using (Bitmap bmp = new Bitmap(_width, _height, PixelFormat.Format24bppRgb))
        {
            BitmapData BitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
            try
            {
                _stride = BitmapData.Stride;
                _Pixels = new byte[_stride * bmp.Height];
                Marshal.Copy(BitmapData.Scan0, _Pixels, 0, _Pixels.Length);
            }
            finally
            {
                bmp.UnlockBits(BitmapData);
            }
        }

        for (int i = 0; i < _width; i++)
            for (int j = 0; j < _height; j++)
            {
                int tmp = (int)data[i, j];
                this[i, j] = Color.FromArgb(tmp);
            }
    }

    /// <summary>
    /// 指定したファイルを読み込む
    /// </summary>
    public void LoadFromFile(string filename)
    {
        if (!File.Exists(filename))
            throw new FileNotFoundException("Bitmap24bppRgbEx.LoadFromFileの引数filenameで指定したファイル「" + filename + "」が存在しません。");

        using (Bitmap bmp = new Bitmap(filename))
        {
            if (bmp.PixelFormat != PixelFormat.Format24bppRgb)
                throw new FileFormatException("Bitmap24bppRgbEx.LoadFromFileの引数filenameで指定したファイル「" + filename + "」の形式がFormat24bppRgbではありません。");

            _width = bmp.Width;
            _height = bmp.Height;

            BitmapData BitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
            try
            {
                _stride = BitmapData.Stride;
                _Pixels = new byte[_stride * bmp.Height];
                Marshal.Copy(BitmapData.Scan0, _Pixels, 0, _Pixels.Length);
            }
            finally
            {
                bmp.UnlockBits(BitmapData);
            }
        }
    }

    /// <summary>
    /// 指定したファイルに保存する
    /// </summary>
    public void SaveToFile(string filename)
    {
        string dir = Path.GetDirectoryName(filename);
        if(!Directory.Exists(dir))
            throw new DirectoryNotFoundException("Bitmap24bppRgbEx.SaveToFileの引数filenameで指定したファイルの保存先「" + dir + "」が存在しません。");

        using (Bitmap bmp = new Bitmap(_width, _height, PixelFormat.Format24bppRgb))
        {
            BitmapData BitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
            try
            {
                Marshal.Copy(_Pixels, 0, BitmapData.Scan0, _Pixels.Length);
                bmp.Save(filename);
            }
            finally
            {
                bmp.UnlockBits(BitmapData);
            }
        }
    }
}